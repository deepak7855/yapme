import React, { Component } from 'react'
import { Text, View, Modal, SafeAreaView } from 'react-native'


export class Custompopup extends Component {
  state = {
    modalVisible: false,
    value: 0,
  };
  render() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.modalVisible}>
        {this.props.children}
      </Modal>
    );
  }
}
