import React, {useMemo} from 'react';
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Colors from '../../../Assets/Colors';
// import fonts from '../../../Assets/Fonts';
import {images} from '../../../Assets/ImageUrl';
import {generateColor} from '../../../Utility';
import {Searchbar, PinkScreen, Icons} from '../../../Component/Common';
import {GButton, GRoundButton} from '../../../Common/GButton';
import CHeader from '../../../Common/CHeader';
import fonts from '../../../Assets/Fonts';

export default Chatgroup = React.FC = (props) => {
  const colors = useMemo(
    () => [...new Array(20)].map(() => generateColor()),
    [],
  );

  return (
    <SafeAreaView style={styles.container}>
      <CHeader navigation={props.navigation} backArrowLeft centerItem  />

      <Searchbar  placeholder="Search Friends" />
      <PinkScreen title={'Fun Group'} />
      <FlatList
        contentContainerStyle={{}}
        data={['', '', '', '', '', '']}
        renderItem={({item: color, index}) => (
          <View style={[styles.item, styles.item2]}>
            <View style={styles.row1}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image
                  resizeMode="contain"
                  style={{height: 35, width: 35}}
                  source={images.Profile}
                />
                <View style={{marginLeft: 20}}>
                  <Text style={{marginBottom: 5}}>Odell smith</Text>
                  <Text style={styles.grytextStyle}>Awesome work</Text>
                </View>
              </View>
              <Image
                resizeMode="contain"
                style={{height: 17, width: 17}}
                source={
                  index % 2 != 0 ? images.radio_circle : images.tick_circle
                }
              />
            </View>
          </View>
        )}
        keyExtractor={(item, idx) => `item_${idx}`}
      />
      <View
        style={{
          shadowRadius: 2,
          shadowOffset: {
            width: 0,
            height: -2,
          },
          shadowColor: '#000',
          shadowOpacity: 0.11,
          elevation: 4,
          backgroundColor: '#fff',
         
        }}>
        <View style={{height:15 }}/>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 10,
            alignItems: 'center',
            marginTop: 15,
          }}>
          <View style={{flex: 0.5}}>
            <Icons
              size={50}
              source={images.camera_circle}
              onPress={() => alert('Click')}
            />
          </View>

          {/* <View
          style={{
            flex: 2.5,
            // alignItems: 'center',
            borderRadius: 10,
            borderColor: 'rgb(76,0,196)',
            borderWidth: 1,
            height:50
            // backgroundColor: 'pink',
          }}> */}

          <TextInput
            placeholder={'Type something '}
          placeholderTextColor={Colors.pinkishGrey}
            
            style={{
              // flex: 2.5,
              // backgroundColor: 'red',
              // width: 250,
              // marginHorizontal: 20,
              // paddingVertical: 15,

              flex: 2.5,
              // alignItems: 'center',
              borderRadius: 10,
              borderColor: 'rgba(76,0,0,0.18)',
              borderWidth: 1,
              height: 50,
              paddingLeft: 12,
              // backgroundColor: 'pink',
            }}
          />
          {/* </View> */}

          <View style={{flex: 0.5, alignItems: 'center'}}>
            {/* <GButton/> */}
            {/* <GRoundButton
                height={50}
                width={50}
                radius={25}
            /> */}
            <Icons
              size={35}
              source={images.smile_acive1}
              onPress={() => alert('hahahaha')}
            />
          </View>
        </View>
        <GButton
          _style={{alignSelf: 'center', marginTop: 20, marginBottom: 5}}
          bText="Create Group Chat"
          fontWeight="bold"
          fontFamily={fonts.NovaXboldios}
          height={50}
          width={210}
          radius={5}
          onPress={()=>props.navigation.navigate('Message',{fromWhere:'Chatgroup'})}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bgcolor,
  },
  item: {
    margin: 10,
    height: 30,
    borderRadius: 10,
  },
  item2: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    paddingHorizontal: 20,
    height: 60,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.15,
    shadowRadius: 1.14,

    elevation: 3,
  },
  row1: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  grytextStyle: {
    color: Colors.greyColor,
    fontSize: 13,
    // fontFamily:fonts.NovaRegular
  },
});
