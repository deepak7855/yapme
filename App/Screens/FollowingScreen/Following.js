import React, { Component } from 'react';
import {
    SafeAreaView, TextInput,
    View, Text, FlatList,
    Image, TouchableOpacity,
} from 'react-native';
import styles from './FollowingStyles';
import Header from '../../Common/Header';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';
import { GButton } from '../../Common/GButton';
import { images } from '../../Assets/ImageUrl';



class Following extends Component {
    constructor(props) {
        super(props)
        this.state = {
            arrFriendRequest: [{ username: 'Paris' }, { username: 'Margaretta' }, { username: 'Glennie' },
            { username: 'Zander' }, { username: 'Ole' }, { username: 'Ariel' }, { username: 'Deangelo' },
            { username: 'Zola' }, { username: 'Jerry' }, { username: 'Terrence' }, { username: 'Paris' }]
        }
        
    }

    _renderFriendRequestList = ({ item, index }) => {
        return (
            <View style={styles.friend_req_view}>
                <View style={styles.img_name_view}>
                    <View style={styles.user_img_view}>
                        <Image source={images.chat5} resizeMode={'cover'} style={styles.user_img} />
                    </View>
                    <View style={styles.name_view}>
                        <Text numberOfLines={1} style={styles.name_text}>{item.username}</Text>
                    </View>
                </View>

                <View style={styles.btn_view}>
                    <TouchableOpacity style={{width:40,height:40,borderRadius:40/2,marginRight:10}}>
                        <Image resizeMode={'cover'} source={images.chatpink_circle} style={{height:'100%',width:'100%'}}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.reject_btn_touch}>
                        <Text style={styles.reject_text}>Unfollow</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    render() {
        return (
            <SafeAreaView style={styles.safe_area_view}>
                <Header
                    backArrowLeft
                    centerImage
                />
                <FlatList
                    contentContainerStyle={{marginTop:10}}
                    showsVerticalScrollIndicator={false}
                    data={this.state.arrFriendRequest}
                    renderItem={this._renderFriendRequestList}
                    extraData={this.state}
                    keyExtractor={(item, index) => index.toString()}
                />
            </SafeAreaView>
        );
    }

}

export default Following;
