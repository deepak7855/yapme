import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from 'react-native';
import {images} from '../../Assets/ImageUrl';
import styles from './OtherUserProfileStyles';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';
import {Input} from '../../Common/InputCommon';
import {GButton} from '../../Common/GButton';
// import Header from '../../Common/Header';
import ViewPager from '@react-native-community/viewpager';
import CHeader from '../../Common/CHeader';
import MasonryList from 'react-native-masonry-list';

class OtherUserProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      index_one_filled: false,
      index_two_filled: false,
      index_three_filled: false,
      index_four_filled: false,
      currentPosition: 0,
    };
  }

  // onPageChange(position) {
  //     console.log("position", position);
  //     this.setState({ currentPosition: position });
  // }

  // onPointContinue = (point) => {
  //     if ((point + 1) == 1) {
  //         this.setState({ index: 1, index_one_filled: true });
  //         this.viewpager.setPage(1);
  //     } else if ((point + 1) == 2) {
  //         this.setState({ index: 2, index_two_filled: true });
  //         this.viewpager.setPage(2);
  //     } else {

  //         this.onPressNext()

  //     }

  // }

  render() {
    console.log(this.props,"otheruserProps")
    return (
      <SafeAreaView style={styles.safe_area_view}>
        {/* <Header
                    backArrowLeft
                /> */}
        <CHeader navigation={this.props.navigation} backArrowLeft centerItem />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.other_user_profile_view}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('FriendSList')}
            style={styles.add_img_view}>
              <Image
                resizeMode={'cover'}
                source={images.user_blue}
                style={styles.add_img}
              />
            </TouchableOpacity>

            <View style={styles.other_user_image_view}>
              <Image
                resizeMode={'cover'}
                source={images.chat7}
                style={styles.other_profile_img}
              />
            </View>

            <TouchableOpacity onPress={()=>this.props.navigation.navigate('Message')}
            style={styles.chat_img_view}>
              <Image
                resizeMode={'cover'}
                source={images.chat_pink}
                style={styles.chat_img}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.other_user_name_view}>
            <Text style={styles.other_user_name_text}>@marsus</Text>
          </View>

          <View style={styles.you_tuber_text_view}>
            <Text style={styles.you_tuber_text}>
              27 | Youtuber | Boston, MA
            </Text>
          </View>

          <View style={styles.fans_following_count_view}>
            <View style={styles.fans_count_view}>
              <Text style={styles.count_text}>215</Text>
              <Text style={styles.fans_following_text}>Fans</Text>
            </View>

            <View style={styles.separator_vertical}></View>

            <View style={styles.following_count_view}>
              <Text style={styles.count_text}>215</Text>
              <Text style={styles.fans_following_text}>Following</Text>
            </View>
          </View>

          <View style={styles.submit_btn_view}>
            <GButton
              height={50}
              width={200}
              radius={5}
              lineHeight={15}
              txtcolor={Colors.white}
              fontFamily={Fonts.NovaBold}
              fontSize={Fonts.fontSize13}
              bText={'Follow'}
              onPress={()=>this.props.navigation.navigate('Fansfollowings',{fromWhere:'fans'})}

            />
          </View>

          <View style={styles.about_me_view}>
            <Text style={styles.about_me_text}>About Me</Text>
          </View>

          <View style={styles.about_des_view}>
            <Text style={styles.about_des_text}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s,
            </Text>
          </View>

          <View style={styles.separator_view}>
            <View style={styles.separator_part} />
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginHorizontal: 15,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  index: 0,
                  index_one_filled: false,
                  index_two_filled: false,
                }),
                  this.viewpager.setPage(0);
              }}
              style={{
                width: '25%',
                borderBottomWidth: 1.5,
                alignItems: 'center',
                borderBottomColor: Colors.purplePink,
                paddingVertical: 10,
              }}>
              <Image
                source={images.play_lg}
                resizeMode={'contain'}
                style={{width: 24.3, height: 19.2}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({index: 1, index_one_filled: true}),
                  this.viewpager.setPage(1);
              }}
              style={{
                width: '25%',
                borderBottomWidth: this.state.index_one_filled ? null : 1,
                alignItems: 'center',
                borderBottomColor: this.state.index_one_filled
                  ? Colors.purplePink
                  : Colors.whiteTwo,
                paddingVertical: 10,
              }}>
              <Image
                source={images.news_ic}
                resizeMode={'contain'}
                style={{width: 24.3, height: 19.2}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              disabled={this.state.index == 0}
              onPress={() => {
                this.setState({index: 2, index_two_filled: true}),
                  this.viewpager.setPage(2);
              }}
              style={{
                width: '25%',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderBottomColor: Colors.whiteTwo,
                paddingVertical: 10,
              }}>
              <Image
                source={images.button}
                resizeMode={'contain'}
                style={{width: 24.3, height: 19.2}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                width: '25%',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderBottomColor: Colors.whiteTwo,
                paddingVertical: 10,
              }}>
              <Image
                source={images.sun_icon}
                resizeMode={'contain'}
                style={{width: 24.3, height: 19.2}}
              />
            </TouchableOpacity>
          </View>

          <ViewPager
            ref={(viewpager) => (this.viewpager = viewpager)}
            style={{flex: 1, backgroundColor: 'red'}}
            initialPage={0}
            scrollEnabled={true}
            onPageSelected={(e) =>
              this.setState({index: e.nativeEvent.position})
            }>
            <View key="1">
              <View>
                <Text>dfdfasfdfasdfdsaf</Text>
              </View>
              {/* <HearingQuestionnaire
                                onPointContinue={(i) => this.onPointContinue(i)}
                                activeIndex={this.state.index}
                            /> */}
            </View>

            <View style={{flex: 1}} key="2">
              <View>
                <Text>dfdfasfdfasdfdsaf</Text>
              </View>

              {/* <HearingQuestionnaireStepTwo
                                onPointContinue={(i) => this.onPointContinue(i)}
                                activeIndex={this.state.index}
                            /> */}
            </View>

            <View style={{flex: 1}} key="3">
              <View>
                <Text>dfdfasfdfasdfdsaf</Text>
              </View>

              {/* <HearingQuestionnaire1
                                onPointContinue={(i) => this.onPointContinue(i)}
                                activeIndex={this.state.index}
                            /> */}
            </View>

            <View style={{flex: 1}} key="3">
              <View>
                <Text>dfdfasfdfasdfdsaf</Text>
              </View>
              {/* <HearingQuestionnaire1
                                onPointContinue={(i) => this.onPointContinue(i)}
                                activeIndex={this.state.index}
                            /> */}

                            </View>
          </ViewPager>

              <MasonryList
                images={[
                  {
                    uri:
                      'https://cdn.pixabay.com/photo/2017/06/09/09/39/adler-2386314_960_720.jpg',
                  },
                  {
                    source: {
                      uri:
                        'https://cdn.pixabay.com/photo/2017/06/02/18/24/fruit-2367029_960_720.jpg',
                    },
                  },
                  {
                    uri:
                      'https://cdn.pixabay.com/photo/2016/08/12/22/34/apple-1589869_960_720.jpg',
                    dimensions: {width: 1080, height: 1920},
                  },
                  {
                    URI:
                      'https://cdn.pixabay.com/photo/2016/08/12/22/34/apple-1589869_960_720.jpg',
                  },
                  {
                    url:
                      'https://cdn.pixabay.com/photo/2016/08/12/22/34/apple-1589869_960_720.jpg',
                  },
                  {
                    URL:
                      'https://cdn.pixabay.com/photo/2016/08/12/22/34/apple-1589869_960_720.jpg',
                  },
                ]}
              />
           
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default OtherUserProfile;
