import React, {Component} from 'react';
import {
  SafeAreaView,
  TextInput,
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import Header from '../../Common/Header';
import {Icons, Searchbar} from '../../Component/Common';
import ArrData from '../../../dummydata';
import CommonStyle from '../../Common/style';
import {GButton} from '../../Common/GButton';
import CHeader from '../../Common/CHeader';

export default class CreatePost extends Component {
    state={text:''}
  render() {
    return (
      <View style={{flex: 1, backgroundColor: Colors.whiteTwo}}>
        <View style={{marginHorizontal: 18}}>
        <CHeader navigation={this.props.navigation} backArrowLeft  />

          <SafeAreaView />
          <Text
            style={{
              fontFamily: fonts.NovaBoldios,
              fontSize: fonts.fontSize16,
              marginVertical: 10,
              textAlign: 'center',
            }}>
            Create Post
          </Text>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 18,
            alignItems: 'center',
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icons
              size={40}
              source={images.Profile}
              _style={{marginVertical: 18}}
            />
            <Text style={{fontFamily: fonts.NovaBoldios, marginLeft: 8}}>
              elemar
            </Text>
          </View>
          <Icons
                size={80}
                source={ this.state.toggleglobe? images.one : images.two}
                onPress={()=>this.setState({toggleglobe:!this.state.toggleglobe})}
                _style={{marginRight: 5,height:40,width:60}}
              />
        </View>

        <View style={{}}>
          <TextInput
            multiline
            placeholder="What's on your mind?"
            onChangeText={(value)=>this.setState({text:value})}
            style={[
              CommonStyle,
              {
                height: 140,
                width: null,
                paddingLeft: 14,
                paddingTop: 14,
                marginHorizontal: 18,
                borderRadius: 8,
                fontFamily: fonts.NovaRegularios,
              },
            ]}
          />
        </View>
        <Text
          style={{
            fontSize: fonts.fontSize13,
            fontFamily: fonts.NovaRegularios,
            color: Colors.brownishGrey,
            textAlign: 'right',
            marginRight: 19,
            marginTop: 12,
          }}>
          {this.state.text.length }/2000
        </Text>

        <View style={[CommonStyle, styles.view2]}>
          <Icons
            size={20}
            source={images.commentaccount}
            _style={{marginHorizontal: 18, marginVertical: 10}}
          />
          <Text style={styles.text2}>Allow Comments</Text>
        </View>

        <Icons
          size={80}
          source={images.camera_black_circle}
          _style={{alignSelf: 'center', marginVertical: 25}}
        />

        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          <TouchableOpacity
            style={{
              height: 55,
              width: 150,
              borderRadius: 5,
              backgroundColor: Colors.violetBlue,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: fonts.NovaBoldios,
                fontSize: fonts.fontSize13,
                color: Colors.white,
              }}>
              Discard
            </Text>
          </TouchableOpacity>

        
          <GButton
            height={55}
            width={150}
            radius={5}
            //   lineHeight={15}
            txtcolor={Colors.white}
            fontFamily={fonts.NovaXbold}
            fontSize={fonts.fontSize13}
            _style={{marginLeft:8}}
            bText={'Post'}
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  safe_area_view: {
    flex: 1,
  },
  view2: {
    marginHorizontal: 18,
    marginVertical: 18,
    borderRadius: 8,
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: 8,
  },
  text2: {
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaRegularios,
  },
});
