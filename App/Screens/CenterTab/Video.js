import React, {Component} from 'react';
import {
  View,
  Text,
  StatusBar,
  SafeAreaView,
  ImageBackground,
  Image,
} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import {Icons} from '../../Component/Common';

export default class Video extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <ImageBackground
        source={images.live_bg}
        style={{flex: 1, justifyContent: 'space-between'}}
        resizeMode={'stretch'}>
        <SafeAreaView />

        <View style={{flex: 1, justifyContent: 'space-between'}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: 10,
              paddingHorizontal:5
              // backgroundColor: 'red',
              // flex:1
            }}>
            <View
              style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              {/* <Text>d</Text> */}
            </View>

            <View
              style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <Text style={{color: Colors.white, fontSize: fonts.fontSize15}}>
                Make
              </Text>
            </View>

            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
                justifyContent: 'flex-end',
              }}>
              <Icons
                size={20}
                source={images.cross}
                _style={{
                  alignSelf: 'flex-end',
                    marginRight: 15,
                  //   marginTop: 10,
                }}
                onPress={() => this.props.navigation.goBack(null)}
              />
            </View>
          </View>

          <View
            style={{
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
              marginRight: 15,
              //   backgroundColor: 'pink',
            }}>
            <View style={{alignItems: 'center',}}>
              <View style={{alignItems: 'center',
                // backgroundColor: 'pink',
               marginVertical: 10}}>
                <Icons
                  size={18}
                  source={images.filter_icon_active}
                  _style={
                    {
                      // alignSelf: 'flex-end',
                      // marginRight: 20,
                      // marginTop: 10,
                    }
                  }
                />
                <Text
                  style={{
                    fontSize: fonts.fontSize10,
                    color: Colors.white,
                    marginTop: 5,
                  }}>
                  FILTER
                </Text>
              </View>



              <View style={{alignItems: 'center',
                // backgroundColor: 'pink',
               marginVertical: 10}}>
                <Icons
                  size={18}
                  source={images.sound_ic}
                  _style={
                    {
                      // alignSelf: 'flex-end',
                      // marginRight: 20,
                      // marginTop: 10,
                    }
                  }
                  onPress={()=>this.props.navigation.navigate('Addmusic')}
                />
                <Text
                  style={{
                    fontSize: fonts.fontSize10,
                    color: Colors.white,
                    marginTop: 5,
                  }}>
                  SOUND
                </Text>
              </View>



              <View style={{alignItems: 'center',
                // backgroundColor: 'pink',
               marginVertical: 10}}>
                <Icons
                  size={18}
                  source={images.speed_icon_unactive}
                  _style={
                    {
                      // alignSelf: 'flex-end',
                      // marginRight: 20,
                      // marginTop: 10,
                    }
                  }
                />
                <Text
                  style={{
                    fontSize: fonts.fontSize10,
                    color: Colors.white,
                    marginTop: 5,
                  }}>
                  SPEED
                </Text>
              </View>




              <View style={{alignItems: 'center',
                // backgroundColor: 'pink',
               marginVertical: 10}}>
                <Icons
                  size={22}
                  source={images.flip}
                  _style={
                    {
                      // alignSelf: 'flex-end',
                      // marginRight: 20,
                      // marginTop: 10,
                    }
                  }
                />
                <Text
                  style={{
                    fontSize: fonts.fontSize10,
                    color: Colors.white,
                    marginTop: 5,
                  }}>
                  FLIP
                </Text>
              </View>


              <View style={{alignItems: 'center',
                // backgroundColor: 'pink',
               marginVertical: 10}}>
                <Icons
                  size={18}
                  source={images.trim_icon_unactive}
                  _style={
                    {
                      // alignSelf: 'flex-end',
                      // marginRight: 20,
                      // marginTop: 10,
                    }
                  }
                />
                <Text
                  style={{
                    fontSize: fonts.fontSize10,
                    color: Colors.white,
                    marginTop: 5,
                  }}>
                  {`TRIM\nMUSIC`}
                </Text>
              </View>


              <View style={{alignItems: 'center',
                // backgroundColor: 'pink',
               marginVertical: 10}}>
                <Icons
                  size={18}
                  source={images.stricker_icon_white}
                  _style={
                    {
                      // alignSelf: 'flex-end',
                      // marginRight: 20,
                      // marginTop: 10,
                    }
                  }
                />
                <Text
                  style={{
                    fontSize: fonts.fontSize10,
                    color: Colors.white,
                    marginTop: 5,
                  }}>
                  STRICKERS
                </Text>
              </View>


              <View style={{alignItems: 'center',
                // backgroundColor: 'pink',
               marginVertical: 10}}>
                <Icons
                  size={22}
                  source={images.text_icon_unactive}
                  _style={
                    {
                      // alignSelf: 'flex-end',
                      // marginRight: 20,
                      // marginTop: 10,
                    }
                  }
                />
                <Text
                  style={{
                    fontSize: fonts.fontSize10,
                    color: Colors.white,
                    marginTop: 5,
                  }}>
                  TEXT
                </Text>
              </View>



              <View style={{alignItems: 'center',
                // backgroundColor: 'pink',
               marginVertical: 10}}>
                <Icons
                  size={20}
                  source={images.timer}
                  _style={
                    {
                      // alignSelf: 'flex-end',
                      // marginRight: 20,
                      // marginTop: 10,
                    }
                  }
                />
                <Text
                  style={{
                    fontSize: fonts.fontSize10,
                    color: Colors.white,
                    marginTop: 5,
                  }}>
                  TIMMER
                </Text>
              </View>



            </View>
          </View>




          <View style={{}}>
            <View
              style={{
                flexDirection: 'row',
                //   backgroundColor: 'red',
                justifyContent: 'space-between',
                paddingHorizontal: 18,
                paddingVertical: 5,
                alignItems: 'center',
              }}>
              <View style={{alignItems: 'center'}}>
                <Icons
                  size={30}
                  source={images.Profile}
                  //   _style={{marginRight: 15}}
                  //   onPress={() => this.setState({modalVisible: true})}
                />
                <Text
                  style={{
                    fontSize: fonts.fontSize10,
                    marginTop: 5,
                    color: Colors.white,
                  }}>
                  Effect
                </Text>
              </View>

              <Icons
                size={40}
                source={images.play}
                _style={{marginBottom: 15}}
                //   onPress={()=>this.props.navigation.navigate('')}
              />

              <View style={{alignItems: 'center'}}>
                <Icons
                  size={20}
                  source={images.upload}
                  //   _style={{marginRight: 15}}
                  onPress={() => this.props.navigation.navigate('PostVideo')}
                />
                <Text
                  style={{
                    fontSize: fonts.fontSize10,
                    marginTop: 5,
                    color: Colors.white,
                  }}>
                  Upload
                </Text>
              </View>
            </View>
          </View>
        </View>
        <SafeAreaView />
      </ImageBackground>
    );
  }
}

// <View style={{alignItems: 'center', marginVertical: 10}}>
// <Icons
//   size={45}
//   source={images.filteractive}
//   _style={{
//     alignSelf: 'flex-end',
//     //   marginRight: 20,
//     //   marginTop: 10,
//   }}
// />
// {/* <Text
//   style={{
//     fontSize: fonts.fontSize10,
//     color: Colors.white,
//     marginTop: 5,
//   }}>
//   FILTER
// </Text> */}
// </View>

// <View style={{alignItems: 'center'}}>
// <Icons
//   size={18}
//   source={images.filter_icon_active}
//   _style={{
//     alignSelf: 'flex-end',
//     //   marginRight: 20,
//     //   marginTop: 10,
//   }}
// />
// <Text
//   style={{
//     fontSize: fonts.fontSize10,
//     color: Colors.hotMagenta,
//     marginTop: 5,
//   }}>
//   FILTER
// </Text>
// </View>

// <View style={{alignItems: 'center', marginVertical: 10}}>
// <Icons
//   size={20}
//   source={images.sound_ic}
//   _style={{
//     alignSelf: 'flex-end',
//     //   marginRight: 20,
//     //   marginTop: 10,
//   }}
//   onPress={() => this.setState({modalVisible: false})}
// />
// <Text
//   style={{
//     fontSize: fonts.fontSize10,
//     color: Colors.white,
//     marginTop: 5,
//   }}>
//   SOUND
// </Text>
// </View>

// <View style={{alignItems: 'center'}}>
// <Icons
//   size={18}
//   source={images.speed_icon_unactive}
//   _style={{
//     alignSelf: 'flex-end',
//     //   marginRight: 20,
//     //   marginTop: 10,
//   }}
//   onPress={() => this.setState({modalVisible: false})}
// />
// <Text
//   style={{
//     fontSize: fonts.fontSize10,
//     color: Colors.white,
//     marginTop: 5,
//   }}>
//   SPEED
// </Text>
// </View>

// <View style={{alignItems: 'center', marginVertical: 10}}>
// <Icons
//   size={18}
//   source={images.cross}
//   _style={{
//     alignSelf: 'flex-end',
//     //   marginRight: 20,
//     //   marginTop: 10,
//   }}
//   onPress={() => this.setState({modalVisible: false})}
// />
// <Text
//   style={{
//     fontSize: fonts.fontSize10,
//     color: Colors.white,
//     marginTop: 5,
//   }}>
//   FLIP
// </Text>
// </View>

// <View style={{alignItems: 'center'}}>
// <Icons
//   size={15}
//   source={images.trim_icon_unactive}
//   _style={{
//     alignSelf: 'flex-end',
//     //   marginRight: 20,
//     //   marginTop: 10,
//   }}
//   onPress={() => this.setState({modalVisible: false})}
// />
// <Text
//   style={{
//     fontSize: fonts.fontSize10,
//     color: Colors.white,
//     marginTop: 5,
//   }}>
//   {` TRIM\nMUSIC`}
// </Text>
// </View>

// <View style={{alignItems: 'center', marginVertical: 10}}>
// <Icons
//   size={18}
//   source={images.stricker_icon_white}
//   _style={{
//     alignSelf: 'flex-end',
//     //   marginTop: 10,
//   }}
//   onPress={() => this.setState({modalVisible: false})}
// />
// <Text
//   style={{
//     fontSize: fonts.fontSize10,
//     color: Colors.white,
//     marginTop: 5,
//   }}>
//   STRICKERS
// </Text>
// </View>

// <View style={{alignItems: 'center', marginVertical: 10}}>
// <Icons
//   size={18}
//   source={images.text_icon_unactive}
//   _style={{
//     alignSelf: 'flex-end',
//     //   marginRight: 20,
//     //   marginTop: 10,
//   }}
//   onPress={() => this.setState({modalVisible: false})}
// />
// <Text
//   style={{
//     fontSize: fonts.fontSize10,
//     color: Colors.white,
//     marginTop: 5,
//   }}>
//   TEXT </Text>
// </View>

// <View style={{alignItems: 'center'}}>
// <Icons
//   size={18}
//   source={images.timer}
//   _style={{
//     alignSelf: 'flex-end',
//     //   marginRight: 20,
//     //   marginTop: 10,
//   }}
//   onPress={() => this.setState({modalVisible: false})}
// />
// <Text
//   style={{
//     fontSize: fonts.fontSize10,
//     color: Colors.white,
//     marginTop: 5,
//   }}>
//   TIMMER
// </Text>
// </View>
