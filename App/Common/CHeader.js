import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import {images} from '../Assets/ImageUrl';
import Fonts from '../Assets/Fonts';
import Colors from '../Assets/Colors';
import fonts from '../Assets/Fonts';

export default class CHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    console.log('thissss', this.props);
    return (
      <SafeAreaView style={styles.header_view}>
        <View
          style={{
            flex: 1,
            // backgroundColor: 'red',
            flexDirection: 'row',
            paddingTop: 10,
            marginLeft: 12,
          }}>
          {this.props.backArrowLeft && (
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack(null)}
              style={{height: 20, width: 20}}>
              <Image
                source={images.back_arrow}
                resizeMode={'contain'}
                style={{
                  height: '100%',
                  width: '100%',
                  tintColor: this.props.whitearrow,
                }}
              />
            </TouchableOpacity>
          )}
        </View>

        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            paddingTop: 10,
            marginLeft: 18,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          {this.props.centerItem && (
            <Image
              source={images.yapme_lg}
              resizeMode={'contain'}
              style={{height: 50, width: 70}}
            />
          )}
          <Text
            style={{
              fontSize: this.props.fontSize,
              fontWeight: this.props.fontWeight,
              fontFamily: fonts.NovaRegularios,
              color: this.props.color,
            }}>
            {this.props.centerText}
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            // backgroundColor: 'red',
            flexDirection: 'row',
            paddingTop: 10,
            marginLeft: 18,
          }}>
          {this.props.children}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header_view: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    // paddingHorizontal: 15,
    // backgroundColor: Colors.vividPurple,
  },
  center_view: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  right_view: {
    width: '20%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  left_view: {
    width: '20%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  right_txt: {
    color: Colors.white,
    fontSize: 15,
    fontFamily: Fonts.RobotoBlack,
  },
  center_txt: {
    color: Colors.white,
    fontSize: 15,
    fontFamily: Fonts.RobotoBlack,
  },
});

{
  /* <View style={styles.left_view}>
{this.props.backArrowLeft && (
  <TouchableOpacity
    onPress={() => this.props.navigation.goBack(null)}
  //   style={{height: 20, width: 15.7}}
    >
    <Image
      source={images.back_arrow}
      // source={this.props.ShowMenu}
      resizeMode={'contain'}
      style={{height: '100%', width: '100%'}}
    />
  </TouchableOpacity>
)}

{this.props.showLeftRespect && (
  <TouchableOpacity
    style={{width: 50}}
    onPress={() => this.props.onLeftRespectPress()}>
    <Image
      source={images.respect}
      resizeMode={'contain'}
      style={{height: 28, width: 28, marginLeft: 10}}
    />
  </TouchableOpacity>
)}
</View>



<View style={styles.center_view}>
{this.props.centerImage && (
  <TouchableOpacity
    style={{
      height: 20.2,
      width: 28.4,
      justifyContent: 'center',
      alignItems: 'center',
    }}>
    <Image
      source={images.yapme_lg}
      resizeMode={'contain'}
      style={{height: '100%', width: '100%'}}
    />
  </TouchableOpacity>
)}
{this.props.centerTitle && (
  <Text style={styles.center_txt}>{this.props.centerTitle}</Text>
)}
</View> */
}

{
  /* right_area */
}

{
  /* usernotification */
}

{
  /* <View style={styles.right_view}>
{this.props.rightTextH && (
  <TouchableOpacity onPress={() => this.props.onVirtualSesHPress()}>
    <Text style={styles.right_txt}>{this.props.rightTextH}</Text>
  </TouchableOpacity>
)}

{this.props.rightShowImage && (
  <TouchableOpacity
  // onPress={() => this.props.onShowRight()}
  >
    <Image
      source={this.props.rightShowImage}
      resizeMode={'contain'}
      style={{height: 20, width: 20}}
    />
  </TouchableOpacity>
)}
</View> */
}
