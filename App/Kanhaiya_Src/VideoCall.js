import React, { Component } from 'react';
import { Image, View, FlatList, StyleSheet, Text, TouchableOpacity, SafeAreaView } from 'react-native';
import ImagePath from '../../common/ImagePath_K';
import { Actions } from 'react-native-router-flux'
import { ScreenRatio, ScreenHeight, ScreenWidth } from '../../common/ScreenDimension'
import ConstantSize from '../../common/ConstantSize';
import fonts from '../../common/fonts';
import colors from '../../common/colors';

export default class VideoCall extends Component {
    static navigationOptions = { header: null }
    constructor(props) {
        super(props)
        this.state = { }
    }

    render() {
        return (
            <SafeAreaView style={Styles.container}>
                <View style={Styles.container}>
                    <Image style={Styles.splashsz}
                        source={ImagePath.splash}>
                    </Image>
                </View>

                <View style={Styles.maincontain}>
                    <View style={[Styles.mainView,]}>
                        <Image resizeMode= 'contain'  style={Styles.backiocn} source={ImagePath.back_arrow}/>
                        <Image resizeMode= 'contain' style={[Styles.Iamgecamera,{marginHorizontal:20}]} source={ImagePath.camera_flip}/>
                    </View>

                    <View style={[Styles.mainView,{justifyContent:'center'}]}>
                       <Text style={{fontSize:18,color:'#fff'}}>00:30</Text>
                    </View>
                    <View style={[Styles.mainView,]}>
                        <Image resizeMode= 'contain' style={Styles.IamgeCall1} source={ImagePath.Rectangle5}/>
                    </View>

                </View>

                <View style={Styles.contain}>
                    <View style={Styles.mainView}>
                        <Image resizeMode= 'contain'  style={Styles.IamgePath} source={ImagePath.call_circle}/>
                    </View>

                    <View style={Styles.mainView}>
                        <Image resizeMode= 'contain'  style={Styles.IamgePath} source={ImagePath.face_circle}/>
                    </View>

                    <View style={Styles.mainView}>
                        <Image resizeMode= 'contain' style={Styles.IamgeCall} source={ImagePath.add_icon}/>
                    </View>

                    <View style={Styles.mainView}>
                        <Image resizeMode= 'contain' style={Styles.IamgePath}  source={ImagePath.microphone_circle}/>
                    </View>

                    <View style={Styles.mainView}>
                        <Image  resizeMode= 'contain'  style={Styles.IamgePath} source={ImagePath.speaker_circle}/>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}
const Styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: colors.white },
    splashsz: { width: ConstantSize.percent_hundred, height: ConstantSize.percent_hundred, resizeMode: 'stretch' },
    maincontain:{position: 'absolute', top: 10,flexDirection:'row',paddingVertical:0,},
    IamgePath: { width: 40, height: 40 },
    Iamgecamera: { width: 35, height: 35 },
    IamgeCall: { width: 60, height: 60,top:-15 },
    backiocn: { width: 20, height: 20 },
    IamgeCall1: { width: 90, height: 90,top:10 },
    mainView:{marginHorizontal:15,flex:0.4, flexDirection:'row',alignItems:'center'},
    contain:{ position: 'absolute', bottom: 10,flexDirection:'row',justifyContent:'center', alignSelf:'center'}
});


