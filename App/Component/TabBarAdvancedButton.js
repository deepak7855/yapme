// import React from 'react';
// import {StyleSheet, TouchableOpacity, Image, View} from 'react-native';
// import {BottomTabBarButtonProps} from '@react-navigation/bottom-tabs';
// // import { FontAwesome as Icon } from '@expo/vector-icons';
// import Icon from 'react-native-vector-icons/FontAwesome';
// import {TabBg} from '../svg';
// import {images} from '../Assets/ImageUrl';

// type Props = BottomTabBarButtonProps & {
//   bgColor?: string,
// };

// export const TabBarAdvancedButton: React.FC<Props> = ({bgColor, ...props}) => (
//   <View style={styles.container} pointerEvents="box-none">
//     <TabBg color={bgColor} style={styles.background} />
//     <TouchableOpacity style={styles.button} 
//     // onPress={props.onPress}
//     // onPress={()=>console.log(props,'porps')}
//     >
//       <Image
//         style={{height: 65, width: 65, bottom: 2}}
//         resizeMode="contain"
//         source={images.plus_icon}
//       />
//     </TouchableOpacity>
//   </View>
// );

// const styles = StyleSheet.create({
//   container: {
//     position: 'relative',
//     width: 75,
//     backgroundColor: 'transparent',
//     alignItems: 'center',
//   },
//   background: {
//     position: 'absolute',
//     top: 0,
//     backgroundColor: 'transparent',


//   },
//   button: {
//     top: -22.5,
//     justifyContent: 'center',
//     alignItems: 'center',
//     width: 50,
//     height: 50,
//     borderRadius: 27,
//     backgroundColor: 'transparent',
//   },
//   buttonIcon: {
//     fontSize: 16,
//     color: '#F6F7EB',
//   },
// });


import React from 'react';
import {StyleSheet, TouchableOpacity, Image, View} from 'react-native';
import {BottomTabBarButtonProps} from '@react-navigation/bottom-tabs';
// import { FontAwesome as Icon } from '@expo/vector-icons';
import Icon from 'react-native-vector-icons/FontAwesome';
import {TabBg} from '../svg';
import {images} from '../Assets/ImageUrl';

type Props = BottomTabBarButtonProps & {
  bgColor?: string,
};

export const TabBarAdvancedButton: React.FC<Props> = ({bgColor, ...props}) => (
  <View style={styles.container} pointerEvents="box-none">
    <TabBg color={bgColor} style={styles.background} />
    <TouchableOpacity style={styles.button} 
    onPress={props.onPress}
    >
      <Image
        style={{height: 65, width: 65, bottom: 2}}
        resizeMode="contain"
        source={images.plus_icon}
      />
    </TouchableOpacity>
  </View>
);

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    width: 75,
    alignItems: 'center',
  },
  background: {
    position: 'absolute',
    top: 0,
  },
  button: {
    top: -22.5,
    justifyContent: 'center',
    alignItems: 'center',
    width: 50,
    height: 50,
    borderRadius: 27,
    backgroundColor: 'transparent',
  },
  buttonIcon: {
    fontSize: 16,
    color: '#F6F7EB',
  },
});
