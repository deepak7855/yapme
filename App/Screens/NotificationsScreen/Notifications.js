import React, {Component} from 'react';
import {
  SafeAreaView,
  TextInput,
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import Header from '../../Common/Header';
import {Icons, Searchbar} from '../../Component/Common';
import ArrData from '../../../dummydata';
import CHeader from '../../Common/CHeader';

export default class Notifications extends Component {
  _renderFriendRequestList = ({item, index}) => {
    return (
      <View style={styles.friend_req_view}>
        <Text
          style={{
            fontFamily: fonts.NovaRegularios,
            fontSize: fonts.fontSize13,
          }}>
          {item.title}
        </Text>

        <View style={styles.btn_view}>
          <Text
            style={{
              fontSize: fonts.fontSize10,
              fontFamily: fonts.NovaRegularios,
              color: Colors.pinkishGreyTwo,
            }}>
            {item.date}
          </Text>
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={{flex: 1, backgroundColor: Colors.whiteTwo}}>
       <SafeAreaView style={styles.safe_area_view} />
          <CHeader navigation={this.props.navigation} backArrowLeft centerItem>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
                justifyContent: 'flex-end',
                paddingRight: 8,
              }}>
              <View style={{alignItems: 'center', flexDirection: 'row'}}>
                <Text
                  style={{
                    fontFamily: fonts.NovaRegularios,
                    fontSize: fonts.fontSize10,
                    color: Colors.brownishGrey,
                  }}>
                  All Activity
                </Text>

                <Icons
                  size={15}
                  source={images.dots_gray}
                  _style={{marginHorizontal:6}}
                  onPress={()=>this.props.navigation.navigate('FriendRequested')}
                />
              </View>
            </View>
          </CHeader>
        <View style={{marginHorizontal: 18}}>
         
          <Text
            style={{
              fontFamily: fonts.NovaBoldios,
              fontSize: fonts.fontSize16,
              marginVertical: 10,
            }}>
            Notification
          </Text>

          <FlatList
            showsVerticalScrollIndicator={false}
            data={ArrData.arrdummytext}
            renderItem={this._renderFriendRequestList}
            extraData={this.state}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  safe_area_view: {
    // flex: 1,
  },
  friend_req_view: {
    borderRadius: 10,
    marginVertical: 6,
    paddingHorizontal: 10,
    paddingVertical: 23,
    paddingLeft: 18,
    borderWidth: 1,
    borderColor: Colors.white,
    marginHorizontal: 3,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.15,
    shadowRadius: 1.94,
    elevation: 3,
  },
  btn_view: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 0.7,
    // backgroundColor: 'red',
    justifyContent: 'flex-end',
  },
});
