import { StyleSheet } from 'react-native';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';


export default TermsofuseStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
    },
    terms_view: {
        marginHorizontal: 13,
        marginVertical: 5
    },
    terms_text: {
        lineHeight: 20,
        fontSize: Fonts.fontSize16,
        color: Colors.black,
        fontFamily: Fonts.NovaBold
    },
    des_text_view: {
        marginHorizontal: 13,
        marginVertical: 5
    },
    guidelines_text: {
        fontSize: Fonts.fontSize13,
        lineHeight: 17,
        color: Colors.black,
        fontFamily: Fonts.NovaRegular
    },
    guidelines_text_view: {
        marginHorizontal: 13,
        marginVertical: 5
    },
    guidelines_text_des: {
        fontFamily: Fonts.NovaRegular,
        lineHeight: 20,
        textAlign: 'left',
        color: Colors.pinkishGreyTwo,
        fontSize: Fonts.fontSize13
    },
    dummy_text:{
        color:Colors.black,
        fontSize:Fonts.fontSize15,
        fontFamily:Fonts.NovaRegular,
        marginTop:15,
    },
    music_view:{
        marginHorizontal: 13, marginVertical: 10
    },
    music_text_View:{
        flexDirection: "row", alignItems: 'center'
    },
    check_box_img:{
        width: 12.5, height: 12.5 
    },
    yapme_text:{
        fontSize: Fonts.fontSize13, lineHeight: 17, fontFamily: Fonts.NovaRegular, color: Colors.pinkishGrey, marginLeft: 10
    },
    music_video_text:{
        fontSize: Fonts.fontSize13, lineHeight: 17, fontFamily: Fonts.NovaRegular, color: Colors.cobaltBlue, marginLeft: 10 
    },
    video_view:{
        marginHorizontal: 13, marginVertical: 10
    },
    video_text_view:{
        flexDirection: "row", alignItems: 'center'
    }

});