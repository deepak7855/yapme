import { StyleSheet } from 'react-native';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';


export default OtherUserProfileStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
    },
    other_user_profile_view:{
        marginTop: 20, 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        marginHorizontal: 34
    },
    add_img_view:{
        height: 35, 
        width: 35, 
        borderRadius: 35 / 2 
    },
    add_img:{
        height: '100%', 
        width: '100%'
    },
    other_user_image_view:{
        height: 70, 
        width: 70, 
        borderRadius: 70 / 2,
    },
    other_profile_img:{
        height: '100%', 
        width: '100%',
    },
    chat_img_view:{
        height: 45, 
        width: 45, 
        borderRadius: 45 / 2,

    },
    chat_img:{
        height: '85%', 
        width: '85%',

    },
    other_user_name_view:{
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop: 5
    },
    other_user_name_text:{
        fontSize: Fonts.fontSize20, 
        lineHeight: 24, 
        color: Colors.black, 
        fontFamily: Fonts.NovaRegular
    },
    you_tuber_text_view:{
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop: 5
    },
    you_tuber_text:{
        fontSize: Fonts.fontSize15, 
        lineHeight: 17, 
        fontFamily: Fonts.NovaRegular, 
        color: Colors.pinkishGrey
    },
    fans_following_count_view:{
        marginTop: 20, 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        marginHorizontal: 45
    },
    fans_count_view:{
        alignItems: 'center', 
        flex: 2
    },
    count_text:{
        fontSize: Fonts.fontSize17, 
        color: Colors.black, 
        fontFamily: Fonts.NovaRegular
    },
    fans_following_text:{
        fontSize: Fonts.fontSize13, 
        color: Colors.pinkishGrey, 
        marginTop:5,
        fontFamily: Fonts.NovaRegular
    },
    following_count_view:{
        alignItems: 'center', 
        flex: 2 
    },
    separator_vertical:{
        width: 1, 
        height: 38, 
        backgroundColor: Colors.whiteThree
    },
    about_me_view:{
        marginHorizontal: 15, 
        marginTop: 20
    },
    about_me_text:{
        fontSize: Fonts.fontSize13, 
        lineHeight: 15, 
        fontFamily: Fonts.NovaBold, 
        color: Colors.black
    },
    about_des_view:{
        marginHorizontal: 15, 
        marginTop: 10
    },
    about_des_text:{
        fontSize: Fonts.fontSize13, 
        fontFamily: Fonts.NovaRegular, 
        lineHeight: 20, 
        color: Colors.pinkishGreyTwo, 
        textAlign: 'left'
    },
    separator_view:{
        marginHorizontal: 15, 
        marginTop: 27
    },
    separator_part:{
        height: 1, 
        width: '100%', 
        backgroundColor: Colors.whiteFour,
    },


    

    separator_view:{
        marginHorizontal: 15, 
        marginTop: 27
    },
    separator_part:{
        height: 1, 
        width: '100%', 
        backgroundColor: Colors.whiteFour,
    },





    image_parent_view: {
        alignItems: 'center',
        marginTop:20,
    },
  
    image_view: {
        alignItems: 'center',
        width: 94,
        height: 94,
        borderRadius: 94 / 2
    },
    profile_img: {
        height: '100%',
        width: '100%',
        borderRadius:100/2,
    },
   
    submit_btn_view: {
        alignItems: 'center',
        marginVertical: 15,
    },


    circle: {
        marginHorizontal:15,
        // height: 34,
        // width: 34,
        // borderRadius: 17,
        justifyContent: 'center',
        alignItems: 'center',
        // borderWidth: 1.5,
    },
    count_text: {
        color: Colors.black,
        fontSize: 12
    },
    separator: {
        height: 1.5,
        flex: 1
    },


});