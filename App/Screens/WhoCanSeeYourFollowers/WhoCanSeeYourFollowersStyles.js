import { StyleSheet } from 'react-native';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';


export default WhoCanSeeYourFollowersStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
    },
    can_see_followers_view:{
        marginVertical: 10, 
        marginHorizontal: 15
    },
    can_see_followers_text:{
        lineHeight: 20, 
        color: Colors.black, 
        fontSize: Fonts.fontSize16, 
        fontFamily: Fonts.NovaBold 
    },
    list_followers_view:{
        marginHorizontal:11,
        borderWidth:1,
        borderColor:'lightgrey',
        marginVertical:8,
        borderRadius:10
    },
    list_item_touch:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingHorizontal:10,
        paddingVertical:15
    },
    see_option:{
        fontSize:Fonts.fontSize13,
        lineHeight:15,
        fontFamily:Fonts.NovaRegular
    },
    radio_img:{
        height:16,
        width:16
    },
   
});