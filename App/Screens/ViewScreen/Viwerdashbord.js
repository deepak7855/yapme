import React, {Component} from 'react';
import {
  ImageBackground,
  Text,
  View,
  StyleSheet,
  StatusBar,
  FlatList,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Image,
} from 'react-native';
import {SafeAreaView} from 'react-navigation';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import CHeader from '../../Common/CHeader';
import {GButton} from '../../Common/GButton';
import {Icons} from '../../Component/Common';
import {Custompopup} from '../../Component/Modal/CustomModel';
import {ComponentHeader} from '../PlayTab/LiveStreaming';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';

var {height, width} = Dimensions.get('window');

const Gift = () => (
  <View>
    <View style={{marginHorizontal: 15, marginVertical: 15}}>
      <Text
        style={{
          fontSize: fonts.fontSize13,
          color: Colors.white,
          fontFamily: fonts.NovaBoldios,
        }}>
        Gifts
      </Text>

      <View style={{flexDirection: 'row', marginTop: 15}}>
        <Text
          style={{
            fontSize: fonts.fontSize13,
            color: Colors.brownishGrey,
            fontFamily: fonts.NovaBoldios,
            marginRight: 20,
          }}>
          Draw
        </Text>
        <Text
          style={{
            fontSize: fonts.fontSize13,
            color: Colors.white,
            fontFamily: fonts.NovaBoldios,
          }}>
          Popular
        </Text>
      </View>
    </View>

    <View style={{borderWidth: 0.4, borderColor: Colors.lightgrey}}></View>

    <FlatList
      data={ArrData.arrGift}
      contentContainerStyle={{
        marginTop: 15,
        marginHorizontal: 20,
      }}
      renderItem={({item}) => (
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            margin: 2,
            marginBottom: 20,
            marginHorizontal: 4,
            // backgroundColor: 'red',
            // borderWidth:1,borderColor:Colors.lightgrey,borderRadius:8
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: 60,
              backgroundColor: 'trnasparent',
              borderWidth: 1,
              borderColor: Colors.lightgrey,
              borderRadius: 8,
            }}>
            <Icons
              size={28}
              source={item.icon}
              // onPress={() => this.props.navigation.navigate('Live')}
              // _style={{marginRight: 5}}
            />
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: fonts.NovaRegularios,
                fontSize: fonts.fontSize10,
                color: Colors.white,
                marginTop: 10,
              }}>
              {item.text}
            </Text>
          </View>
        </View>
      )}
      //Setting the number of column
      numColumns={5}
      keyExtractor={(item, index) => index}
    />

    <View
      style={{
        // position: 'absolute',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 20,
        marginTop: 20,
        // height: 160,
        // right: 15,
      }}>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <Icons
          size={18}
          source={images.diamond_lg}
          // onPress={() => this.props.navigation.navigate('Live')}
          _style={{marginRight: 15}}
        />
        <Text
          style={{
            fontFamily: fonts.NovaBoldios,
            fontSize: fonts.fontSize13,
            color: Colors.white,
          }}>
          724,57
        </Text>
      </View>

      <View
        style={{
          marginLeft: '40%',
          borderWidth: 1,
          borderColor: Colors.vividPurple,
          flexDirection: 'row',
          alignSelf: 'flex-end',
          borderRadius: 5,
        }}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text
            style={{
              fontFamily: fonts.NovaBoldios,
              fontSize: fonts.fontSize13,
              color: Colors.white,
              marginLeft: 5,
            }}>
            1x
          </Text>

          <Icons
            size={10}
            source={images.down_arrow}
            // onPress={() => this.props.navigation.navigate('Live')}
            _style={{marginLeft: 20, marginRight: 10, tintColor: Colors.white}}
          />

          <View
            style={{
              backgroundColor: Colors.vividPurple,
              paddingHorizontal: 20,
              paddingVertical: 3,
              borderTopLeftRadius: 5,
              borderBottomLeftRadius: 5,
            }}>
            <Text
              style={{
                fontFamily: fonts.NovaRegular,
                fontSize: fonts.fontSize13,
                color: Colors.white,
              }}>
              send
            </Text>
          </View>
        </View>
      </View>

      <View></View>
    </View>
  </View>
);

const UserProfilePopup = () => (
  <>
    <View style={{alignItems: 'center'}}>
      <Icons size={80} source={images.Profile} _style={{bottom: 30}} />
      <View style={styles.other_user_name_view}>
        <Text style={styles.other_user_name_text}>@Erric joined</Text>
      </View>

      <View style={styles.you_tuber_text_view}>
        <Icons size={20} source={images.location} />

        <Text style={styles.you_tuber_text}>San Francisco, CA</Text>
      </View>

      <View
        style={{
          paddingHorizontal: 5,
          backgroundColor: Colors.hotMagenta,
          paddingVertical: 3,
          borderRadius: 5,
          marginTop: 10,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Icons size={15} source={images.eye_white} _style={{marginRight: 5}} />
        <Text style={{fontSize: fonts.fontSize13, color: Colors.white}}>
          30
        </Text>
      </View>
    </View>

    <View style={{height: 10}} />

    <View style={styles.fans_following_count_view}>
      <View style={styles.fans_count_view}>
        <Text style={styles.count_text}>
          76.3<Text>k</Text>
        </Text>
        <Text style={styles.fans_following_text}>Fans</Text>
      </View>

      <View style={styles.separator_vertical}></View>

      <View style={styles.following_count_view}>
        <Text style={styles.count_text}>
          3.45<Text>k</Text>
        </Text>
        <Text style={styles.fans_following_text}>Following</Text>
      </View>
    </View>

    <View style={{height: 20}} />

    <View style={styles.submit_btn_view}>
      <GButton
        height={60}
        width={150}
        radius={5}
        txtcolor={Colors.white}
        fontFamily={fonts.NovaBold}
        fontSize={fonts.fontSize13}
        bText={'+ Follow'}
      />

      <View style={{width: 10}} />

      <GButton
        height={60}
        width={150}
        radius={5}
        txtcolor={Colors.white}
        fontFamily={fonts.NovaBold}
        fontSize={fonts.fontSize13}
        bText={'Chat'}
      />
    </View>
  </>
);

export default class Viwerdashbord extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible3: false,
      modalVisible4: false,
    };
  }

  onPresModel() {
    this.setState({modalVisible3: true});
  }

  onPresUserModel() {
    this.setState({modalVisible4: true});
  }

  render() {
    return (
      <ImageBackground style={{flex: 1}} source={images.Rectangle5}>
        <StatusBar
          backgroundColor={Colors.violetBlue}
          barStyle="light-content"
        />
        <SafeAreaView />

        <ComponentHeader
          navigation={this.props.navigation}
          onPresModel={() => this.onPresModel()}
          onPresUserModel={() => this.onPresUserModel()}
        />
        <SafeAreaView />

        <ScrollView
          bounces={false}
          style={{marginBottom: 0}}
          showsVerticalScrollIndicator={false}>
          {/* <View><Text>1111</Text></View> */}

          <View style={{}}>
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                justifyContent: 'space-between',
                // backgroundColor: 'green',
                alignItems: 'center',
                marginTop: 30,
              }}>
              {/* Right View */}

              {/* Left View */}
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'flex-end',
                }}>
                <View style={{}}>
                  <View
                    style={{
                      // width: 100,
                      height: height * 0.48,
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginVertical: 20,
                      backgroundColor: 'rgba(0,0,0,0.3)',
                      paddingBottom: 8,
                      paddingHorizontal: 5,
                      borderRadius: 10,
                    }}>
                    <View>
                      <Text
                        style={{
                          marginVertical: 10,
                          fontFamily: fonts.NovaRegularios,
                          color: Colors.white,
                        }}>
                        Suggested
                      </Text>
                    </View>

                    <FlatList
                      nestedScrollEnabled
                      showsVerticalScrollIndicator={false}
                      data={['', '', '', '', '', '', '', '', '', '', '', '']}
                      contentContainerStyle={{
                        paddingBottom: 5,
                      }}
                      renderItem={({}) => (
                        <View>
                          <Image
                            resizeMode="contain"
                            source={images.Rectangle1}
                            style={{
                              width: width * 0.22,
                              resizeMode: 'contain',
                              height: undefined,
                              aspectRatio: 1,
                            }}
                          />
                        </View>
                      )}
                    />

                    {/* <Icons size={90} source={images.Rectangle1} _style={{}} />
                  <Icons size={90} source={images.Rectangle3} _style={{}} />
                  <Icons size={90} source={images.Rectangle4} _style={{}} />
                  <Icons size={90} source={images.Rectangle5} _style={{}} /> */}
                  </View>
                </View>
              </View>

              {/* <View style={{width: 100, backgroundColor: 'rgba(0,0,0,0.3)'}}>
              <View style={{alignItems: 'center', justifyContent: 'center'}}>
              
                <Icons size={90} source={images.Rectangle1} _style={{}} />

                <Icons size={90} source={images.Rectangle2} _style={{}} />

                <Icons size={90} source={images.Rectangle3} _style={{}} />

                <Icons size={90} source={images.Rectangle4} _style={{}} />
              </View>
            </View>

            <View style={{width: 100, backgroundColor: 'rgba(0,0,0,0.3)'}}>
              <View style={{alignItems: 'center', justifyContent: 'center'}}>
                <View>
                  <Text
                    style={{
                      marginVertical: 10,
                      fontFamily: fonts.NovaRegularios,
                      color: Colors.white,
                    }}>
                    Suggested
                  </Text>
                </View>
                <Icons size={90} source={images.Rectangle1} _style={{}} />

                <Icons size={90} source={images.Rectangle2} _style={{}} />

                <Icons size={90} source={images.Rectangle3} _style={{}} />

                <Icons size={90} source={images.Rectangle4} _style={{}} />
              </View>
            </View> */}
            </View>
          </View>



          <View
            style={{
              //   backgroundColor: 'red',
              //   marginBottom: 70,
              marginLeft: 15,
              marginRight: 10,
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingHorizontal: 10,
                marginBottom: 100,
                marginLeft: 10,
                backgroundColor: 'rgba(0,0,0,0.4)',
                width: 250,
              }}>
              <View
                style={{
                  alignItems: 'center',
                  flexDirection: 'row',
                  paddingVertical: 15,
                }}>
                <Icons
                  size={38}
                  source={images.Profile}
                  _style={{marginRight: 10}}
                />

                <View style={{}}>
                  <Text
                    style={[
                      styles.txt,
                      {
                        fontFamily: fonts.NovaSboldios,
                        fontSize: fonts.fontSize13,
                        //   marginTop: 5,
                      },
                    ]}>
                    Sara Smith
                  </Text>

                  <Text
                    style={[
                      styles.txt,
                      {
                        fontFamily: fonts.NovaSboldios,
                        fontSize: 12,
                        marginTop: 5,
                      },
                    ]}>
                    Send
                  </Text>
                </View>
              </View>

              <Icons
                size={40}
                source={images.gift_ic}
                _style={{marginRight: 10, marginLeft: 30}}
              />
              <Text
                style={{
                  fontSize: fonts.fontSize20,
                  marginBottom: 10,
                  color: Colors.macaroniAndCheese,
                }}>
                +
              </Text>
              <Text
                style={{
                  fontSize: fonts.fontSize20,
                  marginBottom: 10,
                  color: Colors.macaroniAndCheese,
                }}>
                2
              </Text>
            </View>

            <View>
              <View
                style={{
                  flexDirection: 'row',
                  paddingHorizontal: 10,
                  marginBottom: 20,
                }}>
                <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Icons
                      size={30}
                      source={images.Profile}
                      _style={{marginRight: 10}}
                      onPress={() => this.setState({modalVisible4: true})}
                    />
                    <Text
                      style={{
                        fontFamily: fonts.NovaRegularios,
                        fontSize: fonts.fontSize13,
                        color: Colors.white,
                        marginRight: 10,
                      }}>
                      Erric joined
                    </Text>
                  </View>

                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderWidth: 1,
                      borderColor: Colors.white,
                      paddingHorizontal: 10,
                      paddingVertical: 8,
                      borderRadius: 30,
                    }}>
                    <Icons
                      size={20}
                      source={images.wave_ic}
                      _style={{marginRight: 5}}
                    />
                    <Text style={styles.squretxt}>Wave</Text>
                  </View>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: 10,
                  marginBottom:10
                }}>
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: Colors.white,
                    paddingLeft: 20,
                    paddingRight: 70,
                    paddingVertical: 10,
                    borderRadius: 30,
                  }}>
                  <Text
                    style={{
                      fontFamily: fonts.NovaRegularios,
                      fontSize: fonts.fontSize13,
                      color: Colors.white,
                    }}>
                    Comment
                  </Text>
                </View>

                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Icons
                    size={25}
                    source={images.firecracker}
                    _style={{marginRight: 20}}
                  />
                  <Icons
                    size={23}
                    source={images.giftbox}
                    _style={{marginRight: 15}}
                    onPress={() => this.setState({modalVisible3: true})}
                  />
                </View>
              </View>
            </View>
          </View>
    
        <SafeAreaView />

        {/* Gift popUp */}
        <Custompopup modalVisible={this.state.modalVisible3}>
          <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.2)'}}>
          <TouchableOpacity activeOpacity={1}
              onPress={() => this.setState({modalVisible3: false})}
               style={{flex:2}}/>
            <View
              style={{
                flex: 1.5,
                backgroundColor: 'rgba(0,0,0,0.8)',
              }}>
              {this.state.modalVisible3 && <Gift />}
            </View>
          </View>
        </Custompopup>

        <Custompopup modalVisible={this.state.modalVisible4}>
          <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.2)'}}>
          <TouchableOpacity activeOpacity={1}
              onPress={() => this.setState({modalVisible4: false})}
               style={{flex:1}}/>
            <View
              style={{
                flex: 1.5,
                backgroundColor: Colors.white,
                borderTopLeftRadius: 15,
                borderTopRightRadius: 15,
              }}>
               {this.state.modalVisible4 && <UserProfilePopup />}
            </View>
          </View>
        </Custompopup>

        {/* <Custompopup modalVisible={this.state.modalVisible1}>
          <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.2)'}}>
            <View
              onTouchEnd={() => this.setState({modalVisible2: false})}
              style={{flex: 2}}></View>
            <View
              style={{
                flex: 1.5,
                backgroundColor: Colors.white,
                borderTopLeftRadius: 15,
                borderTopRightRadius: 15,
              }}>
             
            </View>
          </View>
        </Custompopup> */}
        

          
        </ScrollView>



      
     
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  View4: {fontFamily: fonts.NovaRegularios, fontSize: fonts.fontSize20},
  txt: {
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaRegularios,
    color: Colors.white,
  },

  thiredView: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 10,
    paddingRight: 10,
    // marginLeft: 18,
    // alignItems:'flex-end',
    // backgroundColor:'red',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    // justifyContent:'center',
    // alignItems:'center',
  },
  headerContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    // backgroundColor:'red'
  },
  squretxt: {
    fontFamily: fonts.NovaRegularios,
    color: Colors.white,
    fontSize: fonts.fontSize13,
  },
  pophead: {
    fontSize: fonts.fontSize16,
    fontFamily: fonts.NovaBoldios,
    color: Colors.vividPurple,
  },

  accept_btn_touch: {
    // marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
    // width: 59,
    // height: 22,
    backgroundColor: Colors.violetBlue,
    borderRadius: 5,
    paddingHorizontal: 5,
    paddingVertical: 6,
  },
  accept_text: {
    fontSize: fonts.fontSize10,
    color: Colors.white,
    fontFamily: fonts.NovaBold,
  },

  adminView: {
    // justifyContent: 'center',
    // alignItems: 'center',
    // borderColor: Colors.violetBlue,
    // borderWidth: 1,

    // borderRadius: 5,
    paddingHorizontal: 8,
    // paddingVertical: 6,
  },
  adminText: {
    fontSize: fonts.fontSize10,
    color: Colors.violetBlue,
    fontFamily: fonts.NovaBold,
  },

  existView: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end',
    borderColor: Colors.red,
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 8,
    paddingVertical: 4,

    width: 80,
  },
  existText: {
    fontSize: fonts.fontSize10,
    color: Colors.red,
    // fontFamily: fonts.NovaBold,
  },

  other_user_name_view: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
  },
  other_user_name_text: {
    fontSize: fonts.fontSize20,
    lineHeight: 24,
    color: Colors.black,
    fontFamily: fonts.NovaRegular,
  },
  you_tuber_text_view: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
    flexDirection: 'row',
  },
  you_tuber_text: {
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaRegular,
    color: Colors.black,
  },
  fans_following_count_view: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 45,
  },
  fans_count_view: {
    alignItems: 'center',
    flex: 2,
  },
  count_text: {
    fontSize: fonts.fontSize15,
    color: Colors.black,
    fontFamily: fonts.NovaXbold,
  },
  fans_following_text: {
    fontSize: fonts.fontSize13,
    color: Colors.black,
    fontFamily: fonts.NovaRegular,
    marginTop: 8,
  },
  following_count_view: {
    alignItems: 'center',
    flex: 2,
  },
  separator_vertical: {
    width: 1,
    height: 38,
    backgroundColor: Colors.whiteThree,
  },

  submit_btn_view: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 15,
    flexDirection: 'row',
  },

  View4: {fontFamily: fonts.NovaRegularios, fontSize: fonts.fontSize20},
  txt: {
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaBoldios,
    color: Colors.white,
  },

  thiredView: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 10,
    paddingRight: 10,
    // marginLeft: 18,
    // alignItems:'flex-end',
    // backgroundColor:'red',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    // justifyContent:'center',
    // alignItems:'center',
  },
  headerContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    // backgroundColor:'red'
  },
  pophead: {
    fontSize: fonts.fontSize16,
    fontFamily: fonts.NovaBoldios,
    color: Colors.vividPurple,
  },

  accept_btn_touch: {
    // marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
    // width: 59,
    // height: 22,
    backgroundColor: Colors.violetBlue,
    borderRadius: 5,
    paddingHorizontal: 5,
    paddingVertical: 6,
  },
  accept_text: {
    fontSize: fonts.fontSize10,
    color: Colors.white,
    fontFamily: fonts.NovaBold,
  },

  adminView: {
    // justifyContent: 'center',
    // alignItems: 'center',
    // borderColor: Colors.violetBlue,
    // borderWidth: 1,

    // borderRadius: 5,
    paddingHorizontal: 8,
    // paddingVertical: 6,
  },
  adminText: {
    fontSize: fonts.fontSize10,
    color: Colors.violetBlue,
    fontFamily: fonts.NovaBold,
  },

  existView: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end',
    borderColor: Colors.red,
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 8,
    paddingVertical: 4,

    width: 80,
  },
  existText: {
    fontSize: fonts.fontSize10,
    color: Colors.red,
    // fontFamily: fonts.NovaBold,
  },
});

{
  /* <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginBottom: 15,
                        }}>
                        <Icons
                          size={35}
                          source={images.Profile}
                          _style={{marginRight: 15}}
                          // onPress={() => alert('hi')}
                        />

                        <View>
                          <Text
                            style={{
                              fontFamily: fonts.NovaRegularios,
                              fontSize: fonts.fontSize13,
                            }}>
                            Cant work out if this is real or not{' '}
                          </Text>

                          <View
                            style={{
                              justifyContent: 'space-between',
                              flexDirection: 'row',
                            }}>
                            <Text
                              style={{
                                fontFamily: fonts.NovaRegularios,
                                fontSize: fonts.fontSize13,
                              }}>
                              View replies(8){' '}
                            </Text>
                            <Text
                              style={{
                                fontFamily: fonts.NovaRegularios,
                                fontSize: fonts.fontSize13,
                              }}>
                              View replies(8){' '}
                            </Text>
                          </View>
                        </View>
                      </View>
                      {index !== 0 && (
                        <TouchableOpacity style={styles.accept_btn_touch}>
                          <Text style={styles.accept_text}>Remove</Text>
                        </TouchableOpacity>
                      )}

                      {index == 0 && (
                        <TouchableOpacity style={styles.adminView}>
                          <Text
                            style={{
                              fontSize: fonts.fontSize13,
                              fontFamily: fonts.NovaRegularios,
                              color: Colors.pinkishGreyTwo,
                            }}>
                            1 day ago
                          </Text>
                          <Icons
                            size={15}
                            source={images.dislike}
                            _style={{alignSelf: 'flex-end', marginTop: 4}}
                            // onPress={() => alert('hi')}
                          />
                        </TouchableOpacity>
                      )}
                    </View> */
}
