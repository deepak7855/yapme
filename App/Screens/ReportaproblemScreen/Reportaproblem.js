import React, {Component} from 'react';
import {SafeAreaView, TextInput, View, Text} from 'react-native';
import styles from './ReportaproblemStyles';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';
import {GButton} from '../../Common/GButton';
import CHeader from '../../Common/CHeader';

class Reportaproblem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <SafeAreaView style={styles.safe_area_view}>
        <CHeader navigation={this.props.navigation} backArrowLeft centerItem />
        <View style={{height: 15}} />
        <View style={styles.report_view}>
          <Text style={styles.report_text}>Report a problem</Text>
        </View>

        <View style={styles.report_view}>
          <Text style={styles.tell_us_text}>Tell us your feedback</Text>
        </View>
        <View style={styles.feedback_input_view}>
          <TextInput
            style={[
              CommonStyle,
              styles.feedback_input,
              {paddingHorizontal: 11},
            ]}
            multiline={true}
            underlineColorAndroid="transparent"
            keyboardType={'default'}
            returnKeyType="done"
            numberOfLines={10}
            textAlignVertical="top"
            placeholder="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,"
          />
        </View>
        <View style={styles.submit_btn_view}>
          <GButton
            height={50}
            width={200}
            radius={5}
            lineHeight={15}
            txtcolor={Colors.white}
            fontFamily={Fonts.NovaBold}
            fontSize={Fonts.fontSize13}
            bText={'Submit'}
            onPress={() => this.props.navigation.goBack(null)}
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default Reportaproblem;
