import React, {Component} from 'react';
import {
  SafeAreaView,
  TextInput,
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import Header from '../../Common/Header';
import {Icons, Searchbar} from '../../Component/Common';
import ArrData from '../../../dummydata';

export default class History extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  _renderFriendRequestList = ({item, index}) => {
    return (
      <View style={styles.friend_req_view}>
        {/* <View style={styles.img_name_view}> */}
        <View style={styles.user_img_view}>
          <Text style={styles.name_text}>{item.title}</Text>
          {/* </View> */}
          <View style={styles.name_view}>
            <Text style={styles.name_text2}>{item.date}</Text>
          </View>
        </View>

        <View style={styles.btn_view}>
          {/* {index == '3'&&<Icons
            size={15}
            source={images.diamond_lg}
            _style={{marginVertical: 18, marginHorizontal: 8}}
            onPress={() => alert('hi')}
          />} */}

          <Text
            style={index == '3' ? {fontSize: 18, fontWeight: 'bold'} : null}>
            ${item.usd}
          </Text>

          <Icons
            size={17}
            // source={images.star_lg}
            source={images.diamond_lg}
            _style={{marginVertical: 18, marginHorizontal: 8}}
            onPress={() => alert('development')}
          />

          {/* <TouchableOpacity style={styles.accept_btn_touch}>
            <Text style={styles.accept_text}>Unfriend</Text>
          </TouchableOpacity> */}
        </View>
      </View>
    );
  };
  render() {
    return (
      <SafeAreaView style={styles.safe_area_view}>
        {/* <Header
                            backArrowLeft
                            centerImage
                        /> */}
        {/* <View style={styles.notification_view}>
                            <Text style={styles.notification_text}>Notification</Text>
                        </View> */}
        {/* <Searchbar _style={{marginVertical: 20}} /> */}
        <FlatList
      contentContainerStyle={{marginTop: 10}}

          showsVerticalScrollIndicator={false}
          data={ArrData.arrFriendRequest}
          renderItem={this._renderFriendRequestList}
          extraData={this.state}
          keyExtractor={(item, index) => index.toString()}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safe_area_view: {
    flex: 1,
  },
  notification_view: {
    marginHorizontal: 15,
    marginVertical: 5,
  },
  friend_req_view: {
    borderRadius: 10,
    marginVertical: 6,
    paddingHorizontal: 10,
    // paddingVertical: 10,
    borderWidth: 1,
    marginHorizontal: 15,
    flexDirection: 'row',
    borderColor: Colors.white,
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.15,
    shadowRadius: 1.94,
    elevation: 3,
  },
  img_name_view: {
    // flexDirection: 'row',
    // alignItems: 'center',
    flex: 4,
  },
  user_img_view: {
    flex: 3,
    justifyContent: 'space-evenly',
    // height: 30,
    // width: 30,
    // borderRadius: 30 / 2,
  },
  user_img: {
    width: '100%',
    height: '100%',
  },
  name_view: {
    // width: 120,
    // marginLeft: 10,
  },
  name_text: {
    // lineHeight: 17,
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaRegularios,
    color: Colors.black,
  },
  name_text2: {
    // lineHeight: 17,
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaRegularios,
    color: Colors.pinkishGreyTwo,
  },
  btn_view: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 0.7,
    // backgroundColor: 'red',
    justifyContent: 'flex-end',
  },
  accept_btn_touch: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    paddingHorizontal: 13,
    paddingVertical: 6,
    borderColor: Colors.violetBlue,
    borderWidth: 1,
  },
  accept_text: {
    fontSize: fonts.fontSize10,
    color: Colors.violetBlue,
    fontFamily: fonts.NovaSboldios,
  },
  reject_btn_touch: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 59,
    height: 22,
    borderColor: Colors.violetBlue,
    borderRadius: 5,
    borderWidth: 1,
  },
  reject_text: {
    //   fontsize: fonts.fontsize10,
    // lineHeight: 12,
    color: Colors.violetBlue,
    //   fontFamily: fonts.NovaBold,
  },
});
