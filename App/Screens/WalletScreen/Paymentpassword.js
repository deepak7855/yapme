import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Alert,
  Text,
  Dimensions,
  TextInput,
  Image,
  ActivityIndicator,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {GButton} from '../../Common/GButton';
import {images} from '../../Assets/ImageUrl';
import Header from '../../Common/Header';
import {Icons, Searchbar} from '../../Component/Common';
import ArrData from '../../../dummydata';

// import OTPTimer from './OTPTimer';
// import Api from '../lib/Api';
// import CountDown from 'react-native-countdown-component';

const ScreenWidth = Dimensions.get('window').width;
class InputBox extends React.PureComponent {
  render() {
    const {
      returnKeyType,
      blurOnSubmit,
      getFocus,
      setFocus,
      value,
      onChangeText,
    } = this.props;
    return (
      <View style={styles.inputView}>
        <TextInput
          style={styles.inputCss}
          textAlign={'center'}
          underlineColorAndroid={Colors.Transparent}
          placeholderTextColor={Colors.black}
          maxLength={1}
          keyboardType={'number-pad'}
          returnKeyType={returnKeyType}
          blurOnSubmit={blurOnSubmit}
          ref={getFocus}
          onSubmitEditing={setFocus}
          value={value}
          onChangeText={onChangeText}
        />
      </View>
    );
  }
}

export default class Paymentpassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otpCount: 59,
      Password: '',
      login: false,
      Otp: [], //this.props.otpvalue || '1234',
      onOtpCode: this.props.onOtpCode,
      predata: this.props.predata,
      Alert_Visibility: false,
      resendEnable: true,
      count: 0,
      showSpinner: true,
    };
  }

  Show_Custom_Alert(visible) {
    this.setState({Alert_Visibility: visible});
  }

  ok_Button = () => {
    Alert.alert('OK Button Clicked.');
  };

  _userAllProfile = () => {
    Actions.Staydrawer();
  };

  gotoNextFied = (value, field) => {
    if (value) {
      if (field === 'First') {
        this.state.Otp[0] = value;
        this.secondField.focus();
      } else if (field === 'Second') {
        this.state.Otp[1] = value;
        this.thirdField.focus();
      } else if (field === 'Third') {
        this.state.Otp[2] = value;
        this.fourthField.focus();
      } else if (field === 'Fourth') {
        this.state.Otp[3] = value;
        this.fourthField.focus();
      }
    } else {
      if (field === 'First') {
        this.state.Otp[0] = '';
      } else if (field === 'Second') {
        this.state.Otp[1] = '';
        this.firstField.focus();
      } else if (field === 'Third') {
        this.state.Otp[2] = '';
        this.secondField.focus();
      } else if (field === 'Fourth') {
        this.state.Otp[3] = '';
        this.thirdField.focus();
      }
    }
    this.setState({});
  };

  setProfile() {
    let data = {...this.state.predata};
    if (
      !this.state.Otp[0] ||
      !this.state.Otp[1] ||
      !this.state.Otp[2] ||
      !this.state.Otp[3]
    ) {
      Helper.showToast('Please enter valid otp');
      return;
    }
    data.otp =
      this.state.Otp[0] +
      this.state.Otp[1] +
      this.state.Otp[2] +
      this.state.Otp[3];

    this.context.toggleLoader(true);
    // Api.User.otpverify(data).then((res) => {
    //   console.log(res, "resresresres")
    //   this.context.toggleLoader(false)
    //   if (res.api_status == 200) {
    //     Helper.headerToken = res.token;
    //     Helper.userInfo = res;
    //     Helper.setData('asyncUserData', res);

    //     if (this.props.fromWhere == 'signup') {
    //     //   handleNavigation({
    //     //     type: 'setRoot',
    //     //     page: 'Profile',
    //     //     componentId: this.props.componentId,
    //     //   });
    //     } else if (res.first_name) {
    //     //   gotoAppStack();
    //     } else {
    //     //   handleNavigation({
    //     //     type: 'setRoot',
    //     //     page: 'Profile',
    //     //     componentId: this.props.componentId,
    //     //   });
    //     }
    //   } else Helper.alert(res.detail || Helper.defaultMessage);
    // });
  }

  back() {
    // handleNavigation({ type: 'pop', componentId: this.props.componentId });
  }
  _resendOtp() {
    this.setState({
      resendEnable: true,
      otpCount: 59,
      showSpinner: false,
      count: this.state.count + 1,
    });
    const data = {...this.state.predata};
    if (data.signup_via) {
      //   Api.User.otp(data).then((res) => {
      //     this.setState({ showSpinner: true })
      //     Helper.showToast(res.detail)
      //   });
      // } else {
      //   Api.User.login(data).then((res) => {
      //     this.setState({ showSpinner: true })
      //     Helper.showToast(res.detail)
      //   });
    }
  }

  render() {
    const {onOtpCode, showSpinner, predata} = this.state;
    const item = this.state;
    console.log(this.state, this.props, 'otp');

    return (
      <ScrollView bounces={false} style={{flex: 1, backgroundColor: Colors.whiteTwo,marginBottom:20}}>
        <View contentContainerStyle={{}}>
          <View style={{height: 50}} />
          <Text
            style={{
              fontFamily: fonts.NovaBoldios,
              fontSize: fonts.fontSize15,
              textAlign: 'center',
            }}>
            Enter Payment Password
          </Text>
          <View style={[styles.inputtext, {alignItems: 'flex-end'}]}>
            <View style={[{flexDirection: 'row'}]}>
              <InputBox
                returnKeyType={'next'}
                blurOnSubmit={false}
                getFocus={(input) => {
                  this.firstField = input;
                }}
                setFocus={() => {
                  this.secondField.focus();
                }}
                onChangeText={(value) => {
                  this.gotoNextFied(value, 'First');
                }}
                value={this.state.Otp[0]}
              />
              <InputBox
                returnKeyType={'next'}
                blurOnSubmit={false}
                getFocus={(input) => {
                  this.secondField = input;
                }}
                setFocus={() => {
                  this.thirdField.focus();
                }}
                onChangeText={(value) => {
                  this.gotoNextFied(value, 'Second');
                }}
                value={this.state.Otp[1]}
              />
              <InputBox
                returnKeyType={'next'}
                blurOnSubmit={false}
                getFocus={(input) => {
                  this.thirdField = input;
                }}
                setFocus={() => {
                  this.fourthField.focus();
                }}
                onChangeText={(value) => {
                  this.gotoNextFied(value, 'Third');
                }}
                value={this.state.Otp[2]}
              />

              <InputBox
                returnKeyType={'next'}
                blurOnSubmit={false}
                getFocus={(input) => {
                  this.thirdField = input;
                }}
                setFocus={() => {
                  this.fourthField.focus();
                }}
                onChangeText={(value) => {
                  this.gotoNextFied(value, 'Fourth');
                }}
                value={this.state.Otp[3]}
              />

              <InputBox
                returnKeyType={'next'}
                blurOnSubmit={false}
                getFocus={(input) => {
                  this.thirdField = input;
                }}
                setFocus={() => {
                  this.fourthField.focus();
                }}
                onChangeText={(value) => {
                  this.gotoNextFied(value, 'Fifth');
                }}
                value={this.state.Otp[4]}
              />

              <InputBox
                returnKeyType={'done'}
                getFocus={(input) => {
                  this.fourthField = input;
                }}
                onChangeText={(value) => {
                  this.gotoNextFied(value, 'Sixth');
                }}
                value={this.state.Otp[5]}
              />
            </View>
          </View>
        </View>

        <View style={{marginTop: 30}}>
          <Text
            style={{
              fontFamily: fonts.NovaBoldios,
              fontSize: fonts.fontSize15,
              textAlign: 'center',
            }}>
            Confirm Payment Password
          </Text>
          <View style={[styles.inputtext, {alignItems: 'flex-end'}]}>
            <View style={[{flexDirection: 'row'}]}>
              <InputBox
                returnKeyType={'next'}
                blurOnSubmit={false}
                getFocus={(input) => {
                  this.firstField = input;
                }}
                setFocus={() => {
                  this.secondField.focus();
                }}
                onChangeText={(value) => {
                  this.gotoNextFied(value, 'First');
                }}
                value={this.state.Otp[0]}
              />
              <InputBox
                returnKeyType={'next'}
                blurOnSubmit={false}
                getFocus={(input) => {
                  this.secondField = input;
                }}
                setFocus={() => {
                  this.thirdField.focus();
                }}
                onChangeText={(value) => {
                  this.gotoNextFied(value, 'Second');
                }}
                value={this.state.Otp[1]}
              />
              <InputBox
                returnKeyType={'next'}
                blurOnSubmit={false}
                getFocus={(input) => {
                  this.thirdField = input;
                }}
                setFocus={() => {
                  this.fourthField.focus();
                }}
                onChangeText={(value) => {
                  this.gotoNextFied(value, 'Third');
                }}
                value={this.state.Otp[2]}
              />

              <InputBox
                returnKeyType={'next'}
                blurOnSubmit={false}
                getFocus={(input) => {
                  this.thirdField = input;
                }}
                setFocus={() => {
                  this.fourthField.focus();
                }}
                onChangeText={(value) => {
                  this.gotoNextFied(value, 'Fourth');
                }}
                value={this.state.Otp[3]}
              />

              <InputBox
                returnKeyType={'next'}
                blurOnSubmit={false}
                getFocus={(input) => {
                  this.thirdField = input;
                }}
                setFocus={() => {
                  this.fourthField.focus();
                }}
                onChangeText={(value) => {
                  this.gotoNextFied(value, 'Fifth');
                }}
                value={this.state.Otp[4]}
              />

              <InputBox
                returnKeyType={'done'}
                getFocus={(input) => {
                  this.fourthField = input;
                }}
                onChangeText={(value) => {
                  this.gotoNextFied(value, 'Sixth');
                }}
                value={this.state.Otp[5]}
              />
            </View>
          </View>
          <Text
            style={{
              fontFamily: fonts.NovaRegularios,
              fontSize: fonts.fontSize13,
              textAlign: 'center',
              color: Colors.cobaltBlue,
            }}>
            Recover Password
          </Text>
        </View>

        <GButton
          height={50}
          width={200}
          radius={5}
          //   lineHeight={15}
          txtcolor={Colors.white}
          fontFamily={fonts.NovaXbold}
          fontSize={fonts.fontSize13}
          _style={{alignSelf: 'center', marginTop: 70}}
          bText={'Ok'}
          onPress={() => this.props.navigation.navigate('AllStarCount')}
        />
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  inputtext: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 25,
    marginHorizontal: 25,
  },
  inputView: {
    height: 80, //ScreenWidth / 5.4,
    width: 55, //ScreenWidth / 7.4,
    borderRadius: 4,
    borderColor: Colors.borderinput,
    // borderWidth: 0.3,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 8,
    paddingBottom: 8,
    backgroundColor: Colors.grinputcolor,
  },
  inputCss: {
    padding: 0,
    fontSize: 16,
    marginTop: 10,
    // fontFamily: fonts.RubikBold,
    // height: ScreenWidth / 7.5-8,
    // width: ScreenWidth / 8-4,
    width: '100%',
    height: '100%',
    color: Colors.black,
    // backgroundColor:'red'
  },
});
