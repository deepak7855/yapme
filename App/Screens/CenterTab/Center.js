import React, {Component} from 'react';
import {
  View,
  Text,
  StatusBar,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Image,
  ImageBackground,
} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import CHeader from '../../Common/CHeader';
import {Icons} from '../../Component/Common';
import {Custompopup} from '../../Component/Modal/CustomModel';

const Header = (props) => {
  // console.log(props, 'propsss');
  return (
    <SafeAreaView style={styles.headerContainer}>
      <View style={styles.firstView}>
        <TouchableOpacity
          onPress={() => props.navigation.goBack(null)}
          style={{height: 20, width: 20}}>
          <Image
            source={images.back_arrow}
            resizeMode={'contain'}
            style={{height: '100%', width: '100%', tintColor: '#fff'}}
          />
        </TouchableOpacity>
      </View>

      <View style={styles.centerView}></View>
      <View style={styles.thiredView}></View>
    </SafeAreaView>
  );
};

export default class Center extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.6)'}}>
        <StatusBar
          backgroundColor={Colors.violetBlue}
          barStyle="dark-content"
        />
        <SafeAreaView />

        <View style={{flex: 1, justifyContent: 'space-between'}}>
          <Header navigation={this.props.navigation} backArrowLeft />

          <View>
            <View
              style={{
                flexDirection: 'row',
                //   backgroundColor: 'red',
                justifyContent: 'center',
                // paddingHorizontal: 10,
                // paddingVertical: 10,
                alignItems: 'center',
              }}>
              <View
                style={{
                  alignItems: 'center',
                  // marginTop: 35, marginRight: 8
                }}>
                <Icons
                  size={30}
                  source={images.liveicon}
                  _style={{}}
                  onPress={() => this.setState({modalVisible: true})}
                />
                <Text
                  style={{
                    marginTop: 5,
                    color: Colors.white,
                    fontSize: fonts.fontSize10,
                  }}>
                  Live
                </Text>
              </View>

              <View style={{width: 60}} />

              <View
                style={{
                  alignItems: 'center',
                  // marginHorizontal: 10,
                  // marginBottom: 20,
                }}>
                <Icons
                  size={30}
                  source={images.story_icon}
                  // _style={{marginHorizontal: 20}}
                  onPress={() => this.props.navigation.navigate('Story')}
                />
                <Text
                  style={{
                    marginTop: 5,
                    color: Colors.white,
                    fontSize: fonts.fontSize10,
                  }}>
                  Story
                </Text>
              </View>

              {/* <View style={{alignItems: 'center', marginTop: 35}}>
                <Icons
                  size={30}
                  source={images.add_user_ic}
                  _style={{}}
                //   onPress={() => this.props.navigation.navigate('Story')}
                />
                <Text
                  style={{
                    marginTop: 5,
                    color: Colors.white,
                    fontSize: fonts.fontSize10,
                  }}>
                  Add user
                </Text>
              </View> */}
            </View>

            <View
              style={{
                flexDirection: 'row',
                //   backgroundColor: 'red',
                justifyContent: 'center',
                paddingHorizontal: 10,
                paddingVertical: 10,
                alignItems: 'center',
              }}>
              <View style={{alignItems: 'center', marginLeft: 10}}>
                <Icons
                  size={30}
                  source={images.play_round}
                  _style={{marginLeft: 10}}
                  onPress={() => this.props.navigation.navigate('Video')}
                />
                <Text
                  style={{
                    marginTop: 5,
                    color: Colors.white,
                    fontSize: fonts.fontSize10,
                  }}>
                  Videos
                </Text>
              </View>

              <Icons
                size={55}
                source={images.plus_icon}
                _style={{marginHorizontal: 20, marginLeft: 40}}
                //   onPress={() => this.setState({modalVisible: true})}
              />

              <View style={{alignItems: 'center'}}>
                <Icons
                  size={30}
                  source={images.personal_wall}
                  _style={{}}
                  onPress={() => this.props.navigation.navigate('GroupTabs')}
                />
                <Text
                  style={{
                    marginTop: 5,
                    color: Colors.white,
                    fontSize: fonts.fontSize10,
                  }}>
                  Personal Wall
                </Text>
              </View>
            </View>
          </View>
        </View>
        <SafeAreaView />

        <Custompopup modalVisible={this.state.modalVisible}>
          {/* <View style={{flex: 1, backgroundColor: Colors.white}}> */}
          <ImageBackground
            source={images.live_bg}
            style={{flex: 1, justifyContent: 'space-between'}}
            resizeMode={'stretch'}>
            <SafeAreaView />

            <View
              style={{
                flex: 1,
                // backgroundColor: 'red',
                justifyContent: 'space-between',
              }}>
              <View>
                <Icons
                  size={20}
                  source={images.cross}
                  _style={{
                    alignSelf: 'flex-end',
                    marginRight: 20,
                    marginTop: 10,
                  }}
                  onPress={() => this.setState({modalVisible: false})}
                />

                <View
                  style={{
                    marginHorizontal: 15,
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <View style={{}}>
                    <Icons
                      size={100}
                      source={images.cover}
                      _style={{
                        alignSelf: 'flex-end',
                        marginRight: 8,
                        // marginTop: 10,
                      }}
                    />
                  </View>
                  <View>
                    <Text
                      style={{
                        fontFamily: fonts.NovaBoldios,
                        color: Colors.white,
                        fontSize:fonts.fontSize15
                      }}>
                      Add a title to chat
                    </Text>
                    <View
                      style={{
                        flexDirection: 'row',
                        paddingHorizontal: 12,
                        paddingVertical: 8,
                        width:110,
                        backgroundColor: 'rgba(0,0,0,0.5)',
                        borderRadius: 15,
                        alignItems: 'center',
                        marginTop: 10,
                      }}>
                      <Text
                        style={{
                          fontFamily: fonts.NovaBoldios,
                          color: Colors.white,
                        }}>
                        Select tag
                      </Text>
                      <Icons
                        size={10}
                        source={images.angleright}
                        _style={{marginLeft: 10}}
                        // onPress={() => this.setState({modalVisible: false})}
                      />
                    </View>
                    <View style={{flexDirection: 'row',alignItems:'center'}}>
                      <Icons
                        size={45}
                        source={
                          !this.state.private
                            ? images.lock_active
                            : images.globe_activebtn
                        }
                        _style={{marginLeft: 10}}
                        onPress={() =>
                          this.setState({private: !this.state.private})
                        }
                      />
                    <Text style={{marginLeft:10,fontSize:fonts.fontSize13,color:Colors.white,fontFamily:fonts.NovaBoldios}}>Private Live</Text>

                    </View>

                  </View>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  //   backgroundColor: 'red',
                  //   justifyContent: 'space-between',
                  paddingHorizontal: 10,
                  paddingVertical: 10,
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({modalVisible: false});
                    this.props.navigation.navigate('LiveStreaming');
                  }}
                  style={{
                    flex: 1,
                    backgroundColor: Colors.white,
                    paddingVertical: 15,
                    borderRadius: 30,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: fonts.NovaRegularios,
                      fontSize: fonts.fontSize13,
                      color: Colors.vividPurple,
                    }}>
                    Go Live
                  </Text>
                </TouchableOpacity>
                <Icons
                  size={30}
                  source={images.stricker_icon}
                  _style={{marginHorizontal: 15}}
                  //   onPress={() => this.setState({modalVisible: true})}
                />
              </View>
            </View>

            <SafeAreaView />
          </ImageBackground>
        </Custompopup>
      </View>
    );
  }
}

{
  /* <Icons
size={30}
source={images.camera_flip}
_style={{alignSelf: 'flex-end', marginRight: 20, marginTop: 10}}
onPress={() => this.setState({modalVisible: false})}
/>

<View style={{flex: 1, justifyContent: 'space-between'}}>
<View
  style={{
    flexDirection: 'row',
    //   backgroundColor: 'red',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 10,
    alignItems: 'center',
  }}>
  <Icons
    size={30}
    source={images.radio_white}
    //   _style={{marginRight: 15}}
    //   onPress={() => this.setState({modalVisible: true})}
  />

  <Icons
    size={40}
    source={images.play_lg}
    //   _style={{marginRight: 15}}
    //   onPress={() => this.setState({modalVisible: true})}
  />

  <Icons
    size={30}
    source={images.camera_flip}
    //   _style={{marginRight: 15}}
    //   onPress={() => this.setState({modalVisible: true})}
  />
</View>
</View> */
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  firstView: {
    flex: 1,
    // backgroundColor: 'red',
    flexDirection: 'row',
    paddingTop: 10,
    marginLeft: 18,
  },
  centerView: {
    flex: 2.0,
    flexDirection: 'row',
    paddingTop: 10,
    // marginLeft: 18,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor:'red'
  },
  thiredView: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 10,
    paddingRight: 10,
    // marginLeft: 18,
    // alignItems:'flex-end',
    // backgroundColor:'red',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    // justifyContent:'center',
    // alignItems:'center',
  },
  headerContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    // backgroundColor:'red'
  },
  pophead: {fontSize: fonts.fontSize13, fontFamily: fonts.NovaBoldios},

  accept_btn_touch: {
    // marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
    // width: 59,
    // height: 22,
    backgroundColor: Colors.violetBlue,
    borderRadius: 5,
    paddingHorizontal: 5,
    paddingVertical: 6,
  },
  accept_text: {
    fontSize: fonts.fontSize10,
    color: Colors.white,
    fontFamily: fonts.NovaBold,
  },
});
