import React, {Component} from 'react';
import {
  Text,
  View,
  TextInput,
  Image,
  FlatList,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Colors from '../Assets/Colors';
import fonts from '../Assets/Fonts';
import {images} from '../Assets/ImageUrl';

export const Searchbar = (porps) => (
  <View
    style={{
      ...porps._style,
      backgroundColor: '#ddd',
      marginHorizontal: 15,
      flexDirection: 'row',
      borderRadius: 20,
      paddingHorizontal: 15,
      height:40,
      justifyContent:'center',
      alignItems:'center'
      // paddingVertical: 10,
    
    }}>
    <TextInput
      style={{flex: 1, paddingLeft: 10}}
      underlineColorAndroid="transparent"
      placeholder={porps.placeholder}
      autoCorrect={false}
      placeholderTextColor={Colors.pinkishGrey}
      secureTextEntry={porps.secureTextEntry}
      value={porps.value}
      onChangeText={porps.onChangeText}
      keyboardType={porps.keyboardType}
      maxLength={porps.maxLength}
      returnKeyType={porps.returnKeyType}
      blurOnSubmit={porps.blurOnSubmit}
      onSubmitEditing={porps.setFocus}
      ref={porps.getFocus}
      editable={porps.inputedit}
    />
    <Image
      resizeMode="contain"
      source={images.search_blue}
      style={{height: 15, width: 15}}
    />
  </View>
);

export const PinkScreen = (props) => (
  <View style={styles.redView}>
    {props.title && <Text style={{fontFamily:fonts.NovaBoldios}}>{props.title}</Text>}
    <FlatList
      horizontal
      contentContainerStyle={
        {
          //   paddingBottom: 80,
          // paddingTop:10,
        }
      }
      data={['', '', '']}
      renderItem={({item: color}) => (
        <View style={{flexDirection: 'row'}}>
          <View style={{alignItems: 'center', marginRight: 15, marginTop: 10}}>
            <Image
              resizeMode="contain"
              style={{height: 40, width: 40}}
              source={images.Profile}
            />

            <Image
              resizeMode="contain"
              style={{height: 20, width: 20, bottom: 10, left: 10}}
              source={images.close_pinkk}
            />
            <Text style={{fontSize: 10}}>Miller</Text>
          </View>
        </View>
      )}
    />
  </View>
);

export const Icons = (props) => (
  <TouchableOpacity onPress={props.onPress}>
    <Image
      resizeMode="contain"
      source={props.source}
      style={[props._style, {height: props.size, width: props.size}]}
    />
  </TouchableOpacity>
);

export const RowoneBythree = (props) => (
  // <View style={{flex: 1, flexDirection: 'row', backgroundColor: props.bgColor}}>
  //   <View
  //     style={{
  //       flex: props.children1flexSize,
  //       flexDirection: props.direction,
  //       alignItems: 'center',
  //     }}>
  //     {props.children1 && <>{props.children}</>}
  //   </View>

  //   <View
  //     style={{
  //       flex: props.children2flexSize,
  //       flexDirection: props.direction,
  //       alignItems: 'center',
  //     }}>
  //     {props.children2 && <>{props.children}</>}
  //   </View>

  //   <View
  //     style={{flex: props.children3flexSize, flexDirection: props.direction}}>
  //     {props.children3 && <>{props.children}</>}
  //   </View>
  // </View>
  <View
    style={{
      flexDirection: 'row',
      backgroundColor: 'pink',
      marginHorizontal: 15,
    }}>
    <View style={{flex: 1, alignItems: 'center'}}>
      {props.children1 && <>{props.children}</>}
    </View>
    <View style={{flex: 2, backgroundColor: 'purple', alignItems: 'center'}}>
      {props.children2 && <>{props.children}</>}
    </View>
    <View style={{flex: 1, alignItems: 'center'}}>
      {props.children3 && <>{props.children}</>}
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bgcolor,
  },
  item: {
    margin: 10,
    height: 30,
    borderRadius: 10,
  },

  redView: {
    // flexDirection: 'row',
    //   marginHorizontal: 25,
    padding: 10,
    marginVertical: 10,
    // alignItems:'center',
    paddingHorizontal: 15,
    backgroundColor: Colors.pinkred,
  },

  grytextStyle: {
    color: Colors.greyColor,
    fontSize: 13,
    // fontFamily:fon
  },
});
