import { StyleSheet } from 'react-native';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';


export default LoginStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,

    },
    image_back_ground: {
        flex: 1,
    },
    image_parent_view: {
        alignItems: 'center',
        marginTop: 100,
        marginVertical: 20
    },
    image_view: {
        alignItems: 'center',
        width: 70,
        height: 70,
        borderRadius: 70 / 2
    },
    profile_img: {
        height: '100%',
        width: '100%'
    },
    welcome_view: {
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 5,
        marginTop: 10
    },
    welcome_text: {
        color: Colors.vividPurple,
        fontSize: Fonts.fontSize24,
        fontFamily: Fonts.NovaBold,
        lineHeight: 29
    },
    sign_view: {
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 5,
    },
    sign_in_to_text: {
        fontSize: Fonts.fontSize16,
        lineHeight: 20,
        color: Colors.pinkishGrey
    },

    email_phone_view:{
        flexDirection: 'row', 
        justifyContent: 'center', 
        marginVertical: 35,
    },

    phone_email_touch:{
        marginHorizontal: 30
    },
    email_text: {
        fontSize: Fonts.fontSize16,
        lineHeight: 20,
        color: Colors.black,
        fontFamily: Fonts.NovaRegular
    },
    selected_email_text: {
        fontSize: Fonts.fontSize16,
        lineHeight: 20,
        color: Colors.pinkishGrey,
        fontFamily: Fonts.NovaRegular
    },

    phone_text: {
        fontSize: Fonts.fontSize16,
        lineHeight: 20,
        color: Colors.black,
        fontFamily: Fonts.NovaRegular,

    },
    selected_phone_text: {
        fontSize: Fonts.fontSize16,
        lineHeight: 20,
        color: Colors.pinkishGrey,
        fontFamily: Fonts.NovaRegular
    },


    email_separator: {
        height: 2,
        backgroundColor: Colors.white,
        marginTop: 5
    },
    selected_email_separator: {
        height: 2,
        backgroundColor: Colors.hotMagenta,
        marginTop: 5
    },
    phone_separator: {
        height: 2,
        backgroundColor: Colors.white,
        marginTop: 5
    },

    selected_phone_separator: {
        height: 2,
        backgroundColor: Colors.hotMagenta,
        marginTop: 5
    },
    input_text_view:{
        marginHorizontal: 45, 
        // marginTop: 10 
    },

    forgot_pass_view:{
        justifyContent: 'center', 
        alignItems: 'center', 
        marginVertical: 15
    },
    forgot_pass_text:{
        fontSize: Fonts.fontSize13, 
        lineHeight: 15, 
        color: Colors.cobaltBlue, 
        fontFamily: Fonts.NovaMedium ,
        marginVertical:15
    },
    btn_view:{
        alignItems: 'center', 
        marginVertical: 10, 
    },
    social_account_view:{
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop: 30
    },
    social_account_text:{
        fontSize: Fonts.fontSize15, 
        lineHeight: 17, 
        fontFamily: Fonts.NovaRegular, 
        color: Colors.black ,
        marginVertical:15
        
    },
    have_account_view:{
        justifyContent: 'center', 
        alignItems: 'center', 
        // marginVertical: 20, 
        marginTop: 30,
        marginBottom:15
    
    },
    have_account_text:{
        fontFamily: Fonts.NovaRegular, 
        fontSize: Fonts.fontSize13, 
        color: Colors.black, 
        lineHeight: 17
    },
    join_yapme_text:{
        fontFamily: Fonts.NovaBold, 
        fontSize: Fonts.fontSize15, 
        lineHeight: 17, 
        color: Colors.cobaltBlue, 
        textDecorationLine: 'underline'
    },

    phone_parent_view:{
        marginTop: 30
    },
    phone_chaild_view:{
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between', 
        marginHorizontal: 45,
        // backgroundColor: 'red',
    },
    code_arrow_view:{
        marginTop: 18, 
        marginRight: 5,
        // backgroundColor: 'red',
    },
    code_arrow_chaild_view:{
        width: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginRight: 20,
    },
    code_text:{
        fontSize: Fonts.fontSize13, 
        lineHeight: 15, 
        color: Colors.black, 
        fontFamily: Fonts.NovaRegular
    },
    arrow_img:{
        height: 5.9, 
        width: 10 ,
        marginLeft:40,
    },
    code_separator_view:{
        height: .8, 
        backgroundColor: Colors.charcoalGrey, 
        marginTop: 15, 
        width: 70
    },
    input_width_view:{
        width: '70%' 
    },


});