import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {
  TextInput,
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Diamonds from './Diamonds';
import Starts from './Starts';
import History from './History';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {SafeAreaView} from 'react-navigation';
import CHeader from '../../Common/CHeader';
import {createStackNavigator} from '@react-navigation/stack';
import Paymentpassword from './Paymentpassword';
import AllStarCount from './AllStarCount';

const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();

const AllWalletTabs = (props) => {
  // <NavigationContainer>

  return (
    <View style={{flex: 1}}>
      <CHeader navigation={props.navigation} backArrowLeft centerItem />
      <Tab.Navigator
      // initialRouteName=""
        tabBarOptions={{
          // activeTintColor: Colors.violetBlue,
          // tabBarLabel: {
          //   color: Colors.violetBlue,
          // },

          // indicatorStyle: {
          //   borderBottomColor: Colors.violetBlue,
          //   borderBottomWidth: 1.5,
          //   borderColor: Colors.violetBlue,
          // },
          // //   activeTintColor: 'red',
          // inactiveTintColor: 'lightgray',

          indicatorStyle:{
            backgroundColor:Colors.vividPurple
          },
          activeTintColor: Colors.violetBlue,
          inactiveTintColor: 'lightgray',

          //   showLabel: false,
          labelStyle: {fontSize: 10},
          // tabStyle: { width: 100 },
          style: { backgroundColor: Colors.whiteTwo },
          labelStyle: {
            textTransform: 'capitalize',
            fontSize: fonts.fontSize16,
            fontFamily: fonts.NovaSboldios,
          },
        }}>
        <Tab.Screen
          name="Diamonds"
          component={Diamonds}
          navigationOptions={
            {tabBarLabel: 'Navigation Title'}
            // tabBarOptions: {
            //     activeTintColor: '#000',
            //     inactiveTintColor: '#fff',
            // }
          }
        />
        <Tab.Screen
          name="Stars"
          component={StartStack}
          tabBarOptions={{tabBarLabel: 'Updates'}}
        />

        <Tab.Screen
          name="History"
          component={History}
          tabBarOptions={{tabBarLabel: 'Updates'}}
        />
      </Tab.Navigator>
    </View>
  );
};

function StartStack(props) {
  // console.log(props,"propsstate")
  return (
    <Stack.Navigator
      headerMode={false}
      // initialRouteName="SplashScreen"
    >
      {/* <Stack.Screen name="AllWalletTabs" component={AllWalletTabs} /> */}

      <Stack.Screen
        name="Starts"
        component={Starts}
        options={{
          tabBarVisible: false,
        }}
      />
      <Stack.Screen
        name="Paymentpassword"
        component={Paymentpassword}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="AllStarCount"
        component={AllStarCount}
        options={{
          tabBarVisible: false,
        }}
      />
    </Stack.Navigator>
  );
}

export default function WalletTab(props) {
  // console.log(props,"propsstate")
  return (
    <Stack.Navigator
      headerMode={false}
      // initialRouteName="SplashScreen"
    >
      <Stack.Screen name="AllWalletTabs" component={AllWalletTabs} />
      <Stack.Screen
        name="Paymentpassword"
        component={Paymentpassword}
        options={{
          tabBarVisible: false,
        }}
      />
    </Stack.Navigator>
  );
}
