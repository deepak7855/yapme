
import * as React from 'react';
import { Alert } from 'react-native';
import { AppConstant } from './AppConstant';
import { showMessage, hideMessage } from "react-native-flash-message";

export default class AppHelper extends React.Component { 

    static confirm(alertMessage, cb) {
        Alert.alert(
            AppConstant.app_name,
            alertMessage,
            [
                { text: 'YES', onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
                { text: 'NO', onPress: () => { if (cb) cb(false); }, style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    static showToast(message,title=AppConstant.app_name, type = 'success', position = 'top') {
        showMessage({
            message: title,
            description: message,
            type: type,
            position:position,
            floating:true
        });
    }
}

