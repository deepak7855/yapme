import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  Platform
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';

import {images} from '../../Assets/ImageUrl';
import styles from './LoginStyles';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';
import {GButton} from '../../Common/GButton';
import {Input} from '../../Common/InputCommon';
import SplashScreen from 'react-native-splash-screen'
import CountryCodePicker from '../../Component/Modal/CountryCodePickerModal';

let os = Platform.OS

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEmailSignUp: false,
      isPhoneSignIn: true,
      isSelected: false,
      isFieldActive: false,
      modalVisibleCode: false,
      countryCode: '',
    };
  }

  componentDidMount() {
    SplashScreen.hide();
    
  }
  

  onPressSignin(){
    this.props.navigation.reset({
        index: 0,
        routes: [{name: 'TabBar'}],
      });
  }

  optionForEmailSignUp = () => {
    this.setState({isEmailSignUp: false, isPhoneSignIn: true});
  };

  optionForPhoneSignIn = () => {
    this.setState({isPhoneSignIn: false, isEmailSignUp: true});
  };

  callCounteryCodeApi = (dial_code) => {
    this.setState({countryCode: dial_code, modalVisibleCode: false});
  };
  render() {
    const {isSelected, isEmailSignUp, isPhoneSignIn} = this.state;
    let signUpEmailStyle = isEmailSignUp
      ? styles.phone_text
      : styles.selected_phone_text;
    let signInPhoneStyle = isPhoneSignIn
      ? styles.email_text
      : styles.selected_email_text;
    let emailSeparatorStyle = isEmailSignUp
      ? styles.email_separator
      : styles.selected_email_separator;
    let phoneSeparatorStyle = isPhoneSignIn
      ? styles.phone_separator
      : styles.selected_phone_separator;
    // console.log("LLLLLLLLLLLLLLLLLLLLLLL",Fonts.fontSize24,"Fffff 20 ",Fonts.fontSize20);
    return (
      <View style={styles.safe_area_view}>
        <ImageBackground
          source={images.login_image_email}
          style={styles.image_back_ground}
          resizeMode={'stretch'}>
         <KeyboardAwareScrollView
          bounces={false}
          showsVerticalScrollIndicator={false}>
            <View style={styles.image_parent_view}>
              <View style={styles.image_view}>
                <Image
                  style={styles.profile_img}
                  resizeMode={'cover'}
                  source={images.Profile}
                />
              </View>
            </View>

            <View style={styles.welcome_view}>
              <Text style={styles.welcome_text}>WELCOME BACK!</Text>
            </View>

            <View style={styles.sign_view}>
              <Text style={styles.sign_in_to_text}>Sign In to YAPME</Text>
            </View>

            <View style={styles.email_phone_view}>
              <TouchableOpacity
                onPress={this.optionForEmailSignUp}
                style={styles.phone_email_touch}>
                <Text style={signInPhoneStyle}>Email</Text>
                <View style={emailSeparatorStyle}></View>
              </TouchableOpacity>

              {/* <TouchableOpacity
                onPress={this.optionForPhoneSignIn}
                style={styles.phone_email_touch}>
                <Text style={signUpEmailStyle}>Phone</Text>
                <View style={phoneSeparatorStyle}></View>
              </TouchableOpacity> */}
            </View>

            {isEmailSignUp ? (
              <View style={styles.phone_parent_view}>
                <View style={styles.phone_chaild_view}>
                  <View style={styles.code_arrow_view}>
                    <View
                      style={styles.code_arrow_chaild_view}
                      onTouchStart={() =>
                        this.setState({modalVisibleCode: true})
                      }>
                      <Text style={styles.code_text}>
                        {this.state.countryCode ? this.state.countryCode : '+1'}
                      </Text>
                      <View>
                        <Image
                          resizeMode={'contain'}
                          source={images.down_arrow}
                          style={styles.arrow_img}
                        />
                      </View>
                    </View>
                    <View style={styles.code_separator_view}></View>
                  </View>

                  <View style={styles.input_width_view}>
                    <Input
                      customeTilteStyle={{width: 90}}
                      labelText={'Phone Number'}
                      // rightImagePath={images.tick}
                      // placeholder={'9884275899'}
                      keyboardType={'phone-pad'}
                    />
                  </View>
                </View>
                <View style={{height: 18}} />

                <View style={styles.input_text_view}>
                  <Input
                    secureTextEntry={true}
                    labelText={'Password'}
                    rightImagePath={images.eye}
                    // placeholder={'themoonisrising'}
                    keyboardType={'default'}
                  />
                </View>
              </View>
            ) : (
              <View style={styles.input_text_view}>
                <Input
                  labelText={'Email Address'}
                  // placeholder={'ciafab811r@gmail.com'}
                  // rightImagePath={images.tick}
                  keyboardType={'email-address'}
                />
                <View style={{height: 18}} />
                <Input
                  onPress={() =>{}}
                  secureTextEntry={true}
                  labelText={'Password'}
                  rightImagePath={images.eye}
                  // placeholder={'themoonisrising'}
                  keyboardType={'default'}
                />
              </View>
            )}

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('ForgotPassword')}
              style={styles.forgot_pass_view}>
              <Text style={styles.forgot_pass_text}>Forgot Password?</Text>
            </TouchableOpacity>

            <View style={styles.btn_view}>
              <GButton
                height={50}
                width={200}
                radius={5}
                lineHeight={15}
                txtcolor={Colors.white}
                fontFamily={Fonts.NovaBold}
                fontSize={Fonts.fontSize13}
                onPress={() => this.onPressSignin()}
                bText={'SIGN IN'}
              />
            </View>

            <View style={styles.social_account_view}>
              <Text style={styles.social_account_text}>
                Connect With Social Account
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginVertical:15,
              marginBottom:25
              }}>

              {os=='ios'&&(<TouchableOpacity
                style={{width: 15, height: 15, marginHorizontal: 8}}>
                <Image
                  resizeMode={'contain'}
                  source={images.apple_icon}
                  style={{width: 18, height: 18}}
                />
              </TouchableOpacity>)}

              <TouchableOpacity
                style={{width: 15, height: 15, marginHorizontal: 8}}>
                <Image
                  resizeMode={'contain'}
                  source={images.google_hangouts}
                  style={{width: 18, height: 18}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                style={{width: 15, height: 15, marginHorizontal: 8}}>
                <Image
                  resizeMode={'contain'}
                  source={images.facebook}
                  style={{width: 18, height: 18}}
                />
              </TouchableOpacity>

              {/* <TouchableOpacity
                style={{width: 15, height: 15, marginHorizontal: 8}}>
                <Image
                  resizeMode={'contain'}
                  source={images.instagram_sketched}
                  style={{width: 18, height: 18}}
                />
              </TouchableOpacity> */}
            </View>

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Signup')}
              style={styles.have_account_view}>
              <Text style={styles.have_account_text}>
                Don't have an account?{' '}
                <Text style={styles.join_yapme_text}>JOIN YAPME</Text>
              </Text>
            </TouchableOpacity>
          <Text style={{textAlign:'right',marginRight:30}}>V 12.08.01</Text>

          </KeyboardAwareScrollView>

        </ImageBackground>

        <CountryCodePicker
          visible={this.state.modalVisibleCode}
          onRequestClose={() => {
            this.setState({modalVisibleCode: false});
          }}
          onPress={() => {
            this.setState({modalVisibleCode: false});
          }}
          callCounteryCodeApi={(dial_code) => {
            this.callCounteryCodeApi(dial_code);
          }}
        />
      </View>
    );
  }
}

export default Login;
