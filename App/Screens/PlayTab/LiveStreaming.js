import React, {Component} from 'react';
import {
  Text,
  View,
  ImageBackground,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  Image,
  StyleSheet,
  FlatList,
} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import {GButton} from '../../Common/GButton';
import {Icons} from '../../Component/Common';
import {Custompopup} from '../../Component/Modal/CustomModel';
import ArrData from '../../../dummydata';

  export const ComponentHeader = (props) => {
  console.log(props,"chefar")
  return (
    <View style={{flexDirection: 'row'}}>
      <View
        style={{
          flex: 0.5,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          onPress={() => props.navigation.goBack(null)}
          style={{height: 20, width: 20}}>
          <Image
            source={images.back_arrow}
            resizeMode={'contain'}
            style={{height: '100%', width: '100%'}}
          />
        </TouchableOpacity>
      </View>

      <View
        style={{
          flex: 4,
          //   backgroundColor: 'green',
          flexDirection: 'row',
          justifyContent: 'space-between',
          // marginLeft: 12,
        }}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Icons size={25} source={images.Profile} _style={{marginRight: 10}} />
          <Text style={{fontSize: 9, fontFamily: fonts.NovaRegularios}}>
            SRG Freedom
          </Text>
        </View>

        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Icons
            size={25}
            source={images.Profile}
            onPress={props.onPresUserModel}
            _style={{marginRight: 5}}
          />
          <Icons
            size={25}
            source={images.Profile}
            // onPress={() => this.props.navigation.navigate('Live')}
            _style={{marginRight: 5}}
          />
          <Icons
            size={25}
            source={images.Profile}
            // onPress={() => this.props.navigation.navigate('Live')}
            _style={{marginRight: 5}}
          />
          <Icons
            size={25}
            source={images.Profile}
            // onPress={() => this.props.navigation.navigate('Live')}
            _style={{marginRight: 5}}
          />
          <Icons
            size={25}
            source={images.Profile}
            // onPress={props.onPresUserModel}
            _style={{marginRight: 5}}
          />
          <TouchableOpacity
            onPress={props.onPresModel}
            style={{
              backgroundColor: Colors.lightgrey,
              padding: 10,
              borderRadius: 30,
              paddingHorizontal: 15,
            }}>
            <Text>312</Text>
          </TouchableOpacity>
        </View>
      </View>

      <View
        style={{
          flex: 0.5,
          //   backgroundColor: 'purple',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          onPress={() => props.navigation.goBack(null)}
          style={{height: 15, width: 15}}>
          <Image
            source={images.cross}
            resizeMode={'contain'}
            style={{height: '100%', width: '100%'}}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
   };

const arrFriendRequest = [
  {username: 'Braulio'},
  {username: 'Jayda'},
  {username: 'Crawford'},
  {username: 'Genevieve'},
  {username: 'Ignatius'},
  {username: 'Ophelia'},
  // {username: 'Ryleigh'},
  // {username: 'Juanita'},
  //   {username: 'Jalon'},
];

const CurrentViewer = () => (
  <>
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        paddingVertical: 20,
      }}>
      <View>
        <Text style={styles.pophead}>Current Viewer</Text>
      </View>
    </View>

    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: Colors.lightgrey,
      }}>
      <TouchableOpacity
        onPress={() => {
          // this.setState({
          //   index: 0,
          //   index_one_filled: false,
          //   index_two_filled: false,
          // }),
          //   this.viewpager.setPage(0);
        }}
        style={{
          width: '30%',
          // borderBottomWidth: 1.5,
          alignItems: 'center',
          borderBottomColor: Colors.greyColor,
          paddingVertical: 13,
        }}>
        <Text
          style={{
            fontSize: fonts.fontSize16,
            fontFamily: fonts.NovaBoldios,
            color: Colors.pinkishGrey,
            marginTop: 10,
          }}>
          Friends
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => {
          // this.setState({index: 1, index_one_filled: true}),
          //   this.viewpager.setPage(1);
        }}
        style={{
          width: '30%',
          // borderBottomWidth: this.state.index_one_filled ? null : 1,
          // borderBottomWidth: 1.5,
          alignItems: 'center',
          borderBottomColor: Colors.greyColor,
          paddingVertical: 13,
        }}>
        <Text
          style={{
            fontSize: fonts.fontSize16,
            fontFamily: fonts.NovaBoldios,
            color: Colors.pinkishGrey,
            marginTop: 10,
          }}>
          Fans
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        // disabled={this.state.index == 0}
        // onPress={() => {
        //   this.setState({index: 2, index_two_filled: true}),
        //     this.viewpager.setPage(2);
        // }}
        style={{
          width: '30%',
          borderBottomWidth: 1.5,
          alignItems: 'center',
          // borderBottomColor: Colors.greyColor,
          borderBottomColor: Colors.vividPurple,
          paddingVertical: 13,
        }}>
        <Text
          style={{
            fontSize: fonts.fontSize16,
            fontFamily: fonts.NovaXboldios,
            color: Colors.vividPurple,
            marginTop: 10,
          }}>
          Public
        </Text>
      </TouchableOpacity>
    </View>

    <View style={{flex:1}}>
      <FlatList
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{marginTop:10,paddingBottom:20,flexGrow:1}}
        data={arrFriendRequest}
        renderItem={_renderFriendRequestList}
        extraData={this.state}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  </>
);

const RecentFriends = () => (
  <>
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: Colors.lightgrey,
        paddingTop:12,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
      }}>
      <TouchableOpacity
        onPress={() => {
          // this.setState({
          //   index: 0,
          //   index_one_filled: false,
          //   index_two_filled: false,
          // }),
          //   this.viewpager.setPage(0);
        }}
        style={{
          width: '30%',
          borderBottomWidth: 1.5,
          alignItems: 'center',
          borderBottomColor: Colors.greyColor,
          paddingVertical: 13,
        }}>
        <Text
          style={{
            fontSize: fonts.fontSize16,
            fontFamily: fonts.NovaBoldios,
            color: Colors.brownishGrey,
            marginTop: 10,
          }}>
          Recent
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => {
          // this.setState({index: 1, index_one_filled: true}),
          //   this.viewpager.setPage(1);
        }}
        style={{
          width: '30%',
          // borderBottomWidth: this.state.index_one_filled ? null : 1,
          borderBottomWidth: 1.5,
          alignItems: 'center',
          borderBottomColor: Colors.greyColor,
          paddingVertical: 13,
        }}>
        <Text
          style={{
            fontSize: fonts.fontSize16,
            fontFamily: fonts.NovaBoldios,
            color: Colors.brownishGrey,
            marginTop: 10,
          }}>
          Friends
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        // disabled={this.state.index == 0}
        // onPress={() => {
        //   this.setState({index: 2, index_two_filled: true}),
        //     this.viewpager.setPage(2);
        // }}
        style={{
          width: '30%',
          borderBottomWidth: 1.5,
          alignItems: 'center',
          // borderBottomColor: Colors.greyColor,
          borderBottomColor: Colors.vividPurple,
          paddingVertical: 13,
        }}>
        <Text
          style={{
            fontSize: fonts.fontSize16,
            fontFamily: fonts.NovaXboldios,
            color: Colors.vividPurple,
            marginTop: 10,
          }}>
          Fans
        </Text>
      </TouchableOpacity>
    </View>

    <FlatList
      contentContainerStyle={{marginTop: 10}}
      data={['', '', '', '', '', '']}
      renderItem={({item: color, index}) => (
        <View style={[styles.item, styles.item2]}>
          <View style={styles.row1}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image
                resizeMode="contain"
                style={{height: 35, width: 35}}
                source={images.Profile}
              />
              <View style={{marginLeft: 20}}>
                <Text style={{marginBottom: 5}}>Odell smith</Text>
              </View>
            </View>
            <Image
              resizeMode="contain"
              style={{height: 17, width: 17}}
              source={index % 2 != 0 ? images.radio_circle : images.tick_circle}
            />
          </View>
        </View>
      )}
      keyExtractor={(item, idx) => `item_${idx}`}
    />
    <View
      style={{
        shadowRadius: 2,
        shadowOffset: {
          width: 0,
          height: -2,
        },
        shadowColor: '#000',
        shadowOpacity: 0.11,
        elevation: 4,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingBottom:15
      }}>
      <GButton
        _style={{
          alignSelf: 'center',
          marginTop: 20,
          marginBottom: 5,
        }}
        bText="Invite"
        height={50}
        width={150}
        radius={5}
        onPress={() => {}}
      />
    </View>
  </>
);

const Gift = () => (
  <View>
    <View style={{marginHorizontal: 15, marginVertical: 15}}>
      <Text
        style={{
          fontSize: fonts.fontSize13,
          color: Colors.white,
          fontFamily: fonts.NovaBoldios,
        }}>
        Gifts
      </Text>

      <View style={{flexDirection: 'row', marginTop: 15}}>
        <Text
          style={{
            fontSize: fonts.fontSize13,
            color: Colors.brownishGrey,
            fontFamily: fonts.NovaBoldios,
            marginRight: 20,
          }}>
          Draw
        </Text>
        <Text
          style={{
            fontSize: fonts.fontSize13,
            color: Colors.white,
            fontFamily: fonts.NovaBoldios,
          }}>
          Popular
        </Text>
      </View>
    </View>

    <View style={{borderWidth: 0.4, borderColor: Colors.lightgrey}}></View>

    <FlatList
      data={ArrData.arrGift}
      contentContainerStyle={{
        marginTop: 15,
        marginHorizontal: 20,
      }}
      renderItem={({item}) => (
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            margin: 2,
            marginBottom: 20,
            marginHorizontal: 4,
            // backgroundColor: 'red',
            // borderWidth:1,borderColor:Colors.lightgrey,borderRadius:8
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: 60,
              backgroundColor: 'trnasparent',
              borderWidth: 1,
              borderColor: Colors.lightgrey,
              borderRadius: 8,
            }}>
            <Icons
              size={28}
              source={item.icon}
              // onPress={() => this.props.navigation.navigate('Live')}
              // _style={{marginRight: 5}}
            />
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: fonts.NovaRegularios,
                fontSize: fonts.fontSize10,
                color: Colors.white,
                marginTop: 10,
              }}>
              {item.text}
            </Text>
          </View>
        </View>
      )}
      //Setting the number of column
      numColumns={5}
      keyExtractor={(item, index) => index}
    />

    <View
      style={{
        // position: 'absolute',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 20,
        marginTop: 20,
        // height: 160,
        // right: 15,
      }}>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <Icons
          size={18}
          source={images.diamond_lg}
          // onPress={() => this.props.navigation.navigate('Live')}
          _style={{marginRight: 15}}
        />
        <Text
          style={{
            fontFamily: fonts.NovaBoldios,
            fontSize: fonts.fontSize13,
            color: Colors.white,
          }}>
          724,57
        </Text>
      </View>

      <View
        style={{
          marginLeft: '40%',
          borderWidth: 1,
          borderColor: Colors.vividPurple,
          flexDirection: 'row',
          alignSelf: 'flex-end',
          borderRadius: 5,
        }}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text
            style={{
              fontFamily: fonts.NovaBoldios,
              fontSize: fonts.fontSize13,
              color: Colors.white,
              marginLeft: 5,
            }}>
            1x
          </Text>

          <Icons
            size={10}
            source={images.down_arrow}
            // onPress={() => this.props.navigation.navigate('Live')}
            _style={{marginLeft: 20, marginRight: 10, tintColor: Colors.white}}
          />

          <View
            style={{
              backgroundColor: Colors.vividPurple,
              paddingHorizontal: 20,
              paddingVertical: 3,
              borderTopLeftRadius: 5,
              borderBottomLeftRadius: 5,
            }}>
            <Text
              style={{
                fontFamily: fonts.NovaRegular,
                fontSize: fonts.fontSize13,
                color: Colors.white,
              }}>
              send
            </Text>
          </View>
        </View>
      </View>

      <View></View>
    </View>
  </View>
);

const UserProfilePopup = () => (
  <>
    <View style={{alignItems: 'center'}}>
      <Icons
        size={80}
        source={images.Profile}
        _style={{bottom:30}}
      />
      <View style={styles.other_user_name_view}>
        <Text style={styles.other_user_name_text}>@Erric joined</Text>
      </View>

      <View style={styles.you_tuber_text_view}>
        
      <Icons
        size={20}
        source={images.location}
      />

        <Text style={styles.you_tuber_text}>San Francisco, CA</Text>
      </View>

      <View
        style={{
          paddingHorizontal: 5,
          backgroundColor: Colors.hotMagenta,
          paddingVertical: 3,
          borderRadius: 5,
          marginTop: 10,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Icons size={15} source={images.eye_white} _style={{marginRight: 5}} />
        <Text style={{fontSize: fonts.fontSize13, color: Colors.white}}>
          30
        </Text>
      </View>

    </View>

    <View style={{height:10}}/>


    <View style={styles.fans_following_count_view}>
      <View style={styles.fans_count_view}>
        <Text style={styles.count_text}>
          76.3<Text>k</Text>
        </Text>
        <Text style={styles.fans_following_text}>Fans</Text>
      </View>

      <View style={styles.separator_vertical}></View>

      <View style={styles.following_count_view}>
        <Text style={styles.count_text}>
          3.45<Text>k</Text>
        </Text>
        <Text style={styles.fans_following_text}>Following</Text>
      </View>
    </View>

    <View style={{height:20}} />


    <View style={styles.submit_btn_view}>
      <GButton
        height={60}
        width={150}
        radius={5}
        txtcolor={Colors.white}
        fontFamily={fonts.NovaBold}
        fontSize={fonts.fontSize13}
        bText={'+ Follow'}
      />

      <View style={{width: 10}} />

      <GButton
        height={60}
        width={150}
        radius={5}
        txtcolor={Colors.white}
        fontFamily={fonts.NovaBold}
        fontSize={fonts.fontSize13}
        bText={'Chat'}
      />
    </View>
  </>
);



export default class LiveStreaming extends Component {
  state = {
    modalVisible: false,
    modalVisible1: false,
    modalVisible2:false,
  };

  onPresModel() {
    this.setState({modalVisible: true});
  }

  onPresUserModel(){
    this.setState({modalVisible2: true});
  }

  render() {
    return (
      <ImageBackground style={{flex: 1}} source={images.Rectangle5}>
        <StatusBar
          backgroundColor={Colors.violetBlue}
          barStyle="light-content"
        />
        <SafeAreaView />
        {/* ComponentHeader */}
        <ComponentHeader
          navigation={this.props.navigation}
          onPresModel={() => this.onPresModel()}
          onPresUserModel={() => this.onPresUserModel()}
        />

        <View style={{flex: 1}}>
          <View
            style={{justifyContent: 'center', alignItems: 'flex-end', flex: 1}}>
            <Icons
              size={35}
              source={images.slide_next_arrow}
              _style={{marginRight: 15}}
              // onPress={() => this.onPresModel()}
            />
          </View>

          <View>
            <View
              style={{
                flexDirection: 'row',
                paddingHorizontal: 10,
                marginBottom: 20,
              }}>
              <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Icons
                    size={30}
                    source={images.Profile}
                    _style={{marginRight: 10}}
                  />
                  <Text
                    style={{
                      fontFamily: fonts.NovaRegularios,
                      fontSize: fonts.fontSize13,
                      color: Colors.white,
                      marginRight: 10,
                    }}>
                    Erric joined
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    borderWidth: 1,
                    borderColor: Colors.white,
                    paddingHorizontal: 10,
                    paddingVertical: 8,
                    borderRadius: 30,
                  }}>
                  <Icons
                    size={20}
                    source={images.wave_ic}
                    _style={{marginRight: 5}}
                  />
                  <Text style={styles.squretxt}>Wave</Text>
                </View>

                <View
                  style={{
                    width: 120,
                    backgroundColor: 'rgba(0,0,0,0.5)',
                    marginLeft: 15,
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      margin: 10,
                    }}>
                    <Text style={styles.squretxt}>Beauty</Text>
                    <Icons size={15} source={images.magic} />
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      margin: 10,
                    }}>
                    <Text style={styles.squretxt}>Effect</Text>
                    <Icons size={20} source={images.Profile} />
                  </View>

                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      margin: 10,
                    }}>
                    <Text style={styles.squretxt}>Stickers</Text>
                    <Icons size={20} source={images.stricker_icon_yellow} />
                  </View>

                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      margin: 10,
                    }}>
                    <Text style={styles.squretxt}>Text</Text>
                    <Icons size={20} source={images.text_icon_unactive} />
                  </View>
                </View>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingHorizontal: 10,
                marginBottom:10
              }}>
              <View
                style={styles.view1}>
                <Text
                  style={styles.commentxt}>
                  Comment
                </Text>
              </View>

              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icons
                  size={20}
                  source={images.userplus}
                  _style={{marginRight: 15}}
                  onPress={() => this.setState({modalVisible1: true})}
                />
                <Icons
                  size={20}
                  source={images.stricker_icon_white}
                  _style={{marginRight: 15}}
                />
                <Icons
                  size={25}
                  source={images.camera_flip}
                  _style={{marginRight: 15}}
                />
              </View>
            </View>

          </View>
        </View>
        <SafeAreaView />

        <Custompopup
          modalVisible={this.state.modalVisible || this.state.modalVisible1}>
          <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.2)'}}>
            <TouchableOpacity
              onPress={() =>
                this.setState({modalVisible: false, modalVisible1: false})
              }
              style={{flex: 2}}></TouchableOpacity>

            <View
              style={{
                flex: 4,
                backgroundColor: Colors.white,
                borderTopLeftRadius: 15,
                borderTopRightRadius: 15,
              }}>
              {this.state.modalVisible && <CurrentViewer />}

              {this.state.modalVisible1 && <RecentFriends />}
            </View>
          </View>
          <SafeAreaView />
        </Custompopup>

        {/* Gift popUp */}
        {/* <Custompopup modalVisible={this.state.modalVisible}>
          <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.2)'}}>
            <View
              onTouchEnd={() => this.setState({modalVisible: false})}
              style={{flex: 2}}></View>
            <View
              style={{
                flex: 1.5,
                backgroundColor: 'rgba(0,0,0,0.8)',
              }}>
              <Gift />
            </View>
          </View>
        </Custompopup> */}

        <Custompopup modalVisible={this.state.modalVisible2}>
          <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.2)'}}>
            <TouchableOpacity
              onPress={() => this.setState({modalVisible2: false})}
              style={{flex: 1}}/>
            <View
              style={{
                flex: 2,
                backgroundColor: Colors.white,
                borderTopLeftRadius: 15,
                borderTopRightRadius: 15,
              }}>
              <UserProfilePopup />
            </View>
          </View>
        </Custompopup>
      </ImageBackground>
    );
  }
}

const _renderFriendRequestList = ({item, index}) => {
  return (
    <View style={styles.friend_req_view}>
      <View style={styles.img_name_view}>
      <Text style={{marginRight:10}}>{index+1}</Text>
        <View style={styles.user_img_view}>

          <Image
            source={images.Profile}
            resizeMode={'cover'}
            style={styles.user_img}
          />
        </View>
        <View style={styles.name_view}>
          <Text numberOfLines={1} style={styles.name_text}>
            {item.username}
          </Text>
          <Text style={{fontSize:fonts.fontSize10,color:Colors.vividPurple,marginTop:5}}>Contributed 180 stars</Text>
        </View>
      </View>

      <View style={styles.btn_view}>
        <Icons
          size={50}
          source={images.user_circle}
          // onPress={() => alert('hi')}
        />

        <Icons
          size={50}
          source={images.mute}
          onPress={() => alert('hi')}
        />

        <Icons
          size={50}
          source={images.chat_circle}
          onPress={() => alert('hi')}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  squretxt: {
    fontFamily: fonts.NovaRegularios,
    color: Colors.white,
    fontSize: fonts.fontSize13,
  },
  txt: {
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaRegularios,
    color: Colors.white,
  },

  friend_req_view: {
    borderRadius: 10,
    marginVertical: 6,
    paddingHorizontal: 10,
    // paddingVertical: 10,
    borderWidth: 1,
    borderColor: Colors.white,
    marginHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.15,
    shadowRadius: 1.94,
    elevation: 3,
  },
  img_name_view: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 4,
  },
  user_img_view: {
    height: 30,
    width: 30,
    borderRadius: 30 / 2,
  },
  user_img: {
    width: '100%',
    height: '100%',
  },
  name_view: {
    width: 120,
    marginLeft: 10,
  },
  name_text: {
    //   fontsize: fonts.fontsize15,
      fontFamily: fonts.NovaSbold,
    color: Colors.black,
  },

  pophead: {
    fontSize: fonts.fontSize16,
    fontFamily: fonts.NovaBoldios,
    color: Colors.vividPurple,
  },

  btn_view: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 3,
    // backgroundColor: 'red',
    justifyContent: 'flex-end',
  },

  item: {
    margin: 10,
    height: 30,
    borderRadius: 10,
  },
  item2: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    paddingHorizontal: 20,
    height: 60,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.15,
    shadowRadius: 1.14,
    // marginVertical:3,
    elevation: 3,
  },
  row1: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  // UserProfilePopupStyle

  other_user_name_view: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
  },
  other_user_name_text: {
    fontSize: fonts.fontSize20,
    lineHeight: 24,
    color: Colors.black,
    fontFamily: fonts.NovaRegular,
  },
  you_tuber_text_view: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
    flexDirection: 'row',
  },
  you_tuber_text: {
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaRegular,
    color: Colors.black,
  },
  fans_following_count_view: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 45,
  },
  fans_count_view: {
    alignItems: 'center',
    flex: 2,
  },
  count_text: {
    fontSize: fonts.fontSize15,
    color: Colors.black,
    fontFamily: fonts.NovaXbold,
  },
  fans_following_text: {
    fontSize: fonts.fontSize13,
    color: Colors.black,
    fontFamily: fonts.NovaRegular,
    marginTop:8,
  },
  following_count_view: {
    alignItems: 'center',
    flex: 2,
  },
  separator_vertical: {
    width: 1,
    height: 38,
    backgroundColor: Colors.whiteThree,
  },

  submit_btn_view: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 15,
    flexDirection: 'row',
  },


commentxt:{
  fontFamily: fonts.NovaRegularios,
  fontSize: fonts.fontSize13,
  color: Colors.white,
},
view1:{
  borderWidth: 1,
  borderColor: Colors.white,
  paddingLeft: 20,
  paddingRight: 70,
  paddingVertical: 10,
  borderRadius: 30,
},

});
