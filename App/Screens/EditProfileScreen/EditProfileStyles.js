import { StyleSheet } from 'react-native';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';


export default EditProfileStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
    },
    image_parent_view: {
        alignItems: 'center',
        marginTop:20,
    },
    edit_profile_view:{
        marginTop: 30 
    },
    edit_profile_text:{
        fontSize: Fonts.fontSize16, 
        lineHeight: 20, 
        fontFamily: Fonts.NovaBold, 
        color: Colors.black, 
        textAlign: 'center'
    },
    image_view: {
        alignItems: 'center',
        width: 94,
        height: 94,
        borderRadius: 94 / 2
    },
    profile_img: {
        height: '100%',
        width: '100%',
        borderRadius:100/2,
    },
    choose_img_touch:{
        height: 29, 
        width: 29, 
        position: 'absolute', 
        right: 160, 
        top: 65
    },
    about_view:{
        marginHorizontal: 17,
        borderRadius: 8,
    },
    about_text:{
        fontSize: Fonts.fontSize13, 
        lineHeight: 15, 
        fontFamily: Fonts.NovaRegular, 
        color: Colors.black ,
        marginLeft: 18,
    },
    about_input:{
        shadowColor: Colors.black, 
        width: 330, 
        height: 134, 
        borderRadius: 10, 
        borderWidth: 1.5, 
        borderColor: Colors.white, 
        fontSize: Fonts.fontSize13, 
        padding: 12,
        color: Colors.pinkishGreyTwo
    },
    input_view: {
        marginHorizontal: 17,
    },

    submit_btn_view: {
        alignItems: 'center',
        marginVertical: 40,
    },


    contact_view:{
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between', 
        // marginHorizontal: 15
    },
    code_arrow_view:{
        marginTop: 28, 
        marginRight: 5
    },
    code_arrow_chaild_view:{
        width: 60, 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        marginRight: 20,
        marginBottom: 10,
    },
    code_text:{
        fontSize: Fonts.fontSize13, 
        // lineHeight: 15, 
        color: Colors.black, 
        fontFamily: Fonts.NovaRegular
    },
    arrow_img:{
        height: 5.9, 
        width: 9.5 
    },
    code_separator_view:{
        height: .8, 
        backgroundColor: Colors.charcoalGrey, 
        // marginTop:8, 
        width: 60
    },
    input_width_view:{
        width: '70%' 
    },



    View1: {
        flexDirection: 'row',
        marginVertical: 10,
      },
      View2: {
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
        alignSelf: 'flex-end',
      },
      View3: {
        fontSize: Fonts.fontSize13,
        fontFamily: Fonts.NovaRegular,
        color: Colors.pinkishGrey,
      },
      phoneInput: {height: 40, borderBottomWidth: 1, fontSize: Fonts.fontSize13},
    
      arrow_img: {
        height: 10,
        width: 10,
        marginLeft: 30,
      },



});