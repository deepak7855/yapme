import {StyleSheet} from 'react-native';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';

export default SignupStyles = StyleSheet.create({
  safe_area_view: {
    flex: 1,
    // backgroundColor: '#000',
  },
  image_back_ground: {
    flex: 1,
  },
  create_view: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 5,
    marginTop: 10,
  },
  create_account_text: {
    color: Colors.vividPurple,
    fontSize: Fonts.fontSize24,
    fontFamily: Fonts.NovaBold,
    lineHeight: 29,
  },
  fill_text_view: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 5,
    marginHorizontal: 25,
  },
  fill_profile_text: {
    fontSize: Fonts.fontSize16,
    lineHeight: 20,
    textAlign: 'center',
    color: Colors.pinkishGrey,
  },
  text_input_view: {
    marginHorizontal: 45,
    marginTop: 30,
  },

  terms_text_view: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 70,
  },
  agree_text: {
    fontSize: Fonts.fontSize10,
    color: Colors.pinkishGrey,
    lineHeight: 12,
    fontFamily: Fonts.NovaRegular,
  },
  terms_text: {
    fontSize: Fonts.fontSize10,
    fontFamily: Fonts.NovaRegular,
    color: Colors.cobaltBlue,
    lineHeight: 12,
  },
  privacy_policy_text: {
    fontSize: Fonts.fontSize10,
    fontFamily: Fonts.NovaRegular,
    color: Colors.cobaltBlue,
    lineHeight: 12,
  },
  submit_btn_view: {
    alignItems: 'center',
    marginVertical: 35,
  },
  social_account_view: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
  social_account_text: {
    fontSize: Fonts.fontSize13,
    lineHeight: 17,
    fontFamily: Fonts.NovaRegular,
    color: Colors.black,
  },
  already_text_view: {
    justifyContent: 'center',
    alignItems: 'center',
    // marginVertical: 20,
    marginBottom: 20,
  },
  already_text: {
    fontFamily: Fonts.NovaRegularios,
    fontSize: Fonts.fontSize13,
    color: Colors.black,
    lineHeight: 17,
  },
  already_sing_in: {
    fontFamily: Fonts.NovaBold,
    fontSize: Fonts.fontSize15,
    lineHeight: 17,
    color: Colors.cobaltBlue,
    textDecorationLine: 'underline',
  },

  nosymbol: {
    fontSize: fonts.fontSize10,
    fontFamily: fonts.NovaRegular,
    color: Colors.pinkishGreyTwo,
    marginBottom: 15,
  },
  appleiocns: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 20,
    marginBottom: 45,
  },

  socicalView: {width: 15, height: 15, marginHorizontal: 8},
  socicalIcons: {width: 18, height: 18},

  View1: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  View2: {
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
    alignSelf: 'flex-end',
  },
  View3: {
    fontSize: Fonts.fontSize13,
    fontFamily: Fonts.NovaRegular,
    color: Colors.pinkishGrey,
  },
  phoneInput: {height: 40, borderBottomWidth: 1, fontSize: fonts.fontSize13},

  arrow_img: {
    height: 10,
    width: 10,
    marginLeft: 30,
  },
});
