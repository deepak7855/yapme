import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, SafeAreaView, TextInput, Modal, FlatList, Image } from 'react-native';


import {images} from '../../Assets/ImageUrl';
import Colors from '../../Assets/Colors';
import Fonts from '../../Assets/Fonts';
const countryData = require('../../Utility/countries.json');



export default class CountryCodePickerModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listData: [],
        }
        this.state.listData = countryData
    }

    searchFilterFunction = (keyword) => {
        if (this.state.listData.length > 0) {
            if (keyword != '') {
                const newData = this.state.listData.filter(item => {
                    console.log(item, "item")
                    const itemData = `${item.name.toUpperCase()}`
                    const textData = keyword.toUpperCase();
                    return itemData.indexOf(textData) > -1;
                });
                if (newData.length > 0) {
                    this.setState({
                        listData: newData, notFound: false,
                    });
                }
                else {
                    this.setState({
                        listData: countryData, notFound: true,
                    });
                }
            } else {
                // this.syncContacts();
                this.setState({
                    listData: countryData, notFound: false,
                });
            }
        }
        else {
            this.setState({
                notFound: true,
            });
        }


    };

    onClickItem = (dial_code) => {
        this.props.callCounteryCodeApi(dial_code);
    }

    _renderSubItem = ({ item }) => (
        <TouchableOpacity activeOpacity={0.8} style={{backgroundColor: '#fff'}} onPress={() => { this.onClickItem(item.dial_code); }}>
            <View style={{ flex: 1, backgroundColor: '#fff', paddingHorizontal: 10, paddingVertical: 10, }}>
                <Text>({item.dial_code})  {item.name}</Text>
            </View>
        </TouchableOpacity>

    )
    render() {
        return (
            <View>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.props.visible}
                    onRequestClose={this.props.onRequestClose}>

                    <SafeAreaView style={{ flex: 1, }}>
                        <View style={{ flex: 1, paddingHorizontal: 20, marginTop: 100 }}>
                            <View style={styles.imgViewcode}>
                                <TextInput
                                    style={{ width: '80%', paddingVertical: 10, fontFamily: Fonts.helvetica_Bold, color: Colors.app_blue }}
                                    placeholder="Filter...."
                                    underlineColorAndroid="transparent"
                                    onChangeText={(text) => this.searchFilterFunction(text)}
                                />
                                <TouchableOpacity onPress={() => this.props.onPress()}>
                                    <View style={styles.nextBtnnwe1}>
                                        <Image
                                            style={{ width: 10, height: 10,tintColor:Colors.black }}
                                            source={images.close_ic}
                                            resizeMode='contain' />
                                    </View>
                                </TouchableOpacity>

                            </View>
                            {this.state.notFound ?
                                <View style={{ height: 300, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center', }}>
                                    <Text style={{ fontSize: 16, color: "#000", fontFamily: 'Metropolis-SemiBold' }}>No data found</Text>
                                </View>
                                :
                                <View style={{ height: 300 }}>
                                    <FlatList
                                        showsVerticalScrollIndicator={false}
                                        data={this.state.listData}
                                        renderItem={this._renderSubItem}
                                        extraData={this.state}

                                    />
                                </View>
                            }
                        </View>
                    </SafeAreaView>
                </Modal>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,


    },
    imgViewcode: {
        backgroundColor: "white",
        width: "100%",
        height: 35,
        borderRadius: 4,
        borderBottomWidth: 1,
        borderBottomColor: 'grey',
        flexDirection: "row",
        justifyContent: 'center',
        alignItems: "center"
    },
    nextBtnnwe1: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingVertical: 10,
        marginHorizontal: 10
    },
})