import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from 'react-native';
import {images} from '../../Assets/ImageUrl';
import styles from './EditProfileStyles';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';
import {Input} from '../../Common/InputCommon';
import {GButton} from '../../Common/GButton';
import Header from '../../Common/Header';
import CameraController from '../../Lib/NewCameraController';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import CountryCodePicker from '../../Component/Modal/CountryCodePickerModal';
import CHeader from '../../Common/CHeader';

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCameraShow: false,
      modalVisibleCode: false,
      countryCode: '',

      editProfileForm: {
        profile_picture: '',
      },
    };
  }

  setValues(key, value) {
    let editProfileForm = {...this.state.editProfileForm};
    editProfileForm[key] = value;
    this.setState({editProfileForm});
  }

  chooseImage = () => {
    this.setState({isCameraShow: true});
  };
  onExitMethod() {
    this.setState({isCameraShow: false});
  }
  UploadMediaMethod(uri) {
    // alert(uri)
    this.setValues('profile_picture', uri);
    this.setState({isCameraShow: false, avatarSource: uri});
  }

  callCounteryCodeApi = (dial_code) => {
    this.setState({countryCode: dial_code, modalVisibleCode: false});
  };

  render() {
    return (
      <SafeAreaView style={styles.safe_area_view}>
        <CHeader navigation={this.props.navigation} backArrowLeft centerItem />
        <KeyboardAwareScrollView
          bounces={false}
          showsVerticalScrollIndicator={false}>
          <View style={styles.edit_profile_view}>
            <Text style={styles.edit_profile_text}>Edit Profile</Text>
          </View>

          <View style={styles.image_parent_view}>
            <View style={styles.image_view}>
              <Image
                source={
                  this.state.editProfileForm.profile_picture
                    ? {uri: this.state.editProfileForm.profile_picture}
                    : images.Profile
                }
                style={styles.profile_img}
                resizeMode={'cover'}
              />
            </View>

            <TouchableOpacity
              onPress={() => {
                this.chooseImage();
              }}
              style={styles.choose_img_touch}>
              <Image
                style={styles.profile_img}
                resizeMode={'cover'}
                source={images.camera_profile}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.input_view}>
            <Input placeholder={'@elemar'} keyboardType={'email-address'} />
            <Input placeholder={'Name'} keyboardType={'default'} />

            <Input placeholder={'Gender'} keyboardType={'default'} />
            <Input
              placeholder={'DOB'}
              keyboardType={'default'}
              rightImagePath={images.calendar}
              customeImageStyle={{width: 11.3, height: 12.5, margin: 10}}
            />

            <View style={[styles.View1, {marginTop: 20}]}>
              <View style={styles.View2}>
                <Text style={styles.code_text}>
                  {this.state.countryCode ? this.state.countryCode : '+ 1'}
                </Text>
                <Image
                  resizeMode={'contain'}
                  source={images.down_arrow}
                  style={styles.arrow_img}
                />
              </View>

              <View style={{width: 20}} />

              <View style={{width: null, flex: 1}}>
                <TextInput
                  style={styles.phoneInput}
                  keyboardType="phone-pad"
                  returnKeyType="done"
                />
              </View>
            </View>

            {/* <View style={styles.contact_view}>
              <View style={styles.code_arrow_view}>
                <View
                  style={styles.code_arrow_chaild_view}
                  onTouchStart={() => this.setState({modalVisibleCode: true})}>
                  <Text style={styles.code_text}>
                    {this.state.countryCode ? this.state.countryCode : '+1'}
                  </Text>
                  <View>
                    <Image
                      resizeMode={'contain'}
                      source={images.down_arrow}
                      style={styles.arrow_img}
                    />
                  </View>
                </View>
                <View style={styles.code_separator_view}></View>
              </View>

              <View style={styles.input_width_view}>
                <Input
                  customeTilteStyle={{width: 200}}
                  placeholder={'Contact'}
                  keyboardType={'phone-pad'}
                />
              </View>
            </View> */}

            <Input placeholder={'Add Location'} keyboardType={'default'} />
            <Input placeholder={'Instagram Link'} keyboardType={'default'} />
            <Input placeholder={'YouTube Link'} keyboardType={'default'} />
            <Input
              placeholder={'Password'}
              keyboardType={'default'}
              secureTextEntry={true}
            />
            <Input
              placeholder={'Confirm Password'}
              keyboardType={'default'}
              secureTextEntry={true}
            />
          </View>
          <View style={{height: 18}} />

          <Text style={styles.about_text}>About Me</Text>

          <View style={[CommonStyle, styles.about_view, {marginTop: 8}]}>
            <View>
              <TextInput
                style={styles.about_input}
                multiline={true}
                underlineColorAndroid="transparent"
                keyboardType={'default'}
                returnKeyType="done"
                numberOfLines={10}
                textAlignVertical="top"
                placeholder="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,"
              />
            </View>
          </View>
          <View style={styles.submit_btn_view}>
            <GButton
              height={50}
              width={200}
              radius={5}
              lineHeight={15}
              txtcolor={Colors.white}
              fontFamily={Fonts.NovaBold}
              fontSize={Fonts.fontSize13}
              bText={'Save Changes'}
              onPress={() => this.props.navigation.goBack()}
            />
          </View>
        </KeyboardAwareScrollView>

        <CountryCodePicker
          visible={this.state.modalVisibleCode}
          onRequestClose={() => {
            this.setState({modalVisibleCode: false});
          }}
          onPress={() => {
            this.setState({modalVisibleCode: false});
          }}
          callCounteryCodeApi={(dial_code) => {
            this.callCounteryCodeApi(dial_code);
          }}
        />

        {this.state.isCameraShow && (
          <CameraController
            UploadMediaMethod={(data) => this.UploadMediaMethod(data)}
            onExitMethod={() => this.onExitMethod()}></CameraController>
        )}
      </SafeAreaView>
    );
  }
}

export default EditProfile;
