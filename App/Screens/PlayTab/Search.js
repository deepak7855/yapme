import React, {Component} from 'react';
import {
  ImageBackground,
  SafeAreaView,
  FlatList,
  Image,
  Text,
  View,
  StyleSheet,
  StatusBar,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import CHeader from '../../Common/CHeader';
import {Icons, Searchbar} from '../../Component/Common';

export default class Search extends Component {
  render() {
    return (
      <View style={{flex: 1,backgroundColor:Colors.white}}>
        <StatusBar
          backgroundColor={Colors.violetBlue}
          barStyle="dark-content"
        />
        <SafeAreaView />
        <CHeader navigation={this.props.navigation} backArrowLeft centerItem />
        <ScrollView showsVerticalScrollIndicator={false}>
          <Searchbar placeholder="Search Friends" />

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginHorizontal: 15,
              marginTop:25
            }}>
            <TouchableOpacity
              onPress={() => {
                // this.setState({
                //   index: 0,
                //   index_one_filled: false,
                //   index_two_filled: false,
                // }),
                //   this.viewpager.setPage(0);
              }}
              style={{
                width: '18%',
                borderBottomWidth: 1.5,
                alignItems: 'center',
                borderBottomColor: Colors.vividPurple,
                paddingVertical: 10,
              }}>
              <Text
                style={{
                  fontSize: fonts.fontSize13,
                  fontFamily: fonts.NovaRegularios,
                  marginBottom:10
                }}>
                All
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                // this.setState({index: 1, index_one_filled: true}),
                //   this.viewpager.setPage(1);
              }}
              style={{
                width: '18%',
                // borderBottomWidth: this.state.index_one_filled ? null : 1,
                borderBottomWidth: 1,
                alignItems: 'center',
                borderBottomColor: Colors.greyColor,
                paddingVertical: 10,
              }}>
              <Text
                style={{
                  fontSize: fonts.fontSize13,
                  fontFamily: fonts.NovaRegularios,
                  color:Colors.pinkishGrey
                }}>
                Trending
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              // disabled={this.state.index == 0}
              // onPress={() => {
              //   this.setState({index: 2, index_two_filled: true}),
              //     this.viewpager.setPage(2);
              // }}
              style={{
                width: '18%',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderBottomColor: Colors.greyColor,
                paddingVertical: 10,
              }}>
              <Text
                style={{
                  fontSize: fonts.fontSize13,
                  fontFamily: fonts.NovaRegularios,
                  color:Colors.pinkishGrey

                }}>
                Users
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                width: '18%',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderBottomColor: Colors.greyColor,
                paddingVertical: 10,
              }}>
             <Text
                style={{
                  fontSize: fonts.fontSize13,
                  fontFamily: fonts.NovaRegularios,
                  color:Colors.pinkishGrey

                }}>
                Videos
              </Text>
            </TouchableOpacity>


            <TouchableOpacity
              style={{
                width: '18%',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderBottomColor: Colors.greyColor,
                paddingVertical: 10,
              }}>
             <Text
                style={{
                  fontSize: fonts.fontSize13,
                  fontFamily: fonts.NovaRegularios,
                  color:Colors.pinkishGrey

                }}>
                Sound
              </Text>
            </TouchableOpacity>


            <TouchableOpacity
              style={{
                width: '18%',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderBottomColor: Colors.pinkishGreyTwo,
                paddingVertical: 10,
              }}>
             <Text
                style={{
                  fontSize: fonts.fontSize13,
                  fontFamily: fonts.NovaRegularios,
                  color:Colors.pinkishGrey
                }}>
                Hash tag
              </Text>
            </TouchableOpacity>
          </View>


{/* UserText */}
          <Text
            style={{
              fontSize: fonts.fontSize13,
              fontFamily: fonts.NovaRegularios,
              marginLeft: 18,
              marginTop: 18,
            }}>
            Users
          </Text>

          <View>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{
                paddingHorizontal: 10,
                marginTop: 15,
              }}
              data={['', '', '', '', '', '', '', '', '']}
              renderItem={({item: color, index}) => (
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      alignItems: 'center',
                      marginRight: 15,
                      marginTop: 5,
                    }}>
                    <Image
                      resizeMode="contain"
                      style={{height: 50, width: 50}}
                      source={images.Profile}
                    />
                    <Icons
                      size={35}
                      source={images.live}
                      _style={{bottom: -15, right: -18, position: 'absolute'}}
                    />
                    <Text
                      style={{
                        marginVertical: 13,
                        marginTop: 25,
                        fontFamily: fonts.NovaRegularios,
                        fontSize: fonts.fontSize13,
                      }}>
                      @sara
                    </Text>
                  </View>
                </View>
              )}
            />

            {/* Videos */}
            <View
              style={{
                paddingHorizontal: 15,
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop:20,
                marginBottom:5,
              }}>
              <View>
                <Text style={styles.txtstyle}>Videos</Text>
              </View>

              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={[styles.txtstyle, {color: Colors.vividPurple}]}>
                  View All
                </Text>
                <Icons
                  size={12}
                  source={images.arrow_next_black}
                  _style={{tintColor: Colors.vividPurple, marginLeft: 10}}
                />
              </View>
            </View>
          </View>

          <View>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{
                //   paddingHorizontal: 10,
                marginTop: 8,
              }}
              data={['', '', '']}
              renderItem={({item: color, index}) => (
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      alignItems: 'center',
                      // marginRight: 15,
                      // marginTop: 5,
                    }}>
                    <View style={{flexDirection: 'row', marginLeft: 10}}>
                      <Image
                        resizeMode="contain"
                        style={{height: 150, width: 100}}
                        source={images.Group4}
                      />
                      <Image
                        resizeMode="contain"
                        style={{height: 150, width: 100}}
                        source={images.Group5}
                      />
                      <Image
                        resizeMode="contain"
                        style={{height: 150, width: 100}}
                        source={images.Group6}
                      />

                      <Image
                        resizeMode="contain"
                        style={{height: 150, width: 100}}
                        source={images.Group7}
                      />
                    </View>
                  </View>
                </View>
              )}
            />
          </View>

          {/* sound */}
          <View
            style={{
              paddingHorizontal: 15,
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop:25,
            }}>
            <View>
              <Text style={styles.txtstyle}>Sound</Text>
            </View>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={[styles.txtstyle, {color: Colors.vividPurple}]}>
                View All
              </Text>
              <Icons
                size={12}
                source={images.arrow_next_black}
                _style={{tintColor: Colors.vividPurple, marginLeft: 10}}
              />
            </View>
          </View>

          <View>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{
                paddingHorizontal: 10,
                marginTop: 10,
              }}
              data={['', '', '', '']}
              renderItem={({item: color, index}) => (
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      alignItems: 'center',
                      marginRight: 5,
                      marginTop: 5,
                    }}>
                    <Image
                      resizeMode="contain"
                      style={{height: 50, width: 50}}
                      source={images.Groupp1}
                    />
                    <Text
                      style={{
                        marginVertical: 13,
                        // marginTop: 25,
                        fontFamily: fonts.NovaRegularios,
                        fontSize: fonts.fontSize13,
                      }}>
                      {`Linkin\n park`}
                    </Text>
                  </View>

                  <View
                    style={{
                      alignItems: 'center',
                      marginRight: 5,
                      marginTop: 5,
                    }}>
                    <Image
                      resizeMode="contain"
                      style={{height: 50, width: 50}}
                      source={images.Groupp2}
                    />
                    <Text
                      style={{
                        marginVertical: 13,
                        // marginTop: 25,
                        fontFamily: fonts.NovaRegularios,
                        fontSize: fonts.fontSize13,
                      }}>
                      {`Alexa \ntrack`}
                    </Text>
                  </View>

                  <View
                    style={{
                      alignItems: 'center',
                      marginRight: 5,
                      marginTop: 5,
                    }}>
                    <Image
                      resizeMode="contain"
                      style={{height: 50, width: 50}}
                      source={images.Groupp3}
                    />
                    <Text
                      style={{
                        marginVertical: 13,
                        // marginTop: 25,
                        fontFamily: fonts.NovaRegularios,
                        fontSize: fonts.fontSize13,
                      }}>
                      {`   Ban \n adams`}
                    </Text>
                  </View>
                </View>
              )}
            />
          </View>

          {/* HasTag  */}
          <View>
            <View
              style={{
                paddingHorizontal: 15,
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 20,
                marginBottom:5
              }}>
              <View>
                <Text style={styles.txtstyle}>Hash tag</Text>
              </View>

              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={[styles.txtstyle, {color: Colors.vividPurple}]}>
                  View All
                </Text>
                <Icons
                  size={12}
                  source={images.arrow_next_black}
                  _style={{tintColor: Colors.vividPurple, marginLeft: 10}}
                />
              </View>
            </View>
          </View>

          <View>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{
                marginTop: 8,
              }}
              data={['', '', '']}
              renderItem={({item: color, index}) => (
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      alignItems: 'center',
                      // marginRight: 15,
                      // marginTop: 5,
                    }}>
                    <View style={{flexDirection: 'row', marginLeft: 10}}>
                      <View>
                        <Image
                          resizeMode="contain"
                          style={{height: 150, width: 100}}
                          source={images.Group4}
                        />
                        <View
                          style={{flexDirection: 'row', alignItems: 'center'}}>
                          <Icons
                            size={20}
                            source={images.hashic}
                            _style={{
                              marginRight: 5,
                            }}
                          />
                          <Text
                            style={{
                              marginVertical: 13,
                              // marginTop: 25,
                              fontFamily: fonts.NovaRegularios,
                              fontSize: fonts.fontSize10,
                            }}>
                            Morgan Smith
                          </Text>
                        </View>
                      </View>

                      <View>
                        <Image
                          resizeMode="contain"
                          style={{height: 150, width: 100}}
                          source={images.Group5}
                        />
                        <View
                          style={{flexDirection: 'row', alignItems: 'center'}}>
                          <Icons
                            size={20}
                            source={images.hashic}
                            _style={{
                              marginRight: 5,
                            }}
                          />
                          <Text
                            style={{
                              marginVertical: 13,
                              // marginTop: 25,
                              fontFamily: fonts.NovaRegularios,
                              fontSize: fonts.fontSize10,
                            }}>
                            morganwallen
                          </Text>
                        </View>
                      </View>

                      <View>
                        <Image
                          resizeMode="contain"
                          style={{height: 150, width: 100}}
                          source={images.Group6}
                        />
                        <View
                          style={{flexDirection: 'row', alignItems: 'center'}}>
                          <Icons
                            size={20}
                            source={images.hashic}
                            _style={{
                              marginRight: 5,
                            }}
                          />
                          <Text
                            style={{
                              marginVertical: 13,
                              // marginTop: 25,
                              fontFamily: fonts.NovaRegularios,
                              fontSize: fonts.fontSize10,
                            }}>
                            morganhorse
                          </Text>
                        </View>
                      </View>

                      <View>
                        <Image
                          resizeMode="contain"
                          style={{height: 150, width: 100}}
                          source={images.Group7}
                        />
                        <View
                          style={{flexDirection: 'row', alignItems: 'center'}}>
                          <Icons
                            size={20}
                            source={images.hashic}
                            _style={{
                              marginRight: 5,
                            }}
                          />
                          <Text
                            style={{
                              marginVertical: 13,
                              // marginTop: 25,
                              fontFamily: fonts.NovaRegularios,
                              fontSize: fonts.fontSize10,
                            }}>
                            morganarrw
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              )}
            />
          </View>
        <SafeAreaView />

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  View4: {fontFamily: fonts.NovaRegularios, fontSize: fonts.fontSize20},
  txt: {
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaRegularios,
    color: Colors.white,
  },
  txtstyle: {fontFamily: fonts.NovaRegularios, fontSize: fonts.fontSize13},
});
