import { StyleSheet } from 'react-native';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';


export default FollowingStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
    },
    notification_view: {
        marginHorizontal: 15,
        marginVertical: 5
    },

    notification_text:{
        fontSize:Fonts.fontSize16,
        lineHeight:20,
        fontFamily:Fonts.NovaBold,
        color:Colors.black
    },
    friend_req_view: {
        // borderRadius: 1,
        marginVertical: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
        // borderWidth: 1,
        // borderColor: Colors.white,
        marginHorizontal: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        // elevation:1
        borderColor:'#ddd',
        borderWidth:1,
        borderRadius:10
  

    },
    img_name_view: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 4
    },
    user_img_view: {
        height: 30,
        width: 30,
        borderRadius: 30 / 2
    },
    user_img: {
        width: '100%',
        height: '100%'
    },
    name_view: {
        width: 120,
        marginLeft: 10,
    },
    name_text: {
        lineHeight: 17,
        fontSize: Fonts.fontSize15,
        fontFamily: Fonts.NovaBold,
        color: Colors.black
    },
    btn_view: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 2.5,
    },
    accept_btn_touch: {
        marginRight: 10,
        justifyContent: 'center',
        alignItems: 'center',
        // width: 59,
        // height: 22,
        padding:5,
        paddingHorizontal:10,
        backgroundColor: Colors.violetBlue,
        borderRadius: 5
    },
    accept_text: {
        fontSize: Fonts.fontSize10,
        lineHeight: 12,
        color: Colors.white,
        fontFamily: Fonts.NovaBold
    },
    reject_btn_touch: {
        justifyContent: 'center',
        alignItems: 'center',
        // width: 59,
        // height: 22,
        padding:4,
        paddingHorizontal:10,
        borderColor: Colors.violetBlue,
        borderRadius: 5,
        borderWidth: 1
    },
    reject_text: {
        fontSize: Fonts.fontSize10,
        lineHeight: 12,
        color: Colors.violetBlue,
        fontFamily: Fonts.NovaBold
    },

});