


import React from 'react';
import { View, Text, ImageBackground,Image, StyleSheet,SafeAreaView, Dimensions, DeviceEventEmitter, TouchableOpacity, } from 'react-native';
import fonts from '../Assets/Fonts';
import Colors from '../Assets/Colors';
export default Header = (prop) => {
    console.log('++++++',prop)
    return (
        prop.navigation.setOptions({
            header:() =>
            <SafeAreaView>
            <View  style={{backgroundColor:Colors.white,flexDirection:"row", width:"100%",height:48,justifyContent:"space-between",paddingHorizontal:15,alignItems:"center" }} >
            <TouchableOpacity onPress={() => prop.leftClick()} >
                <Image source={prop.ImagePath} style={{ height:prop.sport?48:18, width:prop.sport?48:18, resizeMode: "contain", }} />
            </TouchableOpacity>
             <Text style={[styles.text,{fontSize:prop.fontbold?18:16}]} >{prop.Title}</Text>            
           {prop.MydogProfile?
            <TouchableOpacity onPress={() =>prop.rightClick()} >
            <Text style={styles.textocean} >{prop.edit}</Text>            
           </TouchableOpacity>
           :
            <View style={prop.sport?styles.Setting:null} >
            <TouchableOpacity  onPress={() =>prop.rightClick()} >
                <Image source={prop.leftimg} resizeMode= "contain" style={{ height:prop.chat?30:20, width:prop.chat?30:20, }} />
                </TouchableOpacity></View>}
            </View>
            </SafeAreaView>,
            headerTransparent: prop.headerTransparent||false,
        })
       
    )
}
const styles = StyleSheet.create({
  text:{fontFamily:fonts.RobotoRegular,color:Colors.blackColor,},
  textocean:{fontFamily:fonts.RobotoMedium,color:Colors.Ocean,fontSize:16},
  Setting:{elevation:5, backgroundColor:'#fff',paddingHorizontal:5,paddingVertical:5,borderRadius:20}
});








