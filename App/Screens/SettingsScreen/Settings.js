import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {images} from '../../Assets/ImageUrl';
import styles from './SettingsStyles';
import Header from '../../Common/Header';
import CHeader from '../../Common/CHeader';

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    console.log(this.state, 'Setings', this.props);
    return (
      <SafeAreaView style={styles.safe_area_view}>
        <CHeader navigation={this.props.navigation} backArrowLeft centerItem />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.settings_text_view}>
            <Text style={styles.settings_text}>Settings</Text>
          </View>

          <View style={styles.setting_img_title_arrow_view}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('EditProfile')}
              style={styles.setting_title_img_touch}>
              <View style={styles.image_title_view}>
                <Image
                  resizeMode={'contain'}
                  source={images.account_box_multiple_outline}
                  style={styles.img_style}
                />
                <Text style={styles.title_text}>Edit Profile</Text>
              </View>
              <Image
                resizeMode={'contain'}
                source={images.arrow_next_black}
                style={styles.back_arrow_img}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('PrivacyAndSafety')}
              style={styles.setting_title_img_touch}>
              <View style={styles.image_title_view}>
                <Image
                  resizeMode={'contain'}
                  source={images.information_outline}
                  style={styles.img_style}
                />
                <Text style={styles.title_text}>Privacy and safety</Text>
              </View>
              <Image
                resizeMode={'contain'}
                source={images.arrow_next_black}
                style={styles.back_arrow_img}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('WalletTab')}
              style={styles.setting_title_img_touch}>
              <View style={styles.image_title_view}>
                <Image
                  resizeMode={'contain'}
                  source={images.wallet_outline}
                  style={styles.img_style}
                />
                <Text style={styles.title_text}>Wallet</Text>
              </View>
              <Image
                resizeMode={'contain'}
                source={images.arrow_next_black}
                style={styles.back_arrow_img}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('MyDraft')}
              style={styles.setting_title_img_touch}>
              <View style={styles.image_title_view}>
                <Image
                  resizeMode={'contain'}
                  source={images.plan}
                  style={styles.img_style}
                />
                <Text style={styles.title_text}>My Draft</Text>
              </View>
              <Image
                resizeMode={'contain'}
                source={images.arrow_next_black}
                style={styles.back_arrow_img}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Notifications')}
              style={styles.setting_title_img_touch}>
              <View style={styles.image_title_view}>
                <Image
                  resizeMode={'contain'}
                  source={images.bell_outline}
                  style={styles.img_style}
                />
                <Text style={styles.title_text}>Notification</Text>
              </View>
              <Image
                resizeMode={'contain'}
                source={images.arrow_next_black}
                style={styles.back_arrow_img}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Reportaproblem')}
              style={styles.setting_title_img_touch}>
              <View style={styles.image_title_view}>
                <Image
                  resizeMode={'contain'}
                  source={images.report_ic}
                  style={styles.img_style}
                />
                <Text style={styles.title_text}>Report a Problem</Text>
              </View>
              <Image
                resizeMode={'contain'}
                source={images.arrow_next_black}
                style={styles.back_arrow_img}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('HelpCenter')}
              style={styles.setting_title_img_touch}>
              <View style={styles.image_title_view}>
                <Image
                  resizeMode={'contain'}
                  source={images.question_circle}
                  style={styles.img_style}
                />
                <Text style={styles.title_text}>Help Center</Text>
              </View>
              <Image
                resizeMode={'contain'}
                source={images.arrow_next_black}
                style={styles.back_arrow_img}
              />
            </TouchableOpacity>

            <TouchableOpacity style={styles.setting_title_img_touch}>
              <View style={styles.image_title_view}>
                <Image
                  resizeMode={'contain'}
                  source={images.shield_alt}
                  style={styles.img_style}
                />
                <Text style={styles.title_text}>Safety Center</Text>
              </View>
              <Image
                resizeMode={'contain'}
                source={images.arrow_next_black}
                style={styles.back_arrow_img}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Termsofuse')}
              style={styles.setting_title_img_touch}>
              <View style={styles.image_title_view}>
                <Image
                  resizeMode={'contain'}
                  source={images.information_outline}
                  style={styles.img_style}
                />
                <Text style={styles.title_text}>
                  Terms, Policies and Guidlines
                </Text>
              </View>
              <Image
                resizeMode={'contain'}
                source={images.arrow_next_black}
                style={styles.back_arrow_img}
              />
            </TouchableOpacity>

            <TouchableOpacity style={styles.setting_title_img_touch}>
              <View style={styles.image_title_view}>
                <Image
                  resizeMode={'contain'}
                  source={images.comment_account_outline}
                  style={styles.img_style}
                />
                <Text style={styles.title_text}>Connected accounts</Text>
              </View>
              <Image
                resizeMode={'contain'}
                source={images.arrow_next_black}
                style={styles.back_arrow_img}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.reset({
                  index: 0,
                  routes: [{name: 'Login'}],
                });
              }}
              // this.props.navigation.navigate('')}
              style={styles.setting_title_img_touch}>
              <View style={styles.image_title_view}>
                <Image
                  resizeMode={'contain'}
                  source={images.signout}
                  style={styles.img_style}
                />
                <Text style={styles.title_text}>Sign out</Text>
              </View>
              <Image
                resizeMode={'contain'}
                source={images.arrow_next_black}
                style={styles.back_arrow_img}
              />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default Settings;
