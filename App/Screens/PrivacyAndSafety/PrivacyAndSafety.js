import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {images} from '../../Assets/ImageUrl';
import styles from './PrivacyAndSafetyStyles';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';
import Header from '../../Common/Header';
import fonts from '../../Assets/Fonts';
import {Icons} from '../../Component/Common';
import CHeader from '../../Common/CHeader';

class PrivacyAndSafety extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCameraShow: false,
      editProfileForm: {
        profile_picture: '',
      },
    };
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: Colors.whiteTwo}}>
         <CHeader navigation={this.props.navigation} backArrowLeft centerItem  />

        <View style={{marginHorizontal: 18}}>
          <Text style={styles.textstyle}>Privacy And Safety</Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: fonts.fontSize13}}>
              Allow Others to Find Me
            </Text>
            <Icons
              size={40}
              source={ this.state.findMe ?images.on_toggle : images.off_toggle}
              _style={{marginVertical: 18}}
              onPress={()=>this.setState({findMe:!this.state.findMe})}
            />
          </View>

          <Text
            style={{
              fontFamily: fonts.NovaRegularios,
              fontSize: fonts.fontSize10,
              lineHeight: 15,
              color: Colors.pinkishGreyTwo,
            }}>
            if you disable "Allow Others to Find Me". other users will not
            receive suggestions to follow you
          </Text>

          <Text
            style={{
              fontFamily: fonts.NovaRegularios,
              fontSize: fonts.fontSize10,
              marginTop: 20,
              color: Colors.pinkishGreyTwo,
            }}>
            Safety
          </Text>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: fonts.fontSize13}}>
              Allow followers to send you messages
            </Text>
            <Icons
              size={40}
              source={ this.state.sendMessage ?images.on_toggle : images.off_toggle}
              _style={{marginVertical: 18}}
              onPress={()=>this.setState({sendMessage:!this.state.sendMessage})}
            />
          </View>

          <TouchableOpacity onPress={()=>this.props.navigation.navigate('WhoCanSeeYourFollowers')}
          
          style={[CommonStyle, styles.viewElevation]}>
            <Text
              style={{
                fontFamily: fonts.NovaRegularios,
                fontSize: fonts.fontSize13,
              }}>
              Who can see your followers
            </Text>

            <View style={{flexDirection: 'row'}}>
              <Text style={styles.textstyle2}>Everyone</Text>
              <Icons
                size={12}
                source={images.arrow_next_black}
                _style={{marginLeft: 5}}
              />
            </View>
          </TouchableOpacity>

          <View style={[CommonStyle, styles.viewElevation]}>
            <Text>Who can see who you're following</Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.textstyle2}>Friends Only</Text>
              <Icons
                size={12}
                source={images.arrow_next_black}
                _style={{marginLeft: 5}}
              />
            </View>
          </View>

          <View style={[CommonStyle, styles.viewElevation]}>
            <Text>Blocked Users</Text>
            <View style={{flexDirection: 'row'}}>
              {/* <Text
                style={{
                  fontFamily: fonts.NovaRegularios,
                  fontSize: fonts.fontSize10,
                }}>
                Everyone
              </Text> */}
              <Icons size={12} source={images.arrow_next_black} />
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default PrivacyAndSafety;
