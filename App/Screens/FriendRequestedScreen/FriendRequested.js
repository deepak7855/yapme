import React, {Component} from 'react';
import {
  SafeAreaView,
  TextInput,
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import styles from './FriendRequestedStyles';
// import Header from '../../Common/Header';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';
import {GButton} from '../../Common/GButton';
import {images} from '../../Assets/ImageUrl';
import CHeader from '../../Common/CHeader';
import {Icons} from '../../Component/Common';
import fonts from '../../Assets/Fonts';

class FriendRequested extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arrFriendRequest: [
        {username: 'Braulio'},
        {username: 'Jayda'},
        {username: 'Crawford'},
        {username: 'Genevieve'},
        {username: 'Ignatius'},
        {username: 'Ophelia'},
        {username: 'Ryleigh'},
        {username: 'Juanita'},
        {username: 'Jalon'},
      ],
    };
  }

  _renderFriendRequestList = ({item, index}) => {
    return (
      <View style={styles.friend_req_view}>
        <View style={styles.img_name_view}>
          <View style={styles.user_img_view}>
            <Image
              source={images.Profile}
              resizeMode={'cover'}
              style={styles.user_img}
            />
          </View>
          <View style={styles.name_view}>
            <Text numberOfLines={1} style={styles.name_text}>
              {item.username}
            </Text>
          </View>
        </View>

        <View style={styles.btn_view}>
          <TouchableOpacity style={styles.accept_btn_touch}>
            <Text style={styles.accept_text}>Accept</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.reject_btn_touch}>
            <Text style={styles.reject_text}>Reject</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };
  render() {
    return (
      <SafeAreaView style={styles.safe_area_view}>
        {/* <Header
                    backArrowLeft
                    centerImage
                /> */}
        <CHeader navigation={this.props.navigation} backArrowLeft centerItem>
          <View
            style={{
              flex: 1,
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
              paddingRight: 8,
            }}>
            <View style={{alignItems: 'center', flexDirection: 'row'}}>
              <Text
                style={{
                  fontFamily: fonts.NovaRegularios,
                  fontSize: fonts.fontSize10,
                  color: Colors.brownishGrey,
                }}>
                Friend Request
              </Text>

              <Icons
                size={15}
                source={images.dots_gray}
                _style={{marginHorizontal: 6}}
                //   onPress={()=>this.props.navigation.navigate('FriendRequested')}
              />
            </View>
          </View>
        </CHeader>

        <View style={styles.notification_view}>
          <Text style={styles.notification_text}>Notification</Text>
        </View>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={this.state.arrFriendRequest}
          renderItem={this._renderFriendRequestList}
          extraData={this.state}
          keyExtractor={(item, index) => index.toString()}
        />
      </SafeAreaView>
    );
  }
}

export default FriendRequested;
