import {ScreenRatio} from '../Utility/ScreenRatio';

const fonts = {
  fontSize10: ScreenRatio(2.7), //10,
  fontSize13: ScreenRatio(3.5), //13,
  fontSize15: ScreenRatio(4), //15
  fontSize16: ScreenRatio(4.1), //15
  fontSize17: ScreenRatio(4.5), //17
  fontSize20: ScreenRatio(5), //18.5
  fontSize24: ScreenRatio(6), //18.5
  fontSize26: ScreenRatio(6.5), //18.5

  // NovaBold: 'FontsFree-Net-Proxima-Nova-Bold',
  // NovaLight: 'FontsFree-Net-Proxima-Nova-Light',
  // NovaMedium: 'FontsFree-Net-Proxima-Nova-Medium',
  // NovaRegular: 'FontsFree-Net-Proxima-Nova-Regular',
  // NovaSbold: 'FontsFree-Net-Proxima-Nova-Sbold',
  // NovaXbold: 'FontsFree-Net-Proxima-Nova-Xbold',


  NovaBold: 'ProximaNova-Bold',
  NovaLight: 'ProximaNova-Light',
  NovaMedium: 'ProximaNova-Medium',
  NovaRegular: 'ProximaNova-Regular',
  NovaSbold: 'ProximaNova-Semibold',
  NovaXbold: 'ProximaNova-Extrabld',


  //    for Ios

  NovaBoldios: 'ProximaNova-Bold',
  NovaLightios: 'ProximaNova-Light',
  NovaMediumios: 'ProximaNova-Medium',
  NovaRegularios: 'ProximaNova-Regular',
  NovaSboldios: 'ProximaNova-Semibold',
  NovaXboldios: 'ProximaNova-Extrabld',
};
export default fonts;
