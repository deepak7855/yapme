import React, {Component} from 'react';
import {
  View,
  Text,
  StatusBar,
  ScrollView,
  SafeAreaView,
  Image,
  FlatList,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import CHeader from '../../Common/CHeader';
import {Icons, Searchbar} from '../../Component/Common';
import {ScreenRatio, ScreenWidth} from '../../Utility/ScreenRatio';

export default class Addmusic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [
        {image: images.Profile, titile: 'Girls like You'},
        {image: images.Profile, titile: 'Shape like You'},
        {image: images.Profile, titile: 'Mircale'},
        {image: images.Profile, titile: 'Dance Beats'},
        {image: images.Profile, titile: 'Dangeurous'},
        {image: images.Profile, titile: 'Friends'},
      ],
    };
  }

  Onclick = (index) => {
    this.setState({itemKey: index});
  };

  _renderList = ({item, index}) => {
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            this.Onclick(index);
          }}
          style={[CommonStyle, Styles.listItem]}>
          <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
            <View>
              <Image style={Styles.Imageitem} source={item.image}></Image>
            </View>
            <View style={{marginHorizontal: 10}}>
              <Text style={[Styles.MSRP, {fontFamily: fonts.NovaBoldios}]}>
                {item.titile}
              </Text>
              <Text style={[Styles.Samoll]}>00:30</Text>
            </View>
          </View>
          <View style={Styles.itemFirst}>
            <View style={{marginHorizontal: 20}}>
              <Image
                resizeMode={'contain'}
                style={Styles.Itemplay}
                source={images.play_gray_sm}
              />
            </View>
            <View>
              <Image
                resizeMode={'contain'}
                style={{width: 15, height: 15}}
                source={images.heart_gray}
              />
            </View>
          </View>
        </TouchableOpacity>

        {this.state.itemKey == index && (
          <View style={Styles.listItem1}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                marginTop: 0,
                justifyContent: 'space-between',
              }}>
              <View
                style={{
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  flex: 1,
                }}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Image style={Styles.Imageitem} source={item.image} />
                  <View style={{marginLeft: 10}}>
                    <Text
                      style={[Styles.MSRP1, {fontFamily: fonts.NovaBoldios}]}>
                      {item.titile}
                    </Text>
                    <Text style={[Styles.Samoll1]}>00:30</Text>
                  </View>
                </View>

                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Image
                    resizeMode={'contain'}
                    style={Styles.Itemplay}
                    source={images.pause_icon}
                  />
                  <Image
                    resizeMode={'contain'}
                    style={{width: 15, height: 15, marginLeft: 10}}
                    source={images.heart_white}
                  />
                </View>
              </View>
            </View>

            <View
              style={{
                backgroundColor: '#fff',
                borderRadius: 5,
                marginVertical: 10,
              }}>
              <View
                style={{
                  marginHorizontal: 20,
                  paddingVertical: 8,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  style={Styles.ImageitemColor}
                  resizeMode={'contain'}
                  source={images.video_icon_sm}></Image>
                <Text
                  style={[
                    Styles.MSRP1,
                    {color: '#991AE7', marginLeft: 10, fontSize: 14},
                  ]}>
                  Confirm And Start To Shoot
                </Text>
              </View>
            </View>
          </View>
        )}
      </View>
    );
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <StatusBar
          backgroundColor={Colors.violetBlue}
          barStyle="dark-content"
        />
        <SafeAreaView />
        <CHeader navigation={this.props.navigation} backArrowLeft centerItem>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
              paddingRight: 20,
            }}>
            <Icons
              size={18}
              source={images.spotify_green}
              _style={{marginRight: 15}}
            />

            <Icons
              size={15}
              source={images.cross}
              _style={{tintColor: Colors.vividPurple, alignSelf: 'flex-end'}}
              onPress={() => this.props.navigation.goBack(null)}
            />
          </View>
        </CHeader>
        {/* <ScrollView showsVerticalScrollIndicator={false}> */}

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            // backgroundColor: Colors.lightgrey,
          }}>
          <TouchableOpacity
            onPress={() => {
              // this.setState({
              //   index: 0,
              //   index_one_filled: false,
              //   index_two_filled: false,
              // }),
              //   this.viewpager.setPage(0);
            }}
            style={{
              width: '30%',
              borderBottomWidth: 1.5,
              alignItems: 'center',
              borderBottomColor: Colors.vividPurple,
              paddingVertical: 13,
            }}>
            <Text
              style={{
                fontSize: fonts.fontSize16,
                fontFamily: fonts.NovaBoldios,
                color: Colors.vividPurple,
                marginTop: 10,
              }}>
              Trending
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              // this.setState({index: 1, index_one_filled: true}),
              //   this.viewpager.setPage(1);
            }}
            style={{
              width: '30%',
              // borderBottomWidth: this.state.index_one_filled ? null : 1,
              borderBottomWidth: 1.5,
              alignItems: 'center',
              borderBottomColor: Colors.greyColor,
              paddingVertical: 13,
            }}>
            <Text
              style={{
                fontSize: fonts.fontSize16,
                fontFamily: fonts.NovaBoldios,
                color: Colors.pinkishGrey,
                marginTop: 10,
              }}>
              My Favorites
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            // disabled={this.state.index == 0}
            // onPress={() => {
            //   this.setState({index: 2, index_two_filled: true}),
            //     this.viewpager.setPage(2);
            // }}
            style={{
              width: '30%',
              borderBottomWidth: 1.5,
              alignItems: 'center',
              borderBottomColor: Colors.greyColor,
              paddingVertical: 13,
            }}>
            <Text
              style={{
                fontSize: fonts.fontSize16,
                fontFamily: fonts.NovaXboldios,
                marginTop: 10,
                color: Colors.pinkishGrey,
              }}>
              Local Music
            </Text>
          </TouchableOpacity>
        </View>

        <View style={{marginVertical: 20}}>
          <Searchbar placeholder="Search post,videos,friends, and , messages" />

          <FlatList
            data={this.state.dataSource}
            contentContainerStyle={{marginTop: 12}}
            extraData={this.state}
            renderItem={this._renderList}
            keyExtractor={(index) => {
              index.toString();
            }}
          />
        </View>
        {/* </ScrollView> */}
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  contviewlist: {
    width: ScreenWidth(100) / 3,
    marginVertical: ScreenRatio(1.5),
    flexDirection: 'row',
    alignItems: 'center',
  },
  MSRP: {
    fontFamily: fonts.SemiBoldfont,
    fontSize: fonts.fontSize13,
    color: Colors.black,
  },
  Samoll: {
    fontFamily: fonts.Regularfont,
    fontSize: 9,
    color: Colors.black,
    marginTop: 5,
  },
  listItem: {
    marginHorizontal: 10,
    marginVertical: 5,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 7,
    borderRadius: 10,
    backgroundColor: '#fff',
    elevation: 5,
  },
  listItem1: {
    margin: 10,
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 7,
    borderRadius: 10,
    backgroundColor: '#991AE7',
    elevation: 5,
  },
  MSRP1: {
    fontFamily: fonts.SemiBoldfont,
    fontSize: fonts.fontSize13,
    color: Colors.white,
  },
  Samoll1: {
    fontFamily: fonts.Regularfont,
    fontSize: 9,
    color: Colors.white,
    marginTop: 5,
  },

  Imageitem: {width: 40, height: 40, borderRadius: 40 / 2},
  ImageitemColor: {width: 15, height: 15},

  itemFirst: {
    flex: 0.5,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    // marginBottom: 20,
  },
  Itemplay: {width: 15, height: 15},
});
