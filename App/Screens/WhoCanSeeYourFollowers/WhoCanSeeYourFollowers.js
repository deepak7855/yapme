import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import styles from './WhoCanSeeYourFollowersStyles';
import Header from '../../Common/Header';
import {images} from '../../Assets/ImageUrl';
import CHeader from '../../Common/CHeader';

class WhoCanSeeYourFollowers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arrFollowersSee: [
        {id: 1, seeOption: 'Everyone'},
        {id: 2, seeOption: 'Followers and Friends Only'},
        {id: 3, seeOption: 'Friends Only'},
        {id: 4, seeOption: 'No One'},
      ],
    };
  }
  onPressRadio = (item) => {
    this.setState({radio: item.id});
  };

  _SeeFollowersList = ({item, index}) => {
    return (
      <View style={styles.list_followers_view}>
        <TouchableOpacity
          onPress={() => this.onPressRadio(item)}
          style={styles.list_item_touch}>
          <Text style={styles.see_option}>{item.seeOption}</Text>
          <Image
            source={
              this.state.radio == item.id
                ? images.tick_circle
                : images.radio_circle
            }
            style={styles.radio_img}
          />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={styles.safe_area_view}>
        {/* <Header
                    backArrowLeft
                    centerImage
                /> */}
        <CHeader navigation={this.props.navigation} backArrowLeft centerItem />
        <View style={styles.can_see_followers_view}>
          <Text style={styles.can_see_followers_text}>
            Who can see your followers
          </Text>
        </View>

        <FlatList
          showsVerticalScrollIndicator={false}
          data={this.state.arrFollowersSee}
          renderItem={this._SeeFollowersList}
          extraData={this.state}
          keyExtractor={(item, index) => index.toString()}
        />
      </SafeAreaView>
    );
  }
}

export default WhoCanSeeYourFollowers;
