import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  Platform,
  TextInput,
} from 'react-native';
import {images} from '../../Assets/ImageUrl';
import styles from './SiginupStyles';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';
import {GButton} from '../../Common/GButton';
import {Input} from '../../Common/InputCommon';
import fonts from '../../Assets/Fonts';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {debounce} from 'lodash';
import {FacebookLogin, googleLogin} from '../../Lib/SocialLogin';

let os = Platform.OS;

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ischeck: false,
      isUserexist: false,
      signupform: {},
    };
  }

  onPressSignup() {
    this.props.navigation.reset({
      index: 0,
      routes: [{name: 'TabBar'}],
    });
  }

  _checkUserExist(value, whichinput) {
    this.props.userSignup(value, whichinput);
  }

  gotoFacebookSocialSide = () => {
    // FacebookLogin((result) => {
    //   console.log(result);

    //   // if (result) {
    //   //   Helper.showLoader();
    //   //   this.callSocialLogin(result);
    //   // }
    // });
  };

  gotoGoogleSocialSide = () => {
    // googleLogin((result) => {
    //   console.log(result);
    //   // if (result) {
    //   //   Helper.showLoader();
    //   //   this.callSocialLogin(result);
    //   // }
    // });
  };

  render() {
    const {isUserexist} = this.props;
    // console.log(this.props.isUserexist);

    return (
      <ImageBackground
        source={images.login_image_email}
        style={styles.image_back_ground}
        resizeMode={'stretch'}>
        <SafeAreaView style={styles.safe_area_view} />

        <KeyboardAwareScrollView
          bounces={false}
          showsVerticalScrollIndicator={false}>
          <View style={[styles.create_view, {marginTop: 150}]}>
            <Text style={styles.create_account_text}>CREATE ACCOUNT</Text>
          </View>

          <View style={styles.fill_text_view}>
            <Text style={styles.fill_profile_text}>
              Fill your profile details
            </Text>
          </View>

          <View style={styles.text_input_view}>
            <Input
              labelText={'Username'}
              start={' *'}
              // rightImagePath={
              //   isUserexist.username ? images.tick : images.close_pink
              // }
              //   placeholder={'eleMar'}
              //   customeImageStyle={{marginBottom:2}}
              keyboardType={'default'}
              onChangeText={debounce((text) => {
                this.setState({Username: text}, () =>
                  this._checkUserExist(text, 1),
                );
              }, 10)}
              returnKeyType={'next'}
              setFocus={() => this.email.focus()}
            />
            <Text style={styles.nosymbol}>
              Min. 6 Characters with no symbols
            </Text>

            <Input
              labelText={'Email Address'}
              start={' *'}
              //   placeholder={'ciafab811r@gmail.com'}
              // rightImagePath={
              //   isUserexist.email ? images.tick : images.close_pink
              // }
              keyboardType={'email-address'}
              onChangeText={debounce((text) => {
                this._checkUserExist(text, 2);
              }, 10)}
              getFocus={(ref) => (this.email = ref)}
              returnKeyType={'next'}
              setFocus={() => this.password.focus()}
            />

            <View
              style={styles.View1}>
              <View
                style={styles.View2}>
                <Text style={styles.code_text}>
                  {this.state.countryCode ? this.state.countryCode : '+ 1'}
                </Text>
                <Image
                  resizeMode={'contain'}
                  source={images.down_arrow}
                  style={styles.arrow_img}
                />
              </View>

              <View style={{width: 20}} />

              <View style={{width: null,  flex: 1,}}>
                <Text
                  style={styles.View3}>
                  Phone Number
                </Text>
                <TextInput style={styles.phoneInput}
                 keyboardType="phone-pad"
                  returnKeyType="done"
                 />
              </View>
            </View>

            <Input
              secureTextEntry={this.state.passwordEye}
              labelText={'Password'}
              //   placeholder={'password'}
              rightImagePath={
                this.state.passwordEye ? images.Eye_closed : images.eye
              }
              onPress={() =>
                this.setState({passwordEye: !this.state.passwordEye})
              }
              keyboardType={'default'}
              getFocus={(ref) => (this.password = ref)}
              returnKeyType={'next'}
              setFocus={() => this.confirmpassword.focus()}
            />
            <Input
              secureTextEntry={this.state.confirmpassword}
              labelText={'Confirm Password'}
              //   placeholder={'…………'}
              rightImagePath={
                this.state.confirmpassword ? images.Eye_closed : images.eye
              }
              onPress={() =>
                this.setState({confirmpassword: !this.state.confirmpassword})
              }
              keyboardType={'default'}
              getFocus={(ref) => (this.confirmpassword = ref)}
              returnKeyType={'done'}
            />
          </View>

          <View style={styles.terms_text_view}>
            <Text style={styles.agree_text}>
              I agree with the
              <Text style={styles.terms_text}> Terms and Conditions </Text> and
              <Text style={styles.privacy_policy_text}> Privacy Policy.</Text>
            </Text>
          </View>

          <View style={styles.submit_btn_view}>
            <GButton
              height={50}
              width={200}
              radius={5}
              lineHeight={15}
              txtcolor={Colors.white}
              fontFamily={Fonts.NovaBold}
              fontSize={Fonts.fontSize13}
              onPress={() => this.onPressSignup()}
              bText={'SIGN UP'}
            />
          </View>

          <View style={styles.social_account_view}>
            <Text style={styles.social_account_text}>
              Connect With Social Account
            </Text>
          </View>

          <View style={styles.appleiocns}>
            {os == 'ios' && (
              <TouchableOpacity style={styles.socicalView}>
                <Image
                  resizeMode={'contain'}
                  source={images.apple_icon}
                  style={styles.socicalIcons}
                />
              </TouchableOpacity>
            )}

            <TouchableOpacity
              onPress={() => this.gotoGoogleSocialSide()}
              style={styles.socicalView}>
              <Image
                resizeMode={'contain'}
                source={images.google_hangouts}
                style={styles.socicalIcons}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.gotoFacebookSocialSide()}
              style={styles.socicalView}>
              <Image
                resizeMode={'contain'}
                source={images.facebook}
                style={styles.socicalIcons}
              />
            </TouchableOpacity>

            {/* <TouchableOpacity style={styles.socicalView}>
              <Image
                resizeMode={'contain'}
                source={images.instagram_sketched}
                style={styles.socicalIcons}
              />
            </TouchableOpacity> */}
          </View>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Login')}
            style={styles.already_text_view}>
            <Text style={styles.already_text}>
              Already have an account?
              <Text style={styles.already_sing_in}> SIGN IN</Text>
            </Text>
          </TouchableOpacity>
        </KeyboardAwareScrollView>
        <SafeAreaView />
      </ImageBackground>
      //   {/* </SafeAreaView> */}
    );
  }
  setValues(key, value) {
    this.setState((prevState) => ({
      signupform: {
        ...prevState.signupform,
        [key]: value,
      },
    }));
  }
}


export default Signup;
