import { StyleSheet } from 'react-native';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';


export default CommunityGuidelinesStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
    },
    Community_view: {
        marginHorizontal: 13,
        marginVertical: 5
    },
    Community_text: {
        lineHeight: 20,
        fontSize: Fonts.fontSize16,
        color: Colors.black,
        fontFamily: Fonts.NovaBold
    },
    guidelines_view: {
        marginHorizontal: 13,
        marginVertical: 5
    },
    guidelines_text: {
        fontSize: Fonts.fontSize13,
        lineHeight: 17,
        color: Colors.black,
        fontFamily: Fonts.NovaRegular
    },
    guidelines_text_view: {
        marginHorizontal: 13,
        marginVertical: 5
    },
    guidelines_text_des: {
        fontFamily: Fonts.NovaRegular,
        lineHeight: 20,
        textAlign: 'left',
        color: Colors.pinkishGreyTwo,
        fontSize: Fonts.fontSize13
    },

});