import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import styles from './TermsofuseStyles';
import Header from '../../Common/Header';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';
import {images} from '../../Assets/ImageUrl';
import CHeader from '../../Common/CHeader';
import {Custompopup} from '../../Component/Modal/CustomModel';
import fonts from '../../Assets/Fonts';
import {Icons} from '../../Component/Common';

class Termsofuse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSelectedMusic: false,
      isSelectedVideo: false,
      modalVisible: false,
    };
  }

  onChangeMusic = () => {
    this.setState({isSelectedMusic: !this.state.isSelectedMusic});
  };

  onChangeVideo = () => {
    this.setState({isSelectedVideo: !this.state.isSelectedVideo});
  };

  onMusicPress = () => {
    this.setState({modalVisible: true, musicPopup: true, videoPopup: false});
  };
  onVideoPress = () => {
    this.setState({modalVisible: true, videoPopup: true, musicPopup: false});
  };

  render() {
    return (
      <SafeAreaView style={styles.safe_area_view}>
        {/* <Header
                {...this}
                    backArrowLeft
                    centerImage
                /> */}
        <CHeader navigation={this.props.navigation} backArrowLeft centerItem />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.terms_view}>
            <Text style={styles.terms_text}>Community Guidelines</Text>
          </View>
          <View style={styles.des_text_view}>
            <Text style={styles.dummy_text}>Guidelines</Text>
          </View>
          <View style={styles.des_text_view}>
            <Text style={styles.guidelines_text_des}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letraset sheets
              containing Lorem Ipsum passages, and more recently with desktop
              publishing software like Aldus PageMaker including versions of
              Lorem Ipsum.
            </Text>
          </View>

          <View style={styles.des_text_view}>
            <Text style={styles.guidelines_text_des}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley.
            </Text>
          </View>

          {/* <View style={styles.music_view}>
            <View style={styles.music_text_View}>
              <TouchableOpacity onPress={() => this.onChangeMusic()}>
                <Image
                  resizeMode={'contain'}
                  source={
                    this.state.isSelectedMusic
                      ? images.check_square_black
                      : images.check_square
                  }
                  style={styles.check_box_img}
                />
              </TouchableOpacity>
              <TouchableOpacity onPress={this.onMusicPress}>
                <Text style={styles.yapme_text}>
                  I accept the Terms of YAPME's
                  <Text style={styles.music_video_text}>  music regulations</Text>
                </Text>
              </TouchableOpacity>
            </View>
          </View> */}

          <View style={styles.video_view}>
            {/* <View style={styles.video_text_view}>
              <TouchableOpacity onPress={() => this.onChangeVideo()}>
                <Image
                  resizeMode={'contain'}
                  source={
                    this.state.isSelectedVideo
                      ? images.check_square
                      : images.check_square_black
                  }
                  style={styles.check_box_img}
                />
              </TouchableOpacity>

              <TouchableOpacity onPress={this.onVideoPress}>
                <Text style={styles.yapme_text}>
                  I accept the Terms of YAPME's
                  <Text style={styles.music_video_text}>  video regulations</Text>
                </Text>
              </TouchableOpacity>
            </View> */}

            <Custompopup modalVisible={this.state.modalVisible}>
              <View
                style={{
                  flex: 1,
                  backgroundColor: 'rgba(0,0,0,0.6)',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <View
                  style={{
                    backgroundColor: Colors.white,
                    margin: 10,
                    borderRadius: 8,
                  }}>
                  <View>
                    <Image
                      source={images.yapme_lg}
                      resizeMode="contain"
                      style={{height: 60, width: 140, alignSelf: 'center'}}
                    />
                  </View>
                  <View
                    style={{borderWidth: 1, borderColor: Colors.lightgrey}}
                  />
                  <View style={{padding: 25}}>
                    {this.state.videoPopup ? (
                      <Text
                        style={{
                          fontFamily: fonts.NovaRegularios,
                          lineHeight: 18,
                        }}>
                        I acknowledge that the video being uploaded does not
                        have any child pornography, sexual/violent conent,
                        assault, discrimination, drug use, abuse or any
                        copyright infrigement and may result in my account being
                        banned / suspended and be reported to law enforcement.
                      </Text>
                    ) : (
                      <Text
                        style={{
                          fontFamily: fonts.NovaRegularios,
                          lineHeight: 18,
                        }}>
                        I acknowledge that the music used in this video is not
                        of any copyrighted material and /or have permission from
                        the necessary rights holder (publishing/master recording
                        rights) if this box is unticked the audio in this video
                        will be removed.
                      </Text>
                    )}
                    {/* <Text
                      style={{
                        fontFamily: fonts.NovaRegularios,
                        lineHeight: 18,
                      }}>
                      {true?{`I acknowledge that the music used in this video is not of
                      any copyrighted material and /or have permission from the
                      necessary rights holder (publishing/master recording
                      rights) if this box is unticked the audio in this video
                      will be removed.`}
                    :{`I acknowledge that the video being uploaded does 
not have any child pornography, sexual/violent 
conent, assault, discrimination, drug use, abuse or 
any copyright infrigement and may result in my 
account being banned / suspended and be 
reported to law enforcement.`}}
</Text> */}
                  </View>
                </View>
                <Icons
                  size={55}
                  source={images.share_blue}
                  onPress={() => this.setState({modalVisible: false})}
                />
              </View>
            </Custompopup>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default Termsofuse;
