import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Image,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import {GButton} from '../../Common/GButton';
import {Input, InputText} from '../../Common/InputCommon';
import {Icons} from '../../Component/Common';
export default class Starts extends Component {
  render() {
    return (
      <ScrollView bounces={false} style={{
        // color: Colors.whiteTwo,
         marginHorizontal: 15,marginBottom:20}}
         showsVerticalScrollIndicator={false}
         >
        <View style={{alignItems: 'center'}}>
          <Icons
            size={40}
            source={images.star_lg}
            _style={{marginVertical: 10,marginTop:30}}
          />
          <Text style={styles.balance}>Current Stars</Text>
          <GButton
            height={50}
            width={200}
            radius={5}
            //   lineHeight={15}
            txtcolor={Colors.white}
            fontFamily={fonts.NovaXbold}
            fontSize={fonts.fontSize13}
            _style={{alignSelf: 'center', marginTop: 20}}
            bText={'Withdraw'}
            onPress={()=>this.props.navigation.navigate('Paymentpassword')}
          />
        </View>

        <View
          style={{
            flexDirection: 'row',
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            backgroundColor: '#fff',
            shadowOpacity: 0.11,
            shadowRadius: 1.0,
            elevation: 3,
            alignItems: 'center',
            paddingHorizontal: 11,
            paddingVertical: 15,
            borderRadius: 5,
            justifyContent: 'space-between',
            marginVertical: 25,
            marginHorizontal:5,
            borderWidth: 0.3,
            borderColor: Colors.lightgrey,
          }}>
          <TouchableOpacity onPress={()=>this.props.navigation.navigate('Paymentpassword')}
          style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icons size={20} source={images.star_lg} _style={{margin: 10}} />

            <Text
              style={{
                fontSize: fonts.fontSize13,
                fontFamily: fonts.NovaBoldios,
              }}>
              Exchange Stars to Diamonds
            </Text>
          </TouchableOpacity>

          <View>
            <Icons
              size={12}
              source={images.arrow_next_black}
              _style={{margin: 15}}
            />
          </View>
        </View>

        <Text style={{fontFamily: fonts.NovaBoldios}}>
          Set password for exchange rewards and diamonds
        </Text>
        <Text
          style={{
            fontFamily: fonts.NovaRegularios,
            fontSize: fonts.fontSize13,
            marginTop: 10,
            lineHeight: 18,
          }}>
          User must set a pin to exchange and withdraw. if you have forgotten
          your pin. Please contact via the report a problem page.
        </Text>


        <Text style={{fontFamily: fonts.NovaBoldios,marginVertical:18}}>
        What are stars
        </Text>


        <Text style={{fontFamily: fonts.NovaRegularios}}>
        STARS are earnt from virtual gifts
        </Text>

        <Text style={{fontFamily: fonts.NovaBoldios,marginVertical:18}}>
        Exchange Rules
        </Text>


        <Text style={{fontFamily:fonts.NovaRegularios}}>
        1. Withdraw starts from 200 stars and no more then 2000 stars per single withdrawal.
        </Text>

<Text style={{marginTop:20,fontFamily:fonts.NovaRegularios}}>2. Withdrawal is permitted once a week</Text>

      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  safe_area_view: {
    flex: 1,
  },
  balance: {fontFamily: fonts.NovaRegularios, fontSize: fonts.fontSize13},
  totalcoin: {
    fontFamily: fonts.NovaBoldios,
    fontSize: fonts.fontSize17,
    marginTop: 12,
  },
  safe_area_view: {
    flex: 1,
  },
  notification_view: {
    marginHorizontal: 15,
    marginVertical: 5,
  },
  friend_req_view: {
    borderRadius: 5,
    marginVertical: 6,
    height: 60,
    width: 105,
    // paddingVertical: 10,
    borderWidth: 1,
    borderColor: Colors.white,
    marginHorizontal: 15,
    // flexDirection: 'row',
    // justifyContent: 'space-between',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.15,
    shadowRadius: 1.94,
    elevation: 3,
  },
  img_name_view: {
    // flexDirection: 'row',
    // alignItems: 'center',
    flex: 4,
  },
  user_img_view: {
    flex: 3,
    justifyContent: 'space-evenly',
    // height: 30,
    // width: 30,
    // borderRadius: 30 / 2,
  },
  user_img: {
    width: '100%',
    height: '100%',
  },
  name_view: {
    // width: 120,
    // marginLeft: 10,
  },
  name_text: {
    // lineHeight: 17,
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaRegularios,
    color: Colors.black,
  },
  name_text2: {
    // lineHeight: 17,
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaRegularios,
    color: Colors.pinkishGreyTwo,
  },
  btn_view: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 0.7,
    // backgroundColor: 'red',
    justifyContent: 'flex-end',
  },
  accept_btn_touch: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    paddingHorizontal: 13,
    paddingVertical: 6,
    borderColor: Colors.violetBlue,
    borderWidth: 1,
  },
  accept_text: {
    fontSize: fonts.fontSize10,
    color: Colors.violetBlue,
    fontFamily: fonts.NovaSboldios,
  },
  reject_btn_touch: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 59,
    height: 22,
    borderColor: Colors.violetBlue,
    borderRadius: 5,
    borderWidth: 1,
  },
  reject_text: {
    //   fontsize: fonts.fontsize10,
    // lineHeight: 12,
    color: Colors.violetBlue,
    //   fontFamily: fonts.NovaBold,
  },
  formView: {
    // marginVertical: 5,
    // paddingHorizontal: 15,
    // paddingVertical: 5,
  },
  formText: {
    fontFamily: fonts.NovaRegular,
    color: Colors.greyColor,
    marginVertical: 3,
  },
});
