import React, {Component} from 'react';
import {
  Text,
  View,
  TextInput,
  Image,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  StatusBar,
} from 'react-native';
import Colors from '../../Assets/Colors';
import {images} from '../../Assets/ImageUrl';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Icons, Searchbar} from '../../Component/Common';
import ArrData from '../../../dummydata';
import fonts from '../../Assets/Fonts';
import Header from '../../Common/Header';
import CHeader from '../../Common/CHeader';
import {IS_IPHONE_X} from '../../Utility';


export default Group = (props) => {
  return (
    <View style={{}}>
      <StatusBar
          backgroundColor={Colors.violetBlue}
          barStyle="dark-content"
        />
      <CHeader navigation={props.navigation} centerItem />

      <Searchbar placeholder="Search post, videos, friends and messages" />
      <ScrollView style={{marginTop: 10}} showsVerticalScrollIndicator={false}>
        <View style={styles.redView}>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{
              //   paddingBottom: 80,
              // paddingTop:10,
              marginLeft: 10,
              marginTop: 15,
            }}
            data={['', '', '', '', '', '', '', '', '']}
            renderItem={({item: color, index}) => (
              <View style={{}}>
                <View style={styles.view7}>
                  <Icons
                    size={45}
                    source={images.Profile}
                    onPress={() => {
                      props.navigation.navigate(
                        index % 2 == 0 ? 'OtherUserProfile' : 'Viwerdashbord',
                      );
                    }}
                  />
                  <View style={styles.view6}>
                    {/* <Icon
                  name="circle"
                  size={12}
                  color={Colors.green}
                  style={{bottom: 8, left: 10}}
                /> */}
                    {index % 2 == 0 ? (
                      <Icon
                        name="circle"
                        size={12}
                        color={Colors.green}
                        style={{bottom: 8, left: 10}}
                      />
                    ) : (
                      <Icons
                        size={35}
                        source={images.live}
                        _style={{bottom: 0, left: 1}}
                        // onPress={()=>props.navigation.navigate('OtherUserProfile')}
                      />
                    )}
                  </View>

                  {/* {index % 2 == 0 ? (
                <Icon
                  name="circle"
                  size={12}
                  color={Colors.green}
                  style={{bottom: 8, left: 10}}
                />
              ) : (
                <Icons
                  size={35}
                  source={images.live}
                  _style={{bottom: 20, left: 1}}
                />
              )} */}
                </View>
              </View>
            )}
          />
        </View>





        <FlatList
          contentContainerStyle={{
            //   paddingBottom: 80,
            // paddingTop:10,
            paddingHorizontal: 10,
            marginTop: 15,
          }}
          data={ArrData.bgImages}
          renderItem={({item, index}) => (
           <View style={{}}>
              <View style={[styles.view5,{}]}>
                <View style={styles.view4}>
                  <View style={styles.rowCenter}>
                    <Icons
                      size={35}
                      source={images.Profile}
                      _style={{marginRight: 10}}
                      onPress={() =>
                        props.navigation.navigate('OtherUserProfile')
                      }
                    />
                    <View>
                      <Text
                        style={{
                          fontSize: fonts.fontSize13,
                          fontFamily: fonts.NovaBoldios,
                        }}>
                        Maria Susan
                      </Text>
                      <Text style={styles.text5}>10 mint ago</Text>
                    </View>
                  </View>

                  <View style={styles.rowCenter}>
                    <Icons size={20} source={images.active_heart} />

                    <Icons
                      size={20}
                      source={images.comment_blue}
                      _style={{marginHorizontal: 10}}
                    />

                    <Icons size={15} source={images.dots_gray} />
                  </View>
                </View>

                <Image
                  resizeMode="contain"
                  style={{height: 220, width: 320, marginVertical: -10}}
                  source={item.pic1}
                />

                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  contentContainerStyle={
                    {
                      //   paddingBottom: 80,
                      // paddingTop:10,
                      //   marginLeft: 10,
                    }
                  }
                  data={ArrData.bgImages2}
                  renderItem={({item, index}) => (
                    <View style={{flexDirection: 'row'}}>
                      <View style={styles.view3}>
                        <Image
                          resizeMode="contain"
                          style={{height: 45, width: 45}}
                          source={item.pic1}
                        />
                      </View>
                    </View>
                  )}
                />
                <Text style={styles.text3}>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry.
                </Text>
              </View>

              {index == 0 && (
                <View style={styles.view2}>
                  <View style={styles.view1}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Icons
                        size={35}
                        source={images.Profile}
                        _style={{marginRight: 10}}
                      />
                      <View>
                        <Text style={styles.text4}>Albert Wilson</Text>
                        <Text style={styles.text5}>10 mint ago</Text>
                      </View>
                    </View>

                    <View style={styles.rowCenter}>
                      <Icons
                        size={20}
                        source={images.active_heart}
                        // _style={{marginRight: 10}}
                      />

                      <Icons
                        size={20}
                        source={images.comment_blue}
                        _style={{marginHorizontal: 10}}
                      />

                      <Icons
                        size={15}
                        source={images.dots_gray}
                        // _style={{marginRight: 10}}
                      />
                    </View>
                  </View>
                  <Text style={styles.text1}>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </Text>
                </View>
              )}
            </View>
          )}
        />
        <View style={{height:!IS_IPHONE_X ?180:230}} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bgcolor,
  },
  item: {
    margin: 10,
    height: 30,
    borderRadius: 10,
  },
  text1: {
    marginVertical: 13,
    fontSize: fonts.fontSize10,
    fontFamily: fonts.NovaRegularios,
  },
  text2: {
    // marginVertical: 13,
    fontSize: fonts.fontSize10,
    fontFamily: fonts.NovaRegularios,
  },
  view1: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  rowCenter: {flexDirection: 'row', alignItems: 'center'},
  view4: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  view5: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 20,

    shadowColor: '#ddd',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.15,
    shadowRadius: 1.14,
    elevation: 2,
    marginBottom: 15,
  },
  view6: {
    position: 'absolute',
    bottom: -15,
    marginVertical: 5,
  },
  view7: {alignItems: 'center', marginRight: 15, marginTop: 5},

  view2: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 20,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.15,
    // shadowRadius: 1.14,
    elevation: 2,
    marginBottom: 15,
  },
  text3: {
    marginTop: 10,
    fontSize: fonts.fontSize10,
    fontFamily: fonts.NovaRegularios,
  },
  text4: {
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaBoldios,
  },
  text5: {
    fontSize: fonts.fontSize10,
    color: Colors.pinkishGrey,
    marginTop: 3,
  },
  view3: {
    alignItems: 'center',
    marginRight: 8,
    marginTop: 10,
  },
  redView: {
    padding: 10,
    paddingHorizontal: 15,
  },

  grytextStyle: {
    color: Colors.greyColor,
    fontSize: 13,
  },
});
