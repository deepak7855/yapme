import React, {Component} from 'react';
import {
  SafeAreaView,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Button,
  ScrollView,
  FlatList,
} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import CHeader from '../../Common/CHeader';
import {GRoundButton} from '../../Common/GButton';
import {PinkScreen, Icons} from '../../Component/Common';
import {Custompopup} from '../../Component/Modal/CustomModel';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';


const Header = (props) => {
  console.log(props, 'propsss');
  return (
    <SafeAreaView style={styles.headerContainer}>
      <View style={styles.firstView}>
        <TouchableOpacity
          onPress={() => props.navigation.goBack(null)}
          style={{height: 20, width: 20}}>
          <Image
            source={images.back_arrow}
            resizeMode={'contain'}
            style={{height: '100%', width: '100%'}}
          />
        </TouchableOpacity>
      </View>

      <View style={styles.centerView}>
        <View style={{}}>
          {!props.fromWhere ? (
            <Text
              style={{fontFamily: fonts.NovaBold, fontSize: fonts.fontSize16}}>
              Amanda Cox
            </Text>
          ) : (
            <>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icons
                  source={images.Profile}
                  size={25}
                  _style={{marginRight: 8}}
                  onPress={() => {}}
                />
                <Text
                  style={{
                    fontFamily: fonts.NovaBoldios,
                    fontSize: fonts.fontSize16,
                  }}>
                  Fun Group
                </Text>
              </View>
              <Text
                style={{
                  fontSize: fonts.fontSize10,
                  fontFamily: fonts.NovaRegular,
                  marginTop: 7,
                  // marginLeft:10
                }}>
                Lorem Ipsum is simply
              </Text>
            </>
          )}
        </View>
      </View>
      <View style={styles.thiredView}>
        <Icons
          source={images.video_icon_sm}
          size={15}
          _style={{marginRight: 18}}
          // onPress={() => {}}
          onPress={() =>props.navigation.navigate('VideoCall') }
        />

        <Icons
          source={images.phonealt}
          size={15}
          _style={{marginRight: 18}}
          onPress={() => {}}
        />

        <Icons
          source={images.dots_blue}
          size={15}
          _style={{marginRight: 10}}
          onPress={props.onPressOptions}
        />
      </View>
    </SafeAreaView>
  );
};

export default class Message extends Component {
  state = {
    fromWhere: this.props.route.params && this.props.route.params.fromWhere,
    modalVisible: false,
  };

  onPressOptions() {
    this.setState({modalVisible: !this.state.modalVisible});
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: Colors.white}}>
        {/* <SafeAreaView /> */}
        <Header
          fromWhere={this.state.fromWhere}
          navigation={this.props.navigation}
          onPressOptions={() => this.onPressOptions()}
        />
        {this.state.fromWhere && <PinkScreen />}

        {/* center */}
        <KeyboardAwareScrollView
          bounces={false}
          showsVerticalScrollIndicator={false}
           style={{flex: 1, }}>
          <View style={{marginTop:45}}>
            <View style={{flexDirection: 'row',alignItems:'center'}}>
              <Icons
                size={30}
                source={images.Profile}
                _style={{marginLeft: 15,marginBottom:70}}
              />

              <Image source={images.m1} 
              resizeMode='contain'
              style={{height: 100, width: 280}} />
            </View>



            

<Text style={{marginVertical:35,textAlign:'center',fontSize:fonts.fontSize10}}>42 min ago</Text>


            <View style={{flexDirection: 'row',alignItems:'center',
            justifyContent:'flex-end',}}>
             

              <Image source={images.m4} 
              resizeMode='contain'
              style={{height: 55, width: 70,marginRight:15}} />
              
            </View>



            <View style={{flexDirection: 'row',alignItems:'center',
            justifyContent:'flex-end',}}>
             

              <Image source={images.m5} 
              resizeMode='contain'
              style={{height:100, width: 130,marginRight:15}} />
              
            </View>




            <View style={{flexDirection: 'row',alignItems:'center'}}>
              <Icons
                size={30}
                source={images.Profile}
                _style={{marginLeft: 15,marginBottom:70}}
              />

              <Image source={images.m2} 
              resizeMode='contain'
              style={{height: 100, width: 280}} />
            </View>


            <View style={{flexDirection: 'row',alignItems:'center',marginTop:20}}>
              <Icons
                size={30}
                source={images.Profile}
                _style={{marginLeft: 15,marginBottom:70}}
              />

              <Image source={images.m3} 
              resizeMode='contain'
              style={{height: 100, width: 280}} />
            </View>


          </View>
        </KeyboardAwareScrollView>

        {/* footer */}
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 10,
            alignItems: 'center',
          }}>
          <View style={{flex: 0.5}}>
            <Icons
              size={25}
              source={images.microphone}
              onPress={() => alert('development')}
            />
          </View>

          <View
            style={{
              flex: 3.5,
              flexDirection: 'row',
              backgroundColor: 'white',
              borderRadius: 30,
              borderColor: 'rgb(76,0,196)',
              borderWidth: 1,
            }}>
            <TextInput
              placeholder={'Say something…'}
              placeholderTextColor={Colors.pinkishGrey}
              style={{
                flex: 1,
                height:45,
                backgroundColor: 'white',
                marginHorizontal: 20,
                // paddingVertical: 15,
              }}
            />
            <Icons
              size={20}
              source={images.attachment}
              _style={{marginRight: 15, marginTop: 15}}
              onPress={() => alert('development')}
            />
          </View>
          <View style={{flex: 0.7, alignItems: 'center'}}>
            {/* <GButton/> */}
            {/* <GRoundButton
                height={50}
                width={50}
                radius={25}
            /> */}
            <Icons
              size={60}
              source={images.send_ic}
              onPress={
                () => alert('Message Send')
                // this.setState({modalVisible: !this.state.modalVisible})
              }
            />
          </View>
        </View>

        <SafeAreaView />

        <Custompopup modalVisible={this.state.modalVisible}>
          <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.6)'}}>
            {/* <View style={{flex: 1, backgroundColor: 'green'}}> */}

            {/* </View> */}

            {/* <View
              onTouchEnd={() => this.setState({modalVisible: false})}
              style={{flex: 2}}></View> */}
              <TouchableOpacity activeOpacity={1}
              onPress={() => this.setState({modalVisible: false})}
               style={{flex:2}}/>

            <View style={{flex: 3, backgroundColor: Colors.white}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: 15,
                  paddingVertical: 20,
                }}>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                  <Text style={styles.pophead}>Fun Group (6)
                  <Icons
                    size={15}
                    source={images.edit}
                    _style={{marginLeft: 15}}
                    onPress={() => alert('development')}
                  />
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <Text style={styles.pophead}>invite</Text>
                  <Icons
                    size={15}
                    source={images.share_blue}
                    _style={{marginLeft: 15}}
                    onPress={() => alert('development')}
                  />
                </View>
              </View>

              <View style={{paddingHorizontal: 18}}>
                <FlatList
                  contentContainerStyle={{
                    paddingBottom: 10,
                    // marginTop: 15,
                  }}
                  data={['', '', '', '',]}
                  renderItem={({item: color, index}) => (
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginBottom: 15,
                        }}>
                        <Icons
                          size={35}
                          source={images.Profile}
                          _style={{marginRight: 15}}
                          // onPress={() => alert('hi')}
                        />
                        <Text>Odell</Text>
                      </View>
                      {index !== 0 && (
                        <TouchableOpacity style={styles.accept_btn_touch}>
                          <Text style={styles.accept_text}>Remove</Text>
                        </TouchableOpacity>
                      )}

                      {index == 0 && (
                        <TouchableOpacity style={styles.adminView}>
                          <Text style={styles.adminText}>Group Admin</Text>
                        </TouchableOpacity>
                      )}
                    </View>
                  )}
                />
                <TouchableOpacity style={styles.existView}>
                  <Text style={styles.existText}>Exist Group</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <SafeAreaView />
        </Custompopup>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  firstView: {
    flex: 1,
    // backgroundColor: 'red',
    flexDirection: 'row',
    paddingTop: 10,
    marginLeft: 15,
  },
  centerView: {
    flex: 2.0,
    flexDirection: 'row',
    paddingTop: 10,
    // marginLeft: 18,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor:'red'
  },
  thiredView: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 10,
    paddingRight: 10,
    // marginLeft: 18,
    // alignItems:'flex-end',
    // backgroundColor:'red',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    // justifyContent:'center',
    // alignItems:'center',
  },
  headerContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    // backgroundColor:'red'
  },
  pophead: {fontSize: fonts.fontSize13, fontFamily: fonts.NovaBoldios},

  accept_btn_touch: {
    // marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
    // width: 59,
    // height: 22,
    backgroundColor: Colors.violetBlue,
    borderRadius: 5,
    paddingHorizontal: 5,
    paddingVertical: 6,
  },
  accept_text: {
    fontSize: fonts.fontSize10,
    color: Colors.white,
    fontFamily: fonts.NovaBold,
  },

  adminView: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: Colors.violetBlue,
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 8,
    paddingVertical: 6,
  },
  adminText: {
    fontSize: fonts.fontSize10,
    color: Colors.violetBlue,
    fontFamily: fonts.NovaBold,
  },

  existView: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end',
    borderColor: Colors.red,
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 8,
    paddingVertical: 4,

    width: 80,
  },
  existText: {
    fontSize: fonts.fontSize10,
    color: Colors.red,
    // fontFamily: fonts.NovaBold,
  },
});
