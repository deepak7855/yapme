import React, { Component } from 'react';
import { Image, View, FlatList, StyleSheet, ScrollView, Dimensions, Text, ImageBackground, TouchableOpacity, SafeAreaView } from 'react-native';
import ImagePath from '../../common/ImagePath_K';
import { Actions } from 'react-native-router-flux'
import { ScreenRatio, ScreenHeight, ScreenWidth } from '../../common/ScreenDimension'
import ConstantSize from '../../common/ConstantSize';
import fonts from '../../common/fonts';
import colors from '../../common/colors';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
export default class LiveEndedProfile extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props)
        this.state = {
            listData: [ 
            { id: 1, image: ImagePath.Rectangle1, Count: '1024',text:'Scllect fun' },
            { id: 2, image: ImagePath.Rectangle3, Count: '529',text:'Scllect fun' },
            { id: 3, image: ImagePath.Rectangle8, Count: '125',text:'Sara #comedy'},
            { id: 4, image: ImagePath.Rectangle6, Count: '200',text:'Lovely girls' },
            // { id: 5, image: ImagePath.Rectangle8, titile: 'Dangeurous' },
            ]
        }
    }

    _renderSubItem = ({ item, index }) =>
        (
            <TouchableOpacity
                style={{ height: screenWidth / 2 - 22, width: screenWidth / 2 - 22, margin: 1 }}>
                <Image style={{ height: "100%", resizeMode: 'cover', width: "100%" }} source={item.image} />

                <Text style={Styles.lefttext}>{item.Count}</Text>
                <Text style={Styles.righttext}>{item.text}</Text>
                {item.id==3&&
                <Text style={Styles.toplefttext}>#dancing</Text>
                }
            </TouchableOpacity>
        )

    render() {
        return (
            <SafeAreaView style={Styles.container}>
                <ImageBackground source={ImagePath.main_bg} resizeMode='cover' style={{ width: '100%', height: '100%' }}>
                    <ScrollView contentContainerStyle={{ flex: 1, }}>
                        <View style={[Styles.maincontain,Styles.overlay]}>

                            <TouchableOpacity style={[Styles.ColseView,]}>
                                <Image resizeMode='contain' style={Styles.backiocn} source={ImagePath.close_ic} />
                            </TouchableOpacity>

                            <View style={Styles.MainConView}>
                                <Text style={Styles.textLive}>LIVE Ended</Text>

                                <View style={Styles.TimeView}>
                                    <Image resizeMode='contain' style={Styles.Clockiocn} source={ImagePath.clock_gray} />
                                    <Text style={[Styles.texttime,]}> 0:11:21</Text>
                                </View>
                            </View>

                            <View style={Styles.ElemarView}>
                                <Image resizeMode='contain' style={Styles.IamgeCall} source={ImagePath.Profile} />
                                <Text style={[Styles.textLive, { marginTop: 10 }]}>Elemar</Text>
                            </View>

                            {/* <View style={[Styles.ContDurations,{flexDirection:'row',}]}>
                                <Text style={[Styles.textDurations]}>Back</Text>
                            </View> */}
                            <View style={Styles.borderview}></View>


                            <View style={Styles.MayYouLike}>
                                <Text style={Styles.textLive}>You May like</Text>
                            </View>

                            <View style={{ flex: 1, marginHorizontal: 20,paddingTop:15,paddingBottom:40, }}>
                                <FlatList
                                    numColumns={2}
                                    showsVerticalScrollIndicator={false}
                                    data={this.state.listData}
                                    renderItem={this._renderSubItem}
                                    extraData={this.state}
                                />
                            </View>
                        </View>
                    </ScrollView>
                </ImageBackground>
            </SafeAreaView>
        );
    }

}
const Styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: colors.white },
    splashsz: { opacity: 0.2, width: ConstantSize.percent_hundred, height: ConstantSize.percent_hundred, resizeMode: 'stretch' },
    maincontain: { flex: 1, backgroundColor: '#00000099', },
    ColseView: { position: 'absolute', top: 20, right: 20 },
    ElemarView: { alignItems: 'center', marginTop: 20 },
    Clockiocn: { width: 16, height: 16 },
    texttime: { fontSize: 12, marginLeft: 10, color: 'grey' },
    MainConView: { marginTop: 100, alignItems: 'center' },
    MayYouLike: { marginTop: 10, alignItems: 'center' },
    TimeView: { flexDirection: 'row', alignItems: 'center' },
    textLive: { fontSize: 20, color: '#fff', },
    textK: { fontSize: 28, color: '#fff', },
    textView: { fontSize: 14, color: 'grey', },
    backiocn: { width: 15, height: 15 },
    textDurations: { fontSize: 16, color: '#fff', },
    Cont: { flexDirection: 'row', alignItems: 'center', marginHorizontal: 20, paddingVertical: 30, marginTop: 50 },
    borderview: { marginHorizontal: 10, marginVertical: 20, borderBottomWidth: 1, borderBottomColor: 'grey', },
    ContFirst: { flex: 1, alignItems: 'center', justifyContent: 'center', },
    ContDurations: { marginVertical: 40, height: 30, alignSelf: 'center', },
    ContButtonView: { alignItems: 'center', justifyContent: 'center', },
    IamgeCall: { width: 80, height: 80, },
    lefttext:{position:'absolute',top:10,right:5,fontSize:14,color:'#fff'},
    righttext:{position:'absolute',bottom:10,left:5,fontSize:14,color:'#fff'},
    toplefttext:{paddingHorizontal:10,paddingVertical:5,borderRadius:20, position:'absolute',top:10,left:5,fontSize:14,color:'#fff',backgroundColor:'#E71AA3'},
    overlay: {  backgroundColor:'#000000',  opacity: 0.8  },

});


