import {StyleSheet} from 'react-native';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';

export default PrivacyAndSafetyStyles = StyleSheet.create({
  safe_area_view: {
    flex: 1,
  },

  textstyle: {
    fontFamily: fonts.NovaBoldios,
    fontSize: fonts.fontSize16,
  },
  textstyle2:{
    fontFamily: fonts.NovaRegularios,
    fontSize: fonts.fontSize10,
    color:Colors.pinkishGreyTwo

  },
viewElevation:{
    borderRadius: 15,
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 15,
    marginVertical:5,
    paddingVertical:20,
    marginTop:15

  },

});
