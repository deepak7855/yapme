import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  TextInput
} from 'react-native';
import {images} from '../../Assets/ImageUrl';
import styles from './ForgotPasswordStyles';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';
import {Input} from '../../Common/InputCommon';
import {GButton} from '../../Common/GButton';
import CountryCodePicker from '../../Component/Modal/CountryCodePickerModal';
import CHeader from '../../Common/CHeader';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';


class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEmailForgotPass: false,
      isPhoneForgotPass: true,
      isSelected: false,
      isFieldActive: false,
      modalVisibleCode: false,
      countryCode: '',
    };
    // Header({
    //   ...this,
    //   ImagePath:images.chat,
    //   menu: true,
    //   Title: "Edit Dog Profile",
    //   leftClick: () => {
    //     this.goBack();
    //   },
    //   rightClick: () => {
    //     this.rightClick();
    //   },
    // });
  }

  optionForgotPassEmail = () => {
    this.setState({isEmailForgotPass: false, isPhoneForgotPass: true});
  };

  optionForgotPassPhone = () => {
    this.setState({isPhoneForgotPass: false, isEmailForgotPass: true});
  };

  callCounteryCodeApi = (dial_code) => {
    this.setState({countryCode: dial_code, modalVisibleCode: false});
  };

  render() {
    const {isSelected, isEmailForgotPass, isPhoneForgotPass} = this.state;
    let forgotPassEmailStyle = isEmailForgotPass
      ? styles.phone_text
      : styles.selected_phone_text;
    let forgotPassPhoneStyle = isPhoneForgotPass
      ? styles.email_text
      : styles.selected_email_text;

    let forgotPassSeparatorStyle = isEmailForgotPass
      ? styles.email_separator
      : styles.selected_email_separator;
    let forPassPhoneSeparatorStyle = isPhoneForgotPass
      ? styles.phone_separator
      : styles.selected_phone_separator;
    return (
      <View style={styles.safe_area_view}>
        <ImageBackground
          source={images.login_image_email}
          style={styles.image_back_ground}
          resizeMode={'stretch'}>
          <CHeader navigation={this.props.navigation} backArrowLeft />

          <KeyboardAwareScrollView
          bounces={false}
          showsVerticalScrollIndicator={false}>
            <View style={styles.image_parent_view}>
              <View style={styles.image_view}>
                <Image
                  style={styles.profile_img}
                  resizeMode={'cover'}
                  source={images.Profile}
                />
              </View>
            </View>

            <View style={styles.forgot_pass_view}>
              <Text style={styles.forgot_pass_text}>FORGOT PASSWORD</Text>
            </View>

            <View style={styles.email_add_view}>
              <Text style={styles.email_add_text}>
                Enter your email address and we will send you the reset password
                link to your mail
              </Text>
            </View>

            <View style={styles.email_phone_view}>
              <TouchableOpacity
                onPress={this.optionForgotPassEmail}
                style={styles.email_phone_touch}>
                <Text style={forgotPassPhoneStyle}>Email</Text>
                <View style={forgotPassSeparatorStyle}></View>
              </TouchableOpacity>

              {/* <TouchableOpacity
                onPress={this.optionForgotPassPhone}
                style={styles.email_phone_touch}>
                <Text style={forgotPassEmailStyle}>Phone</Text>
                <View style={forPassPhoneSeparatorStyle}></View>
              </TouchableOpacity> */}
            </View>

            {isEmailForgotPass ? (
              <View
              style={[styles.View1,{marginHorizontal:40,marginTop:40}]}>
              <View
                style={styles.View2}>
                <Text style={styles.code_text}>
                  {this.state.countryCode ? this.state.countryCode : '+ 1'}
                </Text>
                <Image
                  resizeMode={'contain'}
                  source={images.down_arrow}
                  style={styles.arrow_img}
                />
              </View>

              <View style={{width: 20}} />

              <View style={{width: null,  flex: 1,}}>
                <Text
                  style={styles.View3}>
                  Phone Number
                </Text>
                <TextInput style={styles.phoneInput}
                 keyboardType="phone-pad"
                  returnKeyType="done"
                 />
              </View>
            </View>
            ) : 
            (
              <View style={styles.input_view}>
                <Input
                  labelText={'Email Address'}
                  // placeholder={'ciafab811r@gmail.com'}
                  // rightImagePath={images.tick}
                  keyboardType={'email-address'}
                />
              </View>
            )}
            <View style={styles.submit_btn_view}>
              <GButton
                height={50}
                width={200}
                radius={5}
                lineHeight={15}
                txtcolor={Colors.white}
                fontFamily={Fonts.NovaBold}
                fontSize={Fonts.fontSize13}
                bText={'SUBMIT'}
                onPress={()=>this.props.navigation.navigate('Login')}
              />
            </View>
          </KeyboardAwareScrollView>
        </ImageBackground>

        <CountryCodePicker
          visible={this.state.modalVisibleCode}
          onRequestClose={() => {
            this.setState({modalVisibleCode: false});
          }}
          onPress={() => {
            this.setState({modalVisibleCode: false});
          }}
          callCounteryCodeApi={(dial_code) => {
            this.callCounteryCodeApi(dial_code);
          }}
        />
      </View>
    );
  }
}

export default ForgotPassword;
