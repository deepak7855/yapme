import React, {Component} from 'react';
import {
  View,
  Text,
  StatusBar,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import CHeader from '../../Common/CHeader';
import {Icons} from '../../Component/Common';

const Header = (props) => {
  // console.log(props, 'propsss');
  return (
    <SafeAreaView style={styles.headerContainer}>
      <View style={styles.firstView}>
        <TouchableOpacity
          onPress={() => props.navigation.goBack(null)}
          style={{height: 20, width: 20}}>
          <Image
            source={images.back_arrow}
            resizeMode={'contain'}
            style={{height: '100%', width: '100%', tintColor: '#fff'}}
          />
        </TouchableOpacity>
      </View>

      <View style={styles.centerView}>
        <View style={{}}>
          <>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: fonts.NovaSboldios,
                  fontSize: fonts.fontSize16,
                  color: Colors.white,
                }}>
                Create Story
              </Text>
            </View>
            <Text
              style={{
                fontSize: fonts.fontSize10,
                fontFamily: fonts.NovaRegular,
                marginTop: 7,
                color: Colors.white,
              }}>
              Story will be shown only for 24 hours
            </Text>
          </>
        </View>
      </View>
      <View style={styles.thiredView}></View>
    </SafeAreaView>
  );
};

export default class Story extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.8)'}}>
        <StatusBar
          backgroundColor={Colors.violetBlue}
          barStyle="dark-content"
        />
        <SafeAreaView />

        <View style={{flex: 1, justifyContent: 'space-between'}}>
          <Header navigation={this.props.navigation} backArrowLeft />

          <View
            style={{
              flexDirection: 'row',
              //   backgroundColor: 'red',
              justifyContent: 'space-between',
              paddingHorizontal: 10,
              paddingVertical: 10,
              alignItems: 'center',
            }}>
            <Icons
              size={30}
              source={images.radio_white}
              //   _style={{marginRight: 15}}
              //   onPress={() => this.setState({modalVisible: true})}
            />

            <Icons
              size={40}
              source={images.play_lg}
              //   _style={{marginRight: 15}}
              //   onPress={() => this.setState({modalVisible: true})}
            />

            <Icons
              size={30}
              source={images.camera_flip}
              //   _style={{marginRight: 15}}
              //   onPress={() => this.setState({modalVisible: true})}
            />
          </View>
        </View>
        <SafeAreaView />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  firstView: {
    flex: 1,
    // backgroundColor: 'red',
    flexDirection: 'row',
    paddingTop: 10,
    marginLeft: 18,
  },
  centerView: {
    flex: 2.0,
    flexDirection: 'row',
    paddingTop: 10,
    // marginLeft: 18,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor:'red'
  },
  thiredView: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 10,
    paddingRight: 10,
    // marginLeft: 18,
    // alignItems:'flex-end',
    // backgroundColor:'red',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    // justifyContent:'center',
    // alignItems:'center',
  },
  headerContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    // backgroundColor:'red'
  },
  pophead: {fontSize: fonts.fontSize13, fontFamily: fonts.NovaBoldios},

  accept_btn_touch: {
    // marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
    // width: 59,
    // height: 22,
    backgroundColor: Colors.violetBlue,
    borderRadius: 5,
    paddingHorizontal: 5,
    paddingVertical: 6,
  },
  accept_text: {
    fontSize: fonts.fontSize10,
    color: Colors.white,
    fontFamily: fonts.NovaBold,
  },
});
