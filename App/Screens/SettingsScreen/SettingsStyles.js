import { StyleSheet } from 'react-native';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';


export default SettingsStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
    },
    settings_text_view:{
        marginHorizontal: 12, 
        marginTop: 25
    },
    settings_text:{
        fontSize: Fonts.fontSize16, 
        lineHeight: 20, 
        fontFamily: Fonts.NovaBold, 
        color: Colors.black
    },
    setting_img_title_arrow_view:{
        marginTop: 25,
        marginHorizontal: 15,
    },
    setting_title_img_touch:{
        flexDirection: 'row', 
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        paddingVertical: 15,
        flexDirection: 'row',
        borderColor: '#ddd',
        borderWidth: 1,
        borderRadius: 10,
        alignItems: 'center',
        marginVertical: 5,
    },
    image_title_view:{
        flexDirection: 'row', 
        alignItems: 'center'
    },
    img_style:{
        height: 14, 
        width: 14 
    },
    title_text:{
        marginLeft: 20, 
        fontSize: Fonts.fontSize15, 
        lineHeight: 17, 
        color: Colors.black, 
        fontFamily: Fonts.NovaSbold
    },
    back_arrow_img:{
        width: 5.9, 
        height: 9.5
    },
    
});