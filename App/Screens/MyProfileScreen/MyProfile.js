import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  TextInput,
  StatusBar,
} from 'react-native';
import {images} from '../../Assets/ImageUrl';
import styles from './MyProfileStyles';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';
import {Input} from '../../Common/InputCommon';
import {GButton} from '../../Common/GButton';
import Header from '../../Common/Header';
import ViewPager from '@react-native-community/viewpager';
import MasonryList from 'react-native-masonry-list';

class MyProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <SafeAreaView style={styles.safe_area_view}>
        <StatusBar
          backgroundColor={Colors.violetBlue}
          barStyle="dark-content"
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{marginTop: 25, marginHorizontal: 15}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View
                style={{flex: 9, flexDirection: 'row', alignItems: 'center'}}>
                <View style={{height: 70, width: 70, borderRadius: 70 / 2}}>
                  <Image
                    source={images.Profile}
                    resizeMode={'cover'}
                    style={{height: '100%', width: '100%'}}
                  />
                </View>
                <View style={{marginLeft: 10, width: 150}}>
                  <Text
                    style={{
                      fontSize: Fonts.fontSize20,
                      lineHeight: 24,
                      color: Colors.black,
                      fontFamily: Fonts.NovaRegular,
                    }}
                    numberOfLines={1}>
                    @elemar
                  </Text>
                </View>
              </View>

              <View style={{flex: 1}}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('Settings')}
                  style={{height: 30, width: 30, borderRadius: 30 / 2}}>
                  <Image
                    source={images.settings}
                    resizeMode={'cover'}
                    style={{height: '100%', width: '100%'}}
                  />
                </TouchableOpacity>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 15,
                alignItems: 'center',
                marginHorizontal: 5,
              }}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Fansfollowings',{fromWhere:'Fans'})}
                style={{alignItems: 'center', justifyContent: 'center'}}>
                <Text
                  style={{
                    fontSize: Fonts.fontSize14,
                    lineHeight: 17,
                    fontFamily: Fonts.NovaRegular,
                    color: Colors.black,
                  }}>
                  215
                </Text>
                <Text
                  style={{
                    fontSize: Fonts.fontSize13,
                    lineHeight: 15,
                    color: Colors.black,
                    fontFamily: Fonts.NovaRegular,
                  }}>
                  Fans
                </Text>
              </TouchableOpacity>

              <View
                style={{
                  height: 38,
                  width: 1,
                  backgroundColor: Colors.whiteFive,
                }}></View>

              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Fansfollowings', {fromWhere:'Following'})}
                style={{alignItems: 'center', justifyContent: 'center'}}>
                <Text
                  style={{
                    fontSize: Fonts.fontSize14,
                    lineHeight: 17,
                    fontFamily: Fonts.NovaRegular,
                    color: Colors.black,
                  }}>
                  500
                </Text>
                <Text
                  style={{
                    fontSize: Fonts.fontSize13,
                    lineHeight: 15,
                    color: Colors.black,
                    fontFamily: Fonts.NovaRegular,
                  }}>
                  Following
                </Text>
              </TouchableOpacity>
              <View
                style={{
                  height: 38,
                  width: 1,
                  backgroundColor: Colors.whiteFive,
                }}></View>

              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('FriendSList')}
                style={{alignItems: 'center', justifyContent: 'center'}}>
                <Text
                  style={{
                    fontSize: Fonts.fontSize14,
                    lineHeight: 17,
                    fontFamily: Fonts.NovaRegular,
                    color: Colors.black,
                  }}>
                  352
                </Text>
                <Text
                  style={{
                    fontSize: Fonts.fontSize13,
                    lineHeight: 15,
                    color: Colors.black,
                    fontFamily: Fonts.NovaRegular,
                  }}>
                  Friends
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                // onPress={() => this.props.navigation.navigate('FriendSList')}
                style={{height: 30, width: 30, borderRadius: 30 / 2}}>
                <Image
                  source={images.user_add}
                  resizeMode={'cover'}
                  style={{height: '100%', width: '100%'}}
                />
              </TouchableOpacity>
            </View>

            <View style={styles.about_me_view}>
              <Text style={styles.about_me_text}>About Me</Text>
            </View>

            <View style={styles.about_des_view}>
              <Text style={styles.about_des_text}>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s,
              </Text>
            </View>
            <View
              style={{
                height: 1,
                backgroundColor: Colors.whiteFour,
                marginTop: 15,
                marginBottom: 3,
              }}
            />
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginHorizontal: 15,
            }}>
            <TouchableOpacity
              onPress={() => {
                // this.setState({
                //   index: 0,
                //   index_one_filled: false,
                //   index_two_filled: false,
                // }),
                //   this.viewpager.setPage(0);
              }}
              style={{
                width: '25%',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderBottomColor: Colors.purplePink,
                paddingVertical: 10,
              }}>
              <Image
                source={images.play_lg}
                resizeMode={'contain'}
                style={{width: 24.3, height: 19.2}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                // this.setState({index: 1, index_one_filled: true}),
                //   this.viewpager.setPage(1);
              }}
              style={{
                width: '25%',
                borderBottomWidth: this.state.index_one_filled ? null : 1,
                alignItems: 'center',
                borderBottomColor: this.state.index_one_filled
                  ? Colors.purplePink
                  : Colors.whiteTwo,
                paddingVertical: 10,
              }}>
              <Image
                source={images.news_ic}
                resizeMode={'contain'}
                style={{width: 24.3, height: 19.2}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              // disabled={this.state.index == 0}
              // onPress={() => {
              //   this.setState({index: 2, index_two_filled: true}),
              //     this.viewpager.setPage(2);
              // }}
              style={{
                width: '25%',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderBottomColor: Colors.whiteTwo,
                paddingVertical: 10,
              }}>
              <Image
                source={images.button}
                resizeMode={'contain'}
                style={{width: 24.3, height: 19.2}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                width: '25%',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderBottomColor: Colors.whiteTwo,
                paddingVertical: 10,
              }}>
              <Image
                source={images.sun_icon}
                resizeMode={'contain'}
                style={{width: 24.3, height: 19.2}}
              />
            </TouchableOpacity>
          </View>

          <MasonryList
            images={[
              {
                uri:
                  'https://cdn.pixabay.com/photo/2017/06/09/09/39/adler-2386314_960_720.jpg',
              },
              {
                source: {
                  uri:
                    'https://cdn.pixabay.com/photo/2017/06/02/18/24/fruit-2367029_960_720.jpg',
                },
              },
              {
                uri:
                  'https://cdn.pixabay.com/photo/2016/08/12/22/34/apple-1589869_960_720.jpg',
                dimensions: {width: 1080, height: 1920},
              },
              {
                URI:
                  'https://cdn.pixabay.com/photo/2016/08/12/22/34/apple-1589869_960_720.jpg',
              },
              {
                url:
                  'https://cdn.pixabay.com/photo/2016/08/12/22/34/apple-1589869_960_720.jpg',
              },
              {
                URL:
                  'https://cdn.pixabay.com/photo/2016/08/12/22/34/apple-1589869_960_720.jpg',
              },
            ]}
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default MyProfile;
