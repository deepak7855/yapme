import { StyleSheet } from 'react-native';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';


export default ReportaproblemStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
    },

    report_view:{
        marginHorizontal:13
    },
    report_text:{
        fontSize:Fonts.fontSize16,
        lineHeight:20,
        color:Colors.black,
        fontFamily:Fonts.NovaBold
    },
    tell_us_text:{
        fontSize:Fonts.fontSize13,
        lineHeight:17,
        color:Colors.black,
        fontFamily:Fonts.NovaRegular,
        marginTop:30
    },
    feedback_input_view:{
        marginHorizontal:13,
        marginVertical:15
    },
    feedback_input:{
        shadowColor: Colors.black, 
        // width: 330, 
        height: 134, 
        borderRadius: 10, 
        borderWidth: 1.5, 
        borderColor: Colors.white, 
        fontSize: Fonts.fontSize13, 
        lineHeight: 20, 
        color: Colors.pinkishGreyTwo
    },

    submit_btn_view:{
        alignItems: 'center', 
        marginVertical: 35 
    },
   
});