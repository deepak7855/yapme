export const images = {


   globesm:require("../Assets/icons/globe-sm.png"),
   globe:require("../Assets/icons/globe.png"),
  live_news:require("../Assets/icons/live_news.png"),
    search_active:require("../Assets/icons/search_active.png"),
    Group4:require("../Assets/icons/Group4.png"),
    Group5:require("../Assets/icons/Group5.png"),
    Group6:require("../Assets/icons/Group6.png"),
    Group7:require("../Assets/icons/Group7.png"),


    Groupp4:require("../Assets/icons/Groupp4.png"),
    Groupp3:require("../Assets/icons/Groupp3.png"),
    Groupp2:require("../Assets/icons/Groupp2.png"),
    Groupp1:require("../Assets/icons/Groupp1.png"),
    hashic:require("../Assets/icons/hash_ic.png"),
    music:require("../Assets/icons/music.png"),


    commentdots:require("../Assets/icons/comment-dots.png"),
    share:require("../Assets/icons/share.png"),
    moreicon:require("../Assets/icons/more_icon.png"),
    mainbg:require("../Assets/icons/main_bg.png"),
    Group635:require("../Assets/icons/Group635.png"),
    dislike:require("../Assets/icons/dislike.png"),
    likelg:require("../Assets/icons/like_lg.png"),

    face:require("../Assets/icons/face.png"),
    at:require("../Assets/icons/at.png"),
    slide_up_arrow:require("../Assets/icons/slide_up_arrow.png"),
    slide_next_arrow:require("../Assets/icons/slide_next_arrow.png"),
    user_circle:require("../Assets/icons/user_circle.png"),
    wave_ic:require("../Assets/icons/wave_ic.png"),
    magic:require("../Assets/icons/magic.png"),
    stricker_icon_white:require("../Assets/icons/stricker_icon_white.png"),
    stricker_icon_yellow:require("../Assets/icons/stricker_icon_yellow.png"),
    text_icon_unactive:require("../Assets/icons/text_icon_unactive.png"),
    
    story_icon:require("../Assets/icons/story_icon.png"),
    camera_flip:require("../Assets/icons/camera_flip.png"),
    live_bg:require("../Assets/icons/live_bg.png"),
    stricker_icon:require("../Assets/icons/stricker_icon.png"),
    cross:require("../Assets/icons/cross.png"),
    cover:require("../Assets/icons/cover.png"),

    angleright:require("../Assets/icons/angleright.png"),


    personal_wall:require("../Assets/icons/personal_wall.png"),

    play_round:require("../Assets/icons/play_round.png"),

    add_user_ic:require("../Assets/icons/add_user_ic.png"),

    m1:require("../Assets/icons/m1.png"),
    m2:require("../Assets/icons/m2.png"),
    m3:require("../Assets/icons/m3.png"),

    m4:require("../Assets/icons/m4.png"),
    m5:require("../Assets/icons/m5.png"),

    smile_acive1:require("../Assets/icons/smile_acive1.png"),
    edit:require("../Assets/icons/edit.png"),


    slide_prev_arrow:require("../Assets/icons/slide_prev_arrow.png"),
    firecracker:require("../Assets/icons/firecracker.png"),
    giftbox:require("../Assets/icons/gift-box.png"),
    gift_ic:require("../Assets/icons/gift_ic.png"),
    emoji_goggle:require("../Assets/icons/emoji_goggle.png"),
    laugh:require("../Assets/icons/laugh.png"),
    emoji_cry:require("../Assets/icons/emoji_cry.png"),
    emoji_angry:require("../Assets/icons/emoji_angry.png"),
    emoji_active4:require("../Assets/icons/emoji_active4.png"),
    emoji_active3:require("../Assets/icons/emoji_active3.png"),

    emoji_active2:require("../Assets/icons/emoji_active2.png"),
    emoji_active3:require("../Assets/icons/emoji_active3.png"),

    information_outline:require("../Assets/icons/information_outline.png"),
    globe_activebtn:require("../Assets/icons/globe_activebtn.png"),




    calldisconnect:require("../Assets/icons/calldisconnect.png"),
    lock_active:require("../Assets/icons/lock_active.png"),
    at_user:require("../Assets/icons/at_user.png"),


    splash: require('../Assets/icons/Rectangle7.png'),
    call_circle: require('../Assets/icons/call_circle.png'),
    face_circle: require('../Assets/icons/face_circle.png'),
    add_icon: require('../Assets/icons/add_icon.png'),
    speaker_circle: require('../Assets/icons/speaker_circle.png'),
    hash_tag_icon: require('../Assets/icons/hash_tag_icon.png'),
    // back_arrow: require('../Assets/icons/back_arrow.png'),
    // camera_flip: require('../Assets/icons/camera_flip.png'),
    // Rectangle5: require('../Assets/icons/Rectangle5.png'),
    // close_ic: require('../Assets/icons/close_ic.png'),
    // Profile: require('../Assets/icons/Profile.png'),
    clock_gray: require('../Assets/icons/clock_gray.png'),
    nudity_ic: require('../Assets/icons/nudity_ic.png'),
    hateful_ic: require('../Assets/icons/hateful_ic.png'),
    violence: require('../Assets/icons/violence.png'),
    threats: require('../Assets/icons/threats.png'),
    Rectangle8: require('../Assets/icons/Rectangle8.png'),
    Rectangle6: require('../Assets/icons/Rectangle6.png'),
    Rectangle1: require('../Assets/icons/Rectangle1.png'),
    Rectangle3: require('../Assets/icons/Rectangle3.png'),
    main_bg: require('../Assets/icons/main_bg.png'),

    


    // savetoalbum:require("../Assets/icons/editIcons/savetoalbum.png"),
    speed_icon_unactive:require("../Assets/icons/editIcons/speed_icon_unactive.png"),
    sound_ic:require("../Assets/icons/editIcons/sound_ic.png"),
    timer_active:require("../Assets/icons/editIcons/timer_active.png"),
    timer:require("../Assets/icons/editIcons/timer.png"),

    filter_icon_active:require("../Assets/icons/editIcons/filter_icon_active.png"),
    filter_icon_unactive:require("../Assets/icons/editIcons/filter_icon_unactive.png"),
    text_icon_active:require("../Assets/icons/editIcons/text_icon_active.png"),
    text_icon_unactive:require("../Assets/icons/editIcons/text_icon_unactive.png"),


    trim_icon_active:require("../Assets/icons/editIcons/trim_icon_active.png"),


    
    trim_icon_unactive:require("../Assets/icons/editIcons/trim_icon_unactive.png"),
    
    

    // speed_active:require("../Assets/icons/editIcons/speed_active.png"),


    filter_icon_unactive:require("../Assets/icons/editIcons/filter_icon_unactive.png"),
    // filter_icon_active:require("../Assets/icons/editIcons/filter_icon_active.png"),

    

    
    




    upload:require("../Assets/icons/upload.png"),
    on_toggle:require("../Assets/icons/on_toggle.png"),
    off_toggle:require("../Assets/icons/off_toggle.png"),
    userplus:require("../Assets/icons/user-plus.png"),
    savetoalbum:require("../Assets/icons/savetoalbum.png"),
    react_icon:require("../Assets/icons/react_icon.png"),
    viewloack:require("../Assets/icons/viewloack.png"),

    love:require("../Assets/icons/love.png"),

    weddingring:require("../Assets/icons/wedding-ring.png"),
    star_icon:require("../Assets/icons/star_icon.png"),
    trophy:require("../Assets/icons/trophy.png"),
    car:require("../Assets/icons/car.png"),
    leaf:require("../Assets/icons/leaf.png"),


    sunbed:require("../Assets/icons/sunbed.png"),
    wine:require("../Assets/icons/wine.png"),
    kiss:require("../Assets/icons/kiss.png"),
    ferriswheel:require("../Assets/icons/ferris-wheel.png"),


    close_pink:require("../Assets/icons/close_pink.png"),
    eye_white:require("../Assets/icons/eye_white.png"),
    location:require("../Assets/icons/location.png"),


    two:require("../Assets/icons/two.png"),
    one:require("../Assets/icons/one.png"),


    
    
  splash_image: require('../Assets/icons/splash_image.png'),

  apple_icon: require('../Assets/icons/apple_icon.png'),
  cover_photo: require('../Assets/icons/cover_photo.png'),

  bg: require('../Assets/icons/bg.png'),

  Profile: require('../Assets/icons/Profile.png'),
  play_gray: require('../Assets/icons/play_gray.png'),
  play_lg: require('../Assets/icons/play_lg.png'),

  posts_icon_color: require('../Assets/icons/posts_icon_color.png'),
  posts_icon: require('../Assets/icons/posts_icon.png'),

  chat: require('../Assets/icons/chat.png'),
  chat_color: require('../Assets/icons/chat_color.png'),
  Profile: require('../Assets/icons/Profile.png'),
  plus_icon: require('../Assets/icons/plus_icon.png'),
  search_blue: require('../Assets/icons/search_blue.png'),
  chat_ic_blue: require('../Assets/icons/chat_ic_blue.png'),
  close_pinkk: require('../Assets/icons/close_pinkk.png'),

  tick_circle: require('../Assets/icons/tick_circle.png'),
  radio_circle: require('../Assets/icons/radio_circle.png'),
  microphone: require('../Assets/icons/microphone.png'),
  microphone_circle: require('../Assets/icons/microphone_circle.png'),
  send_ic: require('../Assets/icons/send_ic.png'),
  attachment: require('../Assets/icons/attachment.png'),
  camera_circle: require('../Assets/icons/camera_circle.png'),
  laugh: require('../Assets/icons/laugh.png'),
  live: require('../Assets/icons/live.png'),
  group_icon: require('../Assets/icons/group_icon.png'),
  bg_post2: require('../Assets/icons/bg_post2.png'),
  bg_post: require('../Assets/icons/bg_post.png'),
  sq6: require('../Assets/icons/sq6.png'),
  sq5: require('../Assets/icons/sq5.png'),
  sq4: require('../Assets/icons/sq4.png'),
  sq3: require('../Assets/icons/sq3.png'),
  sq2: require('../Assets/icons/sq2.png'),
  sq1: require('../Assets/icons/sq1.png'),

  sm6: require('../Assets/icons/sm6.png'),
  sm5: require('../Assets/icons/sm5.png'),
  sm4: require('../Assets/icons/sm4.png'),
  sm3: require('../Assets/icons/sm3.png'),
  sm2: require('../Assets/icons/sm2.png'),
  sm1: require('../Assets/icons/sm1.png'),

  active_heart: require('../Assets/icons/active_heart.png'),
  comment_blue: require('../Assets/icons/comment_blue.png'),
  dots_gray: require('../Assets/icons/dots_gray.png'),
  user_blue: require('../Assets/icons/user_blue.png'),
  chat_circle: require('../Assets/icons/chat_circle.png'),
  phone_circle: require('../Assets/icons/phone_circle.png'),
  video_circle: require('../Assets/icons/video_circle.png'),
  diamond_lg:require("../Assets/icons/diamond_lg.png"),
  star_lg:require("../Assets/icons/star_lg.png"),
  commentaccount:require("../Assets/icons/comment-account.png"),
  dots_blue:require("../Assets/icons/dots_blue.png"),
  phonealt:require("../Assets/icons/phone-alt.png"),
  video_icon_sm:require("../Assets/icons/video_icon_sm.png"),
  share_blue:require("../Assets/icons/share_blue.png"),
  dots_gray:require("../Assets/icons/dots_gray.png"),
  Rectangle1:require("../Assets/icons/Rectangle1.png"),
  Rectangle2:require("../Assets/icons/Rectangle2.png"),
  Rectangle3:require("../Assets/icons/Rectangle3.png"),
  Rectangle4:require("../Assets/icons/Rectangle4.png"),
  Rectangle5:require("../Assets/icons/Rectangle5.png"),
  radio_white:require("../Assets/icons/radio_white.png"),


  spotify_green:require("../Assets/icons/spotify_green.png"),


  pause_icon:require("../Assets/icons/pause_icon.png"),
  heart_white:require("../Assets/icons/heart_white.png"),


  heart_gray:require("../Assets/icons/heart_gray.png"),



  play_gray_sm:require("../Assets/icons/play_gray_sm.png"),

  mute:require("../Assets/icons/mute.png"),
  liveicon:require("../Assets/icons/liveicon.png"),


  

  //manoj side add new image
  new_splash: require('../Assets/icons/new_splash.png'),
  login_image_email: require('../Assets/icons/login_image_email.png'),
  tick: require('../Assets/icons/tick.png'),

  eye: require('../Assets/icons/eye.png'),
  Eye_closed: require('../Assets/icons/Eye_closed.png'),

  google_hangouts: require('../Assets/icons/google_hangouts.png'),
  facebook: require('../Assets/icons/facebook.png'),
  instagram_sketched: require('../Assets/icons/instagram_sketched.png'),
  back_arrow: require('../Assets/icons/back_arrow.png'),

  yapme_lg: require('../Assets/icons/yapme_lg.png'),
  camera_profile: require('../Assets/icons/camera_profile.png'),
  calendar: require('../Assets/icons/calendar.png'),

  camera_black_circle: require('../Assets/icons/camera_black_circle.png'),
  cancel: require('../Assets/icons/cancel.png'),
  button: require('../Assets/icons/button.png'),
  camera_circle: require('../Assets/icons/camera_circle.png'),

  //

  check_square: require('../Assets/icons/check_square.png'),
  check_square_black: require('../Assets/icons/check_square_black.png'),
  down_arrow: require('../Assets/icons/down_arrow.png'),

  close_ic: require('../Assets/icons/close_ic.png'),
  chat7: require('../Assets/icons/chat7.png'),

  user_blue: require('../Assets/icons/user_blue.png'),
  chatpink_circle: require('../Assets/icons/chatpink_circle.png'),
  chat_circle: require('../Assets/icons/chat_circle.png'),
  arrow_next_black: require('../Assets/icons/arrow_next_black.png'),

  question_circle: require('../Assets/icons/question_circle.png'),
  signout: require('../Assets/icons/signout.png'),
  account_box_multiple_outline: require('../Assets/icons/account_box_multiple_outline.png'),
  information_outline: require('../Assets/icons/information_outline.png'),
  information_gray: require('../Assets/icons/information_gray.png'),

  flip: require('../Assets/icons/flip.png'),
  chat_pink: require('../Assets/icons/chat_pink.png'),

  

  wallet_outline: require('../Assets/icons/wallet_outline.png'),
  plan: require('../Assets/icons/plan.png'),
  bell_outline: require('../Assets/icons/bell_outline.png'),
  report_ic: require('../Assets/icons/report_ic.png'),
  shield_alt: require('../Assets/icons/shield_alt.png'),
  comment_account_outline: require('../Assets/icons/comment_account_outline.png'),
  chat5: require('../Assets/icons/chat5.png'),

    splash_image: require('../Assets/icons/splash_image.png'),
    
    apple_icon: require("../Assets/icons/apple_icon.png"),
    cover_photo: require("../Assets/icons/cover_photo.png"),

    

    bg: require("../Assets/icons/bg.png"),

    Profile: require("../Assets/icons/Profile.png"), 
    play_gray: require("../Assets/icons/play_gray.png"), 
    play_lg : require("../Assets/icons/play_lg.png"), 
    play : require("../Assets/icons/play.png"), 

    posts_icon_color : require("../Assets/icons/posts_icon_color.png"), 
    posts_icon: require("../Assets/icons/posts_icon.png"),
    
    chat: require("../Assets/icons/chat.png"),
    chat_color:require("../Assets/icons/chat_color.png"),
    Profile:require("../Assets/icons/Profile.png"),
    plus_icon:require("../Assets/icons/plus_icon.png"),
    search_blue:require("../Assets/icons/search_blue.png"),
    chat_ic_blue:require("../Assets/icons/chat_ic_blue.png"),
    close_pinkk:require("../Assets/icons/close_pinkk.png"),

    tick_circle:require("../Assets/icons/tick_circle.png"),
    radio_circle:require("../Assets/icons/radio_circle.png"),
    microphone:require("../Assets/icons/microphone.png"),
    microphone_circle:require("../Assets/icons/microphone_circle.png"),
    send_ic:require("../Assets/icons/send_ic.png"),
    attachment:require("../Assets/icons/attachment.png"),
    camera_circle:require("../Assets/icons/camera_circle.png"),
    laugh:require("../Assets/icons/laugh.png"),
    live:require("../Assets/icons/live.png"),
    group_icon:require("../Assets/icons/group_icon.png"),
    bg_post2:require("../Assets/icons/bg_post2.png"),
    bg_post:require("../Assets/icons/bg_post.png"),
    sq6:require("../Assets/icons/sq6.png"),
    sq5:require("../Assets/icons/sq5.png"),
    sq4:require("../Assets/icons/sq4.png"),
    sq3:require("../Assets/icons/sq3.png"),
    sq2:require("../Assets/icons/sq2.png"),
    sq1:require("../Assets/icons/sq1.png"),

    sm6:require("../Assets/icons/sm6.png"),
    sm5:require("../Assets/icons/sm5.png"),
    sm4:require("../Assets/icons/sm4.png"),
    sm3:require("../Assets/icons/sm3.png"),
    sm2:require("../Assets/icons/sm2.png"),
    sm1:require("../Assets/icons/sm1.png"),

    active_heart:require("../Assets/icons/active_heart.png"),
    comment_blue:require("../Assets/icons/comment_blue.png"),
    dots_gray:require("../Assets/icons/dots_gray.png"),
    user_blue:require("../Assets/icons/user_blue.png"),
    chat_circle:require("../Assets/icons/chat_circle.png"),



   

    
    
//manoj side add new image
    new_splash:require("../Assets/icons/new_splash.png"),
    login_image_email:require("../Assets/icons/login_image_email.png"),
    tick:require("../Assets/icons/tick.png"),

    eye:require("../Assets/icons/eye.png"),
    Eye_closed:require("../Assets/icons/Eye_closed.png"),


    google_hangouts:require("../Assets/icons/google_hangouts.png"),
    facebook:require("../Assets/icons/facebook.png"),
    instagram_sketched:require("../Assets/icons/instagram_sketched.png"),
    back_arrow:require("../Assets/icons/back_arrow.png"),

    yapme_lg:require("../Assets/icons/yapme_lg.png"),
    camera_profile:require("../Assets/icons/camera_profile.png"),
    calendar:require("../Assets/icons/calendar.png"),

    camera_black_circle:require("../Assets/icons/camera_black_circle.png"),
    cancel:require("../Assets/icons/cancel.png"),
    button:require("../Assets/icons/button.png"),
    camera_circle:require("../Assets/icons/camera_circle.png"),



    //

    check_square:require("../Assets/icons/check_square.png"),
    check_square_black:require("../Assets/icons/check_square_black.png"),
    down_arrow:require("../Assets/icons/down_arrow.png"),

    close_ic:require("../Assets/icons/close_ic.png"),
    chat7:require("../Assets/icons/chat7.png"),
 
    user_blue:require("../Assets/icons/user_blue.png"),
    chatpink_circle:require("../Assets/icons/chatpink_circle.png"),
    chat_circle:require("../Assets/icons/chat_circle.png"),
    arrow_next_black:require("../Assets/icons/arrow_next_black.png"),

    question_circle:require("../Assets/icons/question_circle.png"),
    signout:require("../Assets/icons/signout.png"),
    account_box_multiple_outline:require("../Assets/icons/account_box_multiple_outline.png"),
    information_outline:require("../Assets/icons/information_outline.png"),
    wallet_outline:require("../Assets/icons/wallet_outline.png"),
    plan:require("../Assets/icons/plan.png"),
    bell_outline:require("../Assets/icons/bell_outline.png"),
    report_ic:require("../Assets/icons/report_ic.png"),
    shield_alt:require("../Assets/icons/shield_alt.png"),
    comment_account_outline:require("../Assets/icons/comment_account_outline.png"),
    chat5:require("../Assets/icons/chat5.png"),
   
    news_ic:require("../Assets/icons/news_ic.png"),
    sun_icon:require("../Assets/icons/sun_icon.png"),
    settings:require("../Assets/icons/settings.png"),
    user_add:require("../Assets/icons/user_add.png"),


   
}


