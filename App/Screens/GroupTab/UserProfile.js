import React, {Component} from 'react';
import {Text, View, SafeAreaView, StyleSheet} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import {GButton} from '../../Common/GButton';
import {Icons, RowoneBythree} from '../../Component/Common';

export default class UserProfile extends Component {
  render() {
    return (
      <View>
        <SafeAreaView />
        <View style={{flexDirection: 'row', marginHorizontal: 15}}>
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Icons
              size={35}
              source={images.user_blue}
              // _style={{marginRight: 10}}
            />
          </View>
          <View
            style={{flex: 2, alignItems: 'center', justifyContent: 'center'}}>
            <Icons
              size={70}
              source={images.sm5}
              // _style={{marginRight: 10}}
            />
          </View>
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Icons
              size={60}
              source={images.chat_circle}
              // _style={{marginRight: 10}}
            />
          </View>
        </View>

        <View style={{alignItems: 'center', marginVertical: 20}}>
          <Text style={styles.View4}>@marsus</Text>
          <Text style={styles.View5}>27 | Youtuber | Boston, MA</Text>

          <View style={{flexDirection: 'row', marginVertical: 8}}>
            <View style={{alignItems: 'center'}}>
              <Text style={{fontFamily: fonts.NovaRegularios}}>215</Text>
              <Text style={styles.View5}>Fans</Text>
            </View>

            <View
              style={{
                borderWidth: 0.5,
                marginHorizontal: 50,
                marginRight: 28,
                marginTop: 5,
                marginBottom: 8,
                borderColor: Colors.pinkishGrey,
              }}></View>

            <View style={{alignItems: 'center'}}>
              <Text style={{fontFamily: fonts.NovaRegularios}}>215</Text>
              <Text style={styles.View5}>Following</Text>
            </View>
          </View>

          <GButton
            // _style={{alignSelf: 'center',}}
            bText="Follow"
            fontWeight="bold"
            height={50}
            width={210}
            radius={5}
          />
        </View>

        <View style={{marginHorizontal: 15, marginTop: 20}}>
          <Text style={{fontFamily: fonts.NovaBoldios, marginVertical: 10}}>
            {' '}
            About Me{' '}
          </Text>

          <Text
            style={styles.text6}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s,
          </Text>

          <View
            style={{
              borderWidth: 0.41,
              marginTop: 25,
              borderColor: Colors.pinkishGrey,
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  View4: {fontFamily: fonts.NovaRegularios, fontSize: fonts.fontSize20},
  View5: {
    marginTop: 10,
    marginBottom: 15,
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaRegularios,
    color: Colors.pinkishGrey,
  },
  item: {
    margin: 10,
    height: 30,
    borderRadius: 10,
  },
  text6:{
    fontFamily: fonts.NovaRegularios,
    color: Colors.pinkishGreyTwo,
    marginLeft: 5,
    lineHeight: 20,
  }

});
