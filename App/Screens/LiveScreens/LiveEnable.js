import React, { Component } from 'react';
import { Image, View, FlatList, StyleSheet, Text, ImageBackground, TouchableOpacity, SafeAreaView } from 'react-native';
import { images as ImagePath} from '../../Assets/ImageUrl';
import Colors from '../../Assets/Colors';

export default class LiveEnable extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props)
        this.state = {
            itemKey: '',

        }
    }

    render() {
        return (
            // <SafeAreaView style={Styles.container}>
              
                <ImageBackground source={ImagePath.splash} resizeMode='cover' style={{ width: '100%', height: '100%' }}>
                <SafeAreaView/>
                    <View style={Styles.maincontain}>

                        <TouchableOpacity style={[Styles.ColseView,]}>
                            <Image resizeMode='contain' style={Styles.backiocn} source={ImagePath.close_ic} />
                        </TouchableOpacity>

                        <View style={Styles.MainConView}>
                            <Text style={Styles.textLive}>LIVE Ended</Text>

                            <View style={Styles.TimeView}>
                                <Image resizeMode='contain' style={Styles.Clockiocn} source={ImagePath.clock_gray} />
                                <Text style={[Styles.texttime,]}>0:11:21</Text>
                            </View>
                        </View>

                        <View style={Styles.ElemarView}>
                                <Image resizeMode='contain' style={Styles.IamgeCall} source={ImagePath.Profile} />
                                <Text style={[Styles.textLive, { marginTop: 10 }]}>Elemar</Text>
                            </View>

                            <View style={Styles.BorderView}>
                                <Text style={[Styles.textLive, Styles.textborder]}>The broadcast is banned due to breaking YAPME'S Community Guidelines</Text>
                            </View>

                        <View style={Styles.Cont}>
                            <View style={Styles.ContFirst}>
                                <Image resizeMode='contain' style={[Styles.Iamgecamera]} source={ImagePath.nudity_ic} />
                                <Text style={[Styles.textLive, Styles.title]}>Nudity or sexually suggestive content</Text>
                            </View>

                            <View style={Styles.ContFirst}>
                                <Image resizeMode='contain' style={[Styles.Iamgecamera]} source={ImagePath.hateful_ic} />
                                <Text style={[Styles.textLive, Styles.title]}>Hateful contemt</Text>
                            </View>
                        </View>

                        <View style={Styles.Cont}>
                            <View style={Styles.ContFirst}>
                                <Image resizeMode='contain' style={Styles.Iamgecamera} source={ImagePath.violence} />
                                <Text style={[Styles.textLive, Styles.title]}>Violence and Graphic Contant</Text>
                            </View>

                            <View style={Styles.ContFirst}>
                                <Image resizeMode='contain' style={Styles.Iamgecamera} source={ImagePath.threats} />
                                <Text style={[Styles.textLive, Styles.title]}>Threats,spam or</Text>
                            </View>
                        </View>
                    </View>
                </ImageBackground>
        );
    }

}
const Styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.white },
    MainConView: { marginTop: 120, alignItems: 'center' },
    TimeView: { flexDirection: 'row', alignItems: 'center' ,marginTop:15,},
    ElemarView: { alignItems: 'center', marginVertical: 20 },
    BorderView: { alignItems: 'center', marginHorizontal: 20, marginVertical: 20, paddingVertical: 10, borderTopColor: 'grey', borderTopWidth: 1, borderBottomColor: 'grey', borderBottomWidth: 1, },
    splashsz: { width: '100%', height: '100%', resizeMode: 'stretch' },
    maincontain: { flex: 1, backgroundColor: '#00000099' },
    textLive: { fontSize: 18, color: '#fff', },
    Cont: {marginTop:10, flexDirection: 'row',},
    ContFirst: {flex:1, alignItems:'center',marginVertical:20,marginHorizontal:10 },
   
    title: {  textAlign: 'center', fontSize: 14,marginTop:10, },
    texttime: { fontSize: 12, color: '#fff', marginLeft: 10, color: 'grey' },
    textborder: { textAlign: 'center', fontSize: 14 },
    backiocn: { width: 15, height: 15 },
    Clockiocn: { width: 12, height: 12 },
    Iamgecamera: { width: 55, height: 55 },
    IamgeCall: { width: 80, height: 80, },
    ColseView: { position: 'absolute', top: 10, right: 20 },
    contain: { position: 'absolute', bottom: 10, flexDirection: 'row', justifyContent: 'center', alignSelf: 'center' }


});


