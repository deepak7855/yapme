import React, {Component} from 'react';
import {
  Image,
  View,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  SafeAreaView,
  ImageBackground,
} from 'react-native';
import Colors from '../../Assets/Colors';
import {images as ImagePath} from '../../Assets/ImageUrl';
import {Icons} from '../../Component/Common';

export default class VideoCall extends Component {
  static navigationOptions = {header: null};
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={Styles.container}>
        {/* <View style={Styles.container}>
                    <Image style={Styles.splashsz}
                        source={ImagePath.splash}>
                    </Image>
                </View> */}

        <ImageBackground
          source={ImagePath.live_bg}
          style={{flex: 1, justifyContent: 'space-between'}}
          resizeMode={'stretch'}>
          <SafeAreaView />
          <View style={{...Styles.maincontain}}>
            <View style={[Styles.mainView]}>
              <Icons
                size={20}
                source={ImagePath.back_arrow}
                onPress={() => this.props.navigation.goBack(null)}
              />

              <Icons
                size={30}
                source={ImagePath.camera_flip}
                _style={[Styles.Iamgecamera, {marginHorizontal: 20}]}
              />
            </View>

            <View style={[Styles.mainView, {justifyContent: 'center'}]}>
              <Text style={{fontSize: 20, color: '#fff', fontWeight: 'bold'}}>
                00:30
              </Text>
            </View>
            <View style={[Styles.mainView]}>
              <Image
                resizeMode="contain"
                style={Styles.IamgeCall1}
                source={ImagePath.Rectangle5}
              />
            </View>
          </View>

          <View>
            <View style={Styles.contain}>
              <View style={Styles.mainView}>
                <Icons size={40} source={ImagePath.call_circle} />
              </View>

              <View style={Styles.mainView}>
                <Icons size={40} source={ImagePath.face_circle} />
              </View>

              <View style={Styles.mainView}>
                <Image
                  resizeMode="contain"
                  style={Styles.IamgeCall}
                  source={ImagePath.calldisconnect}
                />
              </View>

              <View style={Styles.mainView}>
                <Icons size={40} source={ImagePath.microphone_circle} />
              </View>

              <View style={Styles.mainView}>
                <Icons size={40} source={ImagePath.speaker_circle} />
              </View>
            </View>
            <SafeAreaView />
          </View>
        </ImageBackground>
      </View>
    );
  }
}
const Styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: Colors.white},
  splashsz: {width: '100%', height: '100%', resizeMode: 'stretch'},
  maincontain: {
    position: 'absolute',
    top: 10,
    flexDirection: 'row',
    paddingVertical: 0,
  },
  IamgePath: {width: 40, height: 40},
  Iamgecamera: {width: 35, height: 35},
  IamgeCall: {width: 60, height: 60,marginBottom:10},
  backiocn: {width: 20, height: 20},
  IamgeCall1: {width: 90, height: 90, top: 30},
  mainView: {
    marginHorizontal: 15,
    flex: 0.4,
    flexDirection: 'row',
    alignItems: 'center',
  },
  contain: {
    // position: 'absolute',
    // bottom: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
  },
});
