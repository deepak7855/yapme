import React, {useMemo} from 'react';
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Colors from '../../../Assets/Colors';
// import fonts from '../../../Assets/Fonts';
import {images} from '../../../Assets/ImageUrl';
import {generateColor} from '../../../Utility';
import {Searchbar} from '../../../Component/Common';
import CHeader from '../../../Common/CHeader';

export default Creategroup = React.FC = (props) => {
  const colors = useMemo(
    () => [...new Array(20)].map(() => generateColor()),
    [],
  );

  return (
    <SafeAreaView style={styles.container}>
      <CHeader navigation={props.navigation} backArrowLeft centerItem />
      <Searchbar placeholder="Search Friends" />
      <TouchableOpacity
      onPress={()=>props.navigation.navigate('Chatgroup')}
        style={{
          flexDirection: 'row',
          marginHorizontal: 25,
          paddingVertical: 10,
          marginVertical:10,
          alignItems:'center',
        //   backgroundColor:Colors.bgcolor,
        }}>
        <Image
          resizeMode="contain"
          style={{height:40, width:40,marginRight:15}}
          source={images.group_icon}
        />
        <Text>New Group</Text>
      </TouchableOpacity>
      <FlatList
        contentContainerStyle={{
          paddingBottom: 80,
          // paddingTop:10,
        }}
        data={['', '', '', '', '', '']}
        renderItem={({item: color}) => (
          <View
            style={[
              styles.item,
              {
                backgroundColor: '#fff',
                justifyContent: 'center',
                paddingHorizontal: 20,
                height: 60,
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.15,
                shadowRadius: 1.14,

                elevation: 3,
              },
            ]}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image
                  resizeMode="contain"
                  style={{height: 35, width: 35}}
                  source={images.Profile}
                />
                <View style={{marginLeft: 20}}>
                  <Text style={{marginBottom: 5}}>Odell smith</Text>
                  <Text style={styles.grytextStyle}>Awesome work</Text>
                </View>
              </View>
              {/* <Text>2</Text> */}

              <Text style={styles.grytextStyle}>8:10</Text>
            </View>
          </View>
        )}
        keyExtractor={(item, idx) => `item_${idx}`}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:Colors.bgcolor,
  },
  item: {
    margin: 10,
    height: 30,
    borderRadius: 10,
  },

  grytextStyle: {
    color: Colors.greyColor,
    fontSize: 13,
    // fontFamily:fonts.NovaRegular
  },
});
