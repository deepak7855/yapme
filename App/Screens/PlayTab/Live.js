import React, {Component} from 'react';
import {
  ImageBackground,
  SafeAreaView,
  FlatList,
  Image,
  Text,
  View,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import {Icons} from '../../Component/Common';

export default class Live extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <StatusBar
          backgroundColor={Colors.violetBlue}
          barStyle="light-content"
        />
        <SafeAreaView style={{backgroundColor: Colors.black}} />

        <View style={{flex: 1}}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: Colors.black,
              paddingVertical: 10,
            }}>
            <View style={{alignItems: 'center'}}>
              <Icons
                size={25}
                source={images.globe}
                _style={{tintColor: '#ddd'}}
                onPress={() => this.props.navigation.navigate('Home')}
              />
              <View
                style={{
                  borderWidth: 1.3,
                  borderColor: Colors.white,
                  width: 30,
                  marginTop: 8,
                }}
              />
            </View>
            <View
              style={{
                borderWidth: 0.8,
                marginHorizontal: 30,
                borderColor: Colors.white,
                height: 20,
              }}
            />

            {/* 2nd */}
            <View style={{alignItems: 'center'}}>
              <Icons
                size={25}
                source={images.live_news}
                _style={{tintColor: Colors.white}}
              />
              <View
                style={{
                  borderWidth: 1.3,
                  borderColor: Colors.white,
                  width: 30,
                  marginTop: 8,
                }}
              />
            </View>
            <View
              style={{
                borderWidth: 0.8,
                marginHorizontal: 30,
                borderColor: Colors.white,
                height: 20,
              }}
            />

            {/* 3rd */}
            <View style={{alignItems: 'center'}}>
              <Icons
                size={20}
                source={images.search_active}
                _style={{tintColor: '#ddd'}}
                onPress={() => this.props.navigation.navigate('Search')}
              />
              <View
                style={{
                  borderWidth: 1.3,
                  borderColor: Colors.white,
                  width: 30,
                  marginTop: 8,
                }}
              />
            </View>
            {/* <View style={{borderWidth:.8,marginHorizontal:30,borderColor:Colors.white,height:20,}}/> */}
          </View>

          {/* <View style={{backgroundColor:'red'}}>
            <Text>d</Text>
            <Text>d</Text>
            <Text>d</Text>
            <Text>d</Text>
            <Text>d</Text>
            <Text>d</Text>
            <Text>d</Text>
        </View> */}

          <View>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{
                // backgroundColor: 'red',
                //   paddingBottom: 80,
                // paddingTop:10,
                // marginLeft: 10,
                paddingHorizontal: 10,
                marginTop: 15,
              }}
              data={['', '', '', '', '', '', '', '', '']}
              renderItem={({item: color, index}) => (
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      alignItems: 'center',
                      marginRight: 15,
                      marginTop: 5,
                    }}>
                    <Image
                      resizeMode="contain"
                      style={{height: 50, width: 50}}
                      source={images.Profile}
                    />
                    {/* <View
                  style={{
                    position: 'absolute',
                    bottom: -15,
                    // right:0,
                    // bottom:0,
                    // left:0,
                    marginVertical: 5,
                  }}>
                
               
                    <Icons
                      size={35}
                      source={images.live}
                      _style={{bottom: 0, left: 1}}
                    />

              
                </View> */}
                    <Icons
                      size={35}
                      source={images.live}
                      _style={{bottom: -15, right: -18, position: 'absolute'}}
                    />
                    <Text
                      style={{
                        marginVertical: 13,
                        fontFamily: fonts.NovaRegularios,
                        fontSize: fonts.fontSize13,
                      }}>
                      @sara
                    </Text>

                    {/* {index % 2 == 0 ? (
                <Icon
                  name="circle"
                  size={12}
                  color={Colors.green}
                  style={{bottom: 8, left: 10}}
                />
              ) : (
                <Icons
                  size={35}
                  source={images.live}
                  _style={{bottom: 20, left: 1}}
                />
              )}  */}
                  </View>
                </View>
              )}
            />
          </View>

          <FlatList
            showsVerticalScrollIndicator={false}
            data={['', '', '', '', '', '', '', '', '', '', '', '']}
            contentContainerStyle={
              {
                //   marginTop: 15,
                paddingBottom:'15%'
              }
            }
            renderItem={({}) => (
              <TouchableOpacity
              onPress={()=>this.props.navigation.navigate('LiveStreaming')}
              
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  margin: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: Colors.lightgrey,
                }}>
                {/* <Icons
                  size={150}
                  source={images.sq1}
                  _style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: 100,
                    // flex:1
                  }}
                /> */}
                {/* <TouchableOpacity 
                style={{flex:1}}
                  onPress={()=>this.props.navigation.navigate('LiveStreaming')}
                
                > */}
                 <Image
                  style={styles.imageThumbnail}
                  source={images.Rectangle5}
                />
                {/* </TouchableOpacity> */}
                
                <View
                  style={{
                    position: 'absolute',
                    justifyContent: 'space-between',
                    height: 160,
                    right: 15,
                  }}>
                  <Text
                    style={{
                      fontFamily: fonts.NovaRegularios,
                      fontSize: fonts.fontSize13,
                      color: Colors.white,
                    }}>
                    145
                  </Text>
                  {/* <Icons
                    size={15}
                    source={images.radio_white}
                    _style={{marginHorizontal: 6}}
                    //   onPress={()=>this.props.navigation.navigate('FriendRequested')}
                  /> */}
                </View>
              </TouchableOpacity>
            )}
            //Setting the number of column
            numColumns={2}
            keyExtractor={(item, index) => index}
          />

        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  View4: {fontFamily: fonts.NovaRegularios, fontSize: fonts.fontSize20},
  txt: {
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaRegularios,
    color: Colors.white,
  },

  settings_text_view: {
    marginHorizontal: 12,
    marginTop: 25,
  },
  settings_text: {
    fontSize: fonts.fontSize16,
    fontFamily: fonts.NovaBold,
    color: Colors.black,
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    // flex: 1,
    height: 180,
    width: '100%',
  },
});
