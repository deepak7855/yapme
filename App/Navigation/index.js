// export {TabBar} from './TabBar';

import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
// import {createDrawerNavigator} from '@react-navigation/drawer';
import Signup from '../Screens/SignupScreen/Signup';
import Login from '../Screens/LoginScreen/Login';
import ForgotPassword from '../Screens/ForgotPasswordScreen/ForgotPassword';
import {TabBar} from './TabBar';
import { Spalsh } from '../Screens/SplashScreen/Splash';
import FriendSList from '../Screens/FriendsListScreen/FriendSList';

const Stack = createStackNavigator();
// const Drawer = createDrawerNavigator();

// const drawerNavigation = () => {
//     return (
//         <Stack.Navigator initialRouteName="HomeScreen">
//             <Stack.Screen name="EditProfile" component={EditProfile} />
//             <Stack.Screen name="HearingQuest" component={HearingQuest} />
//             <Stack.Screen name="HomeScreen" component={HomeScreen} />
//             <Stack.Screen name="PatientForm" component={PatientForm} />
//             <Stack.Screen name="Payment" component={Payment} />
//         </Stack.Navigator>
//     );
// };

// const MyDrawer = () => (
//     <Drawer.Navigator
//         drawerStyle={{
//             width: '80%'
//         }}
//         initialRouteName="Home"
//         drawerType="front" drawerContent={props => <SideMenu {...props} />}>
//         <Drawer.Screen name="Home" component={drawerNavigation} />
//     </Drawer.Navigator>
// )

function Routes() {
  return (
    <NavigationContainer>
      <Stack.Navigator
      headerMode={false}
      // initialRouteName="SplashScreen"
      >
        {/* <Reportaproblem/> */}
        {/* <Following/> */}
        {/* <TabBar barColor="#fff" /> */}
        {/* <Fansfollowings /> */}
        {/* <WalletTab/> */}
        {/* <Paymentpassword/> */}
        {/* <AllStarCount/> */}
        {/* <Notifications/> */}
        {/* <CreatePost/> */}
        {/* <FriendSList/> */}
        {/* <Chatgroup/> */}
        {/* <FriendRequested/> */}
        {/* <PrivacyAndSafety/> */}
        {/* <Routes /> */}
        {/* <FlashMessage ref="myLocalFlashMessage" />
        <ActivityIndicatorApp
          onRef={ref => { Helper.globalLoader = ref }}
        /> */}
        <Stack.Screen
          name="Splash"
          component={Spalsh}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Signup" component={Signup} />
        <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
        <Stack.Screen name="TabBar" component={TabBar} />


        {/* <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen
          name="OrganizationsWebView"
          component={OrganizationsWebView}
        />
        <Stack.Screen name="SignUpScreen" component={SignUpScreen} />
        <Stack.Screen name="PatientForm" component={PatientForm} />
        <Stack.Screen name="Payment" component={Payment} />
        <Stack.Screen name="PatientDetail" component={PatientDetail} />
        <Stack.Screen name="forgotPassword" component={forgotPassword} />
        <Stack.Screen name="ChangePassword" component={ChangePassword} />
        <Stack.Screen name="Feedback" component={Feedback} />
        <Stack.Screen name="Settings" component={Settings} />
        <Stack.Screen name="Result" component={Result} />
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="Home" component={MyDrawer} />
        <Stack.Screen name="Notifications" component={Notifications} />
        <Stack.Screen name="PrivacyPolicy" component={PrivacyPolicy} />
        <Stack.Screen
          name="SaTestingOneCardScreen"
          component={SaveCardScreen}
        />
        <Stack.Screen name="Disclaimer" component={Disclaimer} />
        <Stack.Screen
          name="TermsAndConditions"
          component={TermsAndConditions}
        />
        <Stack.Screen name="SupportTeam" component={SupportTeam} />
        <Stack.Screen name="PatientHistory" component={PatientHistory} />
        <Stack.Screen name="AboutToTest" component={AboutToTest} />
        <Stack.Screen name="TestingOn" component={TestingOn} /> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export function handleNavigation(nav) {
  console.log('====navigation', nav.navigation);
  switch (nav.type) {
    case 'push':
      nav.navigation.navigate(nav.page, nav.passProps);
      break;
    case 'setRoot':
      nav.navigation.reset({index: 0, routes: [{name: nav.page}]});
      break;
    case 'pop':
      nav.navigation.goBack();
      break;
    case 'popTo':
      break;
  }
}

export default Routes;
