import React from 'react';
import {
  TextInput,
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

import Colors from '../Assets/Colors';
import Fonts from '../Assets/Fonts';
import {images} from '../Assets/ImageUrl';

const Input = (props) => {
  let titleStyle = props.customeTilteStyle
    ? props.customeTilteStyle
    : styles.input_style;

  let imageStyle = props.customeImageStyle
    ? props.customeImageStyle
    : styles.image_style;
  return (
    <View style={{marginVertical: 10}}>
      <View style={{flexDirection: 'row'}}>
        <Text
          style={{
            fontSize: Fonts.fontSize13,
            fontFamily: Fonts.NovaRegular,
            color: Colors.pinkishGrey,
            // lineHeight: 15,
          }}>
          {props.labelText}
        </Text>
        <Text
          style={{
            fontSize: Fonts.fontSize10,
            color: Colors.red,
          }}>
          {props.start}
        </Text>
      </View>

      <View
        style={{
          borderBottomWidth: 0.8,
          borderBottomColor: Colors.charcoalGrey,
          flexDirection: 'row',
          width: '100%',
          justifyContent: 'space-between',
        }}>
        <TextInput
          underlineColorAndroid="transparent"
          placeholder={props.placeholder}
          autoCorrect={false}
          placeholderTextColor={Colors.black}
          secureTextEntry={props.secureTextEntry}
          value={props.value}
          onChangeText={props.onChangeText}
          keyboardType={props.keyboardType}
          maxLength={props.maxLength}
          returnKeyType={props.returnKeyType}
          blurOnSubmit={props.blurOnSubmit}
          onSubmitEditing={props.setFocus}
          ref={props.getFocus}
          editable={props.inputedit}
          style={titleStyle}
        />
        <TouchableOpacity onPress={props.onPress}>
          <Image
            source={props.rightImagePath}
            resizeMode={'contain'}
            style={imageStyle}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};
export {Input};

const styles = StyleSheet.create({
  input_style: {
    marginLeft: 5,
    width: 260,
    fontSize: Fonts.fontSize13,
    fontFamily: Fonts.NovaRegular,
    lineHeight: 15,
  },
  image_style: {
    height: 35,
    width: 12,
    // marginTop:10
  },
});

const InputText = (props) => {
  return (
    <View style={{alignItems: 'center'}}>
      <TextInput
        style={{
          borderBottomWidth: 0.5,
          height: 35,
          // width:'100%',
          borderColor: props.borderColor,
          // paddingHorizontal: 15,
          fontSize: props.fontSize ? props.fontSize : 12,
          color: props.color,
          fontFamily: Fonts.NovaRegular,
          //width:'100%',
          width: props.width,
          backgroundColor: props.backgroundColor,
        }}
        placeholder={props.placeholder}
        placeholderTextColor={props.placeholderTextColor}
        maxLength={props.maxLength}
        keyboardType={props.keyboardType}
        secureTextEntry={props.secureTextEntry}
        returnKeyType={props.returnKeyType}
        blurOnSubmit={props.blurOnSubmit}
        onSubmitEditing={props.setFocus}
        ref={props.getFocus}
        value={props.value}
      />
    </View>
  );
};

export {InputText};
