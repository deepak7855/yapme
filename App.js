import React from 'react';
import {
  Text,
  View,
  YellowBox,
  StatusBar,
  LogBox,
  SafeAreaView,
} from 'react-native';
import {TabBar} from './App/Navigation/TabBar';
import Routes from './App/Navigation';
// import FlashMessage from "react-native-flash-message";
// import { Helper, ActivityIndicatorApp } from './app/Api';

// import Login from './App/Screens/LoginScreen/Login';
// import Splash from './App/Screens/SplashScreen/Splash';
// import ForgotPassword from './App/Screens/ForgotPasswordScreen/ForgotPassword';
// import Signup from './App/Screens/SignupScreen/Signup';
// import EditProfile from './App/Screens/EditProfileScreen/EditProfile';
import PrivacyAndSafety from './App/Screens/PrivacyAndSafety/PrivacyAndSafety';
import HelpCenter from './App/Screens/HelpCenter/HelpCenter';
// import WhoCanSeeYourFollowers from './App/Screens/WhoCanSeeYourFollowers/WhoCanSeeYourFollowers';
// import WalletHistory from './App/Screens/WalletHistoryScreen/WalletHistory';
// import Reportaproblem from './App/Screens/ReportaproblemScreen/Reportaproblem';
import FriendRequested from './App/Screens/FriendRequestedScreen/FriendRequested';

import Chatgroup from './App/Screens/MessageTab/CreateGroup/Chatgroup';
import Fansfollowings from './App/Screens/FansFollowing/Fansfollowing';
import FriendSList from './App/Screens/FriendsListScreen/FriendSList';

//new screen 9 Oct
import CommunityGuidelines from './App/Screens/CommunityGuidelinesScreen/CommunityGuidelines';
import Termsofuse from './App/Screens/TermsofuseScreen/Termsofuse';
import OtherUserProfile from './App/Screens/OtherUserProfileScreen/OtherUserProfile';
import Settings from './App/Screens/SettingsScreen/Settings';
import Following from './App/Screens/FollowingScreen/Following';
import WalletTab from './App/Screens/WalletScreen/WalletTab';
import Paymentpassword from './App/Screens/WalletScreen/Paymentpassword';
import AllStarCount from './App/Screens/WalletScreen/AllStarCount';
import Notifications from './App/Screens/NotificationsScreen/Notifications';
import CreatePost from './App/Screens/CenterTab/CreatePost';
import Colors from './App/Assets/Colors';

import MyProfile from './App/Screens/MyProfileScreen/MyProfile';
import Helper from './App/Lib/Helper';
import LiveStreaming from './App/Screens/PlayTab/LiveStreaming';
import Center from './App/Screens/CenterTab/Center';
import Story from './App/Screens/CenterTab/Story';
import Addmusic from './App/Screens/AddMusic/Addmusic';
import PostVideo from './App/Screens/PostVideoScreen/PostVideo';
import Viwerdashbord from './App/Screens/ViewScreen/Viwerdashbord';
import VideoCall from './App/Screens/VideoScreens/VideoCall';
import LiveEnable from './App/Screens/LiveScreens/LiveEnable';
import LiveEnableState from './App/Screens/LiveScreens/LiveEnableState';
import LiveEndedProfile from './App/Screens/LiveScreens/LiveEndedProfile';



import WhoCanSeeYourFollowers from './App/Screens/WhoCanSeeYourFollowers/WhoCanSeeYourFollowers';
import SplashScreen from 'react-native-splash-screen';



// SplashScreen
export default class App extends React.Component {

  componentDidMount() {
    SplashScreen.hide();
    
  }
  
  render() {
    console.disableYellowBox = true;
    return (
      <View style={{flex: 1}}>
        <StatusBar
          backgroundColor={Colors.violetBlue}
          barStyle="dark-content"
        />
        {/* <SafeAreaView/> */}
        {/* <Settings/> */}
        {/* <Reportaproblem/> */}
        {/* <MyProfile/> */}
        {/* <TabBar/> */}
        {/* <TabBar/> */}
        {/* <Following/> */}
        {/* <Fansfollowings /> */}
        {/* <WalletTab/> */}
        {/* <Paymentpassword/> */}
        {/* <AllStarCount/> */}
        {/* <Notifications/> */}
        {/* <CreatePost/> */}
        {/* <FriendSList/> */}
        {/* <Chatgroup/> */}
        {/* <FriendRequested/> */}
        {/* <PrivacyAndSafety/> */}
        {/* <OtherUserProfile /> */}
        {/* <CreatePost/> */}
        {/* <Center/> */}
        {/* <Story/> */}
        {/* <PostVideo/> */}
        <Routes/>
        {/* <LiveEnable/> */}
        {/* <LiveEnableState/> */}
        {/* <LiveEndedProfile/> */}
        {/* <WhoCanSeeYourFollowers/> */}
        {/* <VideoCall/> */}
        {/* <Viwerdashbord/> */}
        {/* <Addmusic/> */}
        {/* <LiveStreaming/> */}
        {/* <FlashMessage ref="myLocalFlashMessage" />
        <ActivityIndicatorApp
          onRef={ref => { Helper.globalLoader = ref }}
        /> */}
      </View>
    );
  }
}
