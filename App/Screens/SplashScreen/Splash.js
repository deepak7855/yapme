import React from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
} from 'react-native';
import Helper from '../../Lib/Helper';
import {images} from '../../Assets/ImageUrl';
import SplashScreen from 'react-native-splash-screen'

export class Spalsh extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  async componentDidMount() {
    SplashScreen.hide();
    setTimeout(() => {
      this.props.navigation.reset({
        index: 0,
        routes: [{name: 'Login'}],
      });
    }, 1000);
    Helper.navigationRef = this.props.navigation;
    // Helper.registerNavigator(this.props.navigation)
  }

  // await Helper.getData("UserData").then((data) => {
  //   console.log(data, "_resintro");
  //   if (data) {
  //     console.log(data, "resintro");
  //     setTimeout(() => {
  //       this.props.navigation.reset({
  //         index: 0,
  //         routes: [{ name: "HomeTab" }],
  //       });
  //     }, 1000);
  //   }else if(data && !data.is_dog_profile){
  //     setTimeout(() => {
  //         this.props.navigation.reset({
  //           index: 0,
  //           routes: [{ name: "DogProfile" }],
  //         });
  //       }, 1000);
  //   }
  //   else if(!data) {
  //     setTimeout(() => {
  //       this.props.navigation.reset({
  //         index: 0,
  //         routes: [{ name: "Intro" }],
  //       });
  //     }, 1000);
  //   }
  // });

  render() {
    // alert('hi')
    return (
      <ImageBackground style={{flex: 1}}  source={images.new_splash}/>

      // <View style={{}}>
      //   <View style={styles.container_splash}>
      //     <Image
      //       resizeMode="contain"
      //       source={images.new_splash}
      //       style={styles.splash}
      //     />
      //   </View>
      // </View>
    );
  }
}
const styles = StyleSheet.create({
  splash: {
    width: '100%',
    height: '100%',
  },
  container_splash: {
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

// import React, { Component } from 'react';
// import {ImageBackground } from 'react-native';
// import styles from './SplashStyles';
// import { images } from '../../Assets/ImageUrl';

// class Splash extends Component {

//     constructor(props) {
//         super(props)
//         this.state = {

//         }
//     }

//     render() {
//         return (
//             <ImageBackground source={images.new_splash} style={styles.image_back_ground}>

//             </ImageBackground>
//         )
//     }
// };

// export default Splash;
