import Colors from "../Assets/Colors";

export default CommonStyle = {
  borderColor: Colors.white,
  backgroundColor: Colors.white,
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 1,
  },
  shadowOpacity: 0.15,
  shadowRadius: 1.94,
  elevation: 3,
};
