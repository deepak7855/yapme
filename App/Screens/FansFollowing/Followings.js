import React, {Component} from 'react';
import {
  SafeAreaView,
  TextInput,
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import Header from '../../Common/Header';
import {Icons} from '../../Component/Common';

export default class Followings extends Component {
    constructor(props) {
        super(props);
        this.state = {
          arrFriendRequest: [
            {username: 'Braulio'},
            {username: 'Jayda'},
            {username: 'Crawford'},
            {username: 'Genevieve'},
            {username: 'Ignatius'},
            {username: 'Ophelia'},
            {username: 'Ryleigh'},
            {username: 'Juanita'},
            {username: 'Jalon'},
          ],
        };
      }
    
      _renderFriendRequestList = ({item, index}) => {
        return (
          <View style={styles.friend_req_view}>
            <View style={styles.img_name_view}>
              <View style={styles.user_img_view}>
                <Image
                  source={images.Profile}
                  resizeMode={'cover'}
                  style={styles.user_img}
                />
              </View>
              <View style={styles.name_view}>
                <Text numberOfLines={1} style={styles.name_text}>
                  {item.username}
                </Text>
              </View>
            </View>
    
            <View style={styles.btn_view}>
              <Icons
                size={50}
                source={images.chat_circle}
                _style={{marginRight: 10}}
                onPress={()=>this.props.navigation.navigate('Message')}
              />
    
              <TouchableOpacity style={styles.accept_btn_touch}>
                <Text style={styles.accept_text}>Unfollow</Text>
              </TouchableOpacity>
            </View>
          </View>
        );
      };
      render() {
        return (
          <SafeAreaView style={styles.safe_area_view}>
            {/* <Header
                        backArrowLeft
                        centerImage
                    /> */}
            {/* <View style={styles.notification_view}>
                        <Text style={styles.notification_text}>Notification</Text>
                    </View> */}
            <FlatList
          contentContainerStyle={{marginTop:10,paddingBottom:20}}
              showsVerticalScrollIndicator={false}
              data={this.state.arrFriendRequest}
              renderItem={this._renderFriendRequestList}
              extraData={this.state}
              keyExtractor={(item, index) => index.toString()}
            />
          </SafeAreaView>
        );
      }
    }
    
    const styles = StyleSheet.create({
      safe_area_view: {
        flex: 1,
      },
      notification_view: {
        marginHorizontal: 15,
        marginVertical: 5,
      },
      friend_req_view: {
        borderRadius: 10,
        marginVertical:6,
        paddingHorizontal: 10,
        // paddingVertical: 10,
        borderWidth: 1,
        borderColor: Colors.white,
        marginHorizontal: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.15,
        shadowRadius: 1.94,
        elevation: 3,
      },
      img_name_view: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 4,
      },
      user_img_view: {
        height: 30,
        width: 30,
        borderRadius: 30 / 2,
      },
      user_img: {
        width: '100%',
        height: '100%',
      },
      name_view: {
        width: 120,
        marginLeft: 10,
      },
      name_text: {
        lineHeight: 17,
        //   fontsize: fonts.fontsize15,
        //   fontFamily: fonts.NovaSbold,
        color: Colors.black,
      },
      btn_view: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 3,
        // backgroundColor: 'red',
        justifyContent: 'flex-end',
      },
      accept_btn_touch: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        paddingHorizontal: 13,
        paddingVertical: 6,
        borderColor: Colors.violetBlue,
        borderWidth: 1,


      },
      accept_text: {
        fontSize: fonts.fontSize10,
        color: Colors.violetBlue,
        fontFamily: fonts.NovaBoldios,
      },
      reject_btn_touch: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 59,
        height: 22,
        borderColor: Colors.violetBlue,
        borderRadius: 5,
        borderWidth: 1,
      },
      reject_text: {
        //   fontsize: fonts.fontsize10,
        // lineHeight: 12,
        color: Colors.violetBlue,
        //   fontFamily: fonts.NovaBold,
      },
    });
    