
import  Helper  from './Helper';


const User = {
  sendotp: (data) => Helper.makeRequest({url:'send-otp',method:'post',data:data,}),
  verifyotp: (data) => Helper.makeRequest({url:'verify-otp',method:'post',data:data}),
  resendOtp:(data) => Helper.makeRequest({url:'resend-otp',method:'post',data:data}),
  confirmotp: (data) => Helper.makeRequest({url:'confirm-otp',method:'post',data:data}),
  mydogs: () => Helper.makeRequest({url:'my-dogs',method:'get',}),
  

  forgotpassword: (data) => Helper.makeRequest({url:'forgot-password',method:'post',data:data}),
  getbreeds: (data) => Helper.makeRequest({url:'get-breeds',method:'get',}),
  adddog: (data) => Helper.makeRequest({url:'add-dog',data:data,method:'post',}),
  updatedog: (data) => Helper.makeRequest({url:'update-dog',data:data,method:'post',}),
  
  login: (data) => Helper.makeRequest({url:'signin',method:'post',data:data}),
  changepassword:(data,fromWhere) => Helper.makeRequest({url:fromWhere == 'forgot' ? 'update-password' : 'change-password',data:data,method:'post',}),
  updateProfile:(data) => Helper.makeRequest({url:'update-profile',method:'post',data:data}),

  

  logout: (data) => Helper.makeRequest({url:'logout',data:data,method:'post',}),

  
  

  userProfile:(data,userId)=> Helper.makeRequest({url:`user-profile/${userId}/`,method:'put',data:data,isImage:true}),  // remain update 
  getUserInfo: (userId) => Helper.makeRequest({url:`user-profile/${userId}/`,method:'get',}),

  



  getCountrycode: (data) => Helper.makeRequest({url:'get-country',method:'get',}),


  forgotPassword:(data) => Helper.makeRequest({url:'forgot-password/',data:data,method:'post',}),
  

  forgetChangePassword:(data) => Helper.makeRequest({url:'forget-change-password/',data:data,method:'put',}),
  


};

export default { User,};
