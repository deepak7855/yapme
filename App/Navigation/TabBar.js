import * as React from 'react';
import 'react-native-gesture-handler';
import {StyleSheet, View, Image, Text} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {
  BottomTabBar,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
// import { FontAwesome as Icon } from '@expo/vector-icons';
import Icon from 'react-native-vector-icons/FontAwesome';

import {
  BottomTabBarWrapper,
  MultiBarButton,
  MultiBarProvider,
} from 'react-native-multibar';

import {TabBarAdvancedButton} from '../Component';
import {EmptyScreen} from '../Screens/HomeScreen/EmptyScreen';
import {IS_IPHONE_X} from '../Utility';
import {images} from '../Assets/ImageUrl';
import Chat from '../Screens/MessageTab/ChatScreen/Chat';
import Creategroup from '../Screens/MessageTab/CreateGroup/Creategroup';
import Chatgroup from '../Screens/MessageTab/CreateGroup/Chatgroup';
import Message from '../Screens/MessageTab/Message';
import EditProfile from '../Screens/EditProfileScreen/EditProfile';
import Group from '../Screens/GroupTab/Group';
import UserProfile from '../Screens/GroupTab/UserProfile';
import MyProfile from '../Screens/MyProfileScreen/MyProfile';
import Fansfollowings from '../Screens/FansFollowing/Fansfollowing';
import FriendSList from '../Screens/FriendsListScreen/FriendSList';
import Settings from '../Screens/SettingsScreen/Settings';
import PrivacyAndSafety from '../Screens/PrivacyAndSafety/PrivacyAndSafety';
import MyDraft from '../Screens/MyDraftScreen/MyDraft';
import Notifications from '../Screens/NotificationsScreen/Notifications';
import Reportaproblem from '../Screens/ReportaproblemScreen/Reportaproblem';
import HelpCenter from '../Screens/HelpCenter/HelpCenter';
import Termsofuse from '../Screens/TermsofuseScreen/Termsofuse';
import FriendRequested from '../Screens/FriendRequestedScreen/FriendRequested';
import WalletTab from '../Screens/WalletScreen/WalletTab';
import Home from '../Screens/PlayTab/Home';
import Live from '../Screens/PlayTab/Live';
import Search from '../Screens/PlayTab/Search';
import LiveStreaming from '../Screens/PlayTab/LiveStreaming';
import {TabBg} from '../svg';
import {Icons} from '../Component/Common';
import Center from '../Screens/CenterTab/Center';
import Video from '../Screens/CenterTab/Video';
import CreatePost from '../Screens/CenterTab/CreatePost';
import Story from '../Screens/CenterTab/Story';
import OtherUserProfile from '../Screens/OtherUserProfileScreen/OtherUserProfile';
import VideoCall from '../Screens/VideoScreens/VideoCall';
import Followings from '../Screens/FansFollowing/Followings';
import Addmusic from '../Screens/AddMusic/Addmusic';
import PostVideo from '../Screens/PostVideoScreen/PostVideo';
import Viwerdashbord from '../Screens/ViewScreen/Viwerdashbord';
import WhoCanSeeYourFollowers from '../Screens/WhoCanSeeYourFollowers/WhoCanSeeYourFollowers';

const Stack = createStackNavigator();

function ChatTabs() {
  return (
    // <NavigationContainer>
    <Stack.Navigator headerMode={false}>
      <Stack.Screen
        name="Chat"
        component={Chat}
        options={{
          tabBarVisible: false,
        }}
      />
      <Stack.Screen
        name="Creategroup"
        component={Creategroup}
        options={{
          tabBarVisible: false,
        }}
      />
    </Stack.Navigator>
    // </NavigationContainer>
  );
}

function GroupTabs() {
  return (
    // <NavigationContainer>
    <Stack.Navigator headerMode={false}>
      <Stack.Screen
        name="Group"
        component={Group}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="OtherUserProfile"
        component={OtherUserProfile}
        options={{
          tabBarVisible: false,
        }}
      />
    </Stack.Navigator>
    // </NavigationContainer>
  );
}

function PlayTabs() {
  return (
    <Stack.Navigator headerMode={false}>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="Live"
        component={Live}
        options={{
          tabBarVisible: false,
        }}
      />
      <Stack.Screen
        name="OtherUserProfile"
        component={OtherUserProfile}
        options={{
          tabBarVisible: false,
        }}
      />
    </Stack.Navigator>
  );
}

const BottomBar = createBottomTabNavigator();
type Props = {
  barColor: string,
};

export const TabBarHome: React.FC<Props> = (propss) => {
  console.log(propss,"propssssss")
  const Tab = createBottomTabNavigator(); //  React.useRef<ReturnType<typeof createBottomTabNavigator>>(createBottomTabNavigator()).current;
  return (
    // <MultiBarProvider
    //   data={[
    //     // ({navigation}) => (
    //     //   <Icon
    //     //     name="chevron-left"
    //     //     color="#E24E1B"
    //     //     size={20}
    //     //     onPress={() => {
    //     //       if (navigation.canGoBack()) {
    //     //         navigation.goBack();
    //     //       }
    //     //     }}
    //     //   />
    //     // ),
    //     ({navigation}) => (
    //       // <View style={{backgroundColor:'red',alignItems:'center',justifyContent:'center'}}>
    //       <>
    //         <Icons size={30} source={images.story_icon} onPress={() => {}} />
    //       </>

    //       // {/* <Text>ddd</Text> */}
    //       // </View>
    //     ),
    //     ({navigation}) => (
    //       <Icon
    //         name="headphones"
    //         color="#E24E1B"
    //         size={20}
    //         onPress={() => {}}
    //       />
    //     ),
    //     ({navigation}) => (
    //       <Icon name="heart" color="#E24E1B" size={20} onPress={() => {}} />
    //     ),
    //     ({navigation}) => (
    //       <Icon name="star" color="#E24E1B" size={20} onPress={() => {}} />
    //     ),
    //     ({navigation}) => (
    //       <Icon name="music" color="#E24E1B" size={20} onPress={() => {}} />
    //     ),
    //   ]}
    //   iconSize={40}
    //   overlayRadius={100}
    //   initialExtrasVisible={false}>
      <Tab.Navigator
        tabBar={(props) => (
          <View style={styles.navigatorContainer}>
            {/* <BottomTabBarWrapper navigation={props.navigation}> */}
              <BottomTabBar {...props} />
            {/* </BottomTabBarWrapper> */}

            {IS_IPHONE_X && (
              <View
                style={[
                  styles.xFillLine,
                  {
                    backgroundColor: '#fff',
                  },
                ]}
              />
            )}
          </View>
        )}
        tabBarOptions={{
          showIcon: true,
          showLabel: false,
          style: styles.navigator,
          tabStyle: {
            backgroundColor: '#fff',
          },
        }}>
        <Tab.Screen
          name="PlayTabs"
          component={PlayTabs}
          options={{
            tabBarIcon: ({color, focused}) => (
              <Image
                style={{height: 25, width: 25}}
                resizeMode="contain"
                source={focused ? images.play_lg : images.play_gray}
              />
            ),
          }}
        />

        <Tab.Screen
          name="GroupTabs"
          component={GroupTabs}
          options={{
            // headerShown: false, // not working
            // tabBarVisible: this.TabVisible(), //working
            tabBarIcon: ({color, focused}) => (
              <Image
                style={{height: 25, width: 25}}
                resizeMode="contain"
                source={focused ? images.posts_icon_color : images.posts_icon}
              />
            ),
          }}
        />
        <Tab.Screen
          name="Center"
          component={Center}

          options={{
          //         tabBarButton: (props) => (
          //     <TabBarAdvancedButton bgColor={'#fff'} {...props} />
          //   ),
            tabBarLabel: '',
            tabBarVisible: false,
            tabBarButton: (props) => (
              <TabBarAdvancedButton
                bgColor={'#fff'}
                {...props}
                onPress={()=>propss.navigation.navigate('Center')}
              />

              // <MultiBarButton
              //   style={{
              //     backgroundColor: 'transparent',
              //   }}>
              //   <View>
              //     <Image
              //       style={{height: 65, width: 65, bottom: 2}}
              //       resizeMode="contain"
              //       source={images.plus_icon}
              //     />
              //   </View>

              //   {/* <TabBg color={'#fff'} style={styles.background} > */}

              //   {/* <TabBarAdvancedButton bgColor={'#fff'} {...props} />  */}

              //   {/* <Icon
              //        name="add"
              //        style={{
              //        }}

              //        />
              //        </TabBg> */}
              // </MultiBarButton>
              // //  </TabBarAdvancedButton>
            ),
          }}
        />
        {/* 3rd */}
        <Tab.Screen
          name="ChatTabs"
          component={ChatTabs}
          options={{
            tabBarIcon: ({color, focused}) => (
              <Image
                style={{height: 25, width: 25}}
                resizeMode="contain"
                source={focused ? images.chat_color : images.chat}
              />
            ),
          }}
        />

        {/* 4th */}
        <Tab.Screen
          name="MyProfile"
          component={MyProfile}
          options={{
            tabBarIcon: ({color, focused}) => (
              <Image
                resizeMode="contain"
                style={{height: 25, width: 25}}
                source={focused ? images.Profile : images.Profile}
              />
            ),
          }}
        />
      </Tab.Navigator>
    // {/* </MultiBarProvider> */}
  );
};

export const TabBar = () => {
  return (
    <Stack.Navigator
      headerMode={false}
      // initialRouteName="SplashScreen"
    >
      <Stack.Screen name="TabBarHome" component={TabBarHome} />
      <Stack.Screen
        name="Story"
        component={Story}
        options={{
          tabBarVisible: false,
        }}
      />
      <Stack.Screen
        name="CreatePost"
        component={CreatePost}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="Center"
        component={Center}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="Video"
        component={Video}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="Settings"
        component={Settings}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="EditProfile"
        component={EditProfile}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="PrivacyAndSafety"
        component={PrivacyAndSafety}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="MyDraft"
        component={MyDraft}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="Notifications"
        component={Notifications}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="Reportaproblem"
        component={Reportaproblem}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="HelpCenter"
        component={HelpCenter}
        options={{
          tabBarVisible: false,
        }}
      />
      <Stack.Screen
        name="Termsofuse"
        component={Termsofuse}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="WalletTab"
        // component={(...props) => <WalletTab {...props} />}
        component={WalletTab}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="FriendRequested"
        component={FriendRequested}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="Fansfollowings"
        component={Fansfollowings}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="FriendSList"
        component={FriendSList}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="Chatgroup"
        component={Chatgroup}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="VideoCall"
        component={VideoCall}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="Message"
        component={Message}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="Search"
        component={Search}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="LiveStreaming"
        component={LiveStreaming}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="Addmusic"
        component={Addmusic}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="PostVideo"
        component={PostVideo}
        options={{
          tabBarVisible: false,
        }}
      />

      <Stack.Screen
        name="Viwerdashbord"
        component={Viwerdashbord}
        options={{
          tabBarVisible: false,
        }}
      />

<Stack.Screen
        name="WhoCanSeeYourFollowers"
        component={WhoCanSeeYourFollowers}
        options={{
          tabBarVisible: false,
        }}
      />




    </Stack.Navigator>
  );
};

// export const TabBar: React.FC<Props> = ({barColor}) => (
//   // <NavigationContainer>
//   <BottomBar.Navigator
//     tabBar={(props) => (
//       <View style={styles.navigatorContainer}>
//         <BottomTabBar {...props} />
//         {IS_IPHONE_X && (
//           <View
//             style={[
//               styles.xFillLine,
//               {
//                 backgroundColor: '#fff',
//               },
//             ]}
//           />
//         )}
//       </View>
//     )}
//     tabBarOptions={{
//       showIcon: true,
//       showLabel: false,
//       style: styles.navigator,
//       tabStyle: {
//         backgroundColor: '#fff',
//       },
//     }}>
//     <BottomBar.Screen
//       name="PlayTabs"
//       component={PlayTabs}
//       options={{
//         tabBarIcon: ({color, focused}) => (
//           <Image
//             style={{height: 20, width: 20}}
//             resizeMode="contain"
//             source={focused ? images.play_lg : images.play_gray}
//           />
//         ),
//       }}
//     />
//     <BottomBar.Screen
//       name="Group"
//       component={Group}
//       options={{
//         // headerShown: false, // not working
//         // tabBarVisible: this.TabVisible(), //working
//         tabBarIcon: ({color, focused}) => (
//           <Image
//             style={{height: 20, width: 20}}
//             resizeMode="contain"
//             source={focused ? images.posts_icon_color : images.posts_icon}
//           />
//         ),
//       }}
//     />

//     <BottomBar.Screen
//       name="Rocket"
//       component={EmptyScreen}
//       options={{
//         tabBarButton: (props) => (
//           <TabBarAdvancedButton bgColor={'#fff'} {...props} />
//         ),
//       }}
//     />

//     {/* 3rd */}
//     <BottomBar.Screen
//       name="ChatTabs"
//       component={ChatTabs}
//       options={{
//         tabBarIcon: ({color, focused}) => (
//           <Image
//             style={{height: 20, width: 20}}
//             resizeMode="contain"
//             source={focused ? images.chat_color : images.chat}
//           />
//         ),
//       }}
//     />

//     {/* 4th Tab */}
//     <BottomBar.Screen
//       name="ProfileTabs"
//       component={ProfileTabs}
//       options={{
//         tabBarIcon: ({color, focused}) => (
//           <Image
//             resizeMode="contain"
//             style={{height: 20, width: 20}}
//             source={focused ? images.Profile : images.Profile}
//           />
//         ),
//       }}
//     />
//   </BottomBar.Navigator>
//   // </NavigationContainer>
// );

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navigatorContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
  },
  navigator: {
    borderTopWidth: 0,
    backgroundColor: 'transparent',
    elevation: 30,
  },
  xFillLine: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 34,
  },
});
