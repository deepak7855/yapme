import { LoginManager, AccessToken } from 'react-native-fbsdk';
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import Helper from './Helper';
import googleClientId from './Config';

export function configureGoogleLogin() {
    try {
        GoogleSignin.configure({
            webClientId:'578513820411-rm4kh6s0h9566lhrjqbok361pt12h5ie.apps.googleusercontent.com',
            // '182688248977-ft9coghtkgtfmn8ua7tkp8qa821ajotm.apps.googleusercontent.com'
            //googleClientId,
            offlineAccess: true,
            hostedDomain: '',
            loginHint: '',
            forceConsentPrompt: true,
            accountName: '',
        });
    } catch ({ message }) {
    }
}

export function googleLogin(cb) {
    try {
        GoogleSignin.hasPlayServices().then(() => {
            GoogleSignin.signOut().then(() => {
                GoogleSignin.signIn().then((result) => {
                    let form = {
                        device_type: Helper.device_type,
                        device_token: Helper.device_token,
                        signup_via: 'Google',
                        social_id: result.user.id,
                        full_name: '',
                        email: '',
                        profile_image: '',
                    }

                    if (result.user.email) {
                        form.email = result.user.email;
                    }
                    if (result.user.name) {
                        form.full_name = result.user.name;
                    }
                    if (result.user.photo) {
                        form.profile_image = result.user.photo;
                    }
                    cb(form)
                }).catch((error) => {
                    console.log(error,"error")
                    cb(false)

                })
            })
        })

    } catch (error) {
        console.log(error,"error")
        cb(false);
        if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            // user cancelled the login flow
        } else if (error.code === statusCodes.IN_PROGRESS) {
            // operation (e.g. sign in) is in progress already
        } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
            // play services not available or outdated
        } else {
            // some other error happened
        }
    }
}

export function FacebookLogin(cb) {
    LoginManager.logOut();
    try {
        LoginManager.setLoginBehavior('native_with_fallback');
        LoginManager.logInWithPermissions(['public_profile', 'email']).then(
            function (result) {
                if (result.isCancelled) {
                    cb(false);
                } else {
                    AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            let token = data.accessToken.toString();
                            fetch('https://graph.facebook.com/me?fields=id,email,name,picture.width(720).height(720).as(picture),friends&access_token=' + token)
                                .then((response) => response.json())
                                .then((json) => {
                                    console.log(JSON.stringify(json));
                                    let form = {
                                        device_type: Helper.device_type,
                                        device_token: Helper.device_token,
                                        signup_via: 'Facebook',
                                        social_id: json.id,
                                        full_name: '',
                                        email: '',
                                        profile_image: '',
                                    }

                                    if (json.email) {
                                        form.email = json.email;
                                    }
                                    if (json.name) {
                                        form.full_name = json.name;
                                    }
                                    if (json.picture.data.url) {
                                        form.profile_image = json.picture.data.url;
                                    }
                                    cb(form);

                                })
                                .catch(() => {
                                    cb(false);
                                })
                        },
                        function (error) {
                            cb(false);
                        }
                    )
                }
            },
            function (error) {
                cb(false);
            }
        );
    } catch ({ message }) {
    }
}


// export function LinkedInLogin(token, cb) {
//     var varheaders = {
//         'Authorization': 'Bearer ' + token
//     }

//     fetch('https://api.linkedin.com/v2/me', {
//         method: 'GET',
//         headers: varheaders,
//     })
//         .then((response) => response.json())
//         .then((json) => {

//             let form = {
//                 device_type: InsuranceConstant.devicePlatform,
//                 device_token: 9999999999,
//                 signup_via: 'Linkedin',
//                 social_id: json.id,
//                 full_name: '',
//                 email: '',
//                 profile_image: '',
//             }

//             if (json.localizedFirstName) {
//                 form.full_name = `${json.localizedFirstName} ${json.localizedLastName}`;
//             }

//             fetch('https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~(emailAddress)))', {
//                 method: 'GET',
//                 headers: varheaders,
//             })
//                 .then((response) => response.json())
//                 .then((json) => {
//                     let handle = 'handle~';
//                     if (json.elements.length>0) {
//                         if(json.elements[0][handle].emailAddress){
//                             form.email = json.elements[0][handle].emailAddress;
//                         }
//                     }
//                     fetch('https://api.linkedin.com/v2/me?projection=(profilePicture(displayImage~digitalmediaAsset:playableStreams))', {
//                         method: 'GET',
//                         headers: varheaders,
//                     })
//                         .then((response) => response.json())
//                         .then((json) => {
//                             let displayImage = 'displayImage~';
//                             if (json.profilePicture[displayImage].elements[json.profilePicture[displayImage].elements.length - 1].identifiers[0].identifier) {
//                                 form.profile_image = json.profilePicture[displayImage].elements[json.profilePicture[displayImage].elements.length - 1].identifiers[0].identifier;
//                             }
//                             cb(form);
//                         })
//                         .catch((err) => {
//                             cb(form);
//                         })
//                 })
//                 .catch((err) => {
//                     cb(form);
//                 })

//         })
//         .catch((err) => {
//             cb(false);
//         })
// }