import { StyleSheet } from 'react-native';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';


export default MyProfileStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
    },
   
    about_des_view:{
        // marginHorizontal: 15, 
        marginTop: 10
    },
    about_des_text:{
        fontSize: Fonts.fontSize13, 
        fontFamily: Fonts.NovaRegular, 
        color: Colors.pinkishGreyTwo, 
        textAlign: 'left'
    },
    about_me_view:{
        // marginHorizontal: 15, 
        marginTop: 20
    },
    about_me_text:{
        fontSize: Fonts.fontSize13, 
        fontFamily: Fonts.NovaBold, 
        color: Colors.black
    },

});