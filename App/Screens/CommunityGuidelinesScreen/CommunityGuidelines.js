import React, { Component } from 'react';
import { SafeAreaView, View, Text, ScrollView, } from 'react-native';
import styles from './CommunityGuidelinesStyles';
import Header from '../../Common/Header';

class CommunityGuidelines extends Component {
    constructor(props) {
        super(props)

    }

    render() {
        return (
            <SafeAreaView style={styles.safe_area_view}>
                <Header
                    backArrowLeft
                    centerImage
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.Community_view}>
                        <Text style={styles.Community_text}>Community Guidelines</Text>
                    </View>

                    <View style={styles.guidelines_view}>
                        <Text style={styles.guidelines_text}>Guidelines</Text>
                    </View>
                    <View style={styles.guidelines_text_view}>
                        <Text style={styles.guidelines_text_des}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</Text>
                    </View>

                    <View style={styles.guidelines_text_view}>
                        <Text style={styles.guidelines_text_des}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</Text>
                    </View>

                    <View style={styles.guidelines_text_view}>
                        <Text style={styles.guidelines_text_des}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</Text>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

export default CommunityGuidelines;
