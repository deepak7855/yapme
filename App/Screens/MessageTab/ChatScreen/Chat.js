import React, {useMemo} from 'react';
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import Colors from '../../../Assets/Colors';
// import fonts from '../../../Assets/Fonts';
import {images} from '../../../Assets/ImageUrl';
import {generateColor} from '../../../Utility';
import {Icons, Searchbar} from '../../../Component/Common';
import CHeader from '../../../Common/CHeader';
import {Input} from '../../../Common/InputCommon';
import fonts from '../../../Assets/Fonts';

export default Chat = React.FC = (props) => {
  console.log(props, 'porps');
  const colors = useMemo(
    () => [...new Array(20)].map(() => generateColor()),
    [],
  );

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor={Colors.violetBlue} barStyle="dark-content" />
      <CHeader navigation={props.navigation} centerItem backArrowLeft />

      <Searchbar placeholder="Search Friends" />
      <View style={{height: 10}} />
      {/* <Input placeholder={'@elemar'} keyboardType={'email-address'} /> */}

      {/* <TextInput
        // style={{flex: 1, paddingLeft: 10}}
        placeholder={'asdsfd'} //"Search Friends"
      /> */}

      <FlatList
        contentContainerStyle={{
          paddingBottom: 80,
          marginTop: 15,
        }}
        data={['', '', '', '', '', '']}
        renderItem={({item: color}) => (
          <View
            style={[
              styles.item,
              CommonStyle,
              {
                justifyContent: 'center',
                paddingHorizontal: 20,
                height: 60,
              },
            ]}>
            <TouchableOpacity
              onPress={() => props.navigation.navigate('Message')}
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image
                  resizeMode="contain"
                  style={{height: 35, width: 35}}
                  source={images.Profile}
                />
                <View style={{marginLeft: 15}}>
                  <Text
                    style={{marginBottom: 5, fontFamily: fonts.NovaRegularios}}>
                    Odell smith
                  </Text>
                  <Text style={styles.grytextStyle}>Awesome work</Text>
                </View>
              </View>
              {/* <Text>2</Text> */}

              <Text style={styles.grytextStyle}>8:10</Text>
            </TouchableOpacity>
          </View>
        )}
        keyExtractor={(item, idx) => `item_${idx}`}
      />
      <View style={{marginBottom: 80, marginTop: 15}}>
        <Icons
          source={images.chat_ic_blue}
          size={35}
          _style={{alignSelf: 'flex-end', marginRight: 20}}
          onPress={() => props.navigation.navigate('Creategroup')}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#fff',
  },
  item: {
    margin: 10,
    height: 30,
    borderRadius: 10,
  },

  grytextStyle: {
    color: Colors.greyColor,
    fontSize: 13,
    // fontFamily:fonts.NovaRegular
  },
});
