import { StyleSheet } from 'react-native';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';


export default HelpCenterStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
    },
    help_center_view:{
        marginHorizontal:13
    },
    help_center_text:{
        fontSize:Fonts.fontSize16,
        lineHeight:20,
        color:Colors.black,
        fontFamily:Fonts.NovaBold
    },
    help_title_view:{
        marginHorizontal:13,
        marginVertical:10
    },
    help_title_text:{
        fontSize:Fonts.fontSize15,
        color:Colors.black,
        marginTop:15,
        fontFamily:Fonts.NovaRegular
    },
    desc_view:{
        marginHorizontal:13,
    },
    desc_text:{
        color:Colors.pinkishGreyTwo,
        fontSize:Fonts.fontSize13,
        lineHeight:20,
        textAlign:'left'
    },
   
});