import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Fans from './Fans';
import Followings from './Followings';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {SafeAreaView} from 'react-native';
import CHeader from '../../Common/CHeader';

import {
  TextInput,
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {Searchbar} from '../../Component/Common';
import {createStackNavigator} from '@react-navigation/stack';
import Message from '../MessageTab/Message';
// import { IS_IPHONE_X } from '../../Utility';


const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();

export default function Fansfollowings(props) {
  console.log(props.route.params.fromWhere,"2")

  return (
    // <NavigationContainer>
    <View style={{flex: 1}}>
      <CHeader navigation={props.navigation} backArrowLeft centerItem />
      <Searchbar placeholder={`Search ${(props.route.params.fromWhere).toLowerCase()}`} />
      <View style={{height: 20}} />
      <Tab.Navigator
      initialRouteName={props.route.params.fromWhere}
        tabBarOptions={{
          
          // tabBarLabel: {
          //   color: Colors.violetBlue,
          // },

          // indicatorStyle: {
          //   borderBottomColor: Colors.violetBlue,
          //   borderBottomWidth: 1.5,
          //   borderColor: Colors.violetBlue,
          // },
          indicatorStyle:{
            backgroundColor:Colors.vividPurple
          },
          activeTintColor: Colors.violetBlue,
          inactiveTintColor: 'lightgray',

          //   showLabel: false,
          // labelStyle: { fontSize: 12 },
          // tabStyle: { width: 100 },
          style: { backgroundColor:'#eee' },
          labelStyle: {
            textTransform: 'capitalize',
            fontSize: fonts.fontSize16,
            fontFamily: fonts.NovaSboldios,
          },
        }}>
        <Tab.Screen
          name="Fans"
          component={Fans}
          navigationOptions={
            {tabBarLabel: 'Navigation Title'}
            // tabBarOptions: {
            //     activeTintColor: '#000',
            //     inactiveTintColor: '#fff',
            // }
          }
        />
        <Tab.Screen
          name="Following"
          component={Followings}
          tabBarOptions={{tabBarLabel: 'Updates'}}
        />
      </Tab.Navigator>


    </View>
  );
}

// export default function Fansfollowings(props) {
//   console.log(props,"propsfansfollowing")
//   return (
//     <Stack.Navigator
//       headerMode={false}
//       // initialRouteName="SplashScreen"
//     > 
//      <Stack.Screen
//         name="FansfollowingsHome"
//         component={FansfollowingsHome}
//         options={{
//           tabBarVisible: false,
//         }}
//       />
     


// {/* <Stack.Screen
//         name="Message"
//         component={Message}
//         options={{
//           tabBarVisible: false,
//         }}
//       /> */}
//     </Stack.Navigator>
//   );
// }


