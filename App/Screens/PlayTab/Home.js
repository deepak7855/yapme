import React, {Component} from 'react';
import {
  ImageBackground,
  Text,
  View,
  StyleSheet,
  StatusBar,
  FlatList,
  TouchableOpacity,
  ScrollView,
  Image,
  Dimensions,
  TextInput,
} from 'react-native';
import {SafeAreaView} from 'react-navigation';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import {Icons} from '../../Component/Common';
import {Custompopup} from '../../Component/Modal/CustomModel';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

// const { width, height } = Image.resolveAssetSource(myImage)

export default class Home extends Component {
  state = {
    modalVisible: false,
  };
  render() {
    return (
      <ImageBackground style={{flex: 1}} source={images.Rectangle2}>
        <StatusBar
          backgroundColor={Colors.violetBlue}
          barStyle="light-content"
        />
        <SafeAreaView />

        <View
              style={{
                // height: '8%',
                // backgroundColor: 'purple',
                justifyContent: 'center',
                paddingVertical:8,
                alignItems: 'center',
              }}>
              {/* <View><Text>1111dddd</Text></View> */}

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <View style={{alignItems: 'center'}}>
                  <Icons
                    size={25}
                    source={images.globe}
                    _style={{tintColor: Colors.white}}
                    // onPress={() => this.props.navigation.navigate('LiveStreaming')}
                  />
                  <View
                    style={{
                      borderWidth: 1.3,
                      borderColor: Colors.white,
                      width: 30,
                      marginTop: 8,
                    }}
                  />
                </View>
                <View
                  style={{
                    borderWidth: 0.8,
                    marginHorizontal: 30,
                    borderColor: Colors.white,
                    height: 20,
                  }}
                />

                {/* 2nd */}
                <View style={{alignItems: 'center'}}>
                  <Icons
                    size={25}
                    source={images.live_news}
                    onPress={() => this.props.navigation.navigate('Live')}
                  />
                  <View
                    style={{
                      borderWidth: 1.3,
                      borderColor: Colors.white,
                      width: 30,
                      marginTop: 8,
                    }}
                  />
                </View>
                <View
                  style={{
                    borderWidth: 0.8,
                    marginHorizontal: 30,
                    borderColor: Colors.white,
                    height: 20,
                  }}
                />

                {/* 3rd */}
                <View style={{alignItems: 'center'}}>
                  <Icons
                    size={20}
                    source={images.search_active}
                    _style={{tintColor: Colors.greyColor}}
                    onPress={() => this.props.navigation.navigate('Search')}
                  />
                  <View
                    style={{
                      borderWidth: 1.3,
                      borderColor: Colors.white,
                      width: 30,
                      marginTop: 8,
                    }}
                  />
                </View>
                {/* <View style={{borderWidth:.8,marginHorizontal:30,borderColor:Colors.white,height:20,}}/> */}
              </View>
            </View>

        <ScrollView bounces={false} style={{marginBottom:50}} showsVerticalScrollIndicator={false}>

            {/* <View><Text>1111</Text></View> */}

            <View style={{}}>
              <View
                style={{
                  flexDirection: 'row',
                  flex: 1,
                  justifyContent: 'space-between',
                  // backgroundColor: 'green',
                  alignItems: 'center',
                  marginTop:30
                }}>
                {/* Right View */}

                <View>
                  <View
                    style={{
                      // flex: 1,
                      // maxHeight:height*0.5,
                      justifyContent: 'center',
                      // alignItems: 'flex-end',
                      // backgroundColor: 'pink',
                    }}>
                    <Icons
                      size={38}
                      source={images.Group635}
                      _style={{alignSelf: 'center',top:2}}
                      onPress={() =>
                      this.props.navigation.navigate('OtherUserProfile')
                    }
                    />

                    <View style={{}}>
                      <View
                        style={{
                          // width: 100,
                          maxHeight: height * 0.44,
                          alignItems: 'center',
                          justifyContent: 'center',
                          // marginBottom:20,
                          backgroundColor: 'rgba(0,0,0,0.3)',
                          paddingVertical: 5,

                          paddingHorizontal: 5,
                          borderRadius: 10,
                        }}>
                        {/* <View>
                    <Text
                      style={{
                        marginVertical: 10,
                        fontFamily: fonts.NovaRegularios,
                        color: Colors.white,
                      }}>
                      Suggested1
                    </Text>
                  </View> */}

                        <FlatList
                          nestedScrollEnabled
                          showsVerticalScrollIndicator={false}
                          data={[
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                          ]}
                          contentContainerStyle={{
                            paddingBottom: 5,
                          }}
                          renderItem={({}) => (
                            <View>
                              <Image
                                resizeMode="contain"
                                source={images.Rectangle1}
                                style={{
                                  width: width * 0.22,
                                  resizeMode: 'contain',
                                  height: undefined,
                                  aspectRatio: 1,
                                }}
                              />
                            </View>
                          )}
                        />

                        {/* <Text>ssddd</Text>
              <Text>ssddd</Text>
              <Text>ssddd</Text>
              <Text>ssddd</Text>
              <Text>ssddd</Text>
               */}
                        {/* <Icons size={90} source={images.Rectangle1} _style={{}} />
                  <Icons size={90} source={images.Rectangle3} _style={{}} />
                  <Icons size={90} source={images.Rectangle4} _style={{}} />
                  <Icons size={90} source={images.Rectangle5} _style={{}} /> */}
                      </View>
                    </View>
                    {/* <Icons
                          size={15}
                          source={images.slide_up_arrow}
                          _style={{alignSelf:'center'}}
                          // onPress={() => alert('hi')}
                        /> */}
                  </View>

                  <Icons
                    size={15}
                    source={images.slide_up_arrow}
                    _style={{alignSelf: 'center', top: 20}}
                    // onPress={() => alert('hi')}
                  />
                </View>

                {/* Left View */}
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                  }}>
                  <View style={{}}>
                    <View
                      style={{
                        // width: 100,
                        height: height * 0.48,
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginVertical: 20,
                        backgroundColor: 'rgba(0,0,0,0.3)',
                        paddingBottom: 8,
                        paddingHorizontal: 5,
                        borderRadius: 10,
                      }}>
                      <View>
                        <Text
                          style={{
                            marginVertical: 10,
                            fontFamily: fonts.NovaRegularios,
                            color: Colors.white,
                          }}>
                          Suggested
                        </Text>
                      </View>

                      <FlatList
                        nestedScrollEnabled
                        showsVerticalScrollIndicator={false}
                        data={['', '', '', '', '', '', '', '', '', '', '', '']}
                        contentContainerStyle={{
                          paddingBottom: 5,
                        }}
                        renderItem={({}) => (
                          <View>
                            <Image
                              resizeMode="contain"
                              source={images.Rectangle1}
                              style={{
                                width: width * 0.22,
                                resizeMode: 'contain',
                                height: undefined,
                                aspectRatio: 1,
                              }}
                            />
                          </View>
                        )}
                      />

                      {/* <Icons size={90} source={images.Rectangle1} _style={{}} />
                  <Icons size={90} source={images.Rectangle3} _style={{}} />
                  <Icons size={90} source={images.Rectangle4} _style={{}} />
                  <Icons size={90} source={images.Rectangle5} _style={{}} /> */}
                    </View>
                  </View>
                </View>

                {/* <View style={{width: 100, backgroundColor: 'rgba(0,0,0,0.3)'}}>
              <View style={{alignItems: 'center', justifyContent: 'center'}}>
              
                <Icons size={90} source={images.Rectangle1} _style={{}} />

                <Icons size={90} source={images.Rectangle2} _style={{}} />

                <Icons size={90} source={images.Rectangle3} _style={{}} />

                <Icons size={90} source={images.Rectangle4} _style={{}} />
              </View>
            </View>

            <View style={{width: 100, backgroundColor: 'rgba(0,0,0,0.3)'}}>
              <View style={{alignItems: 'center', justifyContent: 'center'}}>
                <View>
                  <Text
                    style={{
                      marginVertical: 10,
                      fontFamily: fonts.NovaRegularios,
                      color: Colors.white,
                    }}>
                    Suggested
                  </Text>
                </View>
                <Icons size={90} source={images.Rectangle1} _style={{}} />

                <Icons size={90} source={images.Rectangle2} _style={{}} />

                <Icons size={90} source={images.Rectangle3} _style={{}} />

                <Icons size={90} source={images.Rectangle4} _style={{}} />
              </View>
            </View> */}
              </View>
            </View>

            <View style={{marginTop:70,marginBottom:20}}>
              <View
                style={{
                  //   backgroundColor: 'red',
                  // marginBottom: 70,
                  marginLeft: 15,
                  marginRight: 10,
                }}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Icons
                    size={35}
                    source={images.Group635}
                    _style={{marginRight: 10, marginBottom:0}}
                    onPress={() =>
                      this.props.navigation.navigate('OtherUserProfile')
                    }
                  />

                  <View style={{alignItems: 'center'}}>
                    <Icons
                      size={25}
                      source={images.active_heart}
                      // _style={{marginRight: 10}}
                    />
                    <Text
                      style={[
                        styles.txt,
                        {
                          fontFamily: fonts.NovaSboldios,
                          fontSize: 9,
                          marginTop: 5,
                        },
                      ]}>
                      25k
                    </Text>
                  </View>
                  <View
                    style={{
                      marginBottom: 13,
                      flexDirection: 'row',
                      marginLeft: 14,
                    }}>
                    <Icons
                      size={25}
                      source={images.laugh}
                      // _style={{marginRight: 10}}
                    />
                    <Icons
                      size={25}
                      source={images.emoji_goggle}
                      // source={!this.state.emoji_goggle ? images.emoji_goggle : images.emoji_active2}
                      _style={{marginHorizontal: 10}}
                      onPress={() =>
                        this.setState({emoji_goggle: !this.state.emoji_goggle})
                      }
                    />
                    <Icons
                      size={25}
                      source={images.emoji_cry}
                      // source={!this.state.emoji_cry ? images.emoji_cry : images.emoji_active3 }
                      // _style={{marginRight: 10}}
                      onPress={() =>
                        this.setState({emoji_cry: !this.state.emoji_cry})
                      }
                    />

                    <Icons
                      size={25}
                      source={images.emoji_angry}
                      // source={!this.state.angry?images.emoji_angry:images.emoji_active4}
                      _style={{marginLeft: 10}}
                    />
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    backgroundColor: Colors.whiteTwo,
                    marginVertical: 10,
                  }}>
                  <View
                    style={{
                      backgroundColor: Colors.vividPurple,
                      width: '40%',
                      height: 1.5,
                    }}></View>
                </View>

                {/* <View
              style={{
                borderWidth: 0.2,
                marginVertical: 10,
                // backgroundColor: Colors.white,
                justifyContent: 'center',
              }}>
              <View
                style={{
                  borderWidth: 0.3,
                  backgroundColor: Colors.vividPurple,
                  width: 150,
                  marginTop: 0,
                }}
              />
            </View> */}

                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <Text style={{...styles.txt, fontSize: fonts.fontSize16}}>
                    @elemar
                  </Text>

                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity
                      onPress={() => this.setState({modalVisible: true})}
                      style={{alignItems: 'center'}}>
                      <Icons
                        size={15}
                        source={images.commentdots}
                        // _style={{marginRight: 10}}
                        onPress={() => this.setState({modalVisible: true})}
                      />
                      <Text
                        style={[
                          styles.txt,
                          {
                            fontFamily: fonts.NovaSboldios,
                            fontSize: fonts.fontSize10,
                            marginTop: 5,
                          },
                        ]}>
                        2k
                      </Text>
                    </TouchableOpacity>

                    <View style={{alignItems: 'center', marginHorizontal: 15}}>
                      <Icons
                        size={15}
                        source={images.share}
                        // _style={{marginRight: 10}}
                      />
                      <Text
                        style={[
                          styles.txt,
                          {
                            fontFamily: fonts.NovaSboldios,
                            fontSize: fonts.fontSize10,
                            marginTop: 5,
                          },
                        ]}>
                        25k
                      </Text>
                    </View>
                    <View style={{alignItems: 'center'}}>
                      <Icons
                        size={15}
                        source={images.moreicon}
                        _style={{marginTop: 5}}
                      />
                    </View>
                  </View>
                </View>

                {/* <View style={{flexDirection:'row',alignItems:'center'}}> */}
                <Text style={{...styles.txt, marginVertical: 7}}>
                  #dance #floogen
                </Text>

                {/* // </View> */}

                <View style={{flexDirection: 'row', alignItems: 'center',}}>
                  <Icons
                    size={15}
                    source={images.music}
                    _style={{marginRight: 5}}
                  />
                  <Text style={styles.txt}>
                    The Little Things Give You Away
                  </Text>
                </View>
               
            {/* <View style={{height:40}}/> */}
              </View>
            </View>
        </ScrollView>

             <Custompopup modalVisible={this.state.modalVisible}>
         <KeyboardAwareScrollView
            bounces={false}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              flex: 1,
              backgroundColor: 'rgba(0,0,0,0.2)',
              justifyContent: 'space-between',
            }}>
            {/* <View style={{flex: 1, backgroundColor: 'green'}}> */}

            {/* </View> */}

            {/* <View
              onTouchEnd={() => this.setState({modalVisible: false})}
              style={{flex: 2}}></View> */}
              <TouchableOpacity activeOpacity={1}
              onPress={() => this.setState({modalVisible: false})}
               style={{flex:2}}/>


            <View
              style={{
                flex: 3,
                maxHeight: '60%',
                backgroundColor: Colors.white,
                borderTopLeftRadius: 15,
                borderTopRightRadius: 15,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: 15,
                  paddingVertical: 20,
                }}>
                <View>
                  <Text style={styles.pophead}>25k Comments</Text>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  {/* <Text style={styles.pophead}>invite</Text> */}
                  <Icons
                    size={25}
                    source={images.cancel}
                    _style={{marginLeft: 15}}
                    onPress={() => this.setState({modalVisible: false})}
                  />
                </View>
              </View>

              <View style={{paddingHorizontal: 18}}>
                <FlatList
                  showsVerticalScrollIndicator={false}
                  contentContainerStyle={{
                    paddingBottom: 10,
                    marginTop: 15,
                    // paddingBottom: 80,
                  }}
                  data={['', '', '', '', '', '']}
                  renderItem={({item: color, index}) => (
                    <View>
                      <View
                        style={{
                          flexDirection: 'row',
                          // alignItems: 'center',
                          marginBottom: 15,
                        }}>
                        <Icons
                          size={35}
                          source={images.Profile}
                          _style={{marginRight: 15}}
                          // onPress={() => alert('hi')}
                        />

                        <View>
                          <Text
                            style={{
                              fontFamily: fonts.NovaBoldios,
                            }}>
                            Braulio
                          </Text>

                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                              marginVertical: 10,
                            }}>
                            <View
                              style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                width: 330,
                              }}>
                              <Text
                                style={{
                                  fontFamily: fonts.NovaRegularios,
                                  fontSize: fonts.fontSize13,
                                }}>
                                Cant work out if this is real or not
                              </Text>

                              <Text
                                style={{
                                  fontSize: fonts.fontSize13,
                                  fontFamily: fonts.NovaRegularios,
                                  color: Colors.pinkishGreyTwo,
                                }}>
                                1 day ago
                              </Text>
                            </View>
                          </View>

                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                            }}>
                            <View>
                              <Text
                                style={{
                                  fontSize: fonts.fontSize13,
                                  fontFamily: fonts.NovaRegularios,
                                  color: Colors.pinkishGreyTwo,
                                }}>
                                View replies(8)
                              </Text>
                            </View>
                            <View
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                              }}>
                              <Text
                                style={{
                                  fontFamily: fonts.NovaRegularios,
                                  fontSize: fonts.fontSize13,
                                  color: Colors.pinkishGreyTwo,
                                  marginRight: 20,
                                }}>
                                Reply
                              </Text>
                              <Icons
                                size={15}
                                source={images.likelg}
                                _style={{marginHorizontal: 10}}
                                // onPress={() => alert('hi')}
                              />
                              <Text
                                style={{
                                  fontFamily: fonts.NovaRegularios,
                                  fontSize: fonts.fontSize13,
                                  color: Colors.pinkishGreyTwo,
                                  marginRight: 20,
                                }}>
                                152
                              </Text>
                            </View>
                            <View>
                              <Icons
                                size={15}
                                source={images.dislike}
                                _style={{marginRight: 5}}
                                // onPress={() => alert('hi')}
                              />
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>
                  )}
                />
              </View>
            </View>

            {/* <SafeAreaView /> */}

            <View
              style={{
                height: 90,
                shadowRadius: 2,
                shadowOffset: {
                  width: 0,
                  height: -2,
                },
                shadowColor: '#000',
                shadowOpacity: 0.11,
                elevation: 4,
                backgroundColor: '#fff',

                justifyContent: 'center',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: 15,
                  marginBottom: 15,
                  alignItems: 'center',
                }}>
                <TextInput
                  placeholder={'Add Comment '}
                  placeholderTextColor={Colors.black}
                  style={{
                    // flex: 2.5,
                    // backgroundColor: 'red',
                    // width: 250,
                    marginHorizontal: 20,
                    // paddingVertical: 15,

                    flex: 1,
                    // alignItems: 'center',
                    borderRadius: 10,
                    borderColor: 'rgba(76,0,0,0.18)',
                    borderWidth: 1,
                    height: 50,
                    paddingLeft: 12,
                    fontFamily: fonts.NovaBold,
                    // backgroundColor: 'pink',
                  }}
                />

                {/* <Text style={{fontFamily: fonts.NovaBoldios, marginLeft: 15}}>
                  Add Comment
                </Text> */}

                <View style={{flexDirection: 'row'}}>
                  <Icons
                    size={20}
                    source={images.at}
                    _style={{marginRight: 25}}
                    // onPress={() => alert('hi')}
                  />
                  <Icons
                    size={20}
                    source={images.face}
                    _style={{marginRight: 5}}
                    // onPress={() => alert('hi')}
                  />
                </View>
              </View>
            </View>

            {/* </View> */}
          </KeyboardAwareScrollView>

          {/* <SafeAreaView /> */}
        </Custompopup>
      </ImageBackground>
    );
  }
}

//     return (
//       <ImageBackground style={{flex: 1}} source={images.Rectangle2}>
//         <StatusBar
//           backgroundColor={Colors.violetBlue}
//           barStyle="light-content"
//         />
//         <SafeAreaView />

//         <View style={{flex: 1, justifyContent: 'space-between'}}>
//           <View
//             style={{
//               flexDirection: 'row',
//               alignItems: 'center',
//               justifyContent: 'center',
//             }}>
//             <View style={{alignItems: 'center'}}>
//               <Icons
//                 size={25}
//                 source={images.globe}
//                 _style={{tintColor: Colors.white}}
//                 // onPress={() => this.props.navigation.navigate('LiveStreaming')}
//               />
//               <View
//                 style={{
//                   borderWidth: 1.3,
//                   borderColor: Colors.white,
//                   width: 30,
//                   marginTop: 8,
//                 }}
//               />
//             </View>
//             <View
//               style={{
//                 borderWidth: 0.8,
//                 marginHorizontal: 30,
//                 borderColor: Colors.white,
//                 height: 20,
//               }}
//             />

//             {/* 2nd */}
//             <View style={{alignItems: 'center'}}>
//               <Icons
//                 size={25}
//                 source={images.live_news}
//                 onPress={() => this.props.navigation.navigate('Live')}
//               />
//               <View
//                 style={{
//                   borderWidth: 1.3,
//                   borderColor: Colors.white,
//                   width: 30,
//                   marginTop: 8,
//                 }}
//               />
//             </View>
//             <View
//               style={{
//                 borderWidth: 0.8,
//                 marginHorizontal: 30,
//                 borderColor: Colors.white,
//                 height: 20,
//               }}
//             />

//             {/* 3rd */}
//             <View style={{alignItems: 'center'}}>
//               <Icons
//                 size={20}
//                 source={images.search_active}
//                 _style={{tintColor: Colors.greyColor}}
//                 onPress={() => this.props.navigation.navigate('Search')}
//               />
//               <View
//                 style={{
//                   borderWidth: 1.3,
//                   borderColor: Colors.white,
//                   width: 30,
//                   marginTop: 8,
//                 }}
//               />
//             </View>
//             {/* <View style={{borderWidth:.8,marginHorizontal:30,borderColor:Colors.white,height:20,}}/> */}
//           </View>

//           <View
//             style={{
//               flexDirection: 'row',
//               flex: 1,
//               justifyContent: 'space-between',
//               backgroundColor: 'green',
//               alignItems:'center'

//             }}>

//             {/* Right View */}

//             <View
//               style={{
//                 // flex: 1,
//                 maxHeight:height*0.5,
//                 justifyContent: 'center',
//                 alignItems: 'flex-end',
//                 backgroundColor:'pink'
//               }}>
//               <Text>dsdd</Text>

//               <View style={{}}>
//               <Text>ddd</Text>

//                 <View
//                   style={{
//                     // width: 100,
//                     alignItems: 'center',
//                     justifyContent: 'center',
//                     marginVertical:20,
//                     backgroundColor: 'rgba(0,0,0,0.3)',
//                     paddingBottom: 8,
//                     paddingHorizontal: 5,
//                     borderRadius: 10,
//                   }}>
//               <Text>qddd</Text>

//                   {/* <View>
//                     <Text
//                       style={{
//                         marginVertical: 10,
//                         fontFamily: fonts.NovaRegularios,
//                         color: Colors.white,
//                       }}>
//                       Suggested1
//                     </Text>
//                   </View> */}

//                   <FlatList
//                     showsVerticalScrollIndicator={false}
//                     data={['', '', '', '', '', '', '', '', '', '', '', '']}
//                     contentContainerStyle={
//                       {
//                           paddingBottom:5,
//                       }
//                     }
//                     renderItem={({}) => (
//                       <View>
//                         <Image
//                           resizeMode="contain"
//                           source={images.Rectangle1}
//                           style={{
//                             width: width * 0.22,
//                             resizeMode: 'contain',
//                             height: undefined,
//                             aspectRatio: 1,
//                           }}
//                         />
//                       </View>
//                     )}
//                   />

// {/* <Text>ssddd</Text>
//               <Text>ssddd</Text>
//               <Text>ssddd</Text>
//               <Text>ssddd</Text>
//               <Text>ssddd</Text>
//                */}
//                   {/* <Icons size={90} source={images.Rectangle1} _style={{}} />
//                   <Icons size={90} source={images.Rectangle3} _style={{}} />
//                   <Icons size={90} source={images.Rectangle4} _style={{}} />
//                   <Icons size={90} source={images.Rectangle5} _style={{}} /> */}

//                 </View>
//               <Text>dd</Text>

//               </View>
//               <Text>dd</Text>
//             </View>

// {/* Left View */}
//             <View
//               style={{
//                 flex: 1,
//                 justifyContent: 'center',
//                 alignItems: 'flex-end',
//               }}>
//               <View style={{}}>
//                 <View
//                   style={{
//                     // width: 100,
//                     height:height*0.5,
//                     alignItems: 'center',
//                     justifyContent: 'center',
//                     marginVertical:20,
//                     backgroundColor: 'rgba(0,0,0,0.3)',
//                     paddingBottom: 8,
//                     paddingHorizontal: 5,
//                     borderRadius: 10,
//                   }}>
//                   <View>
//                     <Text
//                       style={{
//                         marginVertical: 10,
//                         fontFamily: fonts.NovaRegularios,
//                         color: Colors.white,
//                       }}>
//                       Suggested
//                     </Text>
//                   </View>

//                   <FlatList
//                     showsVerticalScrollIndicator={false}
//                     data={['', '', '', '', '', '', '', '', '', '', '', '']}
//                     contentContainerStyle={
//                       {
//                           paddingBottom:5,
//                       }
//                     }
//                     renderItem={({}) => (
//                       <View>
//                         <Image
//                           resizeMode="contain"
//                           source={images.Rectangle1}
//                           style={{
//                             width: width * 0.22,
//                             resizeMode: 'contain',
//                             height: undefined,
//                             aspectRatio: 1,
//                           }}
//                         />
//                       </View>
//                     )}
//                   />

//                   {/* <Icons size={90} source={images.Rectangle1} _style={{}} />
//                   <Icons size={90} source={images.Rectangle3} _style={{}} />
//                   <Icons size={90} source={images.Rectangle4} _style={{}} />
//                   <Icons size={90} source={images.Rectangle5} _style={{}} /> */}
//                 </View>
//               </View>
//             </View>

//             {/* <View style={{width: 100, backgroundColor: 'rgba(0,0,0,0.3)'}}>
//               <View style={{alignItems: 'center', justifyContent: 'center'}}>

//                 <Icons size={90} source={images.Rectangle1} _style={{}} />

//                 <Icons size={90} source={images.Rectangle2} _style={{}} />

//                 <Icons size={90} source={images.Rectangle3} _style={{}} />

//                 <Icons size={90} source={images.Rectangle4} _style={{}} />
//               </View>
//             </View>

//             <View style={{width: 100, backgroundColor: 'rgba(0,0,0,0.3)'}}>
//               <View style={{alignItems: 'center', justifyContent: 'center'}}>
//                 <View>
//                   <Text
//                     style={{
//                       marginVertical: 10,
//                       fontFamily: fonts.NovaRegularios,
//                       color: Colors.white,
//                     }}>
//                     Suggested
//                   </Text>
//                 </View>
//                 <Icons size={90} source={images.Rectangle1} _style={{}} />

//                 <Icons size={90} source={images.Rectangle2} _style={{}} />

//                 <Icons size={90} source={images.Rectangle3} _style={{}} />

//                 <Icons size={90} source={images.Rectangle4} _style={{}} />
//               </View>
//             </View> */}

//           </View>

//           {/* bottomView */}

//           <View
//             style={{
//               //   backgroundColor: 'red',
//               marginBottom: 70,
//               marginLeft: 15,
//               marginRight: 10,
//             }}>
//             <View style={{flexDirection: 'row', alignItems: 'center'}}>
//               <Icons
//                 size={35}
//                 source={images.Group635}
//                 _style={{marginRight: 10, marginBottom: 10}}
//                 onPress={() =>
//                   this.props.navigation.navigate('OtherUserProfile')
//                 }
//               />

//               <View style={{alignItems: 'center'}}>
//                 <Icons
//                   size={25}
//                   source={images.active_heart}
//                   // _style={{marginRight: 10}}
//                 />
//                 <Text
//                   style={[
//                     styles.txt,
//                     {
//                       fontFamily: fonts.NovaSboldios,
//                       fontSize: 9,
//                       marginTop: 5,
//                     },
//                   ]}>
//                   25k
//                 </Text>
//               </View>
//               <View
//                 style={{
//                   marginBottom: 13,
//                   flexDirection: 'row',
//                   marginLeft: 14,
//                 }}>
//                 <Icons
//                   size={25}
//                   source={images.laugh}
//                   // _style={{marginRight: 10}}
//                 />
//                 <Icons
//                   size={25}
//                   source={images.emoji_goggle}
//                   // source={!this.state.emoji_goggle ? images.emoji_goggle : images.emoji_active2}
//                   _style={{marginHorizontal: 10}}
//                   onPress={() =>
//                     this.setState({emoji_goggle: !this.state.emoji_goggle})
//                   }
//                 />
//                 <Icons
//                   size={25}
//                   source={images.emoji_cry}
//                   // source={!this.state.emoji_cry ? images.emoji_cry : images.emoji_active3 }
//                   // _style={{marginRight: 10}}
//                   onPress={() =>
//                     this.setState({emoji_cry: !this.state.emoji_cry})
//                   }
//                 />

//                 <Icons
//                   size={25}
//                   source={images.emoji_angry}
//                   // source={!this.state.angry?images.emoji_angry:images.emoji_active4}
//                   _style={{marginLeft: 10}}
//                 />
//               </View>
//             </View>

//             <View
//               style={{
//                 flexDirection: 'row',
//                 width: '100%',
//                 backgroundColor: Colors.whiteTwo,
//                 marginVertical: 15,
//               }}>
//               <View
//                 style={{
//                   backgroundColor: Colors.vividPurple,
//                   width: '40%',
//                   height: 1.5,
//                 }}></View>
//             </View>

//             {/* <View
//               style={{
//                 borderWidth: 0.2,
//                 marginVertical: 10,
//                 // backgroundColor: Colors.white,
//                 justifyContent: 'center',
//               }}>
//               <View
//                 style={{
//                   borderWidth: 0.3,
//                   backgroundColor: Colors.vividPurple,
//                   width: 150,
//                   marginTop: 0,
//                 }}
//               />
//             </View> */}

//             <View
//               style={{
//                 flexDirection: 'row',
//                 alignItems: 'center',
//                 justifyContent: 'space-between',
//               }}>
//               <Text style={{...styles.txt, fontSize: fonts.fontSize16}}>
//                 @elemar
//               </Text>

//               <View style={{flexDirection: 'row'}}>
//                 <TouchableOpacity
//                   onPress={() => this.setState({modalVisible: true})}
//                   style={{alignItems: 'center'}}>
//                   <Icons
//                     size={15}
//                     source={images.commentdots}
//                     // _style={{marginRight: 10}}
//                     onPress={() => this.setState({modalVisible: true})}
//                   />
//                   <Text
//                     style={[
//                       styles.txt,
//                       {
//                         fontFamily: fonts.NovaSboldios,
//                         fontSize: fonts.fontSize10,
//                         marginTop: 5,
//                       },
//                     ]}>
//                     2k
//                   </Text>
//                 </TouchableOpacity>

//                 <View style={{alignItems: 'center', marginHorizontal: 15}}>
//                   <Icons
//                     size={15}
//                     source={images.share}
//                     // _style={{marginRight: 10}}
//                   />
//                   <Text
//                     style={[
//                       styles.txt,
//                       {
//                         fontFamily: fonts.NovaSboldios,
//                         fontSize: fonts.fontSize10,
//                         marginTop: 5,
//                       },
//                     ]}>
//                     25k
//                   </Text>
//                 </View>
//                 <View style={{alignItems: 'center'}}>
//                   <Icons
//                     size={15}
//                     source={images.moreicon}
//                     _style={{marginTop: 5}}
//                   />
//                 </View>
//               </View>
//             </View>

//             {/* <View style={{flexDirection:'row',alignItems:'center'}}> */}
//             <Text style={{...styles.txt, marginVertical: 7}}>
//               #dance #floogen
//             </Text>

//             {/* // </View> */}

//             <View style={{flexDirection: 'row', alignItems: 'center'}}>
//               <Icons
//                 size={15}
//                 source={images.music}
//                 _style={{marginRight: 5}}
//               />
//               <Text style={styles.txt}>The Little Things Give You Away</Text>
//             </View>
//           </View>
//         </View>
//         {/* <SafeAreaView /> */}

//         <Custompopup modalVisible={this.state.modalVisible}>
//           <KeyboardAwareScrollView
//             bounces={false}
//             showsVerticalScrollIndicator={false}
//             contentContainerStyle={{
//               flex: 1,
//               backgroundColor: 'rgba(0,0,0,0.2)',
//               justifyContent: 'space-between',
//             }}>
//             {/* <View style={{flex: 1, backgroundColor: 'green'}}> */}

//             {/* </View> */}

//             <View
//               onTouchEnd={() => this.setState({modalVisible: false})}
//               style={{flex: 2}}></View>

//             <View
//               style={{
//                 flex: 3,
//                 maxHeight: '60%',
//                 backgroundColor: Colors.white,
//                 borderTopLeftRadius: 15,
//                 borderTopRightRadius: 15,
//               }}>
//               <View
//                 style={{
//                   flexDirection: 'row',
//                   justifyContent: 'space-between',
//                   paddingHorizontal: 15,
//                   paddingVertical: 20,
//                 }}>
//                 <View>
//                   <Text style={styles.pophead}>25k Comments</Text>
//                 </View>

//                 <View
//                   style={{
//                     flexDirection: 'row',
//                   }}>
//                   {/* <Text style={styles.pophead}>invite</Text> */}
//                   <Icons
//                     size={25}
//                     source={images.cancel}
//                     _style={{marginLeft: 15}}
//                     onPress={() => this.setState({modalVisible: false})}
//                   />
//                 </View>
//               </View>

//               <View style={{paddingHorizontal: 18}}>
//                 <FlatList
//                   showsVerticalScrollIndicator={false}
//                   contentContainerStyle={{
//                     paddingBottom: 10,
//                     marginTop: 15,
//                     // paddingBottom: 80,
//                   }}
//                   data={['', '', '', '', '', '']}
//                   renderItem={({item: color, index}) => (
//                     <View>
//                       <View
//                         style={{
//                           flexDirection: 'row',
//                           // alignItems: 'center',
//                           marginBottom: 15,
//                         }}>
//                         <Icons
//                           size={35}
//                           source={images.Profile}
//                           _style={{marginRight: 15}}
//                           // onPress={() => alert('hi')}
//                         />

//                         <View>
//                           <Text
//                             style={{
//                               fontFamily: fonts.NovaBoldios,
//                             }}>
//                             Braulio
//                           </Text>

//                           <View
//                             style={{
//                               flexDirection: 'row',
//                               justifyContent: 'space-between',
//                               marginVertical: 10,
//                             }}>
//                             <View
//                               style={{
//                                 flexDirection: 'row',
//                                 justifyContent: 'space-between',
//                                 width: 330,
//                               }}>
//                               <Text
//                                 style={{
//                                   fontFamily: fonts.NovaRegularios,
//                                   fontSize: fonts.fontSize13,
//                                 }}>
//                                 Cant work out if this is real or not
//                               </Text>

//                               <Text
//                                 style={{
//                                   fontSize: fonts.fontSize13,
//                                   fontFamily: fonts.NovaRegularios,
//                                   color: Colors.pinkishGreyTwo,
//                                 }}>
//                                 1 day ago
//                               </Text>
//                             </View>
//                           </View>

//                           <View
//                             style={{
//                               flexDirection: 'row',
//                               justifyContent: 'space-between',
//                             }}>
//                             <View>
//                               <Text
//                                 style={{
//                                   fontSize: fonts.fontSize13,
//                                   fontFamily: fonts.NovaRegularios,
//                                   color: Colors.pinkishGreyTwo,
//                                 }}>
//                                 View replies(8)
//                               </Text>
//                             </View>
//                             <View
//                               style={{
//                                 flexDirection: 'row',
//                                 alignItems: 'center',
//                               }}>
//                               <Text
//                                 style={{
//                                   fontFamily: fonts.NovaRegularios,
//                                   fontSize: fonts.fontSize13,
//                                   color: Colors.pinkishGreyTwo,
//                                   marginRight: 20,
//                                 }}>
//                                 Reply
//                               </Text>
//                               <Icons
//                                 size={15}
//                                 source={images.likelg}
//                                 _style={{marginHorizontal: 10}}
//                                 // onPress={() => alert('hi')}
//                               />
//                               <Text
//                                 style={{
//                                   fontFamily: fonts.NovaRegularios,
//                                   fontSize: fonts.fontSize13,
//                                   color: Colors.pinkishGreyTwo,
//                                   marginRight: 20,
//                                 }}>
//                                 152
//                               </Text>
//                             </View>
//                             <View>
//                               <Icons
//                                 size={15}
//                                 source={images.dislike}
//                                 _style={{marginRight: 5}}
//                                 // onPress={() => alert('hi')}
//                               />
//                             </View>
//                           </View>
//                         </View>
//                       </View>
//                     </View>
//                   )}
//                 />
//               </View>
//             </View>

//             {/* <SafeAreaView /> */}

//             <View
//               style={{
//                 height: 90,
//                 shadowRadius: 2,
//                 shadowOffset: {
//                   width: 0,
//                   height: -2,
//                 },
//                 shadowColor: '#000',
//                 shadowOpacity: 0.11,
//                 elevation: 4,
//                 backgroundColor: '#fff',

//                 justifyContent: 'center',
//               }}>
//               <View
//                 style={{
//                   flexDirection: 'row',
//                   justifyContent: 'space-between',
//                   paddingHorizontal: 15,
//                   marginBottom: 15,
//                   alignItems: 'center',
//                 }}>
//                 <TextInput
//                   placeholder={'Add Comment '}
//                   placeholderTextColor={Colors.black}
//                   style={{
//                     // flex: 2.5,
//                     // backgroundColor: 'red',
//                     // width: 250,
//                     marginHorizontal: 20,
//                     // paddingVertical: 15,

//                     flex: 1,
//                     // alignItems: 'center',
//                     borderRadius: 10,
//                     borderColor: 'rgba(76,0,0,0.18)',
//                     borderWidth: 1,
//                     height: 50,
//                     paddingLeft: 12,
//                     fontFamily: fonts.NovaBold,
//                     // backgroundColor: 'pink',
//                   }}
//                 />

//                 {/* <Text style={{fontFamily: fonts.NovaBoldios, marginLeft: 15}}>
//                   Add Comment
//                 </Text> */}

//                 <View style={{flexDirection: 'row'}}>
//                   <Icons
//                     size={20}
//                     source={images.at}
//                     _style={{marginRight: 25}}
//                     // onPress={() => alert('hi')}
//                   />
//                   <Icons
//                     size={20}
//                     source={images.face}
//                     _style={{marginRight: 5}}
//                     // onPress={() => alert('hi')}
//                   />
//                 </View>
//               </View>
//             </View>

//             {/* </View> */}
//           </KeyboardAwareScrollView>

//           {/* <SafeAreaView /> */}
//         </Custompopup>
//       </ImageBackground>
//     );
//   }
// }
const styles = StyleSheet.create({
  View4: {fontFamily: fonts.NovaRegularios, fontSize: fonts.fontSize20},
  txt: {
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaBoldios,
    color: Colors.white,
  },

  thiredView: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 10,
    paddingRight: 10,
    // marginLeft: 18,
    // alignItems:'flex-end',
    // backgroundColor:'red',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    // justifyContent:'center',
    // alignItems:'center',
  },
  headerContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    // backgroundColor:'red'
  },
  pophead: {
    fontSize: fonts.fontSize16,
    fontFamily: fonts.NovaBoldios,
    color: Colors.vividPurple,
  },

  accept_btn_touch: {
    // marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
    // width: 59,
    // height: 22,
    backgroundColor: Colors.violetBlue,
    borderRadius: 5,
    paddingHorizontal: 5,
    paddingVertical: 6,
  },
  accept_text: {
    fontSize: fonts.fontSize10,
    color: Colors.white,
    fontFamily: fonts.NovaBold,
  },

  adminView: {
    // justifyContent: 'center',
    // alignItems: 'center',
    // borderColor: Colors.violetBlue,
    // borderWidth: 1,

    // borderRadius: 5,
    paddingHorizontal: 8,
    // paddingVertical: 6,
  },
  adminText: {
    fontSize: fonts.fontSize10,
    color: Colors.violetBlue,
    fontFamily: fonts.NovaBold,
  },

  existView: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end',
    borderColor: Colors.red,
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 8,
    paddingVertical: 4,

    width: 80,
  },
  existText: {
    fontSize: fonts.fontSize10,
    color: Colors.red,
    // fontFamily: fonts.NovaBold,
  },
});

{
  /* <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginBottom: 15,
                        }}>
                        <Icons
                          size={35}
                          source={images.Profile}
                          _style={{marginRight: 15}}
                          // onPress={() => alert('hi')}
                        />

                        <View>
                          <Text
                            style={{
                              fontFamily: fonts.NovaRegularios,
                              fontSize: fonts.fontSize13,
                            }}>
                            Cant work out if this is real or not{' '}
                          </Text>

                          <View
                            style={{
                              justifyContent: 'space-between',
                              flexDirection: 'row',
                            }}>
                            <Text
                              style={{
                                fontFamily: fonts.NovaRegularios,
                                fontSize: fonts.fontSize13,
                              }}>
                              View replies(8){' '}
                            </Text>
                            <Text
                              style={{
                                fontFamily: fonts.NovaRegularios,
                                fontSize: fonts.fontSize13,
                              }}>
                              View replies(8){' '}
                            </Text>
                          </View>
                        </View>
                      </View>
                      {index !== 0 && (
                        <TouchableOpacity style={styles.accept_btn_touch}>
                          <Text style={styles.accept_text}>Remove</Text>
                        </TouchableOpacity>
                      )}

                      {index == 0 && (
                        <TouchableOpacity style={styles.adminView}>
                          <Text
                            style={{
                              fontSize: fonts.fontSize13,
                              fontFamily: fonts.NovaRegularios,
                              color: Colors.pinkishGreyTwo,
                            }}>
                            1 day ago
                          </Text>
                          <Icons
                            size={15}
                            source={images.dislike}
                            _style={{alignSelf: 'flex-end', marginTop: 4}}
                            // onPress={() => alert('hi')}
                          />
                        </TouchableOpacity>
                      )}
                    </View> */
}
