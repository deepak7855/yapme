import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Image,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import {GButton} from '../../Common/GButton';
import {Input, InputText} from '../../Common/InputCommon';
import {Icons} from '../../Component/Common';

export default class Diamonds extends Component {
  state = {
    showpaymentMethod: '',
    extraShow: false,
    newCard: false,
    activeCard: 1,
    addcardShow: false,
    editCardText: 'ADD',

    extraShow: false,
    // saveCards: [
    //   {
    //     id: 1,
    //     name: 'Debit Card',
    //     number: '4134 **** **** 1122',
    //     type_img: images.visa_logo,
    //   },
    // ],
    activeCard: 1,
    isChecked: false,
    useReword: false,
    vechileType: '',
    showpaymentMethod: '',
    // cardItem:{number:'12345678'}
    cvv: 123,
  };
  _renderFriendRequestList = ({item, index}) => {
    return (
      <View style={styles.friend_req_view}>
        <View style={{flexDirection: 'row'}}>
          <Icons
            size={17}
            source={images.diamond_lg}
            _style={{marginHorizontal: 5}}
            onPress={() => alert('development')}
          />

          <View>
            <Text
              style={{
                fontFamily: fonts.NovaBoldios,
                fontSize: fonts.fontSize17,
              }}>
              {item.usd}
            </Text>
            <Text
              style={{
                fontFamily: fonts.NovaRegularios,
                fontSize: fonts.fontSize13,
                marginTop: 5,
              }}>
              ${item.usd1}
            </Text>
          </View>
        </View>

        {/* <View style={styles.img_name_view}> */}
        {/* <View style={styles.user_img_view}> */}
        {/* <Text style={styles.name_text}>{item.title}</Text> */}
        {/* </View> */}
        {/* <View style={styles.name_view}>
            <Text style={styles.name_text2}>{item.date}</Text>
          </View>
        </View>

        <View style={styles.btn_view}>
          <Text
            style={index == '3' ? {fontSize: 18, fontWeight: 'bold'} : null}>
            ${item.usd}
          </Text>

          <Icons
            size={17}
            source={images.diamond_lg}
            _style={{marginVertical: 18, marginHorizontal: 8}}
            onPress={() => alert('hi')}
          />
        </View> */}
      </View>
    );
  };
  activeCard(item) {
    this.setState({activeCard: item.id, cardItem: item});
  }

  _renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => this.activeCard(item)}
        style={[
          item.id == this.state.activeCard
            ? {backgroundColor: Colors.pinklight}
            : {backgroundColor: Colors.white},
          {
            flexDirection: 'row',
            marginVertical: 6,
            alignItems: 'center',
            // borderColor: 'green',
            borderRadius: 5,
            padding: 15,
            paddingVertical: 15,

            // backgroundColor: '#fff',
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.15,
            shadowRadius: 1.94,
            elevation: 3,
          },
        ]}>
        <View style={{flex: 0.6}}>
          <Image
            source={
              item.id == this.state.activeCard
                ? images.tick_circle
                : images.radio_circle
            }
            style={{width: 20, height: 20, resizeMode: 'contain'}}
          />
        </View>
        <View style={{flex: 6}}>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                fontFamily: fonts.NovaBoldios,
                fontSize: fonts.fontSize13,
              }}>
              {item.name}
            </Text>
            {item.id == this.state.activeCard ? (
              <Text
                style={{
                  marginLeft: 18,
                  fontFamily: fonts.NovaRegular,
                  color: Colors.lightgrey,
                  fontSize: fonts.fontSize13,
                }}>
                Default card
              </Text>
            ) : null}
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={{marginTop: 8}}>{item.number}</Text>
            <Image
              source={item.type_img}
              style={{
                width: 30,
                height: 15,
                resizeMode: 'contain',
                marginLeft: 5,
                marginTop: 10,
              }}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    console.log(this.state, 'Diamond', this.props);
    return (
      <ScrollView bounces={false} style={{color: Colors.whiteTwo,marginBottom:10}} showsVerticalScrollIndicator={false}>
        <View style={{alignItems: 'center'}}>
          <Icons
            size={40}
            source={images.diamond_lg}
            _style={{marginVertical: 18}}
          /> 
          <Text style={styles.balance}> Diamonds Balance </Text>
          <Text style={styles.totalcoin}> 724 </Text>
        </View>

        <FlatList
          numColumns={3}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            marginTop: 25,
            paddingVertical: 10,
            flex: 1,
            alignItems: 'center',
          }}
          data={ArrData.usdDoller}
          renderItem={this._renderFriendRequestList}
          extraData={this.state}
          keyExtractor={(item, index) => index.toString()}
        />

        <View style={{paddingHorizontal: 18, marginTop: 18, marginBottom: 8}}>
          <Text
            style={{
              fontFamily: fonts.NovaRegularios,
              fontSize: fonts.fontSize15,
            }}>
            Select payment method
          </Text>
        </View>

        <FlatList
          data={ArrData.saveCards}
          contentContainerStyle={{
            marginHorizontal: 18,
          }}
          renderItem={this._renderItem}
          extraData={this.state}
          scrollEnabled={false}
          keyExtractor={(item, index) => index.toString()}
        />
        <TouchableOpacity
          onPress={() => this.setState({newCard: !this.state.newCard})}
          style={{
            flexDirection: 'row',
            marginTop: 15,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Icons
            size={25}
            source={images.plus_icon}
            _style={{marginHorizontal: 5}}
          />
          <Text
            style={{
              fontFamily: fonts.NovaRegularios,
              color: Colors.violetBlue,
            }}>
            Use a new card
          </Text>
        </TouchableOpacity>

        {this.state.newCard && (
          <View
            style={{
              paddingVertical: 12,
              margin: 15,
              borderRadius: 10,
              backgroundColor: Colors.pinklight,
            }}>
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <View style={styles.formView}>
                  <Text style={styles.formText}>Card number</Text>
                  <View>
                    <InputText
                      placeholderTextColor={Colors.white}
                      // color={Colors.white}
                      width={'100%'}
                      height={40}
                      fontSize={15}
                      backgroundColor={Colors.lightnavy}
                      borderColor={Colors.lightnavy}
                      keyboardType={'default'}
                      underlineColorAndroid="transparent"
                      getFocus={(input) => {
                        this.card_number = input;
                      }}
                      setFocus={() => {
                        this.expiry.focus();
                      }}
                      returnKeyType={'next'}
                      blurOnSubmit={false}
                      // value={this.state.cardItem.number}
                      onChangeText={(text) =>
                        this.setState({card_number: text})
                      }
                    />
                  </View>
                </View>
                <View style={styles.formView}>
                  <Text style={styles.formText}>Name on Card</Text>
                  <View>
                    <InputText
                      placeholderTextColor={Colors.white}
                      // color={Colors.white}
                      width={'100%'}
                      height={40}
                      fontSize={15}
                      backgroundColor={Colors.lightnavy}
                      borderColor={Colors.lightnavy}
                      keyboardType={'default'}
                      underlineColorAndroid="transparent"
                      getFocus={(input) => {
                        this.name_on_card = input;
                      }}
                      setFocus={() => {
                        this.cvv.focus();
                      }}
                      returnKeyType={'next'}
                      blurOnSubmit={false}
                      // value={this.state.name_on_card}
                      onChangeText={(text) =>
                        this.setState({name_on_card: text})
                      }
                    />
                  </View>
                </View>
              </View>

              <View style={{flex: 1}}>
                <View style={styles.formView}>
                  <Text style={styles.formText}>Expires</Text>
                  <View>
                    <InputText
                      placeholderTextColor={Colors.white}
                      // color={Colors.white}
                      width={'100%'}
                      height={40}
                      fontSize={15}
                      backgroundColor={Colors.lightnavy}
                      borderColor={Colors.lightnavy}
                      keyboardType={'default'}
                      underlineColorAndroid="transparent"
                      getFocus={(input) => {
                        this.expiry = input;
                      }}
                      setFocus={() => {
                        this.name_on_card.focus();
                      }}
                      returnKeyType={'next'}
                      blurOnSubmit={false}
                      // value={this.state.expiry}
                      onChangeText={(text) => this.setState({expiry: text})}
                    />
                  </View>
                </View>
                <View style={styles.formView}>
                  <Text style={styles.formText}>CVV</Text>
                  <View>
                    <InputText
                      placeholderTextColor={Colors.white}
                      // color={Colors.white}
                      width={'100%'}
                      height={40}
                      fontSize={15}
                      backgroundColor={Colors.lightnavy}
                      borderColor={Colors.lightnavy}
                      keyboardType={'default'}
                      underlineColorAndroid="transparent"
                      getFocus={(input) => {
                        this.cvv = input;
                      }}
                      //setFocus={() => { this.weight.focus(); }}
                      returnKeyType={'done'}
                      blurOnSubmit={false}
                      value={this.state.cvv}
                      onChangeText={(text) => this.setState({cvv: text})}
                    />
                  </View>
                </View>
              </View>
            </View>

            <TouchableOpacity
              onPress={() =>
                this.setState({
                  isChecked: !this.state.isChecked,
                  editCardText: 'ADD',
                })
              }
              style={{
                flexDirection: 'row',
                marginLeft: 10,
                marginVertical: 10,
                alignItems: 'center',
              }}>
              <Image
                source={
                  this.state.isChecked
                    ? images.check_square
                    : images.check_square_black
                }
                style={{width: 15, height: 15, resizeMode: 'contain'}}
              />
              <Text style={[styles.formText, {marginLeft: 10}]}>
                Save card for future use
              </Text>
            </TouchableOpacity>
          </View>
        )}

        <GButton
          height={50}
          width={150}
          radius={5}
          //   lineHeight={15}
          txtcolor={Colors.white}
          fontFamily={fonts.NovaXbold}
          fontSize={fonts.fontSize13}
          _style={{alignSelf: 'center', marginTop: 15}}
          bText={'Submit'}
        />
        <SafeAreaView />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  safe_area_view: {
    flex: 1,
  },
  balance: {fontFamily: fonts.NovaRegularios, fontSize: fonts.fontSize13},
  totalcoin: {
    fontFamily: fonts.NovaBoldios,
    fontSize: fonts.fontSize17,
    marginTop: 12,
  },
  safe_area_view: {
    flex: 1,
  },
  notification_view: {
    marginHorizontal: 15,
    marginVertical: 5,
  },
  friend_req_view: {
    borderRadius: 5,
    marginVertical: 5,
    height: 60,
    width: 100,
    // paddingVertical: 10,
    borderWidth: 1,
    borderColor: Colors.white,
    marginHorizontal: 5,
    // flexDirection: 'row',
    // justifyContent: 'space-between',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.15,
    shadowRadius: 1.94,
    elevation: 3,
  },
  img_name_view: {
    // flexDirection: 'row',
    // alignItems: 'center',
    flex: 4,
  },
  user_img_view: {
    flex: 3,
    justifyContent: 'space-evenly',
    // height: 30,
    // width: 30,
    // borderRadius: 30 / 2,
  },
  user_img: {
    width: '100%',
    height: '100%',
  },
  name_view: {
    // width: 120,
    // marginLeft: 10,
  },
  name_text: {
    // lineHeight: 17,
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaRegularios,
    color: Colors.black,
  },
  name_text2: {
    // lineHeight: 17,
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaRegularios,
    color: Colors.pinkishGreyTwo,
  },
  btn_view: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 0.7,
    // backgroundColor: 'red',
    justifyContent: 'flex-end',
  },
  accept_btn_touch: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    paddingHorizontal: 13,
    paddingVertical: 6,
    borderColor: Colors.violetBlue,
    borderWidth: 1,
  },
  accept_text: {
    fontSize: fonts.fontSize10,
    color: Colors.violetBlue,
    fontFamily: fonts.NovaSboldios,
  },

  reject_text: {
    //   fontsize: fonts.fontsize10,
    // lineHeight: 12,
    color: Colors.violetBlue,
    //   fontFamily: fonts.NovaBold,
  },
  formView: {
    marginVertical: 5,
    paddingHorizontal: 15,
    paddingVertical: 5,
  },
  formText: {
    fontFamily: fonts.NovaRegular,
    // color: Colors.greyColor,
    fontSize: fonts.fontSize13,
    marginVertical: 3,
  },
});
