import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import CHeader from '../../Common/CHeader';
import {Icons} from '../../Component/Common';

export default class MyDraft extends Component {
  render() {
    return (
      <View style={{flex: 1, backgroundColor: Colors.white}}>
        <SafeAreaView />
        <CHeader navigation={this.props.navigation} backArrowLeft centerItem>
          <TouchableOpacity
            style={{
              flex: 1,
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
              paddingRight: 15,
            }}>
            <Text
              style={{
                fontFamily: fonts.NovaBoldios,
                fontSize: fonts.fontSize16,
                color: Colors.violetBlue,
              }}>
              Edit
            </Text>
          </TouchableOpacity>
        </CHeader>
        <View style={styles.settings_text_view}>
          <Text style={styles.settings_text}>My Draft</Text>
        </View>

        {/* <FlatList
          numColumns={2}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            //   flex:1,
            marginTop: 25,
              backgroundColor:'red',
            // alignItems: 'center',
          }}
          data={['',]}
          renderItem={this._renderFriendRequestList}
          extraData={this.state}
          keyExtractor={(item, index) => index.toString()}
        /> */}
        <View
          style={
            {
              flex: 1,
              // justifyContent: 'center',
            }
          }>
          <FlatList
            data={['', '', '', '', '', '', '', '', '', '', '', '']}
            contentContainerStyle={{
              margin:3,
              paddingBottom:5

            }}
            renderItem={({}) => (
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  margin: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: Colors.lightgrey,
                }}>
                {/* <Icons
                  size={150}
                  source={images.sq1}
                  _style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: 100,
                    // flex:1
                  }}
                /> */}
                <Image
                  style={styles.imageThumbnail}
                  source={images.Rectangle5}
                />
                <View
                  style={{
                    position: 'absolute',
                    justifyContent: 'space-between',
                    height: 160,
                    right: 15,
                  }}>
                  <Text
                    style={{
                      fontFamily: fonts.NovaRegularios,
                      fontSize: fonts.fontSize13,
                      color: Colors.white,
                    }}>
                    145
                  </Text>
                  <Icons
                    size={15}
                    source={images.radio_white}
                    _style={{marginHorizontal: 6}}
                    //   onPress={()=>this.props.navigation.navigate('FriendRequested')}
                  />
                </View>
              </View>
            )}
            //Setting the number of column
            numColumns={2}
            keyExtractor={(item, index) => index}
          />
        </View>
      </View>
    );
  }
  //   _renderFriendRequestList = ({item, index}) => {
  //     return (
  //       <View style={{backgroundColor: 'pink', flexDirection: 'row'}}>
  //         <View style={{}}>
  //           <Text>1</Text>
  //           <Text>1</Text>
  //         </View>
  //         <View style={{}}>
  //           <Text>2</Text>
  //         </View>
  //       </View>
  //     );
  //   };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bgcolor,
  },

  settings_text_view: {
    marginHorizontal: 12,
    marginTop: 25,
  },
  settings_text: {
    fontSize: fonts.fontSize16,
    fontFamily: fonts.NovaBold,
    color: Colors.black,
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    height: 180,
    width:'100%',
  },
});
