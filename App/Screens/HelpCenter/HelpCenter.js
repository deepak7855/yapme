import React, {Component} from 'react';
import {SafeAreaView, View, Text, ScrollView} from 'react-native';
import CHeader from '../../Common/CHeader';
import styles from './HelpCenterStyles';

class HelpCenter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCameraShow: false,
      editProfileForm: {
        profile_picture: '',
      },
    };
  }

  render() {
    return (
      <SafeAreaView style={styles.safe_area_view}>
        <CHeader navigation={this.props.navigation} backArrowLeft centerItem />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{height: 10}} />

          <View style={styles.help_center_view}>
            <Text style={styles.help_center_text}>Help Center</Text>
          </View>
          <View style={{height: 15}} />

          <View style={styles.help_title_view}>
            <Text style={styles.help_title_text}>
              Supportive Yapme Community
            </Text>
          </View>

          <View style={styles.desc_view}>
            <Text style={styles.desc_text}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letraset sheets
              containing Lorem Ipsum passages, and more recently with desktop
              publishing software like Aldus PageMaker including versions of
              Lorem Ipsum.
            </Text>
          </View>

          <View style={styles.help_title_view}>
            <Text style={styles.help_title_text}>
              Get Support from a professional
            </Text>
          </View>

          <View style={styles.desc_view}>
            <Text style={styles.desc_text}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letraset sheets
              containing Lorem Ipsum passages, and more recently with desktop
              publishing software like Aldus PageMaker including versions of
              Lorem Ipsum.
            </Text>
          </View>

          <View style={styles.help_title_view}>
            <Text style={styles.help_title_text}>
              Get Support from someone you trust
            </Text>
          </View>

          <View style={styles.desc_view}>
            <Text style={styles.desc_text}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letraset sheets
              containing Lorem Ipsum passages, and more recently with desktop
              publishing software like Aldus PageMaker including versions of
              Lorem Ipsum.
            </Text>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default HelpCenter;
