import React, { Component } from 'react';
import { Image, View, FlatList, StyleSheet, Text,ImageBackground, TouchableOpacity, SafeAreaView } from 'react-native';
import ImagePath from '../../common/ImagePath_K';
import { Actions } from 'react-native-router-flux'
import { ScreenRatio, ScreenHeight, ScreenWidth } from '../../common/ScreenDimension'
import ConstantSize from '../../common/ConstantSize';
import fonts from '../../common/fonts';
import colors from '../../common/colors';

export default class LiveEnableState extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        return (
            <SafeAreaView style={Styles.container}>
                <ImageBackground source={ImagePath.splash} resizeMode='cover' style={{width:'100%',height:'100%'}}>
                   <View style={Styles.maincontain}>

                    <TouchableOpacity style={[Styles.ColseView,]}>
                        <Image resizeMode='contain'  style={Styles.backiocn} source={ImagePath.close_ic}/>
                    </TouchableOpacity>

                        <View style={Styles.ElemarView}>
                            <Image resizeMode='contain' style={Styles.IamgeCall} source={ImagePath.Profile}/>
                            <Text style={[Styles.textLive, { marginTop: 10 }]}>Elemar</Text>
                        </View>

                        <View style={Styles.Cont}>
                            <View style={Styles.ContFirst}>
                               <Text style={[Styles.textK,]}>25K</Text>
                                <Text style={[Styles.textView]}>Total viewers</Text>
                            </View>

                            <View style={Styles.ContFirst}>
                                <Text style={[Styles.textK]}>25K</Text>
                                <Text style={[Styles.textView]}>New Fans</Text>
                            </View>
                        </View>

                        <View style={Styles.Cont1}>
                            <View style={Styles.ContFirst}>
                               <Text style={[Styles.textK,]}>255</Text>
                                <Text style={[Styles.textView]}>New Stars</Text>
                            </View>

                            <View style={Styles.ContFirst}>
                                <Text style={[Styles.textK]}>5K</Text>
                                <Text style={[Styles.textView]}>Like</Text>
                            </View>
                        </View>
                        
                        <View style={[Styles.ContDurations,{flexDirection:'row',}]}>
                               <Text style={[Styles.textView,{fontSize:16}]}>Live Durations </Text>
                                <Text style={[Styles.textDurations]}>0:11:21</Text>
                        </View>
                  </View>
                </ImageBackground>
            </SafeAreaView>
        );
    }

}
const Styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: colors.white },
    splashsz: { width: ConstantSize.percent_hundred, height: ConstantSize.percent_hundred, resizeMode: 'stretch' },
    maincontain: {flex:1, backgroundColor: '#00000099' },
    ColseView: {position: 'absolute', top: 20, right: 20 },
    ElemarView: { alignItems: 'center',marginTop:100 },
    MainConView: { alignItems: 'center',},
    TimeView: { flexDirection: 'row', alignItems: 'center' },
    textLive: { fontSize: 20, color: '#fff', },
    textK: { fontSize: 28, color: '#fff', },
    textView: { fontSize: 14, color: 'grey', },
    backiocn: { width: 15, height: 15 },
    textDurations: { fontSize: 16, color: '#fff', },
    Cont: { flexDirection: 'row', alignItems: 'center', marginHorizontal: 20, paddingVertical: 30,marginTop:50 },
    Cont1: { borderBottomWidth: 1, borderBottomColor: 'grey', paddingBottom: 30, flexDirection: 'row', alignItems: 'center',  marginHorizontal: 20, paddingVertical: 10, },
    ContFirst: { flex: 1, alignItems: 'center', justifyContent: 'center', },
    ContDurations: {marginVertical:40, height: 30, alignSelf: 'center', },
    ContButtonView: { alignItems: 'center', justifyContent: 'center', },

});


