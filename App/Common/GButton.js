import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  onPress,
  StyleSheet,
  Dimensions,
  Button,
} from 'react-native';
// import {fonts} from './fonts';
import LinearGradient from 'react-native-linear-gradient';
// import {Button} from 'react-native-paper';
import Colors from '../Assets/Colors';
import fonts from '../Assets/Fonts';
import { images } from '../Assets/ImageUrl';
import {Icons} from '../Component/Common';
const {height, width} = Dimensions.get('window');

export const GButton = (params) => (
  <TouchableOpacity onPress={params.onPress}>
    <LinearGradient
      style={[
        params._style,
        {
          flexDirection: 'row',
          paddingVertical: 12,
          height: params.height,
          width: params.width,
          borderRadius: params.radius,
          // backgroundColor: params.bgColor,
          borderWidth: params.borderWidth,
          borderColor: params.borderColor,
        },
        style.buttonStyle,
      ]}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      // colors={['#4c669f', '#3b5998', '#192f6a']}
      colors={['#ff23c4', '#8200db', 'rgb(92,1,197)']}>
      <Text
        style={[
          style.txtstyle,
          {
            fontSize: params.fontSize ? params.fontSize : 13,
            color: params.txtcolor|| 'white',
            lineHeight:params.lineHeight,
            fontFamily:params.fontFamily || fonts.NovaBoldios,
            // fontWeight: params.fontWeight || 'normal',
          },
        ]}>
        {params.bText}
      </Text>

      {params.children}
    </LinearGradient>
  </TouchableOpacity>
);

const style = StyleSheet.create({
  txtstyle: {
    fontWeight: 'bold',
  },
  buttonStyle: {
    elevation: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export const GBigButton = (params) => (
  <LinearGradient
    colors={['#ff23c4', '#8200db', 'rgb(92,1,197)']}
    style={{borderRadius: 5}}>
    <Button
      theme={{
        colors: {
          placeholder: Colors.grey,
          primary: Colors.appcolor,
          // underlineColor: colors.appcolor,
          //   text: colors.appcolor,
        },
      }}
      title="df"
      contentStyle={[
        {
          padding: 10,
          // fontFamily: fonts.OpenSansBold
        },
        params._style,
      ]}
      style={{}}
      labelStyle={[
        {
          color: Colors.white,
          // fontFamily: fonts.OpenSansExtraBold,
          fontSize: 17,
          fontWeight: 'bold',
        },
      ]}
      uppercase={false}
      mode="contained"
      onPress={params.onPress}>
      {params.bText || 'Button'}
    </Button>
  </LinearGradient>
  // <Button
  //   disabled={params.disabled}
  //   onPress={params.onPress}
  //   style={{
  //     height: params.height,
  //     borderRadius: params.borderRadius,
  //     borderColor: params.borderColor,
  //     borderWidth: params.borderWidth,
  //     backgroundColor: params.bgColor,
  //     width: params.width,
  //     alignItems: 'center',
  //     justifyContent: 'center',
  //     opacity: params.opacity,
  //   }}>
  //   <Text
  //     style={{
  //       fontSize: 18,
  //       fontFamily: fonts.AvenirHeavy,
  //       fontWeight: '600',
  //       // color: params.color,
  //     }}>
  //     {params.bText}
  //   </Text>
  // </Button>
);

export const GRoundButton = (params) => (
  // <TouchableOpacity onPress={() =>{alert('hi')}}>
  <LinearGradient
  onPress={()=>alert('hiiii')}
    colors={['#ff23c4', '#8200db', 'rgb(92,1,197)']}
    style={{
      justifyContent: 'center',
      alignItems: 'center',
      height: params.height,
      width: params.width,
      borderRadius: params.radius,
    }}>
    <Icons size={25} source={params.source} onPress={() =>{alert('hi')}} />
  </LinearGradient>
  // </TouchableOpacity>

);

export const GSmallButton = (params) => (
  <TouchableOpacity
    disabled={params.disabled}
    onPress={params.onPress}
    style={{
      height: params.height,
      borderRadius: params.borderRadius,
      borderColor: params.borderColor,
      borderWidth: params.borderWidth,
      backgroundColor: params.bgColor,
      width: params.width,
      alignItems: 'center',
      justifyContent: 'center',
      opacity: params.opacity,
    }}>
    <Text
      style={{
        fontSize: 16,
        fontFamily: params.fontFamily ? params.fontFamily : null,
        fontWeight: 'bold',
        color: params.color,
      }}>
      {params.bText}
    </Text>
  </TouchableOpacity>
);
