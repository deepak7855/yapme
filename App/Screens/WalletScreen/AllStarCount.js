import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Image,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import {GButton} from '../../Common/GButton';
import {Input, InputText} from '../../Common/InputCommon';
import {Icons} from '../../Component/Common';
import ArrData from '../../../dummydata';

export default class AllStarCount extends Component {
state={
  
}


  _renderFriendRequestList = ({item, index}) => {
    return (
      <View style={styles.friend_req_view}>
        <View style={{flexDirection: 'row'}}>
          <Icons size={17} source={images.star_lg} />

          <Text style={styles.textvalue}>{item.title}</Text>
        </View>

        <View style={styles.btn_view}>
          <Icons
            size={17}
            source={images.diamond_lg}
            _style={{marginVertical: 18, marginHorizontal: 8}}
            onPress={() => alert('development')}
          />

          <Text style={styles.textvalue}>{item.usd}</Text>
        </View>
      </View>
    );
  };

  render() {
    return (
      <ScrollView bounces={false} showsVerticalScrollIndicator={false}
      style={{color: Colors.whiteTwo, marginHorizontal: 15}}>
        <View style={{alignItems: 'center'}}>
          <Icons
            size={40}
            source={images.star_lg}
            _style={{marginVertical: 18}}
          />
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.balance}>Current Stars</Text>
            <Icons
              size={12}
              source={images.information_gray}
              _style={{marginHorizontal: 5, marginBottom: 4}}
              onPress={()=>this.setState({info:!this.state.info})}
            />
          </View>
         {this.state.info&&(<View style={[CommonStyle,{alignItems:'center',borderRadius:8,padding:10}]}>
            <Text style={styles.text1}>Withdraw rate :100</Text>
            <Text style={[styles.text1,{marginTop:10}]}>Star= $1</Text>
          </View>)}
          <Text style={styles.totalcoin}> 724 </Text>
        </View>

        <View
          style={{
            flexDirection: 'row',
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            backgroundColor: '#fff',
            shadowOpacity: 0.11,
            shadowRadius: 1.0,
            elevation: 3,
            alignItems: 'center',
            paddingHorizontal: 11,
            paddingVertical: 15,
            borderRadius: 5,
            justifyContent: 'space-between',
            marginVertical: 25,
            marginHorizontal:4,
            borderWidth: 0.3,
            borderColor: Colors.lightgrey,
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icons size={20} source={images.star_lg} _style={{margin: 10}} />

            <Text
              style={{
                fontSize: fonts.fontSize13,
                fontFamily: fonts.NovaBoldios,
              }}>
              Exchange Stars to Diamonds
            </Text>
          </View>

          <View>
            <Icons
              size={12}
              source={images.arrow_next_black}
              _style={{margin: 15}}
            />
          </View>
        </View>

        <FlatList
          showsVerticalScrollIndicator={false}
          data={ArrData.arrStartCount}
          renderItem={this._renderFriendRequestList}
          extraData={this.state}
          keyExtractor={(item, index) => index.toString()}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  balance: {fontFamily: fonts.NovaRegularios, fontSize: fonts.fontSize13},
  totalcoin: {
    fontFamily: fonts.NovaBoldios,
    fontSize: fonts.fontSize17,
    marginTop: 12,
  },
  textvalue: {
    fontFamily: fonts.NovaSboldios,
    fontSize: fonts.fontSize15,
    marginLeft: 18,
  },
  friend_req_view: {
    borderRadius: 10,
    marginVertical: 6,
    paddingHorizontal: 10,
    // paddingVertical: 10,
    borderWidth: 1,
    borderColor: Colors.white,
    marginHorizontal: 3,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.15,
    shadowRadius: 1.94,
    elevation: 3,
  },
  btn_view: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 0.7,
    // backgroundColor: 'red',
    justifyContent: 'flex-end',
  },
  text1:{fontFamily:fonts.NovaRegularios,fontSize:fonts.fontSize13},
});
