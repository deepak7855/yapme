import { StyleSheet } from 'react-native';
import Fonts from '../../Assets/Fonts';
import Colors from '../../Assets/Colors';


export default ForgotPasswordStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,

    },
    image_back_ground: {
        flex: 1,
    },
    image_parent_view: {
        alignItems: 'center',
        marginTop: 100,
        marginVertical: 20
    },
    image_view: {
        alignItems: 'center',
        width: 70,
        height: 70,
        borderRadius: 70 / 2
    },
    profile_img: {
        height: '100%',
        width: '100%'
    },
    forgot_pass_view: {
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 5,
        marginTop: 10
    },
    forgot_pass_text: {
        color: Colors.vividPurple,
        fontSize: Fonts.fontSize24,
        fontFamily: Fonts.NovaBold,
        lineHeight: 29
    },
    email_add_view: {
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 5,
        marginHorizontal: 36,
    },
    email_add_text: {
        fontSize: Fonts.fontSize16,
        lineHeight: 20,
        textAlign: 'center',
        color: Colors.pinkishGrey
    },
    email_phone_view: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 35
    },
    email_text: {
        fontSize: Fonts.fontSize16,
        lineHeight: 20,
        color: Colors.black,
        fontFamily: Fonts.NovaRegular
    },
    selected_email_text: {
        fontSize: Fonts.fontSize16,
        lineHeight: 20,
        color: Colors.pinkishGrey,
        fontFamily: Fonts.NovaRegular
    },

    phone_text: {
        fontSize: Fonts.fontSize16,
        lineHeight: 20,
        color: Colors.black,
        fontFamily: Fonts.NovaRegular,

    },
    selected_phone_text: {
        fontSize: Fonts.fontSize16,
        lineHeight: 20,
        color: Colors.pinkishGrey,
        fontFamily: Fonts.NovaRegular
    },

    email_phone_touch: {
        marginHorizontal: 30
    },
    email_separator: {
        height: 2,
        backgroundColor: Colors.white,
        marginTop: 5
    },
    selected_email_separator: {
        height: 2,
        backgroundColor: Colors.hotMagenta,
        marginTop: 5
    },
    phone_separator: {
        height: 2,
        backgroundColor: Colors.white,
        marginTop: 5
    },

    selected_phone_separator: {
        height: 2,
        backgroundColor: Colors.hotMagenta,
        marginTop: 5
    },

    forgot_pass_view: {
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 15
    },
    forgot_pass_text: {
        fontSize: Fonts.fontSize24,
        // lineHeight: 15,
        color: Colors.vividPurple,
        fontFamily: Fonts.NovaBoldios
    },
    input_view: {
        marginHorizontal: 45,
        marginTop: 30
    },
    submit_btn_view: {
        alignItems: 'center',
        marginVertical: 40,
    },




    View1: {
        flexDirection: 'row',
        marginVertical: 10,
      },
      View2: {
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
        alignSelf: 'flex-end',
      },
      View3: {
        fontSize: Fonts.fontSize13,
        fontFamily: Fonts.NovaRegular,
        color: Colors.pinkishGrey,
      },
      phoneInput: {height: 40, borderBottomWidth: 1, fontSize: fonts.fontSize13},
    
      arrow_img: {
        height: 10,
        width: 10,
        marginLeft: 30,
      },



   






});