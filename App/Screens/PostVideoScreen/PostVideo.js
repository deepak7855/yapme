import React, {Component} from 'react';
import {
  View,
  Text,
  ImageBackground,
  SafeAreaView,
  TouchableOpacity,
  StatusBar,
  FlatList,
  StyleSheet,
  TextInput,
  Image,
} from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/Fonts';
import {images} from '../../Assets/ImageUrl';
import CHeader from '../../Common/CHeader';
import {GButton} from '../../Common/GButton';
import {Icons} from '../../Component/Common';
import {Custompopup} from '../../Component/Modal/CustomModel';

export default class PostVideo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      modalVisible: false,
      toggleglobe: false,
    };
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <ImageBackground source={images.Rectangle5} style={{flex: 1}}>
          <CHeader
            navigation={this.props.navigation}
            backArrowLeft
            whitearrow={'white'}
            centerText="Post Video"
            color={Colors.white}
            fontSize={18}
            fontWeight="bold"
          />

          <View style={{alignSelf: 'center'}}>
            <Icons
              size={60}
              source={images.camera_black_circle}
              _style={{alignSelf: 'center', marginVertical: 25}}
            />
            <Text
              style={{
                color: Colors.white,
                fontFamily: fonts.NovaSboldios,
                fontSize: fonts.fontSize15,
              }}>
              Change Cover
            </Text>
          </View>
        </ImageBackground>



        <View style={{flex: 2, backgroundColor: '#fff'}}>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'flex-end',
              marginRight: 18,
            }}>
            <Icons
              size={50}
              source={images.hash_tag_icon}
              _style={{marginTop: -30, marginRight: 15}}
            />
            <Icons
              size={50}
              source={images.at_user}
              _style={{marginTop: -30}}
            />
          </View>

          <View style={{marginTop: 30}}>
            <TextInput
              multiline
              placeholder="Describe your video"
              placeholderTextColor={Colors.brownishGrey}
              onChangeText={(value) => {
                if (value.length == '4') {
                  this.setState({modalVisible: true});
                } else {
                  this.setState({text: value});
                }
              }}
              style={[
                CommonStyle,
                styles.inputstyle,
              ]}
            />
          </View>
          <Text
            style={styles.text3}>
            {this.state.text.length}/150
          </Text>

          <View style={styles.view5}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icons
                size={15}
                source={images.viewloack}
                _style={{marginRight: 25}}
              />
              <Text>Who can view this video</Text>
            </View>

            <View>
              <Icons
                size={60}
                source={this.state.toggleglobe ? images.one : images.two}
                onPress={() =>
                  this.setState({toggleglobe: !this.state.toggleglobe})
                }
                _style={{marginRight: 5, height: 45, width: 60}}
              />
            </View>
          </View>

          <View style={styles.view4}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icons
                size={15}
                source={images.react_icon}
                _style={{marginRight: 25}}
              />
              <Text>Allow comments</Text>
            </View>

            <View>
              <Icons
                size={35}
                source={images.off_toggle}
                _style={{marginRight: 0}}
              />
            </View>
          </View>

          <View style={styles.view3}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icons
                size={15}
                source={images.savetoalbum}
                _style={{marginRight: 25}}
              />
              <Text>Save to album</Text>
            </View>

            <View>
              <Icons
                size={35}
                source={images.on_toggle}
                _style={{marginRight: 0}}
              />
            </View>
          </View>

          <View style={styles.view2}>
            <TouchableOpacity style={styles.touch2}>
              <Text style={styles.text2}>Discard</Text>
            </TouchableOpacity>

            <View style={{width: 10}} />

            <GButton
              height={60}
              width={180}
              radius={5}
              //   lineHeight={15}
              txtcolor={Colors.white}
              fontFamily={fonts.NovaXbold}
              fontSize={fonts.fontSize13}
              _style={{marginLeft: 8}}
              bText={'Post the video'}
            />
          </View>
        </View>

        {/* <View style={{flex:0.5,backgroundColor:'red'}}>

        <Text> PostVideo </Text>
        </View> */}
        {/* </ImageBackground> */}

        <Custompopup modalVisible={this.state.modalVisible}>
          <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.6)'}}>
            {/* <View style={{flex: 1, backgroundColor: 'green'}}> */}

            {/* </View> */}

            <TouchableOpacity activeOpacity={1}
              onPress={() => this.setState({modalVisible: false})}
               style={{flex:2}}/>

            <View style={styles.whiteView}>
              <View style={styles.geryView}>
                <Icons
                  size={13}
                  source={images.information_outline}
                  _style={{marginRight: 10, tintColor: '#000'}}
                />
                <Text style={styles.text1}>
                  Friends of people you tag will also be able to see this post.
                </Text>
              </View>

              <FlatList
                contentContainerStyle={styles.flatlist}
                data={['', '', '', '', '', '']}
                renderItem={({item: color}) => (
                  <View style={[styles.item, CommonStyle, styles.view1]}>
                    <TouchableOpacity
                      // onPress={() => props.navigation.navigate('Message')}
                      style={styles.touch1}>
                      <View
                        style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Image
                          resizeMode="contain"
                          style={{height: 35, width: 35}}
                          source={images.Profile}
                        />
                        <View style={{marginLeft: 20}}>
                          <Text style={{marginBottom: 5}}>Odell smith</Text>
                          <Text style={styles.grytextStyle}>Awesome work</Text>
                        </View>
                      </View>
                      <Text style={styles.grytextStyle}>08:10</Text>
                    </TouchableOpacity>
                  </View>
                )}
                keyExtractor={(item, idx) => `item_${idx}`}
              />
            </View>
          </View>
        </Custompopup>

        <SafeAreaView />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  safe_area_view: {
    flex: 1,
  },

  touch1: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  geryView: {
    flexDirection: 'row',
    // justifyContent: 'space-between',
    paddingHorizontal: 15,
    paddingVertical: 18,
    alignItems: 'center',
    borderTopLeftRadius: 18,
    borderTopRightRadius: 18,
    backgroundColor: 'rgb(245,239,239)',
  },
  whiteView: {
    flex: 2.5,
    backgroundColor: Colors.white,
    marginHorizontal: 10,
    borderRadius: 19,
  },
  grytextStyle: {
    color: Colors.greyColor,
    fontSize: 13,
    // fontFamily:fonts.NovaRegular
  },
  view1: {
    justifyContent: 'center',
    paddingHorizontal: 20,
    height: 60,
    borderRadius: 10,
    marginBottom: 10,
  },
  flatlist: {
    paddingBottom: 80,
    marginTop: 15,
    marginHorizontal: 10,
  },
  text1: {
    fontSize: fonts.fontSize10,
    fontFamily: fonts.NovaRegularios,
  },
  touch2: {
    height: 60,
    width: 180,
    borderRadius: 5,
    backgroundColor: Colors.violetBlue,
    alignItems: 'center',
    justifyContent: 'center',
  },
  view2: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 40,
  },
  view3: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    marginTop: 20,
  },
  text2: {
    fontFamily: fonts.NovaBoldios,
    fontSize: fonts.fontSize13,
    color: Colors.white,
  },
  view4: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    marginTop: 20,
  },
  view5: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    marginTop: 20,
  },
  text3:{
    fontSize: fonts.fontSize13,
    fontFamily: fonts.NovaRegularios,
    color: Colors.brownishGrey,
    textAlign: 'right',
    marginRight: 19,
    marginTop: 12,
  },
  inputstyle:{
    height: 140,
    width: null,
    paddingLeft: 14,
    paddingTop: 14,
    marginHorizontal: 18,
    borderRadius: 8,
    fontFamily: fonts.NovaRegularios,
  },
});
